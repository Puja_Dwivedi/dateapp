//
//  AppDelegate.swift
//  MyFirstApp
//
//  Created by cis on 30/12/20.
//  Copyright © 2020 cis. All rights reserved

import UIKit
import IQKeyboardManagerSwift
import FirebaseAuth
import UserNotifications
import GooglePlaces
import Firebase
import FirebaseCore
import FirebaseMessaging
import CoreLocation


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate, CLLocationManagerDelegate {
    
    let APP_COLOR = #colorLiteral(red: 0, green: 0.6784313725, blue: 0.937254902, alpha: 1)
    var window: UIWindow?
    var isBackgroundActive = false
    var splashUIView = SplashUIView()
    var coverView: UIView?
    var activeCheckin = CreatePostActiveScreen.airplane
    
    static let shared =  UIApplication.shared.delegate as! AppDelegate
    var navController = UINavigationController()
    var locationManager: CLLocationManager?
    
    var textFieldPlaceholderColor = UIColor()
    var selectedPostId = 0
    var isBackPressedFromTag = false
    
    // var googlePlaceAPI = "AIzaSyDXCokn_W0qPmpMN8UrRadhkHg4pTVgHsw" //  Old Key.
    var googlePlaceAPI = "AIzaSyD4UBt2bU6cTI44Elc8NsyECQa1ERmhhcw"//  New key
    
    private var storyboard = UIStoryboard(name: "Main", bundle: nil)
    let splashVM = SplashViewModel()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        textFieldPlaceholderColor = UITextField().attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? #colorLiteral(red: 0.7921568627, green: 0.7921568627, blue: 0.8, alpha: 1)
        
        // keyboard enable :
        IQKeyboardManager.shared.enable = true
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //keyboard cursor color
        UITextField.appearance().tintColor = #colorLiteral(red: 0, green: 0.6784313725, blue: 0.937254902, alpha: 1)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        UIApplication.shared.registerForRemoteNotifications()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        registerForPushNotifications(application: application)
        
        // place api :
        GMSPlacesClient.provideAPIKey(googlePlaceAPI)
        
        return true
    }
    
    
    // MARK: - Notification oberserver methods
    func logOut(){
        // delete all stored data:
        Constants.userDefault.removeObject(forKey:Variables.email)
        Constants.userDefault.removeObject(forKey:Variables.name)
        Constants.userDefault.removeObject(forKey:Variables.phone_number)
        Constants.userDefault.removeObject(forKey:Variables.picture)
        Constants.userDefault.removeObject(forKey: Variables.country_code)
        Constants.userDefault.removeObject(forKey:Variables.user_id)
        
        for controller in AppDelegate.shared.navController.viewControllers as Array {
            
            if controller.isKind(of: WelcomePageVC.self) {
                AppDelegate.shared.navController.popToViewController(controller, animated: false)
                break
            }
        }
    }

    
    func applicationDidEnterBackground(_ application: UIApplication) {
        isBackgroundActive = true
        
        coverView =  SplashView1.instanceFromNib()
        coverView!.frame = window!.frame
        coverView?.backgroundColor = UIColor.white
        let keyWindow = UIApplication.shared.connectedScenes
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        
        keyWindow?.addSubview(coverView!)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        perform(#selector(self.callSplashScreen1), with: self, afterDelay: 0.5)
    }
    
    @objc func callSplashScreen1(){
        coverView?.removeFromSuperview()
        coverView = nil
        
        if isBackgroundActive{
            isBackgroundActive = false
            
            if Constants.userDefault.object(forKey: Variables.phone_number) != nil {
                
                SplashUIView.sharedInstance.Show (objSplashVM : splashVM){[weak self] (status) in
                    
                    switch status {
                    case .blockUser:
                        self?.blockActionCall()
                        break;
                        
                    case .moveToTheWelcomeScreenWithLogout:
                        self?.moveToTheWelcomeScreen(withLogout: true)
                        break;
                        
                    case .deactivatedAccount:
                        self?.deActivatedFunctionCall()
                        break;
                        
                    default:
                        break;
                    }
                }
            }
        }
    }
    
    func blockActionCall(){
        // get root view controller:
        logOut()
        let nav = AppDelegate.shared.navController
        AlertTheme.sharedInstance.Show(popupCategory: .attribute, message: "The account/phone number has been blocked. Please visit the FAQ page for further assistance", attributeRangeString: "FAQ", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
            
            if clicked == "FAQ" {
               
                let objFAQVC = self.storyboard.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
                objFAQVC.activeScreen = .BlockFAQ
                objFAQVC.strWebUrl = "\(self.splashVM.userStatus.FAQLink)"
                nav.pushViewController(objFAQVC, animated: true)
            }
        }
    }
    
    func deActivatedFunctionCall(){
        logOut()
        AlertTheme.sharedInstance.Show(popupCategory: .attribute, message: "Your account was deactivated on \(self.splashVM.userStatus.deactivated_on ). We are unable to register you at this time. Please visit the FAQ page for further details", attributeRangeString: "FAQ", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
            
            if clicked == "FAQ" {
                let nav = AppDelegate.shared.navController
                let objFAQVC = self.storyboard.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
                objFAQVC.activeScreen = .BlockFAQ
                objFAQVC.strWebUrl = "\(self.splashVM.userStatus.FAQLink )"
                nav.pushViewController(objFAQVC, animated: true)
            }
        }
    }
    
    func moveToTheWelcomeScreen(withLogout: Bool){
        
        if withLogout {
            
            logOut()
            moveToWelcomeScreenWithAnimation()
        }else{
            moveToWelcomeScreenWithOutAnimation()
        }
    }
    
    @objc func moveToWelcomeScreenWithAnimation(){
        let nav = AppDelegate.shared.navController
        let objRootVC = self.storyboard.instantiateViewController(withIdentifier: "WelcomePageVC") as! WelcomePageVC
        nav.pushViewController(objRootVC, animated: false)
    }
    
    @objc func moveToWelcomeScreenWithOutAnimation(){
        let nav = AppDelegate.shared.navController
        let objRootVC = self.storyboard.instantiateViewController(withIdentifier: "WelcomePageVC") as! WelcomePageVC
        nav.pushViewController(objRootVC, animated: false)
    }
    
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if Auth.auth().canHandleNotification(notification) {
            Messaging.messaging().appDidReceiveMessage(notification)
            completionHandler(.noData)
            return
        }
    }
    
    // For iOS 9+
    func application(_ application: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        return false
    }
    
    // For iOS 8-
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        return false
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Auth.auth().setAPNSToken(deviceToken, type: .unknown)
        Messaging.messaging().apnsToken = deviceToken;
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
    
    
    //MARK:-
    //MARK:- Notification :
    func registerForPushNotifications(application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        updateFirestorePushTokenIfNeeded()
    }
    
    
    func updateFirestorePushTokenIfNeeded() {
        if let token = Messaging.messaging().fcmToken {
            print("Firebase registration token: updateFirestorePushTokenIfNeeded")
            print("token: \(token)")
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        updateFirestorePushTokenIfNeeded()
        
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        Constants.userDefault.set(fcmToken, forKey: Variables.deviceToken)
    }
    
    
    // MARK: iOS10+, called when presenting notification in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        Messaging.messaging().appDidReceiveMessage(userInfo)
        completionHandler([[.alert, .sound]])
    }
    
    func configureNotification() {
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        Messaging.messaging().appDidReceiveMessage(userInfo)
        completionHandler()
    }
}


