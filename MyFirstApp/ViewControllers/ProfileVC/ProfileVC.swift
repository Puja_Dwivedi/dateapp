//
//  ProfileVC.swift
//  MyFirstApp
//
//  Created by cis on 29/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import SDWebImage

var notificationActiveStatus  = 0
var settingInfo = (email : "", location : "")
var recreate_days = ""

class ProfileVC: UIViewController {
    
    enum EditMode {
        case on
        case off
    }
    
    enum ActionDoneStatus {
        case doneBySetting
        case doneByUpdate
    }
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet var btnMyTraits: UIButton!
    @IBOutlet weak var lblProfileName: UILabel!
    
    @IBOutlet var rootScrollView: UIScrollView!
    @IBOutlet weak var viewEditContainer: UIView!
    @IBOutlet weak var viewBackFromEdit: UIView!
    
    @IBOutlet weak var viewSettingContainer: UIView!
    @IBOutlet weak var lblLineMyAttibutes: UILabel!
    @IBOutlet weak var lblLineMyPreferences: UILabel!
    
    @IBOutlet weak var viewEditProfileContainer: UIView!
    // error container :
    @IBOutlet weak var viewError: UIView!
    @IBOutlet weak var lblErrorMsg: UILabel!
    
    // Tags with textFields:--------
    
    //1. My attribtues:
    
    @IBOutlet weak var scrollViewMyAttibutes: UIScrollView!
    @IBOutlet weak var scrollViewMyAttibutes_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes: PinTextField!
    @IBOutlet weak var txtFldMyAttributes_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes_x: NSLayoutConstraint!
    var arrTagsMyAttributes = [String]()
    
    //2.My Preferences:
    
    @IBOutlet weak var scrollViewMyPreferences: UIScrollView!
    @IBOutlet weak var scrollViewMyPreferences_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences: PinTextField!
    @IBOutlet weak var txtFldMyPreferences_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences_x: NSLayoutConstraint!
    
    @IBOutlet weak var txtMyAttibute_width: NSLayoutConstraint!
    @IBOutlet weak var txtMyPreference_width: NSLayoutConstraint!
    
    
    //MARK:-
    //MARK:- Variables :
    var arrTagsMyPreferences = [String]()
    var profileVM = ProfileViewModel()
    var notificationStatus  = 0
    var isComeFromMatchIfo  = (status : false, user_id : 0)
    var isChangeAnything = false
    var editModeStatus : EditMode =  .off
    var screenWidth = UIScreen.main.bounds.width
    var editProfileInfo = (bio  : "", Work : "", Education : "",strProfileURL : "")
    
    // back option :
    var isEmptyBackAttribute = true
    var isEmptyBackPrefrence = true
    var isBackFromEditProfile = false
    
    // create view popus:
    var objCreateView2 = ProfileTagSuggestionView()
    
    /*
     The hint icons in the profile display nothing. Retrieve the tag categories associated to Plane (event_id = 1, attribute_id = 0)
     */
    var getPostInfo = (activeScreen : CreatePostActiveScreen.none ,flight : "", location : "")
    
    //MARK:-
    //MARK:- App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFldMyAttributes.delegate = self
        txtFldMyPreferences.delegate = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        if isChangeAnything{
            prepairForUpdate(doneStatus: .doneByUpdate)
        }else{
            setEditMode(mode: .off)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
        isChangeAnything = false
        arrTagsMyAttributes.removeAll()
        arrTagsMyPreferences.removeAll()
        initView()
        rootScrollView.isScrollEnabled = true
    }
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionOpenSetting(_ sender: UIButton) {
        
        moveToSettingScreen()
    }
    
    
    @IBAction func actionProfileEdit(_ sender: UIButton) {
        // move to profile edit screen:
        
        let userName = Constants.userDefault.object(forKey: Variables.name) as! String
        let objProfileEditVC = self.storyboard?.instantiateViewController(withIdentifier: "profileEditVC") as! profileEditVC
        objProfileEditVC.isComeFromMatchIfo = self.isComeFromMatchIfo
        objProfileEditVC.valueFromPreviousScreen.profileName = userName
        objProfileEditVC.valueFromPreviousScreen.profilePicture = imgProfile.image!
        objProfileEditVC.activeComeFromStatus = .profileScreen
        objProfileEditVC.valueFromPreviousScreen.bio = editProfileInfo.bio
        objProfileEditVC.valueFromPreviousScreen.work = editProfileInfo.Work
        objProfileEditVC.valueFromPreviousScreen.education = editProfileInfo.Education
        objProfileEditVC.actionScreen = .write
        objProfileEditVC.valueFromPreviousScreen.strProfilePicture = editProfileInfo.strProfileURL
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objProfileEditVC, animated: true)
    }
    
    @IBAction func actionBtnPhotoEdit(_ sender: UIButton) {
        let obj  = ImagePickerView()
        obj.show(self, preview: (isPreview: false, image: UIImage())) { [weak self](selectedImage) in
            if selectedImage.image == UIImage(named: "img_profile_placeholder") {
                let imgData = selectedImage.image?.jpegData(compressionQuality: 1.0)!
                self?.setProfileImage(imageData: Data(), image: selectedImage.image!)
            } else {
                let imgData = selectedImage.image?.jpegData(compressionQuality: 1.0)!
                self?.setProfileImage(imageData: imgData!, image: selectedImage.image!)
            }
            
        }
    }
    
    @IBAction func actionEdit(_ sender: UIButton) {
        setEditMode(mode: .on)
    }
    
    @IBAction func actionBackFromEdit(_ sender: UIButton) {
        if isComeFromMatchIfo.status {
            self.navigationController?.popViewController(animated: true)
        }else{
        }
    }
    
    
    @IBAction func actionMyPreferencesSuggestion(_ sender: UIButton) {
        self.view.endEditing(true)
        //
        self.objCreateView2 =  self.objCreateView2.Show(activeScreen: getPostInfo.activeScreen, strHeaderTitle: "My preferences", isbackButtonActive: true, isTabBarAvailable: true, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self.arrTagsMyPreferences), viewController: self, attribute_text: getPostInfo.flight, attribute_city: getPostInfo.location, totalCount: 5) { [weak self] (arrSelectedTags) in
            self?.isChangeAnything = true
            
            self?.objCreateView2.removeWithAnimation()
            self?.arrTagsMyPreferences = arrSelectedTags
            self?.createTagsForPreferences(OnView: (self?.scrollViewMyPreferences)!, withArray: self!.arrTagsMyPreferences, isCancelButtonVisible: true)
        } as! ProfileTagSuggestionView
    }
    
    @IBAction func actionMyTraitsSuggestion(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.objCreateView2 =  self.objCreateView2.Show(activeScreen: getPostInfo.activeScreen, strHeaderTitle: "My look and traits", isbackButtonActive: true, isTabBarAvailable: true, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self.arrTagsMyAttributes), viewController: self, attribute_text: getPostInfo.flight, attribute_city: getPostInfo.location, totalCount: 12) { [weak self](arrSelectedTags) in
            
            self?.isChangeAnything = true
            self?.objCreateView2.removeWithAnimation()
            self?.arrTagsMyAttributes = arrSelectedTags
            self?.createTagsForAttributes(OnView: (self?.scrollViewMyAttibutes)!, withArray: self!.arrTagsMyAttributes, isCancelButtonVisible: true)
        } as! ProfileTagSuggestionView
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //MARK:-
    //MARK:- Methods:
    @objc func moveToSettingScreen(){
        let userName = Constants.userDefault.object(forKey: Variables.name) as! String
        let objSettingVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        objSettingVC.settingProfileInfo.name = userName
        objSettingVC.settingProfileInfo.image = imgProfile.image!
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objSettingVC, animated: true)
    }
    
    func initView(){
        viewEditProfileContainer.isHidden = true
        viewError.isHidden = false
        lblErrorMsg.isHidden = true
        
        viewBackFromEdit.isHidden = true
        viewEditContainer.isHidden = true
        txtFldMyAttributes.alpha = 0
        getProfileInfo() // api call :
    }
    
    func setEditMode(mode : EditMode) {
        editModeStatus = mode
        
        if mode == .on {
            viewEditProfileContainer.isHidden = false
            createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
            createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
            
            //  UIView.animate(withDuration: 0.5) {
            self.viewBackFromEdit.isHidden = true
            self.viewEditContainer.isHidden = true
            
            self.view.layoutIfNeeded()
            // }
        }else{
            
            viewEditProfileContainer.isHidden = true
            createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: false)
            createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: false)
        }
    }
    
    
    // set info :
    func setInfo(dict : NSDictionary){
        viewError.isHidden = true
        lblErrorMsg.isHidden = true
        
        let item = dict.object(forKey: "data") as! NSDictionary
        
        // get info :
        lblProfileName.text = item.object(forKey: "name") as? String
        
        getPostInfo.activeScreen = .airplane //
        getPostInfo.flight = item.object(forKey: "attribute_text") as? String ?? ""
        getPostInfo.location = item.object(forKey: "attribute_city") as? String ?? ""
        
        let bio =  item.object(forKey: "Description") as! String
        let Work =  item.object(forKey: "Work") as! String
        let Education =  item.object(forKey: "Education") as! String
        
        editProfileInfo.bio = bio
        editProfileInfo.Work = Work
        editProfileInfo.Education = Education
        
        let notification = item.object(forKey: "notification") as! Int
        let profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(item.object(forKey: "picture") as! String)"
        editProfileInfo.strProfileURL = profilePicture
        selectedCountryName =  item.object(forKey: "country") as? String ?? ""
        
        settingInfo.email = item.object(forKey: "email") as? String ?? ""
        settingInfo.location = item.object(forKey: "primary_location") as? String ?? ""
        
        for index in 1...12 {
            let key = "characteristic_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                let value = "\(item.object(forKey: key) as! String)"
                arrTagsMyAttributes.append(value.lowercased())
            }
        }
        
        // partner Prefreence :
        for index in 1...5 {
            let key = "partner_preference_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                let value = "\(item.object(forKey: key) as! String)"
                arrTagsMyPreferences.append(value.lowercased())
            }
        }
        
        // set info:
        imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProfile.sd_setImage(with: URL(string: profilePicture), placeholderImage: UIImage(named: "img_profile_placeholder"))
        notificationActiveStatus = notification
        
        setEditMode(mode: .on)
    }
    
    func showErrorMsg(msg: String){
        viewError.isHidden = false
        lblErrorMsg.isHidden = false
        lblErrorMsg.text = "Error\n\(msg)"
    }
    
    func prepairForUpdate(doneStatus : ActionDoneStatus){
        
        var param = [String : AnyObject]()
        
        // get characteristic_
        for (index,item) in arrTagsMyAttributes.enumerated(){
            let value = item.lowercased()
            param.updateValue(value as AnyObject, forKey: "characteristic_\(index + 1)")
        }
        
        //partner_preference_
        for (index,item) in arrTagsMyPreferences.enumerated(){
            let value1 = item.lowercased()
            param.updateValue( value1 as AnyObject, forKey: "partner_preference_\(index + 1)")
        }
        
        setUpdateProfileInfo(requestParam: param, doneStatus: doneStatus)
    }
}

extension ProfileVC : UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        let str = textView.text
        isChangeAnything = true
        if str?.count == 0 && text == " " {
            return false
        } else if (str?.count)! > 0 {
            if str?.last == " " && text == " " {
                return false
            } else {
                guard let text = textView.text else { return true }
                let newLength = text.count + text.count - range.length
                return newLength <= 250
            }
        }
        return true
    }
}


/*
 MARK:- API's
 */
extension ProfileVC {
    
    // get profile info:
    func getProfileInfo()
    {
        var user_id  = ""
        if isComeFromMatchIfo.status {
            user_id = "\(isComeFromMatchIfo.user_id)"
        }else{
            user_id = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        }
        
        Loader.sharedInstance.showLoader(msg: "Loading...")
        profileVM.getProfileApi(user_id: user_id) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                print("set all the information")
                self.setInfo(dict: dictData)
            }else{
                self.showErrorMsg(msg: message)
                //SnackBar.sharedInstance.show(message: message)
            }
        }
    }
    
    func setProfileImage(imageData : Data, image : UIImage)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        profileVM.setProfileImage(imageData: imageData) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                print("Update the information")
                self.imgProfile.image = image
                
            }else{
                
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    // set profile info:
    
    func setUpdateProfileInfo(requestParam : [String : AnyObject], doneStatus : ActionDoneStatus)
    {
        self.view.endEditing(true)
        Loader.sharedInstance.showLoader(msg: "Loading...")
        profileVM.setUpdateApi(requestParam: requestParam) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                print("Update the information")
                if doneStatus == .doneBySetting {
                    self.perform(#selector(self.moveToSettingScreen), with: self, afterDelay: 0)
                }
                self.setEditMode(mode: .on)
            }else{
            }
        }
    }
}

//MARK:-
//MARK:- Tags with Textfilds:

extension ProfileVC :  UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        isChangeAnything = true
        let searchText  = textField.text! + string
        
        let maxLength = 21
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        //     print("newString.length : \(newString.length)")
        
        if newString.length == maxLength && string == " "{
            print("add space")
        }else{
            guard newString.length < maxLength else{ return false}
        }
        
        // My attributes :
        
        if textField == txtFldMyAttributes {
            isEmptyBackAttribute = false
            //Hide remove space :
            if range.location == 0 && string == " " {
                txtFldMyAttributes.text = ""
                return false;
            } else if (textField.text!.count) > 0 {
                if textField.text!.last == " " && string == " " {
                    return false
                }
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            if !checkIsTagRepeated(str: str, strArr: arrTagsMyAttributes) {
                // add new element :
                if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                    txtFldMyAttributes.text = ""
                    isEmptyBackAttribute = true
                    arrTagsMyAttributes.append(searchText.replace(string: " ", replacement: ""))
                    createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
                    return false
                }
            }
            return true
            
            // My preferences:-------------------------------------------------------------------------------
        }else if textField == txtFldMyPreferences {
            isEmptyBackPrefrence = false
            //Hide remove space :
            if range.location == 0 && string == " " {
                txtFldMyPreferences.text = ""
                return false;
            }else if (textField.text!.count) > 0 {
                if textField.text!.last == " " && string == " " {
                    return false
                }
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            if !checkIsTagRepeated(str: str, strArr: arrTagsMyPreferences) {
                // add new element :
                if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                    txtFldMyPreferences.text = ""
                    isEmptyBackPrefrence = true
                    arrTagsMyPreferences.append(searchText.replace(string: " ", replacement: ""))
                    createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
                    // createTagCloud(OnView: scrollViewTxtView, withArray: tagsArray)
                    return false
                }
            }
            return true
        }
        return true
    }
}


extension ProfileVC : PinTexFieldDelegate{
    func didPressBackspace(textField : PinTextField){
        print("back prashed")
        
        
        // My attibutes :
        if textField == txtFldMyAttributes {
            
            // for removed :
            if arrTagsMyAttributes.count > 0 && (txtFldMyAttributes.text?.isEmpty ?? false){
                
                if isEmptyBackAttribute {
                    arrTagsMyAttributes.remove(at: arrTagsMyAttributes.count - 1)
                    createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
                }
                isEmptyBackAttribute = true
                
            }
            
            // My Preferences :
        }else if textField == txtFldMyPreferences{
            
            // for removed :
            if arrTagsMyPreferences.count > 0 && (txtFldMyPreferences.text?.isEmpty ?? false){
                
                if isEmptyBackPrefrence{
                    arrTagsMyPreferences.remove(at: arrTagsMyPreferences.count - 1)
                    createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
                }
                isEmptyBackPrefrence = true
            }
        }
    }
}

protocol PinTexFieldDelegate : UITextFieldDelegate {
    func didPressBackspace(textField : PinTextField)
}


class PinTextField: UITextField {
    
    override func deleteBackward() {
        super.deleteBackward()
        
        // If conforming to our extension protocol
        if let pinDelegate = self.delegate as? PinTexFieldDelegate {
            pinDelegate.didPressBackspace(textField: self)
        }
    }
}


//My Attributes support methods : for Tag with textfield:
extension ProfileVC {
    
    func createTagsForAttributes(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let spaceHeight : CGFloat = 20
        let height = 30
        let screenWidth = UIScreen.main.bounds.size.width - 50.0
        
        for str in data  {
            
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name:"Poppins-Regular", size: 14.0)!)
            
            // edit :
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            
            if checkWholeWidth > screenWidth {
                //we are exceeding size need to change xpos
                xPos = 5.0
                ypos = ypos + spaceHeight + 8.0 + 11 // here 11 is the buttom space
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 35.0 , height: CGFloat(height)))
            
            bgView.backgroundColor = .clear
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 14.0, y: -0.5, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "Poppins-Regular", size: 14.0)
            textlable.text = startstring
            textlable.textAlignment = .center
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
            bgView.addSubview(textlable)
         //   bgView.addShadowToView()

            //when close buttton enable
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 28.0, height: 28.0)
                button.backgroundColor = UIColor.clear
                button.setImage(#imageLiteral(resourceName: "cross_icon"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagMyAttibutes(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldMyAttributes.isHidden = true
            btnMyTraits.isHidden = false
            
            if arrTagsMyAttributes.count == 12 {
                
                txtFldMyAttributes_y.constant = ypos
                txtFldMyAttributes_x.constant = xPos
            }else{
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    //we are exceeding size need to change xpos
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldMyAttributes_y.constant = ypos
                    txtFldMyAttributes_x.constant = xPos
                    
                    
                }else{
                    txtFldMyAttributes_y.constant = ypos
                    txtFldMyAttributes_x.constant = xPos
                }
            }
            txtMyAttibute_width.constant = screenWidth - xPos + 50// here 30 is padding....
        }else{
            txtFldMyAttributes.isHidden = true
            btnMyTraits.isHidden = true
        }
        
        
        if arrTagsMyAttributes.count <= 12 {
            self.txtFldMyAttributes.alpha = 0
            self.btnMyTraits.alpha = 1
            scrollViewMyAttibutes_h.constant = ypos + 34 + 10//Here 50 is the height of the txtview, and 10 is the bottom space of
            
            if arrTagsMyAttributes.count == 12 {
                self.txtFldMyAttributes.alpha = 0
                self.btnMyTraits.alpha = 0
                self.txtFldMyAttributes.resignFirstResponder()
                self.view.endEditing(true)
            }
            
            // the scrollview
        }else{
            self.txtFldMyAttributes.alpha = 0
            self.btnMyTraits.alpha = 0
            self.txtFldMyAttributes.resignFirstResponder()
            self.view.endEditing(true)
            scrollViewMyAttibutes_h.constant = ypos//Here 50 is the height of the txtview, and 10 is the bottom space of
            // the scrollview
        }
    }
    
    @objc func removeTagMyAttibutes(_ sender: AnyObject) {
        if editModeStatus == .on{ isChangeAnything = true }
        arrTagsMyAttributes.remove(at: (sender.tag - 1))
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
    }
}

//My Preerences support methods :
extension ProfileVC {
    
    func createTagsForPreferences(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let spaceHeight : CGFloat = 16
        let height = 30
        let screenWidth = UIScreen.main.bounds.size.width - 60.0
        
        for str in data  {
            
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name:"Poppins-Regular", size: 14.0)!)
            // edit :
            
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            if checkWholeWidth > screenWidth{
                //we are exceeding size need to change xpos
                xPos = 0.0
                ypos = ypos + spaceHeight + 8.0 + 11
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 35.0 , height: CGFloat(height)))
            
            bgView.backgroundColor = .clear
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 14.0, y: -0.5, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "Poppins-Regular", size: 14.0)
            textlable.text = startstring
            textlable.textAlignment = .center
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
            bgView.addSubview(textlable)
        //    bgView.addShadowToView()
            //when close buttton enable
            
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                //
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 28.0, height: 28.0)
                button.backgroundColor = UIColor.clear
                button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
                
                button.setImage(#imageLiteral(resourceName: "cross_icon"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagPreferences(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldMyPreferences.isHidden = false
            
            if arrTagsMyPreferences.count == 5 {
                txtFldMyPreferences_y.constant = ypos
                txtFldMyPreferences_x.constant = xPos
            }else{
                
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    //we are exceeding size need to change xpos
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldMyPreferences_y.constant = ypos
                    txtFldMyPreferences_x.constant = xPos
                }else{
                    txtFldMyPreferences_y.constant = ypos
                    txtFldMyPreferences_x.constant = xPos
                }
            }
            txtMyPreference_width.constant = screenWidth - xPos + 50// here 30 is padding....
        }else{
            txtFldMyPreferences.isHidden = true
        }
        
        if arrTagsMyPreferences.count <= 5 {
            self.txtFldMyPreferences.alpha = 1
            scrollViewMyPreferences_h.constant = ypos + 34 + 10//Here 50 is the height of the txtview, and 10 is the bottom space of
            
            if arrTagsMyPreferences.count == 5 {
                self.txtFldMyPreferences.alpha = 0
                self.txtFldMyPreferences.resignFirstResponder()
                self.view.endEditing(true)
            }
            // the scrollview
        }else{
            
            self.txtFldMyPreferences.alpha = 0
            self.txtFldMyPreferences.resignFirstResponder()
            self.view.endEditing(true)
            scrollViewMyPreferences_h.constant = ypos//Here 50 is the height of the txtview, and 10 is the bottom space of
            // the scrollview
        }
    }
    
    
    @objc func removeTagPreferences(_ sender: AnyObject) {
        if editModeStatus == .on{ isChangeAnything = true }
        arrTagsMyPreferences.remove(at: (sender.tag - 1))
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
    }
}
