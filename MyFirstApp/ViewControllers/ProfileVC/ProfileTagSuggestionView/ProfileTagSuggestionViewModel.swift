//
//  ProfileTagSuggestionViewModel.swift
//  MyFirstApp
//
//  Created by cis on 31/08/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ProfileTagSuggestionViewModel: NSObject {
    
    var arrCategory = [CategoryModel]()
    var arrTextTags = [String]()
    var arrTag = [TagsModel]()
    
    func getAllData_Api( completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.getProfileTagCategoryList
        
        param.updateValue(0 as AnyObject ,forKey: "attribute_id")
        param.updateValue(0 as AnyObject, forKey: "event_id")
        param.updateValue(userID as AnyObject ,forKey: "user_id")
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    self.arrCategory = self.retrieveCategory(dict: dict)
                    self.arrTag = self.retrieveTags(dict: dict)
                    self.arrTextTags = self.retrieveTxtTag(dict: dict)
                    
                    completion("success", message)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                }else { // error :
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                completion("error", message)
            }
        }
    }
    
    
    func sendDesCategoryInfo_API(tag_suggestion_text : String,tag_category_name : String, attribute_id : String,event_id : String, completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        param.updateValue(tag_suggestion_text as AnyObject, forKey:"tag_suggestion_text")
        param.updateValue(tag_category_name as AnyObject, forKey:"tag_category_name")
        param.updateValue(attribute_id as AnyObject, forKey:"attribute_id")
        param.updateValue(event_id as AnyObject, forKey:"event_id")
        
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.sendTagSuggestion
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            let dict = data.response
            
            if result == "success"
            {
                print(data.response)
                
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                    
                }else{
                    message = data.message
                }
                
                completion("success", message)
            }
            else
            {
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                    
                }else{
                    message = data.message
                }
                
                completion("error",message)
            }
        }
    }
    
    func retrieveCategory(dict : NSDictionary)-> [CategoryModel] {
        var arr = [CategoryModel]()
        
        let arrCategory = dict.object(forKey: "tagCategory") as! [Any]
        for item in arrCategory {
            let itemDict = item as! NSDictionary
            let tag_category_id = itemDict.object(forKey: "tag_category_id") as! Int
            let tag_category_name = "\(itemDict.object(forKey: "tag_category_name") ?? "")"
            let tag_category_description = "\(itemDict.object(forKey: "tag_category_description") ?? "")"
            let event_id = itemDict.object(forKey: "event_id") as! Int
            arr.append(CategoryModel(title: tag_category_name, des: tag_category_description, id: tag_category_id, event_id: event_id))
        }
        return arr
    }
    
    func retrieveTags(dict : NSDictionary)-> [TagsModel] {
        var arr = [TagsModel]()
    
        let arrCategory = dict.object(forKey: "tagData") as! [Any]
        for item in arrCategory {
            let itemDict = item as! NSDictionary
            let tag_name = itemDict.object(forKey: "tag_name") as! String
            let tag_picture = itemDict.object(forKey: "tag_picture") as! String

            arr.append(TagsModel(title: tag_name, isSelected: false, img: tag_picture))
        }
        return arr
    }
    
    func retrieveTxtTag(dict : NSDictionary)-> [String] {
        var arr = [String]()
        
        let itemDict = dict.object(forKey: "userData") as! NSDictionary
        for i in 1...5 {
            let key = "partner_preference_\(i)"
            if let value = itemDict.object(forKey: "\(key)") as? String {
                if value != "" {
                    arr.append(value)
                }
            }
        }
        return arr
    }
    
    //-----------------------------------------------------
    // get tags accroding to the category:
    func getTagByCategory_Api(tag_category_id : Int , completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        
        let strService = APPURL.Urls.getTagListByCategory
        
        param.updateValue(tag_category_id as AnyObject ,forKey: "tag_category_id")
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                //  completion("success", data.message, .moveToHomeScreen, "")
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    self.arrTag = self.retrieveTagsBtCategory(dict: dict)
                    
                    completion("success", message)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("error", message)
                }else { // error :
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                completion("error", message)
            }
        }
    }
    
    
    func retrieveTagsBtCategory(dict : NSDictionary)-> [TagsModel] {
        var arr = [TagsModel]()
    
        let arrCategory = dict.object(forKey: "data") as! [Any]
        for item in arrCategory {
            let itemDict = item as! NSDictionary
            let tag_name = itemDict.object(forKey: "tag_name") as! String
            let tag_picture = itemDict.object(forKey: "tag_picture") as! String

            arr.append(TagsModel(title: tag_name, isSelected: false, img: tag_picture))
        }
        return arr
    }
}

