//
//  ProfileViewModel.swift
//  MyFirstApp
//
//  Created by cis on 30/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ProfileViewModel: NSObject {
    
    // Updating profile values :
    func setProfileImage(imageData : Data, completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var message  = ""
        let param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.userUpdateProfilePic + "/\(userID)"
        
        let arrDocuments = NSMutableArray()
        arrDocuments.setValue(String(format: "pictureImage%02d", 1), forKey: "picture")
        arrDocuments.setValue(imageData, forKey: "picture")
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.formRequestWith(strUrl: strService, imageData: imageData, imageName: "picture", parameters: param, headers: header) { (result, data) in
            
            let dict = data.response
            
            if let statusCode = dict.object(forKey: "code") as? Int  { // user is already register :
                
                if statusCode == 200 {
                    let dict = data.response
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    completion("success", message, dict)
                }else{
                    print("data : \(data)")
                    if data.message == "" {
                        if data.message == "" {
                            message = "\(dict.object(forKey: "message") ?? "")"
                            
                        }else{
                            message = data.message
                        }
                        
                    }else{
                        message = data.message
                    }
                    completion("error", message, NSDictionary())
                }
            }else{
                print("data : \(data)")
                if data.message == "" {
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                }else{
                    message = data.message
                }
                completion("error", message, NSDictionary())
            }
        }
    }
    
    
    func getProfileApi(user_id : String, completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        let param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.getUserDetails + "/\(user_id)"
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                let dictData = dict.object(forKey: "data") as! NSDictionary
                recreate_days = "\(dictData.object(forKey: "recreate_days") ?? "")"
                
                completion("success", data.message, dict)
            }
            else
            {
                completion("error", data.message, NSDictionary())
            }
        }
    }
    // Updating profile values :
    
    func setUpdateApi(requestParam : [String : AnyObject], completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var message  = ""
        let param = requestParam
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.setProfileUpdate + "/\(userID)"
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                    
                }else{
                    message = data.message
                }
                completion("success",message, dict)
            }
            else
            {
                if data.message == "" {
                    SnackBar.sharedInstance.show(message: "Something went wrong,Please try again.", showMsgAt: .bottom)
                    completion("error", "Something went wrong,Please try again.", NSDictionary())
                }else{
                    SnackBar.sharedInstance.show(message: data.message, showMsgAt: .bottom)
                    completion("error", "Something went wrong,Please try again.", NSDictionary())
                    completion("error", data.message, NSDictionary())
                }
            }
        }
    }
}
