//
//  SplashVC.swift
//  MyFirstApp
//
//  Created by cis on 30/12/20.
//  Copyright © 2020 cis. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet weak var acitivityIndicator: UIActivityIndicatorView!
    
    //MARK:-
    //MARK:- Variables
    
    var splashVM = SplashViewModel()
    
    //MARK:-
    //MARK:- App flow
    
    override func viewWillAppear(_ animated: Bool) {
        acitivityIndicator.isHidden = true
        getInfo()
        
        if Constants.userDefault.object(forKey: Variables.phone_number) != nil {
            getUserStatus()
          
        }else{
            
            self.moveToWelcomeScreenWithOutAnimation()
        }
    }
    
    func getInfo(){
        
        splashVM.getInfoData { (status, message) in }
            //---------------------------------------------------
        }
    
    //MARK:-
    //MARK:- Methods :
    
    @objc func moveToWelcomeScreenWithAnimation(){
        let objRootVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomePageVC") as! WelcomePageVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objRootVC, animated: true)
    }
    
    @objc func moveToWelcomeScreenWithOutAnimation(){
        let objRootVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomePageVC") as! WelcomePageVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objRootVC, animated: false)
    }
    
    
    func moveToTheWelcomeScreen(withLogout: Bool){
        
        if withLogout {
            Constants.userDefault.removeObject(forKey:Variables.email)
            Constants.userDefault.removeObject(forKey:Variables.name)
            Constants.userDefault.removeObject(forKey:Variables.phone_number)
            Constants.userDefault.removeObject(forKey:Variables.picture)
            Constants.userDefault.removeObject(forKey: Variables.country_code)
            Constants.userDefault.removeObject(forKey:Variables.user_id)
            moveToWelcomeScreenWithAnimation()
        }else{
            moveToWelcomeScreenWithOutAnimation()
        }
    }
}


extension UIViewController {
    
    func presentLikeNavigationAnimation(viewController : UIViewController){
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromTop
        navigationController?.view.layer.add(transition, forKey: nil)
        navigationController?.pushViewController(viewController, animated: false)
    }
    
    
    func popLikeNavigationAnimation(){
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        navigationController?.view.layer.add(transition, forKey: nil)
        _ = navigationController?.popViewController(animated: false)
    }
}


extension SplashVC {
    
    func userBlock(){
        self.moveToTheWelcomeScreen(withLogout: true)
        
        AlertTheme.sharedInstance.Show(popupCategory: .attribute, message: "The account/phone number has been blocked. Please visit the FAQ page for further assistance", attributeRangeString: "FAQ", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
            
            if clicked == "FAQ" {
                let objFAQVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
                AppDelegate.shared.navController = self.navigationController!
                objFAQVC.activeScreen = .BlockFAQ
                objFAQVC.strWebUrl = "\(self.splashVM.userStatus.FAQLink)"
                self.navigationController?.pushViewController(objFAQVC, animated: true)
            }
        }
    }
    
    // get profile info:
    func getUserStatus()
    {
        acitivityIndicator.isHidden = false
        acitivityIndicator.startAnimating()
        let device_token = "\(Constants.userDefault.object(forKey: Variables.deviceToken) ?? "")"
        
        let country_code = "\(Constants.userDefault.object(forKey: Variables.country_code) ?? "")"
        let phone_number = "\(Constants.userDefault.object(forKey: Variables.phone_number) ?? "")"
         
        splashVM.getUserStatus(country_code: country_code, phone_number: phone_number) { [weak self](status, message)  in
            //Loader.sharedInstance.stopLoader()
            self?.acitivityIndicator.isHidden = true
            self?.acitivityIndicator.stopAnimating()
            
            if status == "success200"
            {
                //1.-----------------------------------------
                if self?.splashVM.userStatus.blockStatus == "1"{ // different
                    self?.userBlock()
                }else
                
                //2.-----------------------------------------
                if self?.splashVM.userStatus.deviceToken != device_token {
                    
                    self?.moveToTheWelcomeScreen(withLogout: true)
                    
                    //3.-----------------------------------------
                }else
                
                if Constants.userDefault.object(forKey: Variables.phone_number) != nil  {
                    
                    if (self?.splashVM.userStatus.last_login_local_time)! > self?.splashVM.userStatus.login_check_days ?? 0 {
                     
                        self?.moveToTheWelcomeScreen(withLogout: true)
                        
                    }else{
                        //   // dashboard API calling for update local time
                    
                        Singleton.shared.isLoginActive = false
                        self?.moveToTheWelcomeScreen(withLogout: false) // Call API in Dashboard.
                    }
                }else{ // user logout
                    self?.moveToTheWelcomeScreen(withLogout: true)
                }
                
            }else if status  == "success401" {
                
                self?.moveToTheWelcomeScreen(withLogout: true)
                
                AlertTheme.sharedInstance.Show(popupCategory: .attribute, message: "Your account was deactivated on \(self?.splashVM.userStatus.deactivated_on ?? ""). We are unable to register you at this time. Please visit the FAQ page for further details", attributeRangeString: "FAQ", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
                    
                    if clicked == "FAQ" {
                        // print("clicked FAQ button")
                        let objFAQVC = self?.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
                        AppDelegate.shared.navController = self!.navigationController!
                        objFAQVC.activeScreen = .BlockFAQ
                        objFAQVC.strWebUrl = "\(self?.splashVM.userStatus.FAQLink ?? "")"
                        self?.navigationController?.pushViewController(objFAQVC, animated: true)
                    }
                }
            }else if status == "success400"{
                
                self?.moveToTheWelcomeScreen(withLogout: false)
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    func updateLocalTime(){
        acitivityIndicator.isHidden = false
        acitivityIndicator.startAnimating()
        
        splashVM.setUserLocalTime { [weak self](status, message)  in
            //Loader.sharedInstance.stopLoader()
            self?.acitivityIndicator.isHidden = true
            self?.acitivityIndicator.stopAnimating()
            
            if status == "success" {
                self?.moveToTheWelcomeScreen(withLogout: false)
            }else{
                self?.moveToTheWelcomeScreen(withLogout: true)
            }
        }
    }
}
