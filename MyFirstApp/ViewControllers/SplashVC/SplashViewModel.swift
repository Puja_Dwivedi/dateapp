//
//  SplashViewModel.swift
//  MyFirstApp
//
//  Created by cis on 24/09/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class SplashViewModel: NSObject {
    let postVM = PostViewModel()
    
    var userStatus = (blockStatus : "" , deviceToken: "",FAQLink : "", diffDays : "", user_id : "",recrate_days : "", deactivated_on : "", post_total_count : 0, last_login_local_time : 0, login_check_days : 0)
    
    func getUserStatus( country_code: String, phone_number : String, completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        param.updateValue(country_code as AnyObject, forKey:"country_code")
        param.updateValue(phone_number as AnyObject, forKey:"phone_number")
        
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.checkUserStatus
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { [weak self](result, data) in
            
            if result == "success"
            {
                print("\n\n--------------------------------------------\nAPI: \(strService)\n \(data.response)")
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    let itemData = data.response.object(forKey: "data")  as! NSDictionary
                    self?.userStatus.blockStatus = "\(itemData.object(forKey: "blocked_indicator") ?? "")"
                    self?.userStatus.deviceToken = "\(itemData.object(forKey: "device_token") ?? "")"
                    self?.userStatus.FAQLink = "\(itemData.object(forKey: "FAQ") ?? "")"
                    self?.userStatus.user_id =  "\(itemData.object(forKey: "user_id") ?? "")"
                    
                    let strDate = "\(itemData.object(forKey: "last_login_local_time") ?? "0")"
                    
                    if strDate != "" {
                        self?.userStatus.last_login_local_time   = (self?.postVM.calclulateRemainDate(strCreateDate: strDate))!
                    }
                    
                    if let count = itemData.object(forKey: "login_check_days") as? String {
                        self?.userStatus.login_check_days = Int(count) ?? 0
                    }else{
                        self?.userStatus.login_check_days = 0
                    }
                    
                    if let count = itemData.object(forKey: "post_total_count") as? Int {
                        self?.userStatus.post_total_count = count
                    }else{
                        self?.userStatus.post_total_count = 0
                    }
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    completion("success200", message)
                    
                }else if dict.object(forKey: "code") as! Int == 401 {
                    
                    let itemDictData = dict.object(forKey: "data") as! NSDictionary
                    
                    self?.userStatus.blockStatus = "\(itemDictData.object(forKey: "blocked_indicator") ?? "")"
                    self?.userStatus.deviceToken = "\(itemDictData.object(forKey: "device_token") ?? "")"
                    self?.userStatus.FAQLink = "\(itemDictData.object(forKey: "FAQ") ?? "")"
                    
                    self?.userStatus.diffDays = "\(itemDictData.object(forKey: "diffDays") ?? "")"
                    self?.userStatus.user_id =  "\(itemDictData.object(forKey: "user_id") ?? "")"
                    self?.userStatus.recrate_days = "\(itemDictData.object(forKey: "recreate_days") ?? "")"
                    let deactivated_date  = "\(itemDictData.object(forKey: "deactivated_on") ?? "")"
                    let country = "\(itemDictData.object(forKey: "country_code") ?? "")"
                    
                    
                    if let count = itemDictData.object(forKey: "post_total_count") as? Int {
                        self?.userStatus.post_total_count = count
                    }else{
                        self?.userStatus.post_total_count = 0
                    }
                    
                    if country == "+1"{ // for US
                        self?.userStatus.deactivated_on = "\(self?.convertDateFormaterForUS(deactivated_date, dateFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                        
                    }else{ // For Non US
                        
                        self?.userStatus.deactivated_on =  "\(self?.convertDateFormaterForNonUS(deactivated_date, dateFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                    }
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    completion("success401", message)
                }else if  dict.object(forKey: "code") as! Int == 400 { // user not registered
                    completion("success400", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    func convertDateFormaterForUS(_ date: String, dateFormate : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormate
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    func convertDateFormaterForNonUS(_ date: String, dateFormate : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormate
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    
    func setUserLocalTime( completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let currentDate = Date.getCurrentDate(date: Date())
        
        var param = AppTheme.sharedInstance.GetBasicParam()
        param.updateValue(userid as AnyObject, forKey:"user_id")
        param.updateValue(currentDate as AnyObject, forKey:"last_activity_local_time")
        
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.updateLastActivityLocalTime
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            
            if result == "success"
            {
                print("\n\n--------------------------------------------\nAPI: \(strService)\n \(data.response)")
                print(data.message)
                
                let dict = data.response
                
                //  completion("success", data.message, .moveToHomeScreen, "")
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    completion("success", message)
                    
                }else{
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    
    // Get filter array option :-------------------------------------------------------------------
    func getInfoData(completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        let strService = APPURL.Urls.getLinkPostMatch
        
        print("strService \(strService)")
        
        RV_GetPostMethod.getService_API(API: strService) { (result, data)  in
            
            if result == "success"
            {
                let dict = data as NSDictionary
                
                if dict.object(forKey: "code") as! Int == 200 { //
                    Singleton.shared.InfoPage = dict.object(forKey: "data") as! [Any]
                    completion("success", "Record found")
                }else{ // error :
                    completion("error", "something went wron, Please try again")
                }
            }
        }
    }
}

