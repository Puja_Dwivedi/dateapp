//
//  EmailVC.swift
//  MyFirstApp
//
//  Created by cis on 12/07/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class EmailVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet:
    
    @IBOutlet weak var txtFldEmail: UITextField!
    
    //MARK:-
    //MARK:- Variables:
    
    var isChangeAnything = false
    let emailVM = EmailViewModel()
    
    
    //MARK:-
    //MARK:- App Flow:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFldEmail.text = settingInfo.email
        txtFldEmail.delegate = self
    }
    
    //MARK:-
    //MARK:- Action Method:
    
    @IBAction func actionBack(_ sender: UIButton) {
        if isChangeAnything{
            updateEmail_API(email: txtFldEmail.text!)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK:-
//MARK:- API:

extension EmailVC {
    
    func updateEmail_API(email : String )
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        emailVM.updateEmailApi(emailID: email) { (status, message, dictResult) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                settingInfo.email = email
                self.navigationController?.popViewController(animated: true)
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}

//MARK:-
//MARK:- text field delegate :

extension EmailVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        isChangeAnything = true
        return true
    }
}
