//
//  LoginViewModel.swift
//  MyFirstApp
//
//  Created by cis on 15/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewModel: NSObject {
    
    var varificationID = ""
 
    /*
     user is not registered, first user will have to register into firebase then register our database
     */
    func authonticationWithFirebase(strCompleteMobileNumber : String,completion: @escaping (_ result: String, _ message : String) -> Void){
        
        PhoneAuthProvider.provider().verifyPhoneNumber(strCompleteMobileNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                self.varificationID = ""
                SnackBar.sharedInstance.show(message: error.localizedDescription, showMsgAt: .bottom)
                completion("error", error.localizedDescription)
                return
            }else{
               
                self.varificationID = "\(verificationID ?? "")"
                SnackBar.sharedInstance.show(message: "Verification send to your mobile number", showMsgAt: .bottom)
                completion("success","Verification send to your mobile number")
            }
        }
    }
}

