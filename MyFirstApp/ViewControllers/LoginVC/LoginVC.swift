//
//  LoginVC.swift
//  MyFirstApp
//
//  Created by cis on 31/12/20.
//  Copyright © 2020 cis. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase


class LoginVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var txtFldNumber: UITextField!
    @IBOutlet weak var btnNext_bottom: NSLayoutConstraint!
    
    //MARK:-
    //MARK:- variables:
    var loginVM = LoginViewModel()
    var splashVM = SplashViewModel()
    var arrCountryList = [CountryCodeModelClass]()
    var location = Location()
    var settingVM = SettingViewModel()
    var countryInfo = (lat :0.0, long: 0.0, countryCode : "")
    var selectedCountryCodeInfo:  CountryCodeModelClass =  CountryCodeModelClass(countryFlag: "", countryCode: "", countryName: "", countryNameWithCode: "", max_phoneLength: 0)
    var isProceedAction = false
    
    var tempUserId = ""
    //MARK:-
    //MARK:- App Flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Constants.userDefault.object(forKey: Variables.phone_number) != nil {
            self.txtFldNumber.text = ""
            let objRootVC = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
            AppDelegate.shared.navController = self.navigationController!
            self.navigationController?.pushViewController(objRootVC, animated: false)
            return;
        }
        
        txtFldNumber.text = ""
        txtFldNumber.becomeFirstResponder() // keyboard open
        // for retrieve country code :
        location.setupLocationManager()
        location.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        loadCountryJsonList()
    }
    
    
    //MARK:-
    //MARK:- Action Methods :
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.popLikeNavigationAnimation()
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        checkValidation()
    }
    
    @IBAction func actionCountryCode(_ sender: UIButton) {
        self.view.endEditing(true)
        
        CountryCodeView.sharedInstance.Show(arr: arrCountryList) { [weak self](searchModel) in
            print("selected :\n countryName :  \(searchModel.countryName)")
            self?.selectedCountryCodeInfo = searchModel
            
            UIView.animate(withDuration: 0.5) {
                self?.lblCountryCode.text = "\(searchModel.countryFlag) \(searchModel.countryCode)"
                self?.txtFldNumber.becomeFirstResponder() // keyboard open
                self?.view.layoutIfNeeded()
            }
        }
    }
    
    //MARK:-
    //MARK:-  Methods :
    
    func checkValidation(){
        self.view.endEditing(true)
        let strMobileNumber  = txtFldNumber.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard strMobileNumber.count > 0 else {
            SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.pleaseEnterMobile, showMsgAt: .bottom); return }
        
        guard strMobileNumber.count == selectedCountryCodeInfo.max_phoneLength  else {
            SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.PhoneNumberShouldBe) \(selectedCountryCodeInfo.max_phoneLength ) \(AlertMessages.shareInstance.digits)", showMsgAt: .bottom); return }
        
        guard selectedCountryCodeInfo.countryCode.count > 0  else {
            SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.pleaseEnterCountryCode)", showMsgAt: .bottom); return }
        
        let str1 = strMobileNumber.replace(string: "(", replacement: "")
        let str2 = str1.replace(string: ")", replacement: "")
        let str3 = str2.replace(string: "-", replacement: "")
        let str4 = str3.replace(string: " ", replacement: "")
        
        
        // Call OTP directly
        self.callOTP(countryName: self.selectedCountryCodeInfo.countryName, country_code: self.selectedCountryCodeInfo.countryCode, phone_number: str4)
    }
    
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                UIView.animate(withDuration: 0.5) {
                    self.btnNext_bottom.constant =  keyboardSize.height + 5
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            UIView.animate(withDuration: 0.5) {
                self.btnNext_bottom.constant = 5
                self.view.layoutIfNeeded()
            }
        }
    }
    
    /*
     Move to next screens:
     */
    func moveToVerificationCodeScreen(countryName : String , country : String, mobileNumber : String ,verificationID : String){
        let objOtp = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationVC") as! OtpVerificationVC
        objOtp.otpInfo.countryCode = country
        objOtp.otpInfo.mobileNumber = mobileNumber
        objOtp.otpInfo.verificationID = verificationID
        objOtp.otpInfo.countryName = countryName
        objOtp.otpInfo.countryFlag = selectedCountryCodeInfo.countryFlag
        
        if isProceedAction {
            objOtp.tempUserId = "\(self.splashVM.userStatus.user_id )"
            objOtp.activeeScreen = .proceed
        }else{
            objOtp.tempUserId = ""
            objOtp.activeeScreen = .normal
        }
        
        self.presentLikeNavigationAnimation(viewController: objOtp)
    }
    
    
    func moveToHomeScreen(){
        let objRootVC = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objRootVC, animated: true)
    }
    
    
    func loadCountryJsonList()
    {
        guard let path = Bundle.main.path(forResource: "CountryCode", ofType:"json") else {
            debugPrint( "path not found")
            return
        }
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? [String : Any]
            {
                print(object)
            }
            else if let Anyjson = json as? [Any]
            {
                for (_,dict) in Anyjson.enumerated(){
                    
                    let dictItem = dict as! NSDictionary
                    
                    let countryFlag = "\(dictItem.object(forKey: "flag") ?? "")"
                    let countryCode = "\(dictItem.object(forKey: "dial_code") ?? "")"
                    let countryName = "\(dictItem.object(forKey: "name") ?? "")"
                    let countryNameWithCode = "\(dictItem.object(forKey: "code") ?? "")"
                    let maxPhoneLength = dictItem.object(forKey: "max_phoneLength") as! Int
                    arrCountryList.append(CountryCodeModelClass(countryFlag: countryFlag, countryCode: countryCode, countryName: countryName, countryNameWithCode: countryNameWithCode, max_phoneLength: maxPhoneLength))
                }
            }
            else
            {
                print("error in formating")
            }
            
        } catch {
            print(error.localizedDescription)
        }
    }
}

/*
 MAEK:- Text field delegates:
 */
extension LoginVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= selectedCountryCodeInfo.max_phoneLength
        
    }
}

/*
 MARK:- Country code delegate :
 */
extension LoginVC : LocationDelegate {
    
    func didReceivedCurrentLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        countryInfo.lat = latitude
        countryInfo.long = longitude
        
        location.getCountryCode(lat: countryInfo.lat, long: countryInfo.long) { [weak self](countryCode) in
            self?.countryInfo.countryCode = countryCode
            print("country code : \(countryCode)")
            
            let index =  self?.arrCountryList.firstIndex(where: {$0.countryNameWithCode == countryCode})
            
            if index != nil {
                self?.selectedCountryCodeInfo.countryName = self?.arrCountryList[index!].countryName ?? ""
                self?.selectedCountryCodeInfo.countryCode = self?.arrCountryList[index!].countryCode ?? ""
                self?.selectedCountryCodeInfo.countryFlag = self?.arrCountryList[index!].countryFlag ?? ""
                self?.selectedCountryCodeInfo.countryNameWithCode = self?.arrCountryList[index!].countryNameWithCode ?? ""
                self?.selectedCountryCodeInfo.max_phoneLength = self!.arrCountryList[index!].max_phoneLength
                
                self?.lblCountryCode.text = "\(self?.arrCountryList[index!].countryFlag ?? "") \(self?.arrCountryList[index!].countryCode ?? "")"
            }
            //}
        }
    }
    
    func didReceivedtLocationError(error: String) {
        print(error)
    }
}


/*
 MARK:- API's
 */

extension LoginVC {
    
    func callOTP(countryName  : String , country_code : String, phone_number : String){
        
        Loader.sharedInstance.showLoader(msg: "Loading...")
        let phoneNumber = country_code + phone_number
        
        loginVM.authonticationWithFirebase(strCompleteMobileNumber: phoneNumber) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"{
                self.moveToVerificationCodeScreen(countryName: countryName, country: country_code, mobileNumber: phone_number, verificationID: self.loginVM.varificationID)
            }
        }
    }
}


extension LoginVC {
    
    // get profile info:
    func getUserStatus(country_code : String ,phone_number : String )
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        let device_token = "\(Constants.userDefault.object(forKey: Variables.deviceToken) ?? "")"
        
        splashVM.getUserStatus(country_code: country_code, phone_number: phone_number) { [weak self](status, message)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success200"
            {
                
                //1.
                if self?.splashVM.userStatus.deviceToken == device_token {
                    
                    if self?.splashVM.userStatus.blockStatus == "0"{ // user unblock //&&
                        
                        //every time when user want to login then it will redirect to the otp screen then move to home screen if user already exist and if not then user will be redirect to the registration page.
                        self?.callOTP(countryName: self?.selectedCountryCodeInfo.countryName ?? "", country_code: self?.selectedCountryCodeInfo.countryCode ?? "", phone_number: phone_number)
                        
                    }else{ // account block
                        
                        AlertTheme.sharedInstance.Show(popupCategory: .attribute, message: "The account/phone number has been blocked. Please visit the FAQ page for further assistance", attributeRangeString: "FAQ", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
                            
                            if clicked == "FAQ" {
                                // print("clicked FAQ button")
                                let objFAQVC = self?.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
                                AppDelegate.shared.navController = self!.navigationController!
                                objFAQVC.activeScreen = .BlockFAQ
                                objFAQVC.strWebUrl = "\(self?.splashVM.userStatus.FAQLink ?? "")"
                                self?.navigationController?.pushViewController(objFAQVC, animated: true)
                            }
                        }
                    }
                    
                }else{ // not found same device token
                    
                    // add some code here for -->
                    if self?.splashVM.userStatus.post_total_count ?? 0 <= 0 {
                        
                        self?.deleteAccount(userID: "\(self?.splashVM.userStatus.user_id ?? "")", phone_number: phone_number)
                        // first delete the account then call OTP
                    }else
                    
                    if self?.splashVM.userStatus.blockStatus == "0"{ // user not blocked
                        
                        AlertTheme.sharedInstance.Show(popupCategory: .normal, message: "Sign up from new device detected. Existing account will be terminated", attributeRangeString: "", isCancelButtonVisible: true, arrBtn: ["Proceed"], isBottomTxt: "Active posts, matches and conversations will be removed") { (clicked) in
                            
                            if clicked == "Proceed" {
                                self?.isProceedAction = true
                                self?.callOTP(countryName: self?.selectedCountryCodeInfo.countryName ?? "", country_code: self?.selectedCountryCodeInfo.countryCode ?? "", phone_number: phone_number)
                                
                                
                                // first delete the account then call OTP
                                
                                // delete app
                            }
                        }
                    }else{ // user blocked
                        
                        AlertTheme.sharedInstance.Show(popupCategory: .attribute, message: "The account/phone number has been blocked. Please visit the FAQ page for further assistance", attributeRangeString: "FAQ", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
                            
                            if clicked == "FAQ" {
                                // print("clicked FAQ button")
                                let objFAQVC = self?.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
                                AppDelegate.shared.navController = self!.navigationController!
                                objFAQVC.activeScreen = .BlockFAQ
                                objFAQVC.strWebUrl = "\(self?.splashVM.userStatus.FAQLink ?? "")"
                                self?.navigationController?.pushViewController(objFAQVC, animated: true)
                            }
                        }
                    }
                }
                
            }else if status == "success400"{
                
                self?.callOTP(countryName: self?.selectedCountryCodeInfo.countryName ?? "", country_code: self?.selectedCountryCodeInfo.countryCode ?? "", phone_number: phone_number)
                
            } else if status == "success401" {
                
                let recrate_days = "\(self!.splashVM.userStatus.recrate_days)"
                let diffDays  = "\(self!.splashVM.userStatus.diffDays)"
                
                if (recrate_days as NSString).integerValue < (diffDays as NSString).integerValue {
                    
                    //continue login
                    self?.callOTP(countryName: self?.selectedCountryCodeInfo.countryName ?? "", country_code: self?.selectedCountryCodeInfo.countryCode ?? "", phone_number: phone_number)
                    
                }else{
                    AlertTheme.sharedInstance.Show(popupCategory: .attribute, message: "Your account was deactivated on \(self?.splashVM.userStatus.deactivated_on ?? ""). We are unable to register you at this time. Please visit the FAQ page for further details", attributeRangeString: "FAQ", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
                        
                        if clicked == "FAQ" {
                            // print("clicked FAQ button")
                            let objFAQVC = self?.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
                            AppDelegate.shared.navController = self!.navigationController!
                            objFAQVC.activeScreen = .BlockFAQ
                            objFAQVC.strWebUrl = "\(self?.splashVM.userStatus.FAQLink ?? "")"
                            self?.navigationController?.pushViewController(objFAQVC, animated: true)
                        }
                    }
                }
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    
    //---------------------
    // delete account :
    func deleteAccount(userID: String ,phone_number : String)
    {
        // let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        settingVM.deleteAccountApi(userID: userID) { [weak self](status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self?.callOTP(countryName: self?.selectedCountryCodeInfo.countryName ?? "", country_code: self?.selectedCountryCodeInfo.countryCode ?? "", phone_number: phone_number)
            }else{
                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.somethingWentWrong)", showMsgAt: .bottom)
            }
        }
    }
}



