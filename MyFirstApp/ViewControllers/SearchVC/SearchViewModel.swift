//
//  SearchViewModel.swift
//  MyFirstApp
//
//  Created by cis on 25/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class SearchViewModel: NSObject {
    
    var arrPostSearchData = [Any]()
    var operatorFlight = ""
    
    func callSearchPost_Api(requestParam : [String : AnyObject], completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.searchData
        param =  requestParam
        
        print("header \(header) \n API: \(strService) \n request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 {
                    self.arrPostSearchData =  self.retrieveData(data: dict.object(forKey: "data") as! [Any])
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                completion("error", message)
            }
        }
    }
    
    //--------- retrieve data accroding to the date:
    
    func toDate(strDate: String, dateFormate : String)-> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormate
        return  dateFormatter.date(from: strDate)!
    }
    
    func calclulateRemainDate(strCreateDate : String)-> Int{
        
        let createdPostDate = Date.getFormattedDate(strDate: strCreateDate, oldformatter: "dd/MM/yyyy", newformatter: "dd/MM/yyyy")
        let currentDate = Date.getCurrentDate(date: Date())
        let currentSystemDate = Date.getFormattedDate(strDate: currentDate, oldformatter: "dd/MM/yyyy", newformatter: "dd/MM/yyyy")
        
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: createdPostDate)
        let date2 = calendar.startOfDay(for: currentSystemDate)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        return components.day ?? 0
    }
    
    
    func retrieveData(data : [Any])->[Any]{
        var arrTemp = [Any]()
        
        for index in 0..<data.count {
            let dict = data[index] as! NSDictionary
            
            let strDate = "\(dict.object(forKey: "date") ?? "")"
            if strDate != "" {
                var remainigDays = calclulateRemainDate(strCreateDate: strDate)
                remainigDays = 7 - remainigDays
                if remainigDays > 0 {
                    arrTemp.append(dict)
                }
            }
        }
        return arrTemp
    }
    
    //----------------------------------------------------------------------
    
    func callCheckFlightDataExist(attribute : String, date : String, city : String, completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        param.updateValue(attribute as AnyObject, forKey: "attribute")
        param.updateValue(date as AnyObject, forKey: "date")
        param.updateValue(city as AnyObject, forKey: "city")
        let strService = APPURL.Urls.checkFlightDataExist
        
        
        print("header \(header) \n API: \(strService) \n request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 {
                    self.operatorFlight =  "\(dict.object(forKey: "operator") ?? "")"
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                completion("error", message)
            }
        }
    }
    
    
    func getCountryCodeFrom(dict : NSDictionary)-> String{
        
        if let arrComponents = dict.object(forKey: "address_components") as? [Any]{
            
            for dictItem in arrComponents{
                
                if let dictComponent = dictItem as? NSDictionary{
                    
                    if let arrType = dictComponent.object(forKey: "types") as? [Any] {
                        
                        for type in arrType {
                            
                            if "\(type)" == "country" {
                                return "\(dictComponent.object(forKey: "short_name") ?? "")"
                            }
                        }
                    }
                }
            }
        }
        return ""
    }
    
    func getTimeStatus() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "a"
        let time12 = formatter.string(from: date)
        return time12
    }
    
    
    func getTimeInterval()-> String{
       // let calendar = Calendar.current
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as? Int ?? 0
       
        if countryDialCode == 1 { // for us date formate : 12 hours
            let calendar = Calendar.current
            
            let date = Date()
            let formatter = DateFormatter()
            
            formatter.dateStyle = .none
            formatter.timeStyle = .short
            formatter.locale = Locale(identifier: "en_US_POSIX")
            
            formatter.dateFormat = "hh a" // "a" prints "pm" or "am"
            let hourString = formatter.string(from:date) // "12 AM"
            let minutes = calendar.component(.minute, from: date)
            let hours = hourString.components(separatedBy: " ")
            
            var minValue = ""
            if hours.count > 1 {
                minValue = hours[1]
            } else {
                minValue = getTimeStatus()
            }

            if 0 <= minutes && 15 >= minutes {
                return "\(hours[0]):00-15 \(minValue)"
            }else if 15 <= minutes && 30 >= minutes{
                return "\(hours[0]):15-30 \(minValue)"
            }else if  30 <= minutes && 45 >= minutes{
                return "\(hours[0]):30-45 \(minValue)"
            }else if  45 <= minutes {
                return "\(hours[0]):45-00 \(minValue)"
            }
            
        }else{// for non US : date formate 24 hours :
            let date = Date()
            
            let hour = Calendar.current.component(.hour, from: date)
            let minutes = Calendar.current.component(.minute, from: date)
            
            if 0 <= minutes && 15 >= minutes {
                return "\(String(format: "%02d", hour)):00-15"
            }else if 15 <= minutes && 30 >= minutes{
                return "\(String(format: "%02d", hour)):15-30"
            }else if  30 <= minutes && 45 >= minutes{
                return "\(String(format: "%02d", hour)):30-45"
            }else if  45 <= minutes {
                return "\(String(format: "%02d", hour)):45-00"
            }
        }
        return ""
    }
    
    func checkPlaceCondition(placeAPIDetailData1 : NSDictionary)-> (status :Bool,countryName: String, types : [String],firstOffset : String){
        let offsetCountryName = retrieveLastOffset(placeAPIDetailData1: placeAPIDetailData1)
        let retrieveType = retrieveTypes(placeAPIDetailData1: placeAPIDetailData1)
        
        //1. condition
        var ischeckCountry = false
        if (offsetCountryName.lastOffsetValue.lowercased() == "USA".lowercased() ||  offsetCountryName.lastOffsetValue.lowercased() == "Canada".lowercased()) {
            ischeckCountry = true
        }else{
            ischeckCountry = false
        }
        
        //2.condition:
        
        if ischeckCountry && retrieveType.status {
            return (status :true,countryName: offsetCountryName.lastOffsetValue , types : retrieveType.item,firstOffset : offsetCountryName.firstOffsetValue)
        }else{
            return (status :false, countryName:"", types : [String()],firstOffset : "")
        }
    }
    
    
    func retrieveLastOffset(placeAPIDetailData1 : NSDictionary)->(firstOffsetValue :String, lastOffsetValue : String){
        var firstOffset = ""
        var lastOffset = ""
        if let arrTerms  = placeAPIDetailData1.object(forKey: "terms") as? [Any]{
            if let lastValue = arrTerms.last as? NSDictionary {
                lastOffset = "\(lastValue.object(forKey: "value") ?? "")"
            }
            if let firstValue = arrTerms.first as? NSDictionary {
                firstOffset = "\(firstValue.object(forKey: "value") ?? "")"
            }
            
        }
     return (firstOffsetValue :firstOffset, lastOffsetValue : lastOffset)
    }
    
    func retrieveTypes(placeAPIDetailData1 : NSDictionary)-> (status: Bool, item : [String]){
        
        let arrMatch = ["route", "sublocality_level_1", "sublocality","administrative_area_level_2","administrative_area_level_1"]
        let arrTypes = placeAPIDetailData1.object(forKey: "types") as! [String]
        
        var isMatch = false
            for item in arrTypes {
                if arrMatch.contains(item.lowercased()){
                    isMatch = true
                }
            }
        
        
       return  (status: isMatch, item : arrTypes)

    }
    
    //------------------------------------------
    
    func makeRequestForPlacestreet(request Param : [String : AnyObject], placeAPIDetailData1 : NSDictionary, placeAPIDetailData2 : NSDictionary)-> [String : AnyObject]{
        var param =   Param
        let dictFormate = placeAPIDetailData1.object(forKey: "structured_formatting") as! NSDictionary
        if let main_text = dictFormate.object(forKey: "main_text") as? String {
            param.updateValue(main_text as AnyObject ,forKey: "main_text")
        }else{
            param.updateValue("" as AnyObject ,forKey: "main_text")
        }
        
        
        //4.secondary_text
        if let secondary_text = dictFormate.object(forKey: "secondary_text") as? String {
            param.updateValue(secondary_text as AnyObject ,forKey: "secondary_text")
        }else{
            param.updateValue("" as AnyObject ,forKey: "secondary_text")
        }
        
        
        //5. types
        let arrType = placeAPIDetailData1.object(forKey: "types") as! [String]
        if arrType.count > 0 {
            param.updateValue((arrType as NSArray).componentsJoined(by: ",") as AnyObject ,forKey: "types")
            
        }else{
            param.updateValue("" as AnyObject ,forKey: "types")
        }
        
        
        // 2nd API: --------------------------------------------------->
        
        param.updateValue("" as AnyObject ,forKey: "administrative_area_level_2")
        
        if let arrComponents = placeAPIDetailData2.object(forKey: "address_components") as? [Any]{
            
            for dictItem in arrComponents{
                
                if let dictComponent = dictItem as? NSDictionary{
                    
                    if let arrType = dictComponent.object(forKey: "types") as? [Any] {
                        
                        for type in arrType {
                            
                            if "\(type)" == "administrative_area_level_2" {
                                param.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "administrative_area_level_2")
                            }
                        }
                    }
                }
            }
        }
        
        param.updateValue("\(placeAPIDetailData2.object(forKey: "formatted_address") ?? "")" as AnyObject ,forKey: "formatted_address")
        
        return param
    }
    
    
    
    func checkValidation(strTickets : String,
                         selectedDate : String,
                         activeScreenStatus : CreatePostActiveScreen,
                         placeID : String ,
                         placeAPIDetailData1 : NSDictionary,
                         placeAPIDetailData2 : NSDictionary,
                         isPlaceAddressActive : Bool,
                         strSubAttribute : String,
                         arrLookingTag :  [String],
                         arrInterestedTag : [String],
                         isIncludeTimeInPost : Bool,
                         timeIntervalID : Int)->[String : AnyObject]{
        
        let userID = Constants.userDefault.object(forKey: Variables.user_id)
        
        // create request :
        var param = [String : AnyObject]()
        param.updateValue( userID as AnyObject, forKey:"user_id")
        
        param.updateValue( strTickets as AnyObject, forKey:"attribute")
        
        param.updateValue( selectedDate as AnyObject, forKey:"date")
        
        param.updateValue( "" as AnyObject, forKey:"city")
        
        
        // add 1 more paramter for place and street, use place id
        switch activeScreenStatus {
        case .airplane:
            param.updateValue( 1 as AnyObject ,forKey:"event_subtype")
            break;
            
        case .subway:
            param.updateValue( 2 as AnyObject ,forKey:"event_subtype")
            break;
            
        case .place:
            param.updateValue( 3 as AnyObject ,forKey:"event_subtype")
            param.updateValue( placeID as AnyObject ,forKey:"attribute")
            break;
            
        case .street:
            param.updateValue( 4 as AnyObject ,forKey:"event_subtype")
            param.updateValue( placeID as AnyObject ,forKey:"attribute")
            break;
            
        case .bus:
            param.updateValue( 5 as AnyObject ,forKey:"event_subtype")
            break;
            
        case .train:
           // strSubAttribute = strTrainValue
            param.updateValue( 6 as AnyObject ,forKey:"event_subtype")
            break;
        case .none:
            break;
        }
    
        if activeScreenStatus == .place || activeScreenStatus == .street{
            
            param =  makeRequestForPlacestreet(request: param, placeAPIDetailData1: placeAPIDetailData1, placeAPIDetailData2: placeAPIDetailData2)
            
            if isPlaceAddressActive {
                param.updateValue( 1 as AnyObject ,forKey:"toggle_on_status")
            }else{
                param.updateValue( 0 as AnyObject ,forKey:"toggle_on_status")
            }
        }else{
            param.updateValue( "" as AnyObject ,forKey:"main_text")
            param.updateValue( "" as AnyObject ,forKey:"formatted_address")
            param.updateValue( "" as AnyObject ,forKey:"administrative_area_level_2")
            param.updateValue( "" as AnyObject ,forKey:"secondary_text")
            param.updateValue( 0 as AnyObject ,forKey:"toggle_on_status")
        }
        
        
        if activeScreenStatus == .place {
            
            let value = checkPlaceCondition(placeAPIDetailData1: placeAPIDetailData1)
            
            if value.status {
                param.updateValue( value.countryName as AnyObject ,forKey:"country_name")
                param.updateValue( value.types.joined(separator: ",") as AnyObject ,forKey:"types_name")
                param.updateValue( 0 as AnyObject ,forKey:"toggle_on_status")
                param.updateValue( value.firstOffset as AnyObject ,forKey:"first_offset")
                // place address active desable:
                param.updateValue( "" as AnyObject ,forKey:"sub_attribute")
                param.updateValue( 1 as AnyObject ,forKey:"is_types_include")
            }else{
                param.updateValue( 0 as AnyObject ,forKey:"is_types_include")
                if isPlaceAddressActive{
                    
                    param.updateValue( strSubAttribute as AnyObject ,forKey:"sub_attribute")
                    
                }else{
                    param.updateValue( "" as AnyObject ,forKey:"sub_attribute")
                }
            }
        }else{
            param.updateValue( strSubAttribute as AnyObject ,forKey:"sub_attribute")
        }
        
        
        // from : search_from
        
        if arrLookingTag.count > 0 {
            var strSearch_from = ""
            for index in 0..<arrLookingTag.count{
                
                if strSearch_from == "" {
                    strSearch_from = arrLookingTag[index]
                }else{
                    strSearch_from = strSearch_from + "," + arrLookingTag[index]
                }
                
                param.updateValue( strSearch_from as AnyObject ,forKey: "search_from")
            }
        }else{
            param.updateValue( "" as AnyObject ,forKey: "search_from")
        }
        
        if arrInterestedTag.count > 0 {
            // To : search_to
            var strSearch_to = ""
            for (index,_) in arrInterestedTag.enumerated() {
                if strSearch_to == ""{
                    strSearch_to = arrInterestedTag[index]
                }else{
                    strSearch_to = strSearch_to + "," + arrInterestedTag[index]
                }
                
                param.updateValue( strSearch_to as AnyObject ,forKey: "search_to")
            }
        }else{
            param.updateValue( "" as AnyObject ,forKey: "search_to")
        }
        
        //new param:
        if isIncludeTimeInPost {
            param.updateValue(timeIntervalID as AnyObject ,forKey: "time_interval")
        }else{
            param.updateValue("" as AnyObject ,forKey: "time_interval")
        }
        
        // add new param request:
        param.updateValue("" as AnyObject ,forKey: "search_tag")
        
        // api call:
        print("requet param : \(param)")
        return param
    }
}
