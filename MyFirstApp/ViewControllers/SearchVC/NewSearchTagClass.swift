//
//  SearchTagClass.swift
//  MyFirstApp
//
//  Created by cis on 16/11/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

//MARK:-
//MARK:- Tags with Textfilds:

extension NewSearchVC :  UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let searchText  = textField.text! + string
        
        let maxLength = 21
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        print("newString.length : \(newString.length)")
        
        if newString.length == maxLength && string == " "{
            print("add space")
        }else{
            guard newString.length < maxLength else{ return false}
        }
        
        // My attributes :
        if textField == txtFldWriteHere {
            isEmptyBackLooking = false
            
            if newString.length == maxLength {
                txtFldWriteHere.text = ""
                isEmptyBackLooking = true
                arrLookingTag.append(searchText.replace(string: " ", replacement: ""))
                createTagsForLooking(OnView: scrollViewLooking, withArray: arrLookingTag, isCancelButtonVisible: true)
                return false
            }
            
            //Hide remove space :
            if range.location == 0 && string == " " {
                txtFldWriteHere.text = ""
                return false;
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if !checkIsTagRepeated(str: str, strArr: arrLookingTag) {
                // add new element :
                if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                    txtFldWriteHere.text = ""
                    isEmptyBackLooking = true
                    arrLookingTag.append(searchText.replace(string: " ", replacement: ""))
                    createTagsForLooking(OnView: scrollViewLooking, withArray: arrLookingTag, isCancelButtonVisible: true)
                    return false
                }
            }
            return true
            
            // My preferences:-------------------------------------------------------------------------------
        }else if textField == txtFldWriteHereInterested {
            isEmptyBackInterest = false
            
            if newString.length == maxLength {
                textField.text = ""
                isEmptyBackInterest = true
                arrInterestedTag.append(searchText.replace(string: " ", replacement: ""))
                createTagsForInterested(OnView: scrollViewInterested, withArray: arrInterestedTag, isCancelButtonVisible: true)
                return false
            }
            
            //Hide remove space :
            if range.location == 0 && string == " " {
                textField.text = ""
                return false;
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if !checkIsTagRepeated(str: str, strArr: arrInterestedTag) {
                // add new element :
                if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                    textField.text = ""
                    isEmptyBackInterest = true
                    arrInterestedTag.append(searchText.replace(string: " ", replacement: ""))
                    createTagsForInterested(OnView: scrollViewInterested, withArray: arrInterestedTag, isCancelButtonVisible: true)
                    return false
                }
            }
            return true
        }
        return true
    }
}

extension NewSearchVC : PinTexFieldDelegate{
    func didPressBackspace(textField : PinTextField){
        print("back prashed")
        
        // My attibutes :
        if textField == txtFldWriteHere {
            
            // for removed :
            if arrLookingTag.count > 0 && (txtFldWriteHere.text?.isEmpty ?? false){
                print("value :")
                if isEmptyBackLooking {
                    arrLookingTag.remove(at: arrLookingTag.count - 1)
                    createTagsForLooking(OnView: scrollViewLooking, withArray: arrLookingTag, isCancelButtonVisible: true)
                }
                isEmptyBackLooking = true
            }
            
            // My Preferences :
        }else if textField == txtFldWriteHereInterested{
            
            // for removed :
            if arrInterestedTag.count > 0 && (txtFldWriteHereInterested.text?.isEmpty ?? false){
                
                if isEmptyBackInterest {
                    arrInterestedTag.remove(at: arrInterestedTag.count - 1)
                    createTagsForInterested(OnView: scrollViewInterested, withArray: arrInterestedTag, isCancelButtonVisible: true)
                }
                isEmptyBackInterest = true
            }
        }
    }
}

//My Preerences support methods :
extension NewSearchVC {
    
    func createTagsForInterested(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let height = 30
        let spaceHeight : CGFloat = 20
        let screenWidth = UIScreen.main.bounds.size.width - 60.0
        
        for str in data  {
            
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name: "Poppins-Regular", size: 14.0)!)
            // edit :
            
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            if checkWholeWidth > screenWidth {
                //we are exceeding size need to change xpos
                xPos = 0.0
                ypos = ypos + spaceHeight + 8.0 + 11
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 38.5 , height: CGFloat(height)))
            //  bgView.layer.cornerRadius = 14.5
            bgView.backgroundColor = .clear//UIColor(red: 33.0/255.0, green: 135.0/255.0, blue:199.0/255.0, alpha: 1.0)
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 14.0, y: -0.5, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "Poppins-Regular", size: 14.0)
            textlable.text = startstring
            
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
            bgView.addSubview(textlable)
            
            //when close buttton enable
            
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                //
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 28.0, height: 28.0)
                button.backgroundColor = UIColor.clear
                button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
                
                button.setImage(#imageLiteral(resourceName: "cross_icon"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagPreferences(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldWriteHereInterested.isHidden = false
            btnYou.isHidden = false
            
            if arrInterestedTag.count == 12 {
                txtFldWriteHereInterested_top.constant = ypos
                txtFldWriteHereInterested_left.constant = xPos
                btnYou.isHidden = true
            }else{
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    //we are exceeding size need to change xpos
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldWriteHereInterested_top.constant = ypos
                    txtFldWriteHereInterested_left.constant = xPos
                }else{
                    txtFldWriteHereInterested_top.constant = ypos
                    txtFldWriteHereInterested_left.constant = xPos
                }
            }
            
            txtFldWriteHereInterested_width.constant = screenWidth - xPos + 50// here 30 is padding....
        }else{
            txtFldWriteHereInterested.isHidden = true
            btnYou.isHidden = true
        }
        
        if arrInterestedTag.count <= 12 {
            self.txtFldWriteHereInterested.alpha = 1
            scrollViewInterested_h.constant = ypos + 34 + 10//Here 50 is the height of the txtview, and 10 is the bottom space of
            // the scrollview
            if arrInterestedTag.count == 12 {
                self.txtFldWriteHereInterested.alpha = 0
                self.txtFldWriteHereInterested.resignFirstResponder()
                self.view.endEditing(true)
            }
        }else{
            self.txtFldWriteHereInterested.alpha = 0
            self.txtFldWriteHereInterested.resignFirstResponder()
            self.view.endEditing(true)
            scrollViewInterested_h.constant = ypos//Here 50 is the height of the txtview, and 10 is the bottom space of
            // the scrollview
        }
    }
    
    
    @objc func removeTagPreferences(_ sender: AnyObject) {
        removeDD(isKeyboardHide: true)
        arrInterestedTag.remove(at: (sender.tag - 1))
        createTagsForInterested(OnView: scrollViewInterested, withArray: arrInterestedTag, isCancelButtonVisible: true)
    }
}

//My Attributes support methods : for Tag with textfield:
extension NewSearchVC {
    
    func createTagsForLooking(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let height = 30
        let spaceHeight : CGFloat = 20
        let screenWidth = UIScreen.main.bounds.size.width - 60.0
        
        for str in data  {
            
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name: "Poppins-Regular", size: 14.0)!)
            
            // edit :
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to0 righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            
            if checkWholeWidth > screenWidth{
                //we are exceeding size need to change xpos
                xPos = 5.0
                ypos = ypos + spaceHeight + 8.0 + 11 // here 11 is the buttom space
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 38.5 , height: CGFloat(height)))
            //  bgView.layer.cornerRadius = 14.5
            bgView.backgroundColor = .clear//UIColor(red: 33.0/255.0, green: 135.0/255.0, blue:199.0/255.0, alpha: 1.0)
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 14.0, y: -0.5, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "Poppins-Regular", size: 14.0)
            textlable.text = startstring
            
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
            bgView.addSubview(textlable)
            
            //when close buttton enable
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 28.0, height: 28.0)
                button.backgroundColor = UIColor.clear
                
                button.setImage(#imageLiteral(resourceName: "cross_icon"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagMyAttibutes(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldWriteHere.isHidden = false
            btnMe.isHidden = false
            
            if arrLookingTag.count == 12 {
                txtFldWriteHere_top.constant = ypos
                txtFldWriteHere_left.constant = xPos
                btnMe.isHidden = true
                
            }else{
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    //we are exceeding size need to change xpos
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldWriteHere_top.constant = ypos
                    txtFldWriteHere_left.constant = xPos
                }else{
                    txtFldWriteHere_top.constant = ypos
                    txtFldWriteHere_left.constant = xPos
                }
            }
            
            txtFldWriteHere_width.constant = screenWidth - xPos + 50// here 30 is padding....
        }else{
            txtFldWriteHere.isHidden = true
            btnMe.isHidden = true
        }
        
        if arrLookingTag.count <= 12 {
            self.txtFldWriteHere.alpha = 1
            scrollViewLooking_h.constant = ypos + 34 + 10//Here 50 is the height of the txtview, and 10 is the bottom space of
            // the scrollview
            if arrLookingTag.count == 12 {
                self.txtFldWriteHere.alpha = 0
                self.txtFldWriteHere.resignFirstResponder()
                self.view.endEditing(true)
            }
        }else{
            self.txtFldWriteHere.alpha = 0
            self.txtFldWriteHere.resignFirstResponder()
            self.view.endEditing(true)
            scrollViewLooking_h.constant = ypos//Here 50 is the height of the txtview, and 10 is the bottom space of
            // the scrollview
        }
    }
    
    @objc func removeTagMyAttibutes(_ sender: AnyObject) {
        removeDD(isKeyboardHide: true)
        arrLookingTag.remove(at: (sender.tag - 1))
        createTagsForLooking(OnView: scrollViewLooking, withArray: arrLookingTag, isCancelButtonVisible: true)
    }
}
