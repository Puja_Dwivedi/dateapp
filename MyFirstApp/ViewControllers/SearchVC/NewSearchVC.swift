//
//  NewSearchVC.swift
//  MyFirstApp
//
//  Created by cis on 30/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import GooglePlaces 
import SearchTextField

extension NewSearchVC: BackDeleteFromSearchResult {
    func refreshScreen(placeAPIDetailData1: NSDictionary, placeAPIDetailData2: NSDictionary) {
        self.placeAPIDetailData1 = placeAPIDetailData1
        self.placeAPIDetailData2 = placeAPIDetailData2
        currentCountryCode = searchVM.getCountryCodeFrom(dict: placeAPIDetailData2)
        isBackFromSearchResult = true
    }
    
    func refreshScreen(){
        
    }
}

class NewSearchVC: UIViewController {
    
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet var viewStreet: UIView!
    @IBOutlet var viewPlane: UIView!
    @IBOutlet var viewBus: UIView!
    @IBOutlet var viewSubway: UIView!
    @IBOutlet weak var viewTabAirplane1Container: UIView!
    @IBOutlet var viewPlace: UIView!
    @IBOutlet weak var viewTabSubway2Container: UIView!
    @IBOutlet weak var viewTabPlace3Container: UIView!
    @IBOutlet weak var viewTabStreet4Container: UIView!
    @IBOutlet weak var viewTabBus5Container: UIView!
    @IBOutlet weak var viewTabTrain6Container: UIView!
    
    @IBOutlet var btnYou: UIButton!
    @IBOutlet var btnMe: UIButton!
    //-------------------------------------------
    @IBOutlet var viewTimeToggle: UIView!
    @IBOutlet var viewAddressToggle: UIView!
    @IBOutlet weak var lblFlightTitle: UILabel!
    @IBOutlet weak var txtFldFlight: SearchTextField!
    
    @IBOutlet weak var lblAirpot: UILabel!
    @IBOutlet weak var txtFldAirpot: UITextField!
    
    @IBOutlet weak var todaySelectView: UIView!
    @IBOutlet weak var yesterdaySelectView: UIView!
    
    //------------- date 1
    @IBOutlet weak var lblUnselectDay1: GradientLabel!
    @IBOutlet weak var viewSelectDay1: UIView!
    
    // date : 2
    @IBOutlet weak var lblUnselectDay2: GradientLabel!
    @IBOutlet weak var viewSelectDay2: UIView!
    
    // date : 3
    @IBOutlet weak var lblUnselectDay3: GradientLabel!
    @IBOutlet weak var viewSelectDay3: UIView!
    
    // date : 4
    @IBOutlet weak var lblUnselectDay4: GradientLabel!
    @IBOutlet weak var viewSelectDay4: UIView!
    
    //Date : 5
    @IBOutlet weak var lblUnselectDay5: GradientLabel!
    @IBOutlet weak var viewSelectDay5: UIView!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var viewOffTime: UIView!
    @IBOutlet weak var viewTimeLeftCons: NSLayoutConstraint!
    
    @IBOutlet weak var scrollViewLooking: UIScrollView!
    @IBOutlet weak var scrollViewLooking_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldWriteHere: PinTextField!
    @IBOutlet weak var txtFldWriteHere_top: NSLayoutConstraint!
    @IBOutlet weak var txtFldWriteHere_left: NSLayoutConstraint!
    @IBOutlet weak var txtFldWriteHere_width: NSLayoutConstraint!
    
    @IBOutlet weak var scrollViewInterested: UIScrollView!
    
    @IBOutlet weak var scrollViewInterested_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldWriteHereInterested: PinTextField!
    @IBOutlet weak var txtFldWriteHereInterested_top: NSLayoutConstraint!
    @IBOutlet weak var txtFldWriteHereInterested_left: NSLayoutConstraint!
    @IBOutlet weak var txtFldWriteHereInterested_width: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleToday: GradientLabel!
    @IBOutlet weak var lblYesterDay: GradientLabel!
    
    @IBOutlet weak var lblTitleDayOfWeek: UILabel!
    
    @IBOutlet weak var tblViewFlightAutoComplete: UITableView!
    @IBOutlet weak var tblViewFlightAutoComplete_h: NSLayoutConstraint!
    
    //Place address view:
    @IBOutlet weak var lblPlaceAddress: UILabel!
    @IBOutlet weak var viewPlaceAddressToggle: UIView!
    @IBOutlet weak var viewPlaceAddressToggle_left: NSLayoutConstraint!
    @IBOutlet weak var viewPlaceAddressContainer: UIView!
    
    @IBOutlet weak var viewTrainContainer: UIView!
    @IBOutlet weak var txtFldTrainValue: UITextField!
    
    @IBOutlet weak var locationField_topSpace: NSLayoutConstraint!
    @IBOutlet weak var viewContainerLocation: UIView!
    @IBOutlet weak var rootScrollView: UIScrollView!
    
    
    //MARK:-
    //MARK:- Variables :
    
    var activeScreenStatus = CreatePostActiveScreen.airplane // default
    var timeIntervalID = 0
    var isIncludeTimeInPost = false
    var arrLookingTag = [String]()
    var arrInterestedTag = [String]()
    var createPostVM = CreatePostViewModel()
    var createPostPM1 = CreatePostView1ViewModel()
    var arrDate = [(date : String,day : String)]()
    var selectedDate = ""
    var viewHoursVM = ViewHoursViewModel() // for getting array of ids :
    
    var location = Location()
    var searchVM = SearchViewModel()
    var activePlaceTextFld = ActivePlaceTextFldOption.non
    var isBackFromSearchResult = false
    
    var strSubAttribute = ""
    
    var isEmptyBackLooking = true
    var isEmptyBackInterest = true
    var isPlaceAddressActive = false // for place address
    var strDestinationTitle = ""
    var selectedCountry = ""
    var currentStateLocationName = ""
    var stateNameLong = ""
    var currentCity = ""
    
    var todayDate: Date? = nil
    var yesterdayDate : Date? = nil
    var placeID = ""
    let dropDownHeight = UIScreen.main.bounds.height/5.0
    var defaultUserCurrentLocation = ""
    
    // create view popus:
    var objCreateView2 = CreatePostView2()
    var countryDialCode = 0
    var placeAPIDetailData1 = NSDictionary()
    var placeAPIDetailData2 = NSDictionary()
    
    var tabViews: [UIView] = []

    
    
    //MARK:-
    //MARK:- App Flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabViews.append(viewPlane)
        tabViews.append(viewSubway)
        tabViews.append(viewPlace)
        tabViews.append(viewStreet)
        tabViews.append(viewBus)
        rootScrollView.refreshControl = UIRefreshControl()
        rootScrollView.refreshControl?.addTarget(self, action:
                                                    #selector(handleRefreshControl),
                                                 for: .valueChanged)
        
        UIView.performWithoutAnimation {
            self.setInitDefault()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        viewPlane.addShadowToView()
        viewSubway.addShadowToView()
        viewPlace.addShadowToView()
        viewStreet.addShadowToView()
        viewBus.addShadowToView()
        
        if isBackFromSearchResult{
            isBackFromSearchResult = false
        }else{
            deselectViews()
            viewPlane.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            activeScreenStatus = CreatePostActiveScreen.airplane
            location.setupLocationManager()
            location.delegate = self
            placeAPIDetailData1 = NSDictionary()
            placeAPIDetailData2 = NSDictionary()
            initFirstView()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.rootScrollView.scrollToTop()
    }


    
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionCloseDD(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
    }
    
    
    @IBAction func actionTabAirplane(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        deselectViews()
        viewPlane.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
        txtFldFlight.text = ""
        strSubAttribute = ""
        //txtFldFlight.placeholder = "Enter your flight number (E.g., DL1234, AA2345)"
        // lblPlaceAddress.text = ""
        
        viewTabSubway2Container.isHidden = true
        viewTabPlace3Container.isHidden = true
        viewTabStreet4Container.isHidden = true
        viewTabBus5Container.isHidden = true
        viewTabTrain6Container.isHidden = true
        viewPlaceAddressContainer.isHidden = true
        
        
        UIView.animate(withDuration: 0.5) {
            self.viewTabAirplane1Container.isHidden = false
            self.locationField_topSpace.constant = -180
            self.viewTrainContainer.alpha = 0
            self.view.layoutIfNeeded()
            self.viewContainerLocation.alpha = 0
            self.lblAirpot.alpha = 0
        } completion: { (_) in
            
        }
        
        activeScreenStatus = .airplane
        
        intiView()
    }
    
    @IBAction func actionTabSubway(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        deselectViews()
        viewSubway.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
        txtFldFlight.text = ""
        strSubAttribute = ""
        //txtFldFlight.placeholder = "Select a rail line/number"
        txtFldAirpot.text = self.currentCity
        
        viewTabAirplane1Container.isHidden = true
        viewTabPlace3Container.isHidden = true
        viewTabStreet4Container.isHidden = true
        viewTabBus5Container.isHidden = true
        viewTabTrain6Container.isHidden = true
        viewPlaceAddressContainer.isHidden = true
        
        UIView.animate(withDuration: 0.5) {
            self.viewTabSubway2Container.isHidden = false
            self.viewTrainContainer.alpha = 0
            self.locationField_topSpace.constant = -50
            self.viewContainerLocation.alpha = 1
            self.lblAirpot.alpha = 1
            self.view.layoutIfNeeded()
        }
        
        activeScreenStatus = .subway
        intiView()
    }
    
    
    @IBAction func actionTabPlace(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        deselectViews()
        viewPlace.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
        txtFldFlight.text = ""
        strSubAttribute = ""
        //txtFldFlight.placeholder = "Select a specific venue (bar, restaurant, etc.)"
        lblPlaceAddress.text = ""
        
        viewTabAirplane1Container.isHidden = true
        viewTabSubway2Container.isHidden = true
        viewTabStreet4Container.isHidden = true
        viewTabBus5Container.isHidden = true
        viewTabTrain6Container.isHidden = true
        viewPlaceAddressContainer.isHidden = true
        // self.txtFldFlight.inlineMode = true
        
        UIView.animate(withDuration: 0.5) {
            self.viewTabPlace3Container.isHidden = false
            self.viewTrainContainer.alpha = 0
            self.locationField_topSpace.constant = -180
            self.viewContainerLocation.alpha = 0
            self.lblAirpot.alpha = 0
            self.view.layoutIfNeeded()
        } completion: { (_) in
            
        }
        
        activeScreenStatus = .place
        intiView()
    }
    
    @IBAction func actionTabStreet(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        deselectViews()
        viewStreet.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
        txtFldFlight.text = ""
        strSubAttribute = ""
        lblPlaceAddress.text = ""
        //txtFldFlight.placeholder = "Select a street, avenue, boulevard, etc."
        
        viewTabAirplane1Container.isHidden = true
        viewTabSubway2Container.isHidden = true
        viewTabPlace3Container.isHidden = true
        viewTabBus5Container.isHidden = true
        viewTabTrain6Container.isHidden = true
        viewPlaceAddressContainer.isHidden = true
        
        
        UIView.animate(withDuration: 0.5) {
            self.viewTabStreet4Container.isHidden = false
            self.viewTrainContainer.alpha = 0
            self.locationField_topSpace.constant = -180
            self.view.layoutIfNeeded()
            self.viewContainerLocation.alpha = 0
            self.lblAirpot.alpha = 0
        } completion: { (_) in
            
        }
        
        activeScreenStatus = .street
        intiView()
    }
    
    
    @IBAction func actionTabBus(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        deselectViews()
        viewBus.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
        txtFldFlight.text = ""
        strSubAttribute = ""
        txtFldAirpot.text = self.currentCity
        //txtFldFlight.placeholder = "Select a bus line/number"
        
        self.viewTabAirplane1Container.isHidden = true
        viewTabSubway2Container.isHidden = true
        viewTabPlace3Container.isHidden = true
        viewTabStreet4Container.isHidden = true
        viewTabTrain6Container.isHidden = true
        viewPlaceAddressContainer.isHidden = true
        self.viewContainerLocation.alpha = 1
        self.lblAirpot.alpha = 1
        
        UIView.animate(withDuration: 0.5) {
            self.viewTabBus5Container.isHidden = false
            self.viewTrainContainer.alpha = 0
            self.locationField_topSpace.constant = -50
            self.view.layoutIfNeeded()
        }
        
        activeScreenStatus = .bus
        intiView()
    }
    
    
    @IBAction func actionTabTrain(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        txtFldFlight.text = ""
        strSubAttribute = ""
        lblPlaceAddress.text = ""
        
        viewTabAirplane1Container.isHidden = true
        viewTabSubway2Container.isHidden = true
        viewTabPlace3Container.isHidden = true
        viewTabStreet4Container.isHidden = true
        viewTabBus5Container.isHidden = true
        viewTabTrain6Container.isHidden = true
        self.viewContainerLocation.alpha = 1
        self.lblAirpot.alpha = 1
        
        UIView.animate(withDuration: 0.5) {
            self.viewTabTrain6Container.isHidden = false
            self.viewTrainContainer.alpha = 1
            self.locationField_topSpace.constant = 102
            self.view.layoutIfNeeded()
        }
        activeScreenStatus = .train
        intiView()
    }
    
    
    func deselectViews() {
        tabViews.forEach { $0.backgroundColor = .white }
    }
    
    
    //MARK:-
    //MARK:- Methods:
    
    @objc func handleRefreshControl() {
        
        // Dismiss the refresh control.
        DispatchQueue.main.async {
            self.rootScrollView.refreshControl?.endRefreshing()
            UIView.performWithoutAnimation {
                self.activeScreenStatus = CreatePostActiveScreen.airplane // default
                self.deselectViews()
                self.viewPlane.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                self.setInitDefault()
                self.initFirstView()
                self.intiView()
            }
        }
    }

    func initFirstView(){
        
        UIView.performWithoutAnimation {
            self.resetAllFields()
            
            self.setDateAndTime()
        }
        self.intiView()
    }
    
    
    func setDateAndTime(){
        
        let objDate = GetDateClass()
        let currentDate = objDate.getDateTodayAndYesterDay()
        todayDate = currentDate.today
        yesterdayDate = currentDate.yesterDay
        
        arrDate = objDate.getDate()
        lblUnselectDay1.text = arrDate[0].day; lblUnselectDay2.text = arrDate[1].day
        lblUnselectDay3.text = arrDate[2].day; lblUnselectDay4.text = arrDate[3].day
        lblUnselectDay5.text = arrDate[4].day
        
        todaySelectView.isHidden = true; yesterdaySelectView.isHidden = true; viewSelectDay1.isHidden = true
        viewSelectDay2.isHidden = true; viewSelectDay3.isHidden = true; viewSelectDay4.isHidden = true
        viewSelectDay5.isHidden = true
        
        lblTime.text = searchVM.getTimeInterval()
        
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        if countryDialCode == 1 { // for US : 12 hours ti0me foemate
            if  let index =  viewHoursVM.arrIDs.firstIndex(where: {$0.timeOfTwelve ==  lblTime.text ?? ""}){
                timeIntervalID = viewHoursVM.arrIDs[index].id
            }
        }else{ // for non us : 24 hours time interval
            if  let index =  viewHoursVM.arrIDs.firstIndex(where: {$0.timeOfTwentyFour ==  lblTime.text ?? ""}){
                timeIntervalID = viewHoursVM.arrIDs[index].id
            }
        }
    }
    
    
    func resetAllFields(){
        removeDD(isKeyboardHide: true)
        viewPlaceAddressContainer.isHidden = true
        txtFldFlight.text = ""
        strSubAttribute = ""
        txtFldAirpot.text = ""
        stateNameLong = ""
        
        arrLookingTag.removeAll()
        arrInterestedTag.removeAll()
        placeAPIDetailData1 = NSDictionary()
        placeAPIDetailData2 = NSDictionary()
        
        txtFldWriteHere_top.constant = 5
        txtFldWriteHere_left.constant = 0
        
        txtFldWriteHereInterested_top.constant = 5
        txtFldWriteHereInterested_left.constant = 0
        
        createTagsForLooking(OnView: scrollViewLooking, withArray: arrLookingTag, isCancelButtonVisible: true)
        createTagsForLooking(OnView: scrollViewInterested, withArray: arrInterestedTag, isCancelButtonVisible: true)
    }
    
    
    func removeDD(isKeyboardHide : Bool){
        if isKeyboardHide {
            view.endEditing(true)
        }
        createPostPM1.arrAutoComplete.removeAll()
        setHeightOfAutoCompleteTableview()
    }
    
    
    func setInitDefault(){
        
        setupAutoCompleteView()
        locationField_topSpace.constant = -180
        viewTrainContainer.alpha = 0
        self.viewContainerLocation.alpha = 0
        lblAirpot.alpha = 0
        
        
        viewTabAirplane1Container.isHidden = false
        viewTabSubway2Container.isHidden = true
        viewTabPlace3Container.isHidden = true
        viewTabStreet4Container.isHidden = true
        viewTabBus5Container.isHidden = true
        viewTabTrain6Container.isHidden = true
        
        txtFldFlight.text = ""
        strSubAttribute = ""
        
        let widthTodayFrame = (UIScreen.main.bounds.width - 30)/2
        let todayFrame = todaySelectView
        todayFrame?.frame = CGRect(x: todaySelectView.bounds.origin.x, y: todaySelectView.bounds.origin.y, width: widthTodayFrame, height: todaySelectView.bounds.height)
        todayFrame?.gradientTestBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 20.0)
        
        let yesterdayFrame = yesterdaySelectView
        yesterdayFrame?.frame =  CGRect(x: yesterdaySelectView.bounds.origin.x, y: yesterdaySelectView.bounds.origin.y, width: widthTodayFrame, height: yesterdaySelectView.bounds.height)
        yesterdayFrame?.gradientTestBorder(width: 1, colors:[UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 20.0)
        
        //-----------------------------------------------------------------
        let widthDayFrame = (UIScreen.main.bounds.width - 30)/5
        
        let selectDay1 = viewSelectDay1
        selectDay1?.frame =  CGRect(x: viewSelectDay1.bounds.origin.x, y: viewSelectDay1.bounds.origin.y, width: widthDayFrame, height: viewSelectDay1.bounds.height)
        selectDay1?.gradientTestBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.bottomLeft], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 20.0)
        viewSelectDay1.frame.size.width = widthDayFrame
        
        
        let selectDay2 = viewSelectDay2
        selectDay2?.frame =  CGRect(x: viewSelectDay2.bounds.origin.x, y: viewSelectDay2.bounds.origin.y, width: widthDayFrame, height: viewSelectDay2.bounds.height)
        selectDay2?.gradientTestBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 0)
        
        let selectDay3 = viewSelectDay3
        selectDay3?.frame =  CGRect(x: viewSelectDay3.bounds.origin.x, y: viewSelectDay3.bounds.origin.y, width: widthDayFrame, height: viewSelectDay3.bounds.height)
        selectDay3?.gradientTestBorder(width: 1, colors:[UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 0)
        
        let selectDay4 = viewSelectDay4
        selectDay4?.frame =  CGRect(x: viewSelectDay4.bounds.origin.x, y: viewSelectDay4.bounds.origin.y, width: widthDayFrame, height: viewSelectDay4.bounds.height)
        selectDay4?.gradientTestBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 0)
        
        let selectDay5 = viewSelectDay5
        selectDay5?.frame =  CGRect(x: viewSelectDay5.bounds.origin.x, y: viewSelectDay5.bounds.origin.y, width: widthDayFrame, height: viewSelectDay5.bounds.height)
        selectDay5?.gradientTestBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 20.0)
    }
    
    func intiView(){
        viewPlaceAddressContainer.isHidden = true
        strSubAttribute = ""
        lblFlightTitle.text = ""
        txtFldTrainValue.text = ""
        
        switch activeScreenStatus {
        case .airplane:
            lblFlightTitle.text = "Flight #"
            lblAirpot.text = "City"
            strDestinationTitle = "City"
            lblTitleDayOfWeek.text = "Departure date"
            
            break;
            
        case .subway:
            lblFlightTitle.text = "Subway/rail"
            lblAirpot.text = "City"
            strDestinationTitle = "City"
            break;
            
        case .place:
            lblFlightTitle.text = "Place"
            lblAirpot.text = "City"
            strDestinationTitle = "City"
            break;
            
        case .street:
            lblFlightTitle.text = "Street"
            lblAirpot.text = "City"
            strDestinationTitle = "City"
            break;
            
        case .bus:
            lblFlightTitle.text = "Bus"
            lblAirpot.text = "City"
            strDestinationTitle = "City"
            break;
            
        case .train:
            lblFlightTitle.text = "Route"
            lblAirpot.text = "City"
            strDestinationTitle = "City"
            break;
        case .none:
            break;
        }
        defaultSetDate()
    }
    
    func setPlaceAddressToggle(){
        
        if  isPlaceAddressActive { // on to off
            UIView.animate(withDuration: 0.3) {
                self.viewAddressToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
                self.viewPlaceAddressToggle_left.constant = 1
                self.viewPlaceAddressToggle.alpha = 1
                self.view.layoutIfNeeded()
                self.isPlaceAddressActive = false
            }
        }else{ // off to on
            UIView.animate(withDuration: 0.3) {
                self.viewAddressToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                self.viewPlaceAddressToggle_left.constant = 28
                self.viewPlaceAddressToggle.alpha = 0
                self.view.layoutIfNeeded()
                self.isPlaceAddressActive = true
            }
        }
    }
    
    func setInfo(dict : NSDictionary){
        let item = dict.object(forKey: "data") as! NSDictionary
        
        // looking for :
        for index in 1...5 {
            let key = "partner_preference_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                let value = "\(item.object(forKey: key) as! String)"
                arrLookingTag.append(value.lowercased())
            }
        }
        
        // intetested in :
        for index in 1...12 {
            let key = "characteristic_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                let value1 = "\(item.object(forKey: key) as! String)"
                arrInterestedTag.append(value1)
            }
        }
        
        createTagsForLooking(OnView: scrollViewLooking, withArray: arrLookingTag, isCancelButtonVisible: true)
        createTagsForInterested(OnView: scrollViewInterested, withArray: arrInterestedTag, isCancelButtonVisible: true)
    }
    
    
    func defaultSetDate(){ // this is same as today
        
        lblTitleToday.gradientColors = [UIColor.MyTheme.FirstColor.purple.cgColor,UIColor.MyTheme.SecondColor.purple.cgColor]
        lblYesterDay.gradientColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay1.gradientColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay2.gradientColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay3.gradientColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay4.gradientColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay5.gradientColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),UIColor.MyTheme.SecondColor.black.cgColor]
        
        todaySelectView.isHidden = false
        yesterdaySelectView.isHidden = true
        viewSelectDay1.isHidden = true
        viewSelectDay2.isHidden = true
        viewSelectDay3.isHidden = true
        viewSelectDay4.isHidden = true
        viewSelectDay5.isHidden = true
        
        lblTitleToday.font = UIFont(name: "Poppins-SemiBold", size: 18.0)
        lblYesterDay.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay1.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay2.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay3.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay4.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay5.font = UIFont(name: "Poppins-Regular", size: 18.0)
        
        let todayDateValue = "\(todayDate?.string(format: "dd/MM/yyyy") ?? "-------")"
        selectedDate = todayDateValue
        
        if activeScreenStatus == .airplane {
            lblTitleDayOfWeek.text = "Departure date (\(retrieveDayDate(fromCurrentDateCount: 0)))"
        }else{
            lblTitleDayOfWeek.text = "Date (\(retrieveDayDate(fromCurrentDateCount: 0)))"
        }
    }
    
    
    func retrieveDayDate(fromCurrentDateCount : Int)-> String{
        let strTime = (Date() - fromCurrentDateCount).string(format: "dd/MM/yyyy") // today
        
        if countryDialCode == 1 { // for US : 12 hours time foemate
            return  convertDateFormaterForUS(strTime)
        }else{
            return convertDateFormaterForNonUS(strTime)
        }
    }
    
    func convertDateFormaterForUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.string(from: date!)
    }
    
    func convertDateFormaterForNonUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
  
    @IBAction func actionFlight(_ sender: UITextField) {
        
        if activeScreenStatus == CreatePostActiveScreen.place {
            removeDD(isKeyboardHide: false)
            txtFldFlight.resignFirstResponder()
            activePlaceTextFld = .txtFlight
            
            PlaceViewAPI.sharedInstance.Show(activeAction: .place) {  [weak self](city, country, shortCountryName,address,placeID, selectedItemDict)  in
                self?.txtFldFlight.text = city
                
                self?.viewPlaceAddressContainer.isHidden = false //for text:
                self?.isPlaceAddressActive = false
                self?.setPlaceAddressToggle()
                self?.placeID = placeID
                self?.placeAPIDetailData1 = selectedItemDict
                
                // get formated address from 2nd API:
                // call API to get the state name:
                PlaceDetailClass.getDetailByPlaceID(placeID: placeID) { (country, shortName,stateNameLong , formatedAddress,detailData) in
                    self?.strSubAttribute = formatedAddress
                    self?.lblPlaceAddress.text = self?.strSubAttribute
                    self?.placeAPIDetailData2 = detailData
                    currentCountryCode = self?.searchVM.getCountryCodeFrom(dict: detailData) ?? ""
                }
            }
            
        }else if  activeScreenStatus == CreatePostActiveScreen.street  {
            removeDD(isKeyboardHide: false)
            
            PlaceViewAPI.sharedInstance.Show(activeAction: .street) {  [weak self](city, country, shortCountryName,address,placeID, selectedItemDict)  in
                self?.placeID = placeID
                self?.txtFldFlight.text = city
                self?.placeAPIDetailData1 = selectedItemDict
                // get formated address from 2nd API:
                // call API to get the state name:
                PlaceDetailClass.getDetailByPlaceID(placeID: placeID) { (country, shortName, stateNameLong, formatedAddress,detailData) in
                    self?.strSubAttribute = formatedAddress
                    self?.lblPlaceAddress.text = self?.strSubAttribute
                    self?.placeAPIDetailData2 = detailData
                    currentCountryCode = self?.searchVM.getCountryCodeFrom(dict: detailData) ?? ""
                    
                }
            }
        }
        else if activeScreenStatus == CreatePostActiveScreen.airplane {
            removeDD(isKeyboardHide: false)
        }
    }
    
    @IBAction func actionAirpot(_ sender: UITextField) {
        removeDD(isKeyboardHide: false)
        txtFldAirpot.resignFirstResponder()
        
        activePlaceTextFld = .txtLocation
        
        PlaceViewAPI.sharedInstance.Show(activeAction: .city) { [weak self](city, country, shortCountryName,address,placeID, selectedItemDict)  in
            self?.txtFldAirpot.text = city
            
            // call API to get the state name:
            PlaceDetailClass.getDetailByPlaceID(placeID: placeID) { (country, shortName,stateNameLong, formatedAddress,detailData) in
                self?.txtFldFlight.text = ""
                self?.selectedCountry = country
                self?.currentStateLocationName = shortName
                self?.stateNameLong = stateNameLong
                self?.currentCity = city
                currentCountryCode = self?.searchVM.getCountryCodeFrom(dict: detailData) ?? ""
            }
        }
    }
    
    
    @IBAction func actionUserTagHint(_ sender: UIButton) {
        var strLocation = ""
        
        if activeScreenStatus == .bus || activeScreenStatus == .subway {
            if selectedCountry.lowercased() == "United States".lowercased() || selectedCountry.lowercased() == "US".lowercased(){
                SelectedCityFullNameCreatePost = strLocation
                strLocation = currentStateLocationName // use state name insted of country.
            }else{
                strLocation = selectedCountry
            }
        }else{
            if selectedCountry.lowercased() == "United States".lowercased() || selectedCountry.lowercased() == "US".lowercased(){
                SelectedCityFullNameCreatePost = strLocation
                strLocation = currentStateLocationName // use state name insted of country.
            }else{
                strLocation = defaultUserCurrentLocation
            }
        }
        
        self.objCreateView2 =  self.objCreateView2.Show(activeScreen: activeScreenStatus, strHeaderTitle: "Me", isbackButtonActive: true, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self.arrLookingTag), viewController: self, attribute_text: txtFldFlight.text!, attribute_city: strLocation, totalCount: 12) { [weak self] (arrSelectedTags) in
            
            self?.objCreateView2.removeWithAnimation()
            self?.arrLookingTag = arrSelectedTags
            self?.createTagsForLooking(OnView: (self?.scrollViewLooking)!, withArray: self!.arrLookingTag, isCancelButtonVisible: true)
        } as! CreatePostView2
    }
    
    
    @IBAction func actionMatchTagsHint(_ sender: UIButton) {
        var strLocation = ""
        if activeScreenStatus == .bus || activeScreenStatus == .subway {
            if selectedCountry.lowercased() == "United States".lowercased() || selectedCountry.lowercased() == "US".lowercased(){
                SelectedCityFullNameCreatePost = strLocation
                strLocation = currentStateLocationName // use state name insted of country.
            }else{
                strLocation = selectedCountry
            }
        }else{
            if selectedCountry.lowercased() == "United States".lowercased() || selectedCountry.lowercased() == "US".lowercased(){
                SelectedCityFullNameCreatePost = strLocation
                strLocation = currentStateLocationName // use state name insted of country.
            }else{
                strLocation = defaultUserCurrentLocation
            }
        }
        
        self.objCreateView2 =  self.objCreateView2.Show(activeScreen: activeScreenStatus, strHeaderTitle: "You", isbackButtonActive: true, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self.arrInterestedTag), viewController: self, attribute_text: txtFldFlight.text!, attribute_city: strLocation, totalCount: 12) { [weak self] (arrSelectedTags) in
            
            self?.objCreateView2.removeWithAnimation()
            self?.arrInterestedTag = arrSelectedTags
            self?.createTagsForInterested(OnView: (self?.scrollViewInterested)!, withArray: self!.arrInterestedTag, isCancelButtonVisible: true)
        } as! CreatePostView2
    }
    
    @IBAction func actionToday(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        lblTitleToday.gradientColors = [UIColor.MyTheme.FirstColor.purple.cgColor,UIColor.MyTheme.SecondColor.purple.cgColor]
        lblYesterDay.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay1.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay2.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay3.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay4.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay5.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        
        todaySelectView.isHidden = false
        yesterdaySelectView.isHidden = true
        viewSelectDay1.isHidden = true
        viewSelectDay2.isHidden = true
        viewSelectDay3.isHidden = true
        viewSelectDay4.isHidden = true
        viewSelectDay5.isHidden = true
        
        lblTitleToday.font = UIFont(name: "Poppins-SemiBold", size: 18.0)
        lblYesterDay.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay1.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay2.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay3.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay4.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay5.font = UIFont(name: "Poppins-Regular", size: 18.0)
        
        let todayDate = Date().string(format: "dd/MM/yyyy")
        selectedDate = todayDate
        
        if activeScreenStatus == .airplane {
            lblTitleDayOfWeek.text = "Departure date (\(retrieveDayDate(fromCurrentDateCount: 0)))"
        }else{
            lblTitleDayOfWeek.text = "Date (\(retrieveDayDate(fromCurrentDateCount: 0)))"
        }
    }
    
    
    @IBAction func actionYesterday(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        
        lblTitleToday.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblYesterDay.gradientColors = [UIColor.MyTheme.FirstColor.purple.cgColor,UIColor.MyTheme.SecondColor.purple.cgColor]
        lblUnselectDay1.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay2.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay3.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay4.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay5.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        
        todaySelectView.isHidden = true
//        yesterdaySelectView.isHidden = false
        viewSelectDay1.isHidden = true
        viewSelectDay2.isHidden = true
        viewSelectDay3.isHidden = true
        viewSelectDay4.isHidden = true
        viewSelectDay5.isHidden = true
        
        lblTitleToday.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblYesterDay.font = UIFont(name: "Poppins-SemiBold", size: 18.0)
        lblUnselectDay1.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay2.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay3.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay4.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay5.font = UIFont(name: "Poppins-Regular", size: 18.0)
        
        //let yesterDay :Date = Date() - 1// Jan 21, 2019
        selectedDate = "\(yesterdayDate?.string(format: "dd/MM/yyyy") ?? "-----")"
        
        if activeScreenStatus == .airplane {
            lblTitleDayOfWeek.text = "Departure date (\(retrieveDayDate(fromCurrentDateCount: 1)))"
        }else{
            lblTitleDayOfWeek.text = "Date (\(retrieveDayDate(fromCurrentDateCount: 1)))"
        }
    }
    
    @IBAction func actionDay1(_ sender: Any) {
        removeDD(isKeyboardHide: true)
        
        lblTitleToday.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblYesterDay.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay1.gradientColors = [UIColor.MyTheme.FirstColor.purple.cgColor,UIColor.MyTheme.SecondColor.purple.cgColor]
        lblUnselectDay2.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay3.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay4.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay5.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        
        todaySelectView.isHidden = true
        yesterdaySelectView.isHidden = true
        viewSelectDay1.isHidden = false
        viewSelectDay2.isHidden = true
        viewSelectDay3.isHidden = true
        viewSelectDay4.isHidden = true
        viewSelectDay5.isHidden = true
        
        lblTitleToday.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblYesterDay.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay1.font = UIFont(name: "Poppins-SemiBold", size: 18.0)
        lblUnselectDay2.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay3.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay4.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay5.font = UIFont(name: "Poppins-Regular", size: 18.0)
        
        selectedDate =  arrDate[0].date
        
        if activeScreenStatus == .airplane {
            lblTitleDayOfWeek.text = "Departure date (\(retrieveDayDate(fromCurrentDateCount: 2)))"
        }else{
            lblTitleDayOfWeek.text = "Date (\(retrieveDayDate(fromCurrentDateCount: 2)))"
        }
    }
    
    @IBAction func actionDay2(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        
        lblTitleToday.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblYesterDay.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay1.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay2.gradientColors = [UIColor.MyTheme.FirstColor.purple.cgColor,UIColor.MyTheme.SecondColor.purple.cgColor]
        lblUnselectDay3.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay4.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay5.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        
        todaySelectView.isHidden = true
        yesterdaySelectView.isHidden = true
        viewSelectDay1.isHidden = true
        viewSelectDay2.isHidden = false
        viewSelectDay3.isHidden = true
        viewSelectDay4.isHidden = true
        viewSelectDay5.isHidden = true
        
        lblTitleToday.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblYesterDay.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay1.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay2.font = UIFont(name: "Poppins-SemiBold", size: 18.0)
        lblUnselectDay3.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay4.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay5.font = UIFont(name: "Poppins-Regular", size: 18.0)
        
        selectedDate =  arrDate[1].date
        
        if activeScreenStatus == .airplane {
            lblTitleDayOfWeek.text = "Departure date (\(retrieveDayDate(fromCurrentDateCount: 3)))"
        }else{
            lblTitleDayOfWeek.text = "Date (\(retrieveDayDate(fromCurrentDateCount: 3)))"
        }
    }
    
    
    @IBAction func actionDay3(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        
        lblTitleToday.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblYesterDay.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay1.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay2.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay3.gradientColors = [UIColor.MyTheme.FirstColor.purple.cgColor,UIColor.MyTheme.SecondColor.purple.cgColor]
        lblUnselectDay4.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay5.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        
        todaySelectView.isHidden = true
        yesterdaySelectView.isHidden = true
        viewSelectDay1.isHidden = true
        viewSelectDay2.isHidden = true
        viewSelectDay3.isHidden = false
        viewSelectDay4.isHidden = true
        viewSelectDay5.isHidden = true
        
        lblTitleToday.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblYesterDay.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay1.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay2.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay3.font = UIFont(name: "Poppins-SemiBold", size: 18.0)
        lblUnselectDay4.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay5.font = UIFont(name: "Poppins-Regular", size: 18.0)
        
        selectedDate =  arrDate[2].date
        
        if activeScreenStatus == .airplane {
            lblTitleDayOfWeek.text = "Departure date (\(retrieveDayDate(fromCurrentDateCount: 4)))"
        }else{
            lblTitleDayOfWeek.text = "Date (\(retrieveDayDate(fromCurrentDateCount: 4)))"
        }
    }
    
    @IBAction func actionDay4(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        
        lblTitleToday.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblYesterDay.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay1.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay2.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay3.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay4.gradientColors = [UIColor.MyTheme.FirstColor.purple.cgColor,UIColor.MyTheme.SecondColor.purple.cgColor]
        lblUnselectDay5.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        
        todaySelectView.isHidden = true
        yesterdaySelectView.isHidden = true
        viewSelectDay1.isHidden = true
        viewSelectDay2.isHidden = true
        viewSelectDay3.isHidden = true
        viewSelectDay4.isHidden = false
        viewSelectDay5.isHidden = true
        
        lblTitleToday.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblYesterDay.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay1.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay2.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay3.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay4.font = UIFont(name: "Poppins-SemiBold", size: 18.0)
        lblUnselectDay5.font = UIFont(name: "Poppins-Regular", size: 18.0)
        
        selectedDate =  arrDate[3].date
        
        if activeScreenStatus == .airplane {
            lblTitleDayOfWeek.text = "Departure date (\(retrieveDayDate(fromCurrentDateCount: 5)))"
        }else{
            lblTitleDayOfWeek.text = "Date (\(retrieveDayDate(fromCurrentDateCount: 5)))"
        }
    }
    
    @IBAction func actionDay5(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        
        lblTitleToday.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblYesterDay.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay1.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay2.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay3.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay4.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        lblUnselectDay5.gradientColors = [UIColor.MyTheme.FirstColor.purple.cgColor,UIColor.MyTheme.SecondColor.purple.cgColor]
        
        todaySelectView.isHidden = true
        yesterdaySelectView.isHidden = true
        viewSelectDay1.isHidden = true
        viewSelectDay2.isHidden = true
        viewSelectDay3.isHidden = true
        viewSelectDay4.isHidden = true
//        viewSelectDay5.isHidden = false
        
        lblTitleToday.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblYesterDay.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay1.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay2.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay3.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay4.font = UIFont(name: "Poppins-Regular", size: 18.0)
        lblUnselectDay5.font = UIFont(name: "Poppins-SemiBold", size: 18.0)
        selectedDate =  arrDate[4].date
        
        if activeScreenStatus == .airplane {
            lblTitleDayOfWeek.text = "Departure date (\(retrieveDayDate(fromCurrentDateCount: 6)))"
        }else{
            lblTitleDayOfWeek.text = "Date (\(retrieveDayDate(fromCurrentDateCount: 6)))"
        }
    }
    
    //------------------------------------------------
    
    @IBAction func actionTime(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        if countryDialCode == 1{
            CreatePostViewHours.Show(hoursFormate: .twelve, previousSelectedTime: self.lblTime.text! ) { [weak self](hours,id) in
                self?.lblTime.text =  hours
                //print("id : \(id)")
                self?.timeIntervalID = id
            }
        }else{
            CreatePostViewHours.Show(hoursFormate: .twentyFour, previousSelectedTime: self.lblTime.text! ) { [weak self](hours,id) in
                self?.lblTime.text =  hours
                
                self?.timeIntervalID = id
            }
        }
    }
    
    @IBAction func actionPlaceAddressInclude(_ sender: UIButton) {
        removeDD(isKeyboardHide: true)
        setPlaceAddressToggle()
    }
    
    
    @IBAction func actionTimeInclude(_ sender: UIButton) {
        // set notification :
        removeDD(isKeyboardHide: true)
        if  isIncludeTimeInPost {
            UIView.animate(withDuration: 0.3) {
                self.viewTimeToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
                self.viewTimeLeftCons.constant = 1
                self.viewOffTime.alpha = 1
                self.view.layoutIfNeeded()
                self.isIncludeTimeInPost = false
            }
            
        }else{
            UIView.animate(withDuration: 0.3) {
                self.viewTimeToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                self.viewTimeLeftCons.constant = 28
                self.viewOffTime.alpha = 0
                self.view.layoutIfNeeded()
                self.isIncludeTimeInPost = true
            }
        }
    }
    
    @IBAction func actionSearchPost(_ sender: UIButton) {
        checkValidation()
    }
    
    func moveToNext(requestParam : [String : AnyObject]){
        
        let arrResponse =  searchVM.arrPostSearchData
        searchVM.arrPostSearchData.removeAll()
        
        let objSearchPost = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
        objSearchPost.arrSearchResult = arrResponse
        AppDelegate.shared.navController = self.navigationController!
        objSearchPost.backDelegate = self
        objSearchPost.requestParam = requestParam
        objSearchPost.info.activeScreen = activeScreenStatus
        objSearchPost.info.attribute_text = txtFldFlight.text ?? ""
        objSearchPost.info.attribute_city = txtFldAirpot.text ?? ""
        objSearchPost.info.timeIntervalActive = isIncludeTimeInPost
        objSearchPost.info.placeAPIDetailData1 = placeAPIDetailData1
        objSearchPost.info.placeAPIDetailData2 = placeAPIDetailData2
        
        
        if isPlaceAddressActive {
            objSearchPost.info.placeEnable = true
        }else{
            objSearchPost.info.placeEnable = false
        }
        
        if activeScreenStatus == .train {
            objSearchPost.info.trainValue = txtFldTrainValue.text ?? ""
        }else{
            objSearchPost.info.trainValue = ""
        }
        self.navigationController?.pushViewController(objSearchPost, animated: true)
    }
    
    
    func checkValidation(){
        removeDD(isKeyboardHide: true)
        
        let strTickets  = txtFldFlight.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strLocation  = txtFldAirpot.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strTrainValue  = txtFldTrainValue.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        switch activeScreenStatus {
        case .airplane:
            if strTickets == "" {
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.flightTitleAlert, showMsgAt: .bottom); return
            }
            break;
            
        case .subway:
            if strTickets == "" {
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.subwayTitleAlert, showMsgAt: .bottom); return
            }
            
            guard strLocation.count > 0 else { SnackBar.sharedInstance.show(message: "Enter \(strDestinationTitle)", showMsgAt: .bottom); return}
            break;
            
        case .place:
            if strTickets == "" {
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.placeTitleAlert, showMsgAt: .bottom); return
            }
            break;
            
        case .street:
            if strTickets == "" {
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.streetTitleAlert, showMsgAt: .bottom); return
            }
            break;
            
        case .bus:
            if strTickets == "" {
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.busTitleAlert, showMsgAt: .bottom); return
            }
            guard strLocation.count > 0 else { SnackBar.sharedInstance.show(message: "Enter \(strDestinationTitle)", showMsgAt: .bottom); return}
            break;
            
        default:
            break;
        }
        
        let requestParam = searchVM.checkValidation(strTickets: strTickets,
                                                    selectedDate: selectedDate,
                                                    activeScreenStatus: activeScreenStatus,
                                                    placeID: placeID,
                                                    placeAPIDetailData1: placeAPIDetailData1,
                                                    placeAPIDetailData2: placeAPIDetailData2,
                                                    isPlaceAddressActive: isPlaceAddressActive,
                                                    strSubAttribute: strSubAttribute,
                                                    arrLookingTag: arrLookingTag,
                                                    arrInterestedTag: arrInterestedTag,
                                                    isIncludeTimeInPost: isIncludeTimeInPost,
                                                    timeIntervalID: timeIntervalID)
        
        // api call:
        print("requet param : \(requestParam)")
        
        if activeScreenStatus == .airplane {
            call_searchPost(param: requestParam)
            
        }else{
            call_searchPost(param: requestParam)
        }
    }
}


//MARK:-
//MARK:- place api :

extension NewSearchVC : GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if activePlaceTextFld == .subAtrributes {
            
        }else if  activePlaceTextFld == .txtLocation {
            
        }else if activePlaceTextFld == .txtFlight {
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK:- API's
extension NewSearchVC {
    
    func call_searchPost(param : [String : AnyObject])
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        searchVM.callSearchPost_Api(requestParam : param) { (status, message) in
            
            Loader.sharedInstance.stopLoader()
            
            self.moveToNext(requestParam: param)
            
        }
    }
    
    
    func call_checkFlightDataExist(attribute : String, date : String, city : String, requestParam : [String: AnyObject])
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        searchVM.callCheckFlightDataExist(attribute: attribute, date: date, city: city) { (status, message) in
            
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                var tempRequestParam = requestParam
                let operatorValue = "\(self.searchVM.operatorFlight)"
                tempRequestParam["attribute"] = operatorValue as AnyObject
                self.call_searchPost(param: tempRequestParam )
                
            }else if status == "error"{
                self.call_searchPost(param: requestParam )
            }
        }
    }
}


/*
 MARK:- Country code delegate :
 */

extension NewSearchVC : LocationDelegate {
    
    func didReceivedCurrentLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        location.getCitName(lat: latitude, long: longitude) { (cityName,countryName,stateName,countryCode)   in
            self.selectedCountry = countryName
            self.currentStateLocationName = stateName
            self.currentCity = cityName
            self.txtFldAirpot.text = cityName
            currentCountryCode = countryCode
            self.defaultUserCurrentLocation = countryName
        }
    }
    
    func didReceivedtLocationError(error: String) {
        print(error)
    }
}


//MARK:-
//MARK:- Auto complete :

extension NewSearchVC : UITableViewDelegate, UITableViewDataSource{
    
    func setHeightOfAutoCompleteTableview(){
        
        tblViewFlightAutoComplete.reloadData()
        tblViewFlightAutoComplete.updateConstraints()
        tblViewFlightAutoComplete.layoutIfNeeded()
        
        if self.tblViewFlightAutoComplete.contentSize.height > dropDownHeight {
            UIView.animate(withDuration: 0.5) {
                self.tblViewFlightAutoComplete_h.constant = self.dropDownHeight
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.5) {
                self.tblViewFlightAutoComplete_h.constant = self.tblViewFlightAutoComplete.contentSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func dismissMyKeyboard(){
        //endEditing causes the view (or one of its embedded text fields) to resign the first responder status.
        //In short- Dismiss the active keyboard.
        view.endEditing(true)
        createPostPM1.arrAutoComplete.removeAll()
        setHeightOfAutoCompleteTableview()
    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField) {
        if activeScreenStatus == .subway || activeScreenStatus == .bus{
            if textfield.text?.count ?? 0 > 0 {
                call_dropDownList(serchText: textfield.text ?? "", eventType: "\(activeScreenStatus.rawValue + 1)")
            }else{
                self.tblViewFlightAutoComplete_h.constant = 0
                createPostPM1.arrAutoComplete.removeAll()
                setHeightOfAutoCompleteTableview()
            }
        }
    }
    
    
    func setupAutoCompleteView(){
        
        // registe cell :
        let nib = UINib.init(nibName: "autoCompleteCell", bundle: nil)
        tblViewFlightAutoComplete.register(nib, forCellReuseIdentifier: "autoCompleteCell")
        
        tblViewFlightAutoComplete.estimatedRowHeight = 50
        tblViewFlightAutoComplete.rowHeight = UITableView.automaticDimension
        
        tblViewFlightAutoComplete_h.constant = 0
        self.txtFldFlight.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
    }
    
    // delegates :--------------------------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return createPostPM1.arrAutoComplete.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "autoCompleteCell", for: indexPath) as! autoCompleteCell
        
        cell.lblTitle.text = createPostPM1.arrAutoComplete[indexPath.row].attribute_text
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtFldFlight.text = createPostPM1.arrAutoComplete[indexPath.row].attribute_text
        self.tblViewFlightAutoComplete_h.constant = 0
    }
    
    func call_dropDownList(serchText : String, eventType: String)
    {
        var tempStateLongName = ""
        var tempStateShortName = ""
        if selectedCountry.lowercased() == "United States".lowercased() || selectedCountry.lowercased() == "US".lowercased(){
            tempStateLongName = stateNameLong
            tempStateShortName = currentStateLocationName
        }else{
            tempStateLongName =  selectedCountry
            tempStateShortName = ""
        }
        
        createPostPM1.arrAutoComplete.removeAll()
        createPostPM1.getAtrributeList(event_type: eventType, country: selectedCountry, state_short_name: tempStateShortName , state_long_name: tempStateLongName, search_text: serchText) { [weak self](status, message) in
            
            if status == "success"{
                
                if self?.txtFldFlight.text?.count ?? 0 > 0 {
                    self?.setHeightOfAutoCompleteTableview()
                }
                
            }else{ // getting error
                //SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.somethingWentWrong, showMsgAt: .bottom)
            }
        }
    }
}



