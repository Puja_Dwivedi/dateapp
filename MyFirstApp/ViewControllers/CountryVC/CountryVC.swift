//
//  CountryVC.swift
//  MyFirstApp
//
//  Created by cis on 29/01/21.
//  Copyright © 2021 cis. All rig hts reserved.
//

import UIKit

var selectedCountryName  = ""

class CountryVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet weak var tblview: UITableView!
    
    //MARK:-
    //MARK:- variables
    
    var arrCountry = [(name : String, code : String,nameCode: String,active : Bool)]()
    var arrCountryList = [CountryCodeModelClass]()
    var countryVM =  CountryViewModel()
    
    //MARK:-
    //MARK:- App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        arrCountry.append((name: "US", code: "+1",nameCode: "MDY - 12h", active : false))
        arrCountry.append((name: "Non-US", code: "--",nameCode : "DMY - 24h", active : false))
        
        initView()
    }
    
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-
    //MARK:- Methods:
    
    func initView(){
        tblview.register(UINib(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "CountryCell")
        tblview.rowHeight = UITableView.automaticDimension;
        tblview.estimatedRowHeight = 20.0; // set to whatever your "average" cell height is
        
        arrCountry[0].active = false
        arrCountry[1].active = false
        
        if selectedCountryName.lowercased() == "US".lowercased(){
            arrCountry[0] = (name: "US", code: "+1",nameCode: "MDY - 12h", active : true)
        }else{
            arrCountry[1] = (name: "Non-US", code: "--",nameCode : "DMY - 24h", active : true)
        }
        
        self.tblview.reloadData()
        
    }
}


extension CountryVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCountry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryCell
        cell.lblTitle.text = arrCountry[indexPath.row].nameCode
        
        
        if arrCountry[indexPath.row].active {
            cell.imgSelect.isHidden = false
        }else{
            cell.imgSelect.isHidden = true
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrCountry[indexPath.row].name.lowercased() == "US".lowercased(){
            updateCountry(countryName: arrCountry[indexPath.row].name, date_format: 1, code: arrCountry[indexPath.row].code)
        }else{
            updateCountry(countryName: arrCountry[indexPath.row].name, date_format: 0, code: arrCountry[indexPath.row].code)
        }
    }
}


//MARK:-
//MARK:- API :

extension CountryVC {
    
    func updateCountry(countryName : String , date_format : Int, code : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        countryVM.updateCountryApi(countryName: countryName, date_format: date_format) { (status, message, dictResult) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                selectedCountryName = countryName
                Constants.userDefault.set(date_format, forKey: Variables.date_format)
                self.initView()
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}

