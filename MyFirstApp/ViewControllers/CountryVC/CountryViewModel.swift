//
//  CountryViewModel.swift
//  MyFirstApp
//
//  Created by cis on 18/03/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class CountryViewModel: NSObject {
    
    //notification: 1 or 0   -  {on/off}
    func updateCountryApi(countryName : String, date_format : Int , completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        param.updateValue(countryName as AnyObject, forKey:"country")
        param.updateValue(date_format as AnyObject, forKey:"date_format")
        
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.updateCountry + "/\(userID)"
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("success", message, dict)
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("error", message, NSDictionary())
            }
        }
    }
}
