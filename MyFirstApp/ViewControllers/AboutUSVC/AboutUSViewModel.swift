//
//  AboutUSViewModel.swift
//  MyFirstApp
//
//  Created by cis on 26/07/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class AboutUSViewModel: NSObject {
    
    /*
     For FAQ, type_id = 1 and subtype_id = 1
     For Terms, type_id = 1 and subtype_id = 2
     For Privacy policy, type_id = 1 and subtype_id = 3
     For Cookie policy, type_id = 1 and subtype_id = 4
     */
    
    func getInfoApi(type_id : String,subtype_id : String, completion: @escaping (_ result: String, _ message : String,_ outputURL : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        param.updateValue(type_id as AnyObject, forKey:"type_id")
        param.updateValue(subtype_id as AnyObject, forKey:"subtype_id")
        
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.getAboutURL
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                //  print(data.response)
                //  print(data.message)
                
                let dict = data.response
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                let arrData = dict.object(forKey: "data") as! [Any]
                let dictData = arrData[0] as! NSDictionary
                let outputURL = "\(dictData.object(forKey: "output") ?? "")"
                
                completion("success", message, outputURL)
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("error", message, "")
            }
        }
    }
}
