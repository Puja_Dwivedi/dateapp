//
//  AboutUSVC.swift
//  MyFirstApp
//
//  Created by cis on 29/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import WebKit




class AboutUSVC: UIViewController {
    
    enum ActiveScreen {
        case FAQ
        case TermsAndConditions
        case PrivacyPolicy
        case CookiePolicy
        case BlockFAQ
        case DeleteFAQ
    }
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    //MARK:-
    //MARK:- IBOutlet
    var activeScreen = ActiveScreen.FAQ
    var aboutUSVM = AboutUSViewModel()
    var strWebUrl = ""
    
    
    //MARK:-
    //MARK:- App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        switch activeScreen {
            
        case .FAQ: // FAQ
            lblHeaderTitle.text = "FAQ"
            getURL_API(subtype_id: "1")
            break;
            
        case .TermsAndConditions: // Terms of service
            lblHeaderTitle.text = "Terms of service"
            getURL_API(subtype_id: "2")
            break;
            
        case .PrivacyPolicy: // privacy policy
            lblHeaderTitle.text = "Privacy policy"
            getURL_API(subtype_id: "3")
            break;
            
        case .CookiePolicy: // cookie policy
            lblHeaderTitle.text = "Cookie policy"
            getURL_API(subtype_id: "4")
            break;
            
        case .BlockFAQ: // cookie policy
            Loader.sharedInstance.showLoader(msg: "Loading...")
            lblHeaderTitle.text = "FAQ"
            if strWebUrl != "" {
                loadUrl(strUrl: strWebUrl)
            }
            break;
            
        case .DeleteFAQ: // cookie policy
            Loader.sharedInstance.showLoader(msg: "Loading...")
            lblHeaderTitle.text = "FAQ"
            if strWebUrl != "" {
                loadUrl(strUrl: strWebUrl)
            }
            break;
            
        }
    }
    
    //MARK:-
    //MARK:- Action methods
    
    @IBAction func actionBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:-
    //MARK:- Methods:
    
    func loadUrl(strUrl : String)  {
         
        let myURL = URL(string:strUrl)
        let myRequest = URLRequest(url: myURL!)
        self.webView.load(myRequest)
    }
}

//MARK:-
//MARK:- API:

extension AboutUSVC {
    
    func getURL_API(subtype_id : String )
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        aboutUSVM.getInfoApi(type_id: "1", subtype_id: subtype_id) {[weak self] (status, message, url) in
            
            if status == "success"
            {
                print("Url: \(url)")
                self?.loadUrl(strUrl: url)
            }else{
                Loader.sharedInstance.stopLoader()
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}


extension AboutUSVC : WKNavigationDelegate{
    
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        debugPrint("didCommit")
        Loader.sharedInstance.stopLoader()
    }

     func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("didFinish")
         Loader.sharedInstance.stopLoader()
    }

     func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        debugPrint("didFail")
         Loader.sharedInstance.stopLoader()
    }
}


