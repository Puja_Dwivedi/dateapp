//
//  NewPostCell.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class NewPostCell: UITableViewCell {

    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var lblPostStatus: UILabel!
    @IBOutlet weak var viewRootContainer: UIViewClass!

    @IBOutlet weak var viewcontainer_left: NSLayoutConstraint!
    @IBOutlet weak var viewContainer_right: NSLayoutConstraint!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblCity: UILabel!
    
    
    @IBOutlet weak var lblActiveCount: UILabel!
    @IBOutlet weak var btnActive: UIButton!
    
    @IBOutlet weak var lblPublicCount: UILabel!
    @IBOutlet weak var btnPublic: UIButton!
    
    @IBOutlet weak var lblMatchCount: UILabel!
    @IBOutlet weak var btnMatch: UIButton!
    
    @IBOutlet weak var lblDeleteTitle: UILabel!
    
    // titles:
    @IBOutlet weak var lblTotalTitle: GradientLabel!
    @IBOutlet weak var lblPublicTitle: GradientLabel!
    @IBOutlet weak var lblMatchTitle: GradientLabel!
    
    
    @IBOutlet weak var viewDeleteContainer: UIStackView!
    @IBOutlet weak var viewInfoContainer: UIView!
    @IBOutlet weak var btnInfo1: UIButton!
    @IBOutlet weak var btnInfo2: UIButton!
    @IBOutlet weak var btnInfo3: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        lblTotalTitle.text = "Total\nPost(s)"
//        lblPublicTitle.text = "Public\nPost(s)"
//        lblMatchTitle.text = "Match(es)"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
