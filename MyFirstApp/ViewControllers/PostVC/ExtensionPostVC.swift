//
//  ExtensionPostVC.swift
//  MyFirstApp
//
//  Created by cis on 31/10/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

/*================================================================
 MARK:- API
 ================================================================*/

extension PostVC {
    
    func getPostData(sort_type : Int)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        postVM.getPostData(sort_type: sort_type) { (status, message) in
            Loader.sharedInstance.stopLoader()
            self.refreshControl.endRefreshing()
            if status == "success"
            {
                if self.postVM.arrPostData.count > 0 {
                    self.viewNoPost.isHidden = true
                    self.tblViewPost.isHidden = false
                    self.tblViewPost.reloadData()
                    
                }else{
                    self.viewNoPost.isHidden = false
                    self.tblViewPost.isHidden = true
                }
            }
            else
            {
                if self.postVM.arrPostData.count > 0 {
                    self.viewNoPost.isHidden = true
                    self.tblViewPost.isHidden = false
                    self.tblViewPost.reloadData()
                    
                }else{
                    self.viewNoPost.isHidden = false
                    self.tblViewPost.isHidden = true
                }
                
                if message == "No Data Available." || message == "No more data."
                {
                    // SnackBar.sharedInstance.show(message: message)
                }
                else
                {
                    //  SnackBar.sharedInstance.show(message: message)
                }
            }
        }
    }
    
    func removePostData( post_id : String, index : Int)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        postVM.removePostData(post_id: post_id) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.postVM.arrPostData.remove(at: index)
                self.slideActiveRow = nil
                self.tblViewPost.reloadData()
            }
            else
            {
            }
        }
    }
    
    
    /*
     MARK:- check user will able to post or not
     */
    func checkCreatePost()
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        let todaysDate = Date.getCurrentDate(date: Date())
        postVM.checkCreatePost(today_local_date: todaysDate) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                //Check user will able to create a post or showing message
                // check and redirect to the create process:
                
                if self.postVM.isUserWillAbleToPost {
                    self.perform(#selector(self.initCreatePost), with: nil, afterDelay: 0)
                }else{
                    AlertTheme.sharedInstance.Show(popupCategory : .normal,message: AlertMessages.shareInstance.youAreAlreadyPostToday, attributeRangeString: "", isCancelButtonVisible: true, arrBtn: ["Ok"], isBottomTxt: "") { (_) in }
                }
            }
            else
            {
                
                if message == "No Data Available." || message == "No more data."
                {
                    SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.youAreAlreadyPostToday, showMsgAt: .bottom)
                    // SnackBar.sharedInstance.show(message: message)
                }
                else
                {
                }
            }
        }
    }
}


//================================================================
//MARK:- Back handle
//================================================================

extension PostVC: BackCreatePostDelegate {
    
    func loadCreatePost1Info(activeScreen: CreatePostActiveScreen, flight: String, location: String, subAtrribute: String, timeStatus: String, arrPrefrenceTags: [String], placeAPIData1: NSDictionary, placeAPIData2: NSDictionary, isCountryFound: Bool,country : String, state: String) {
        // set updated tags:
        objCreateView2.updateInfo(isTabBarAvailable: true, isAvailable: (isTxtPreviousTag: true, arrTxtTags: arrPrefrenceTags), attribute_text: flight, attribute_city: location)
        
        // set info in create post view 1
        objCreateView1.updateInfo(activeScreen: activeScreen, strFlight: flight, strSubAttribute: subAtrribute, strLocation: location, timeStatus: timeStatus, placeAPIData1: placeAPIData1, placeAPIData2: placeAPIData2, isCountryFound: isCountryFound,country: country,state: state)
    }
    
    func backFromCreatePost() {
        objCreateView1.removeFromSuperview()
        objCreateView2.removeFromSuperview()
    }
}


  //================================================================
 //MARK:- click to search API's
//================================================================

extension PostVC {
    
    func call_searchPost(param : [String : AnyObject])
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        searchVM.callSearchPost_Api(requestParam : param) { (status, message) in
            
            Loader.sharedInstance.stopLoader()
            self.moveToNext(requestParam: param)
        }
    }
    
    
    func moveToNext(requestParam : [String : AnyObject]){
        
        let arrResponse =  searchVM.arrPostSearchData
        searchVM.arrPostSearchData.removeAll()
        
        let objSearchPost = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
        objSearchPost.arrSearchResult = arrResponse
        AppDelegate.shared.navController = self.navigationController!
        objSearchPost.requestParam = requestParam
        objSearchPost.info.attribute_text = requestParam["attribute"] as! String
        objSearchPost.info.attribute_city = requestParam["city"] as! String
        
        objSearchPost.info.placeEnable = false
        objSearchPost.info.trainValue = ""
        
        switch postVM.getEventSubtype(requestParam: requestParam) {
        case 1: objSearchPost.info.activeScreen = .airplane; break
        case 2: objSearchPost.info.activeScreen = .subway; break
        case 3: objSearchPost.info.activeScreen = .place; objSearchPost.info.placeEnable = false; break
        case 4: objSearchPost.info.activeScreen = .street; objSearchPost.info.placeEnable = false; break
        case 5: objSearchPost.info.activeScreen = .bus; break
        case 6: objSearchPost.info.activeScreen = .train; objSearchPost.info.trainValue = requestParam["city"] as! String; break
            
        default: break
        }
        self.navigationController?.pushViewController(objSearchPost, animated: true)
    }
    
    
    
    func makeSearchRequest(index : Int){
        let dict = postVM.arrPostData[index] as! NSDictionary
        call_searchPost(param: postVM.makeSearchRequest(dict: dict))
    }
}
