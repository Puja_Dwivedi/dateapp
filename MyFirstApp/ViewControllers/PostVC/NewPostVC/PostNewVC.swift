//
//  PostNewVC.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import GooglePlaces

//protocol BackCreatePostDelegate {
//    func backFromCreatePost()
//    func loadCreatePost1Info(activeScreen : CreatePostActiveScreen ,flight : String, location : String, subAtrribute : String, timeStatus : String, arrPrefrenceTags : [String],placeAPIData1 : NSDictionary, placeAPIData2: NSDictionary, isCountryFound :Bool,country : String, state: String)
//}

class PostNewVC: UIViewController, PostTypeSelectionDelegate {
    
    //MARK:-
    //MARK:- IBOutlet:
    
    @IBOutlet weak var viewNoPost: UIView!
    @IBOutlet weak var imgNewPost: UIImageView!
    @IBOutlet weak var tblViewPost: UITableView!
    @IBOutlet weak var viewNewPostCrate: UIView!
    @IBOutlet weak var viewSort: UIView!
    @IBOutlet weak var btnNext: UIButton!
    
    //-------------------------
    @IBOutlet weak var btnInfoPage1: UIButton!
    @IBOutlet weak var btnInfoPage2: UIButton!
    @IBOutlet weak var btnInfoPage3: UIButton!
    
    //MARK:-
    //MARK:- IBOutlet:
    let postVM = PostViewModel()
    let splashVM = SplashViewModel()
    var selectedValue = "Newest to Oldest"
    var slideActiveRow   : Int? = nil
    var countryDialCode = 0//Constants.userDefault.object(forKey: Variables.date_format) as! Int
    var dateFormateStatus : Int? = nil // 1 for us , 0 for non us,
    /* (1) US: MM/DD/YYYY
     (2) Non US : DD/MM/YYY
     */
    
    // create view popus:
    var objCreateView1 = CreatePostView1()
    var objCreateView2 = CreatePostStep2View()
    var searchVM = SearchViewModel()
    var refreshControl: UIRefreshControl!
    
    var info1 = (title: "" , url : "")
    var info2 = (title: "" , url : "")
    var info3 = (title: "" , url : "")
    
    
    //MARK:-
    //MARK:- App flow:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblViewPost.addSubview(refreshControl)
        
//        NotificationCenter.default.addObserver(self,selector:#selector(createPost(_:)),name: NSNotification.Name ("moveToStep3"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(moveToStep2(_:)),name: NSNotification.Name ("moveToStep2"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(moveToStep1(_:)),name: NSNotification.Name ("moveToStep1"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(moveToSelectType(_:)),name: NSNotification.Name ("moveToSelectType"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(moveToRoot(_:)),name: NSNotification.Name ("moveToRoot"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(moveToSelectType(_:)),name: NSNotification.Name ("moveToSelectPostType"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(moveToStep2A(_:)),name: NSNotification.Name ("moveToStep2A"),object: nil)

    }
    
    @objc func moveToRoot(_ notification: Notification){
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
        self.objCreateView1.removeFromSuperview()
    }
    
    @objc func moveToStep2A(_ notification: Notification){
        moveToView2()
    }
    
    @objc func createPost(_ notification: Notification){
        let objStep1 = self.objCreateView1
        self.objCreateView1.removeFromSuperview()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostStep3VC") as! CreatePostStep3VC
        vc.objCreateView1 = objStep1
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func moveToStep2(_ notification: Notification){
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func moveToSelectType(_ notification: Notification){
        NotificationCenter.default.post(name: NSNotification.Name("hideTabs"),object: nil)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectPostTypeVC") as! SelectPostTypeVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func moveToStep1(_ notification: Notification){
        self.objCreateView2.removeFromSuperview()
//        if let val = UserDefaults.standard.value(forKey: "MoveFromStep3") as? Bool {
//            if val == true {
//                moveToView2()
//            }
//        }
    }
    
    func moveToView2() {
        objCreateView1 =  objCreateView1.Show(viewController: self, actionScreenUpdate: .new, activeScreen: .airplane, strFlight: "", strSubAttribute: "", strLocation: "", timeStatus: "today",placeAPIData1 : NSDictionary() ,placeAPIData2: NSDictionary(), isCountryFound : Bool(), country: "",state: "") { [weak self](activeScreen, flight, location,subAttribute,timeStatus,placeAPIData1, placeAPIData2, isCountryFound,country,state)   in
            
            self?.moveToStep2AVC(activeScreen: activeScreen)

            
//            self?.objCreateView2 =   self?.objCreateView2.Show(activeScreen: activeScreen, strHeaderTitle: "Describe my crush", isbackButtonActive: false, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: false, arrTxtTags: [String()]), viewController: self!, attribute_text: flight, attribute_city: location, totalCount: 12) { (arrSelectedTags) in
//
//                self?.moveToCreatePostScreen(activeScreen: activeScreen , flight: flight, location: location, subAttribute: subAttribute, timeStatus: timeStatus, arrSelectedPreferenceTag: arrSelectedTags,placeAPIData1 : placeAPIData1, placeAPIData2 : placeAPIData2, isCountryFound: isCountryFound,country: country,state: state)
//            } as! CreatePostStep2View
        } as! CreatePostView1
        
        return;
    }
    
    @objc func refresh(_ sender: Any) {
        //"Newest to Oldest", "Oldest to Newest", "Matches"
        // sort type :
        
        var sortTypeValue = 0
        
        if selectedValue == "Newest to Oldest" {
            sortTypeValue = 1
        }else if selectedValue == "Oldest to Newest" {
            sortTypeValue = 2
        }else if selectedValue == "Matches" {
            sortTypeValue = 3
        }
        
        getPostData(sort_type: sortTypeValue)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        if UserDefaults.standard.bool(forKey: "isBackTapped") == true {
            NotificationCenter.default.post(name: NSNotification.Name("hideTabs"),object: nil)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
        }
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        postVM.arrPostData.removeAll()
        
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        initView()
        
        // update local active time on server:
        if Singleton.shared.isLoginActive == false {
            
            splashVM.setUserLocalTime {(status, message)  in
                if status == "success" {
                    Singleton.shared.isLoginActive = true
                }
            }
        }
    }
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionNewPostCreate(_ sender: UIButton) {
        removeUserDefaults()
        AppDelegate.shared.selectedPostId = 0
        AppDelegate.shared.isBackPressedFromTag = false
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectPostTypeVC") as! SelectPostTypeVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func postTypeSelected(_ id: Int) {
        UserDefaults.standard.removeObject(forKey: "MoveFromStep3")
        UserDefaults.standard.set(true, forKey: "isBackTapped")
        self.initCreatePost()
    }
    
    @IBAction func actionSortBy(_ sender: UIButton) {
        
        let frame =  viewSort.frame.origin
        let window = UIApplication.shared.windows[0]
        let topPadding = window.safeAreaInsets.top
        
        ssSortView.sharedInstance.Show(x: frame.x, y: frame.y - topPadding, selectedValue: selectedValue) { [weak self](value) in
            self?.selectedValue = value
            self?.initView()
        }
    }
    
    
    @IBAction func actionInfoPage(_ sender: UIButton) {
        
        switch sender.tag {
        case 1: // How to Post
            callURL(selectInfoPage: info1.url)
            break;
            
        case 2: // Understanding the post list
            callURL(selectInfoPage: info2.url)
            break;
            
        case 3: // Editing the post
            callURL(selectInfoPage: info3.url)
            break;
            
        default:
            break;
        }
    }
    
    
    func callURL(selectInfoPage : String){
        if let url = URL(string: selectInfoPage), UIApplication.shared.canOpenURL(url) {
           if #available(iOS 10.0, *) {
              UIApplication.shared.open(url, options: [:], completionHandler: nil)
           } else {
              UIApplication.shared.openURL(url)
           }
        }
    }
    
    //MARK:-
    //MARK:- Methods:
    
     func setUnderlineAttributeString(title : String) -> NSAttributedString? {
        let attributes : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0),
            NSAttributedString.Key.foregroundColor : UIColor.gray,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
        ]
        let attributedString = NSAttributedString(string: title, attributes: attributes)
        return attributedString
      }

    
    func initView(){
        getInfoList()
        slideActiveRow = nil
        dateFormateStatus =  Constants.userDefault.object(forKey: Variables.date_format) as? Int
        
        tblViewPost.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right:0)
        
        viewNoPost.isHidden = true
        tblViewPost.isHidden = true
        
        let nib = UINib.init(nibName: "NewPostCell1", bundle: nil)
        tblViewPost.register(nib, forCellReuseIdentifier: "NewPostCell1")
        
        tblViewPost.rowHeight = UITableView.automaticDimension
        tblViewPost.estimatedRowHeight = 100

        
        // after response of API :
        viewNoPost.isHidden = true
        tblViewPost.isHidden = false
        
        //"Newest to Oldest", "Oldest to Newest", "Matches"
        // sort type :
        var sortTypeValue = 0
        if selectedValue == "Newest to Oldest" {
            sortTypeValue = 1
        }else if selectedValue == "Oldest to Newest" {
            sortTypeValue = 2
        }else if selectedValue == "Matches" {
            sortTypeValue = 3
        }
        getPostData(sort_type: sortTypeValue)
    }
    
    
    func getInfoList(){
    
//        for item in Singleton.shared.InfoPage{
//            if let dict = item as? NSDictionary {
//
//                if (dict.object(forKey: "type_id") as? Int) != nil  {
//                    if let subtype_id = dict.object(forKey: "subtype_id") as? Int{
//                        switch subtype_id {
//                        case 5:
//                            if let output = dict.object(forKey: "output") as? String {
//                                if output.count > 0 {
////                                    btnInfoPage1.isHidden = false
//                                    info1.title = "\(dict.object(forKey: "subtype_description") ?? "--")".replacingOccurrences(of: "\n", with: "")
//                                    info1.url = output
//                                    btnInfoPage1.setAttributedTitle(setUnderlineAttributeString(title:  info1.title), for: .normal)
//
//                                  //  print("lblInfoPage1.text : \(lblInfoPage1.text)")
//                                }else{
//                                    btnInfoPage1.isHidden = true
//                                }
//                            }else{
//                                btnInfoPage1.isHidden = true
//                            }
//
//                            break
//
//                        case 6:
//
//                            if let output = dict.object(forKey: "output") as? String {
//                                if output.count > 0 {
////                                    btnInfoPage2.isHidden = false
//                                    info2.title = "\(dict.object(forKey: "subtype_description") ?? "--")".replacingOccurrences(of: "\n", with: "")
//                                    info2.url = output
//                                    btnInfoPage2.setAttributedTitle(setUnderlineAttributeString(title:  info2.title), for: .normal)
//
//                                 //   print("lblInfoPage2.text : \(lblInfoPage2.text)")
//                                }else{
//                                    btnInfoPage2.isHidden = true
//                                }
//                            }else{
//                                btnInfoPage2.isHidden = true
//                            }
//
//                            break
//
//                        case 7:
//
//                            if let output = dict.object(forKey: "output") as? String {
//                                if output.count > 0 {
////                                    btnInfoPage3.isHidden = false
//                                    info3.title = "\(dict.object(forKey: "subtype_description") ?? "--")".replacingOccurrences(of: "\n", with: "")
//                                    info3.url = output
//                                    btnInfoPage3.setAttributedTitle(setUnderlineAttributeString(title:  info3.title), for: .normal)
//                                  //  print("lblInfoPage3.text : \(lblInfoPage3.text)")
//                                }else{
//                                    btnInfoPage3.isHidden = true
//                                }
//                            }else{
//                                btnInfoPage3.isHidden = true
//                            }
//
//                            break
//
//                        default:
//                            break
//                        }
//                    }
//                }else{
//                    btnInfoPage1.isHidden = true
//                    btnInfoPage2.isHidden = true
//                    btnInfoPage3.isHidden = true
//                }
//        }
//    }
//        tblViewPost.reloadData()
    }
    
    
    @objc func initCreatePost(){
        NotificationCenter.default.post(name: NSNotification.Name("hideTabs"),object: nil)
        objCreateView1 =  objCreateView1.Show(viewController: self, actionScreenUpdate: .new, activeScreen: .airplane, strFlight: "", strSubAttribute: "", strLocation: "", timeStatus: "today",placeAPIData1 : NSDictionary() ,placeAPIData2: NSDictionary(), isCountryFound : Bool(), country: "",state: "") { [weak self](activeScreen, flight, location,subAttribute,timeStatus,placeAPIData1, placeAPIData2, isCountryFound,country,state)   in
            self?.moveToStep2AVC(activeScreen: activeScreen)
//            print("place 1 : \(placeAPIData1)\n--------------------------------------------------------\n")
//            print("place 2 : \(placeAPIData2)")
//            self?.objCreateView2 =   self?.objCreateView2.Show(activeScreen: activeScreen, strHeaderTitle: "Describe my crush", isbackButtonActive: false, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: false, arrTxtTags: [String()]), viewController: self!, attribute_text: flight, attribute_city: location, totalCount: 12) { (arrSelectedTags) in
//
//                self?.moveToCreatePostScreen(activeScreen: activeScreen , flight: flight, location: location, subAttribute: subAttribute, timeStatus: timeStatus, arrSelectedPreferenceTag: arrSelectedTags,placeAPIData1 : placeAPIData1, placeAPIData2 : placeAPIData2, isCountryFound: isCountryFound,country: country,state: state)
//            } as! CreatePostStep2View
        } as! CreatePostView1
        
        return;
    }
    
    
    func moveToCreatePostScreen(activeScreen : CreatePostActiveScreen, flight : String, location : String ,subAttribute : String,timeStatus : String, arrSelectedPreferenceTag : [String],placeAPIData1 : NSDictionary, placeAPIData2 : NSDictionary, isCountryFound : Bool,country : String, state: String){
        slideActiveRow = nil
        let objCreatePostVC = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
        objCreatePostVC.getCreatePostInfo.activeScreen = activeScreen
        objCreatePostVC.getCreatePostInfo.arrPrefrenceTags = arrSelectedPreferenceTag
        objCreatePostVC.getCreatePostInfo.flight = flight
        objCreatePostVC.getCreatePostInfo.location = location
        objCreatePostVC.getCreatePostInfo.subAtrribute = subAttribute
        objCreatePostVC.getCreatePostInfo.time = timeStatus
        objCreatePostVC.getCreatePostInfo.placeAPIData1 = placeAPIData1
        objCreatePostVC.getCreatePostInfo.placeAPIData2 = placeAPIData2
        objCreatePostVC.getCreatePostInfo.isCountryFound = isCountryFound
        objCreatePostVC.getCreatePostInfo.country = country
        objCreatePostVC.getCreatePostInfo.state = state
        objCreatePostVC.backCreateDelegate = self
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objCreatePostVC, animated: true)
    }
    
    func moveToEditScreen(indexPath : IndexPath){
        slideActiveRow = nil
        guard postVM.arrPostData.count > 0 else { return }
        if let dict = postVM.arrPostData[indexPath.row] as? NSDictionary {
            let objEditPostVC = self.storyboard?.instantiateViewController(withIdentifier: "NewEditPostVC") as! NewEditPostVC
            objEditPostVC.post_id = dict.object(forKey: "post_id") as! Int
            self.navigationController?.pushViewController(objEditPostVC, animated: true)
        }
    }
    
    func moveToStep2AVC(activeScreen: CreatePostActiveScreen){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostStep2AVC") as! CreatePostStep2AVC
        vc.activeScreen = activeScreen
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    func moveToMatchScreen(index : Int){
        slideActiveRow = nil
        guard postVM.arrPostData.count > 0 else { return }
        if let dict = postVM.arrPostData[index] as? NSDictionary {
            let post_id = dict.object(forKey: "post_id") as! Int
            let totalMatch = "\(dict.object(forKey: "match_total") ?? "")"
            guard totalMatch != "0" && totalMatch != "" else { SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.noMatchAvailableForThisPost)", showMsgAt: .bottom) ;return}
            
            let objMatchesVC = self.storyboard?.instantiateViewController(withIdentifier: "NewMatchesVC") as! NewMatchesVC
            objMatchesVC.post_id = post_id
            objMatchesVC.postTitle = getPostTitle(postType: "\(dict.object(forKey: "event_subtype") ?? "")", dict: dict)
            objMatchesVC.postType = "\(dict.object(forKey: "event_subtype") ?? "")"
            AppDelegate.shared.navController = self.navigationController!
            self.navigationController?.pushViewController(objMatchesVC, animated: true)
        }
    }
    
    func getPostTitle(postType: String, dict: NSDictionary) -> String {
        switch  postType {
            
        case "1": //airplane
            return "\(dict.object(forKey: "attribute") ?? "")"
            
        case "2": //subway
            return "\(dict.object(forKey: "attribute") ?? "")"
            
        case "3": //place
            return "\(dict.object(forKey: "main_text") ?? "")"
            
        case "4": //street
            return "\(dict.object(forKey: "main_text") ?? "")"
            
        case "5": //bus
            return "\(dict.object(forKey: "attribute") ?? "")"
            
        case "6": //train
            if "\(dict.object(forKey: "sub_attribute") ?? "")" != "" {
                return "\(dict.object(forKey: "attribute") ?? ""). \(dict.object(forKey: "sub_attribute") ?? "")"
            }else{
                return "\(dict.object(forKey: "attribute") ?? "")"
            }
        default:
            break
        }
        return ""
    }
}


//MARK:-
//MARK:- Tableview deeletes:

extension PostNewVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if postVM.arrPostData.count > 0 {
            return postVM.arrPostData.count + 1
        }
        viewNoPost.isHidden = false
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewPostCell1", for: indexPath) as! NewPostCell1
        
        if postVM.arrPostData.count > indexPath.row {
            print("index: \(indexPath.row)")
            //Hide info page
            cell.viewRootContainer.isHidden = false
            
            if indexPath.row == 0 {
                cell.lblDescription.text = "Hello Puja Dwivedi"
            }
            
            let dict = postVM.arrPostData[indexPath.row] as! NSDictionary
            
            cell.lblMatchTitle.text = "Match : "
            cell.lblDescription.text = "\(dict.object(forKey: "description") ?? "")"
            
            let strDate = "\(dict.object(forKey: "date") ?? "")"
            print("strDate : \(strDate)")
            
            if strDate != "" {
                
                var remainigDays = postVM.calclulateRemainDate(strCreateDate: strDate)
                remainigDays = 7 - remainigDays
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "MMM dd,yyyy"
            }
            
            
            if let publicIndicator = dict.object(forKey: "public_indicator") as? Int {
                if publicIndicator == 1 {
                    cell.imgPostEye.image = UIImage.init(named: "open_eye_post")
                }else{
                    cell.imgPostEye.image = UIImage.init(named: "eye_post")
                }
            }
            
            
            
            if countryDialCode == 1 { // for US : 12 hours time formate
                
                var timeFormate = ""
                var strDate = "\(dict.object(forKey: "post_create_locat_time") ?? "")"
                
                if strDate == "" {
                    
                    strDate = "\(dict.object(forKey: "date") ?? "")"
                    timeFormate = "dd/MM/yyyy"
                }else{
                    timeFormate = "dd/MM/yyyy HH:mm:ss ssZZZ"
                }
                
                strDate = convertDateFormaterForUS(strDate, dateFormate: timeFormate)
                cell.lblDate.text = "\(strDate)"
                
            }else{
                
                var timeFormate = ""
                var strDate = "\(dict.object(forKey: "post_create_locat_time") ?? "")"
                
                if strDate == "" {
                    strDate = "\(dict.object(forKey: "date") ?? "")"
                    timeFormate = "dd/MM/yyyy"
                    
                }else{
                    timeFormate = "dd/MM/yyyy HH:mm:ss ssZZZ"
                }
                strDate = convertDateFormaterForNonUS(strDate, dateFormate: timeFormate)
                cell.lblDate.text = "\(strDate)"
            }
            
            
            //        cell.lblTitle.text = "\(dict.object(forKey: "description") ?? "")"
            
            //            if "\(dict.object(forKey: "flight") ?? "")" != "" {
            //                cell.lblSubTitle.text = "Flight no: \(dict.object(forKey: "attribute") ?? "")"
            //                // cell.lblCity.isHidden = false
            //                // cell.lblCity.text = "\(dict.object(forKey: "city") ?? "")"
            //
            //            }
            //            else if "\(dict.object(forKey: "city") ?? "")" != "" || "\(dict.object(forKey: "bus") ?? "")" != ""{
            //                cell.lblSubTitle.text = "Line no: \(dict.object(forKey: "attribute") ?? "")"
            //                // cell.lblCity.isHidden = false
            //                // cell.lblCity.text = "\(dict.object(forKey: "city") ?? "")"
            //
            //            } else if "\(dict.object(forKey: "location") ?? "")" != "" {
            //                cell.lblSubTitle.text = "Venue: \(dict.object(forKey: "attribute") ?? "")"
            //                // cell.lblCity.isHidden = false
            //                // cell.lblCity.text = "\(dict.object(forKey: "city") ?? "")"
            //
            //            } else{
            //                //            // cell.lblCity.isHidden = true
            //                cell.lblSubTitle.text = "Train Line: \(dict.object(forKey: "attribute") ?? "")"
            //            }
            
            if slideActiveRow == indexPath.row{
                
                cell.viewcontainer_left.constant = -106
                cell.viewContainer_right.constant = 106
            }else{
                
                cell.viewcontainer_left.constant = 0
                cell.viewContainer_right.constant = 0
            }
            
            
            cell.lblSubTitle.numberOfLines = 0
            
            switch  "\(dict.object(forKey: "event_subtype") ?? "")"{
                
            case "1": //airplane
                cell.imgPost.image = UIImage(named: "flight_post")
                cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? "")"
                break
                
            case "2": //subway
                cell.imgPost.image = UIImage(named: "train_post")
                cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? "")"
                break
                
            case "3": //place
                cell.lblSubTitle.numberOfLines = 0
                //            // cell.lblCity.isHidden = true
                cell.imgPost.image = UIImage(named: "location_post")
                
                cell.lblSubTitle.text = "\(dict.object(forKey: "main_text") ?? ""), \(dict.object(forKey: "formatted_address") ?? "")"
                
                break
                
            case "4": //street
                cell.imgPost.image = UIImage(named: "street_post")
                cell.lblSubTitle.numberOfLines = 0
                // cell.lblCity.isHidden = true
                cell.lblSubTitle.text = "\(dict.object(forKey: "main_text") ?? ""), \(dict.object(forKey: "secondary_text") ?? "")"
                break
                
            case "5": //bus
                cell.imgPost.image = UIImage(named: "bus_post")
                cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? "")"
                break
                
            case "6": //train
                cell.lblSubTitle.numberOfLines = 0
                cell.imgPost.image = UIImage(named: "train_post")
                if "\(dict.object(forKey: "sub_attribute") ?? "")" != "" {
                    cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? ""). \(dict.object(forKey: "sub_attribute") ?? "")"
                }else{
                    cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? "")"
                }
                
                break
                
            default:
                break
            }
            
            // Active count:-
            let activeCount = dict.object(forKey: "number_of_post_active") as! Int
            cell.lblTotalPostsCount.text = getPostActiveCount(count: activeCount)
            
            // Public count:-
            let publicCount = dict.object(forKey: "number_of_post_public") as! Int
            cell.lblPublicPostsCount.text = getPostPublic(count: publicCount)
            
            //Match count:-
            let matchCount = dict.object(forKey: "match_total") as! Int
            cell.lblMatchCount.text = getPostMatch(count: matchCount)
            
            if matchCount == 0 {
                cell.lblMatchCount.font = UIFont(name: "Poppins-Regular", size: 12)
            } else {
                cell.lblMatchCount.font = UIFont(name: "Poppins-SemiBold", size: 14)
            }
            
            cell.btnMatch.tag = indexPath.row
            cell.btnPublic.tag = indexPath.row

            cell.btnMatch.addTarget(self, action: #selector(self.btnMatchAction(sender:)), for: .touchUpInside)
            cell.btnPublic.addTarget(self, action: #selector(self.btnPublicAction(sender:)), for: .touchUpInside)
            cell.contentView.tag = indexPath.row
            cell.contentView.isUserInteractionEnabled = true
            
            cell.contentView.tag = indexPath.row
            cell.contentView.isUserInteractionEnabled = true
            
            cell.btnEditPost.tag = indexPath.row
            cell.btnEditPost.addTarget(self, action: #selector(self.actionEdit(sender: )), for: .touchUpInside)
            
            cell.btnDeletePost.tag = indexPath.row
            cell.btnDeletePost.addTarget(self, action: #selector(self.actionDelete(sender: )), for: .touchUpInside)
            return cell
            //----------------------------------------------------------------------------------------------------
        }else{
            //Hide info page
            cell.viewRootContainer.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        moveToEditScreen(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func actionInfoButton(sender: UIButton){
        switch sender.tag {
        case 1: // How to Post
            callURL(selectInfoPage: info1.url)
            break;
            
        case 2: // Understanding the post list
            callURL(selectInfoPage: info2.url)
            break;
            
        case 3: // Editing the post
            callURL(selectInfoPage: info3.url)
            break;
            
        default:
            break;
        }
    }
    
    @objc func actionEdit(sender: UIButton) {
        self.moveToEditScreen(indexPath: IndexPath(row: sender.tag, section: 0))
    }
    
    func getPostActiveCount(count : Int)-> String{
        if count > 1000 {
            return "1000+"
        }
        else{
            return "\(count)"
        }
    }
    
    
    func getPostPublic(count: Int)-> String{
        if count > 100 {
            return "100+"
        }
        else{
            return "\(count)"
        }
    }
    
    
    func getPostMatch(count : Int)-> String{
        if count > 10 {
            return "10+"
        }
        else{
            return "\(count)"
        }
    }
    
    
    @objc func actionDelete(sender: UIButton) {
        var daysRemaining = ""
        
        
        guard postVM.arrPostData.count > 0 else { return }
        
        if let dict = postVM.arrPostData[sender.tag] as? NSDictionary {
            let days = (dict.object(forKey: "expire_days") ?? "")
            
            if let day = days as? Int64 {
                if day == 1 {
                    daysRemaining = "\(day) day"
                } else {
                    daysRemaining = "\(day) days"
                }
            }
            
            let msg = "Your post is still valid for \(daysRemaining). If you delete it now, all associated matches and chats will be removed"
            //Your post is still valid for 7 days. If you delete it now, all associated matches and chats will be removed
            
            let attr_msg = addAttributeText(attributedText: daysRemaining, message: msg)
            
            AlertTheme.sharedInstance.ShowAttributedAlert(popupCategory : .attribute, attributed_string: attr_msg ,message: msg, attributeRangeString: "\(daysRemaining)", isCancelButtonVisible: false, arrBtn: ["\(AlertMessages.shareInstance.deleteNow)","\(AlertMessages.shareInstance.cancel)"], isBottomTxt: "") { [weak self] (clicked) in
                if clicked == "\(AlertMessages.shareInstance.deleteNow)" {
                    print("delete account")
                    // action logout perform
                    let dict = self?.postVM.arrPostData[sender.tag] as! NSDictionary
                    self?.removePostData(post_id: "\(dict.object(forKey: "post_id") ?? "")", index: sender.tag)
                }
            }
        }
    }
    
    func addAttributeText(attributedText: String, message: String) -> NSAttributedString {
        
        let text = message
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: attributedText)
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Poppins Bold", size: 15)!, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: range1)
        return underlineAttriString
    }
    
    @objc func labelSwipedLeft(sender: UITapGestureRecognizer) {
        
        if slideActiveRow != nil {
            
            // close
            let indexPath = IndexPath(row: slideActiveRow!, section: 0)
            if let cell = tblViewPost.cellForRow(at: indexPath) as? NewPostCell1{
                
                UIView.animate(withDuration: 0, animations: {
                    cell.viewcontainer_left.constant = 0
                    cell.viewContainer_right.constant =  0
                    
                }) { (_) in
                    // new
                    let newIndexPath = IndexPath(row: sender.view!.tag, section: 0)
                    let newCell = self.tblViewPost.cellForRow(at: newIndexPath) as! NewPostCell1
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        newCell.viewcontainer_left.constant = -106
                        newCell.viewContainer_right.constant = 106
                        self.view.layoutIfNeeded()
                    }) { (_) in
                        self.slideActiveRow = sender.view?.tag
                        self.tblViewPost.reloadData()
                    }
                }
            }else{
                
                // new
                let newIndexPath = IndexPath(row: sender.view!.tag, section: 0)
                let newCell = tblViewPost.cellForRow(at: newIndexPath) as! NewPostCell1
                
                UIView.animate(withDuration: 0.5, animations: {
                    newCell.viewcontainer_left.constant = -106
                    newCell.viewContainer_right.constant = 106
                    self.view.layoutIfNeeded()
                    
                }) { (_) in
                    self.slideActiveRow = sender.view?.tag
                    self.tblViewPost.reloadData()
                }
            }
            
        }else{
            let indexPath = IndexPath(row: sender.view!.tag, section: 0)
            let cell = tblViewPost.cellForRow(at: indexPath) as! NewPostCell1
            
            UIView.animate(withDuration: 0.5, animations: {
                cell.viewcontainer_left.constant = -106
                cell.viewContainer_right.constant = 106
                self.view.layoutIfNeeded()
                
            }) { (_) in
                self.slideActiveRow = sender.view?.tag
                self.tblViewPost.reloadData()
            }
        }
    }
    
    
    @objc func labelSwipedRight(sender: UITapGestureRecognizer) {
        
        if slideActiveRow != nil {
            // close
            let indexPath = IndexPath(row: slideActiveRow!, section: 0)
            let cell = tblViewPost.cellForRow(at: indexPath) as? NewPostCell1
            
            UIView.animate(withDuration: 0, animations: {
                cell?.viewcontainer_left.constant = 0
                cell?.viewContainer_right.constant =  0
                //self.view.layoutIfNeeded()
            }) { (_) in
                
                let currentIndexPath = IndexPath(row: sender.view!.tag, section: 0)
                let currentCell = self.tblViewPost.cellForRow(at: currentIndexPath) as! NewPostCell1
                
                UIView.animate(withDuration: 0.5, animations: {
                    currentCell.viewcontainer_left.constant = 0
                    currentCell.viewContainer_right.constant =  0
                    self.view.layoutIfNeeded()
                }) { (_) in
                    self.slideActiveRow = nil
                    self.tblViewPost.reloadData()
                }
            }
        }
    }
    
    func convertDateFormaterForUS(_ date: String, dateFormate : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormate
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    func convertDateFormaterForNonUS(_ date: String, dateFormate : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormate
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    
    //MARK:-
    //----- cell button actions
    @objc func btnActiveActions(sender : UIButton){
    }
    
    @objc func btnPublicAction(sender : UIButton){
        // move to search screen
        guard postVM.arrPostData.count > 0 else { return }

        if let dict = postVM.arrPostData[sender.tag] as? NSDictionary {
            let number_of_post_public =  "\(dict.object(forKey: "number_of_post_public") ?? "0")"
            
            guard number_of_post_public != "" && number_of_post_public != "0" else {
                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.noPublicPostAvailable)", showMsgAt: .bottom); return}
            
            makeSearchRequest(index: sender.tag)
            
        }
    }
    
    @objc func btnMatchAction(sender : UIButton){
        print("show button tag : \(sender.tag)")
        moveToMatchScreen(index: sender.tag)
    }
}
