//
//  NewPostCell.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class NewPostCell1: UITableViewCell {

    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet var viewEditPost: UIView!
    @IBOutlet weak var viewRootContainer: UIViewClass!
    @IBOutlet weak var viewcontainer_left: NSLayoutConstraint!
    @IBOutlet weak var viewContainer_right: NSLayoutConstraint!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet var viewDeletePost: UIView!
    @IBOutlet weak var lblMatchCount: UILabel!
    @IBOutlet weak var btnMatch: UIButton!
    @IBOutlet weak var btnPublic: UIButton!

    // titles:
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblPublicPostsCount: UILabel!
    @IBOutlet var lblTotalPostsCount: UILabel!
    @IBOutlet var lblMatchTitle: UILabel!
    @IBOutlet var imgPostEye: UIImageView!
    @IBOutlet var btnEditPost: UIButton!
    @IBOutlet var viewEye: UIView!
    @IBOutlet var viewMatches: UIView!
    @IBOutlet var btnDeletePost: UIButton!
    
    override func layoutSubviews() {
          super.layoutSubviews()
          //set the values for top,left,bottom,right margins
          let margins = UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 0)
          contentView.frame = contentView.frame.inset(by: margins)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        lblTotalTitle.text = "Total\nPost(s)"
//        lblPublicTitle.text = "Public\nPost(s)"
//        lblMatchTitle.text = "Match(es)"
        viewMatches.addShadowToView()
        viewEye.addShadowToView()
        viewEditPost.addShadowToView()
        viewDeletePost.addShadowToView()
        viewRootContainer.addShadowToView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
