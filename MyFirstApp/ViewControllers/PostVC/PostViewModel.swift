//
//  PostViewModel.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class PostViewModel: NSObject {
    
    var arrPostData = [Any]()
    var isUserWillAbleToPost = false
    
    func getPostData( sort_type : Int ,completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.getPostList
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userid as AnyObject, forKey: "user_id")
        param.updateValue(sort_type as AnyObject, forKey: "sort_type")
        
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    let arrData = RV_CheckDataType.getArray(anyArray: data.response.value(forKey: KEY.ServiceKeys.dataResponse) as AnyObject)
                    if arrData.count > 0
                    {
                        self.arrPostData =  self.retrieveData(data:  arrData as! [Any])
                    }
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else { // error :
                    if data.message == "" {
                        message = AlertMessages.shareInstance.somethingWentWrong
                    }else{
                        message = data.message
                    }
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = AlertMessages.shareInstance.somethingWentWrong
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    func checkCreatePost(today_local_date : String, completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.checkUserAbleCreatePost
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userid as AnyObject, forKey: "user_id")
        param.updateValue(today_local_date as AnyObject, forKey: "today_local_date")
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    self.isUserWillAbleToPost  = dict.object(forKey: "create_post") as! Bool
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else { // error :
                    if data.message == "" {
                        message =  AlertMessages.shareInstance.somethingWentWrong
                    }else{
                        message = data.message
                    }
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message =  AlertMessages.shareInstance.somethingWentWrong
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    func calclulateRemainDate(strCreateDate : String)-> Int{
        
        let createdPostDate = Date.getFormattedDate(strDate: strCreateDate, oldformatter: "dd/MM/yyyy", newformatter: "dd/MM/yyyy")
        
        let currentDate = Date.getCurrentDate(date: Date())
        let currentSystemDate = Date.getFormattedDate(strDate: currentDate, oldformatter: "dd/MM/yyyy", newformatter: "dd/MM/yyyy")
        
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: createdPostDate)
        let date2 = calendar.startOfDay(for: currentSystemDate)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        return components.day ?? 0
    }
    
    
    func retrieveData(data : [Any])->[Any]{
        var arrTemp = [Any]()
        
        for index in 0..<data.count {
            let dict = data[index] as! NSDictionary
            
            let strDate = "\(dict.object(forKey: "date") ?? "")"
            if strDate != "" {
                var remainigDays = calclulateRemainDate(strCreateDate: strDate)
                remainigDays = 7 - remainigDays
                if remainigDays > 0 {
                    arrTemp.append(dict)
                }
            }
        }
        return arrTemp
    }
    
    
    func removePostData( post_id : String ,completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.deletePost
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userid as AnyObject, forKey: "user_id")
        param.updateValue(post_id as AnyObject, forKey: "post_id")
        
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else { // error :
                    if data.message == "" {
                        message = AlertMessages.shareInstance.somethingWentWrong
                    }else{
                        message = data.message
                    }
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = AlertMessages.shareInstance.somethingWentWrong
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    
    // make request
    func makeSearchRequest(dict : NSDictionary)-> [String : AnyObject]{
        let userID = Constants.userDefault.object(forKey: Variables.user_id)
        let attribute = "\(dict.object(forKey: "attribute") ?? "")"
        let date = "\(dict.object(forKey: "date") ?? "")"
        let event_subtype = "\(dict.object(forKey: "event_subtype") ?? "")"
        let city = "\(dict.object(forKey: "city") ?? "")"
        let sub_attribute = "\(dict.object(forKey: "sub_attribute") ?? "")"
        
        let main_text = "\(dict.object(forKey: "main_text") ?? "")"
        let secondary_text = "\(dict.object(forKey: "secondary_text") ?? "")"
        let toggle_on_status = "1"
        let administrative_area_level_2 = "\(dict.object(forKey: "administrative_area_level_2") ?? "")"
        let formatted_address = "\(dict.object(forKey: "formatted_address") ?? "")"
        
        // create request :
        var param = [String : AnyObject]()
        param.updateValue( userID as AnyObject, forKey:"user_id")
        param.updateValue( attribute as AnyObject, forKey:"attribute")
        param.updateValue( date as AnyObject, forKey:"date")
        param.updateValue( city as AnyObject, forKey:"city")
        param.updateValue( Int(event_subtype) as AnyObject ,forKey:"event_subtype")
        param.updateValue( sub_attribute as AnyObject ,forKey:"sub_attribute")
        param.updateValue("" as AnyObject ,forKey: "time_interval")
        param.updateValue( "" as AnyObject ,forKey: "search_from")
        param.updateValue( "" as AnyObject ,forKey: "search_to")
        param.updateValue("" as AnyObject ,forKey: "search_tag")
        
        
        param.updateValue(main_text as AnyObject ,forKey: "main_text")
        param.updateValue(secondary_text as AnyObject ,forKey: "secondary_text")
        param.updateValue( toggle_on_status as AnyObject ,forKey: "toggle_on_status")
        param.updateValue(administrative_area_level_2 as AnyObject ,forKey: "administrative_area_level_2")
        param.updateValue(formatted_address as AnyObject ,forKey: "formatted_address")
        
        return param
    }
    
    func getEventSubtype(requestParam : [String : AnyObject])-> Int{
        if "\(requestParam["event_subtype"] ?? "0" as AnyObject)" == "1" {
            return 1
        }else if "\(requestParam["event_subtype"] ?? "0" as AnyObject)" == "2" {
            return 2
        }else if "\(requestParam["event_subtype"] ?? "0" as AnyObject)" == "3"{
            return 3
        }else if "\(requestParam["event_subtype"] ?? "0" as AnyObject)" == "4"{
            return 4
        }else if "\(requestParam["event_subtype"] ?? "0" as AnyObject)" == "5"{
            return 5
        }else if "\(requestParam["event_subtype"] ?? "0" as AnyObject)" == "6"{
            return 6
        }
        
    return 0}
}

extension Date {
    
    static func getFormattedDate(strDate: String ,oldformatter: String, newformatter:String) -> Date{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = oldformatter
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = newformatter
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        return date!
    }
}
