//
//  OtpVerificationVC.swift
//  MyFirstApp
//
//  Created by cis on 02/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class OtpVerificationVC: UIViewController {
    
    enum ActiveeScreen {
        case normal
        case proceed
        case none
    }
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    @IBOutlet weak var txtFld1: UITextField!
    @IBOutlet weak var txtFld2: UITextField!
    @IBOutlet weak var txtFld3: UITextField!
    @IBOutlet weak var txtFld4: UITextField!
    @IBOutlet weak var txtFld5: UITextField!
    @IBOutlet weak var txtFld6: UITextField!
    
    @IBOutlet weak var lblLine1: UILabel!
    @IBOutlet weak var lblLine2: UILabel!
    @IBOutlet weak var lblLine3: UILabel!
    @IBOutlet weak var lblLine4: UILabel!
    @IBOutlet weak var lblLine5: UILabel!
    @IBOutlet weak var lblLine6: UILabel!
    
    @IBOutlet weak var btnNext_bottom: NSLayoutConstraint!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgNext: UIImageView!
    
    //MARK:-
    //MARK:- Variables:
    var otpInfo = (countryCode: "", mobileNumber : "",verificationID : "", countryName  : "",countryFlag: "")
    var tempUserId = ""
    var otpVM = OtpVerificationViewModel()
    var settingVM = SettingViewModel()
    var activeeScreen = ActiveeScreen.none
    var isFaqPopupOpen = (status: false, faqLink : "" )
   
    
    //MARK:-
    //MARK:- App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.isFaqPopupOpen.status {
            view.endEditing(true)
                AlertTheme.sharedInstance.Show(popupCategory: .attribute, message: "The account/phone number has been blocked. Please visit the FAQ page for further assistance", attributeRangeString: "FAQ", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { [weak self](clicked) in
                    
                    if clicked == "FAQ" {
                        
                        let objFAQVC = self?.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
                        AppDelegate.shared.navController = (self?.navigationController!)!
                        objFAQVC.activeScreen = .BlockFAQ
                        objFAQVC.strWebUrl = "\(self?.isFaqPopupOpen.faqLink ?? "")"
                        self?.navigationController?.pushViewController(objFAQVC, animated: true)
                    }
                    if clicked == "Ok"{
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
        }else{
            txtFld1.becomeFirstResponder() // keyboard open
            initView()
        }
    }
    
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.popLikeNavigationAnimation()
    }
    
    @IBAction func actionResendOTP(_ sender: UIButton) {
        let strMobile = otpInfo.countryCode + otpInfo.mobileNumber
        otpVM.resendOTP(strCompleteMobileNumber: strMobile) { [weak self](verificationID) in
            self?.otpInfo.verificationID = verificationID
        }
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        checkValidation()
        
    }
    
    //MARK:-
    //MARK:- Methods :
    func initView(){
     
        lblPhoneNumber.text = "Sent to \(otpInfo.countryFlag) \(otpInfo.mobileNumber)"
        txtFld1.text = ""
        txtFld2.text = ""
        txtFld3.text = ""
        txtFld4.text = ""
        txtFld5.text = ""
        txtFld6.text = ""
        
        txtFld1.delegate = self
        txtFld2.delegate = self
        txtFld3.delegate = self
        txtFld4.delegate = self
        txtFld5.delegate = self
        txtFld6.delegate = self
        
        txtFld1.textContentType = .oneTimeCode
        self.txtFld1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.txtFld1.becomeFirstResponder()
        
        imgNext.alpha = 0.5
        btnNext.isUserInteractionEnabled = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(OtpVerificationVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(OtpVerificationVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
             NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        // if #available(iOS 12.0, *) {
        if textField.textContentType == UITextContentType.oneTimeCode{
           
        }
    }
    
    
    func checkValidation(){
        self.view.endEditing(true)
        
        let strTxt1  = txtFld1.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strTxt2  = txtFld2.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strTxt3  = txtFld3.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strTxt4  = txtFld4.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strTxt5  = txtFld5.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strTxt6  = txtFld6.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard strTxt1.count > 0 else { lblLine1.layer.shake(); return }
        guard strTxt2.count > 0 else { lblLine2.layer.shake(); return }
        guard strTxt3.count > 0 else { lblLine3.layer.shake(); return }
        guard strTxt4.count > 0 else { lblLine4.layer.shake(); return }
        guard strTxt5.count > 0 else { lblLine5.layer.shake(); return }
        guard strTxt6.count > 0 else { lblLine6.layer.shake(); return }
        
        let code = strTxt1 + strTxt2 + strTxt3 + strTxt4 + strTxt5 + strTxt6
        
        if activeeScreen == .normal {
            
            checkOTP(verificationCode: code)
            
        }else if activeeScreen == .proceed {
            
            deleteAccount(userID: tempUserId, phone_number: otpInfo.mobileNumber, verificationCode: code)
            
        }
    }
    
    func checkOTP(verificationCode : String){
        
        // check on firebase :
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: otpInfo.verificationID, verificationCode: verificationCode)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            
            if let error = error {
                Loader.sharedInstance.stopLoader()
                SnackBar.sharedInstance.show(message: error.localizedDescription, showMsgAt: .bottom)
                print(error.localizedDescription)
            } else {
                
                // check the login if user is already exist or not:
                self.checkUserExistOrNot()
            }
        }
    }
    
    
    func callAPI(textCode : String){
        // check on firebase :
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: otpInfo.verificationID, verificationCode: textCode)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            
            if let error = error {
                Loader.sharedInstance.stopLoader()
                SnackBar.sharedInstance.show(message: error.localizedDescription, showMsgAt: .bottom)
                print(error.localizedDescription)
            } else {
                
                // check the login if user is already exist or not:
                self.checkUserExistOrNot()
            }
        }
    }
    
    func moveToNextVC(){
        self.isFaqPopupOpen.status = false
        let objNameVC = self.storyboard?.instantiateViewController(withIdentifier: "NameVC") as! NameVC
        objNameVC.info.countryCode = otpInfo.countryCode
        objNameVC.info.mobileNumber = otpInfo.mobileNumber
        objNameVC.info.countryName = otpInfo.countryName
        self.presentLikeNavigationAnimation(viewController: objNameVC)
    }
    
    
    func moveToHomeScreen(){
        self.isFaqPopupOpen.status = false
        let objRootVC = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objRootVC, animated: true)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                UIView.animate(withDuration: 0.5) {
                    self.btnNext_bottom.constant =  keyboardSize.height + 5
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
          
            UIView.animate(withDuration: 0.5) {
                self.btnNext_bottom.constant = 5
                self.view.layoutIfNeeded()
            }
            
             self.checkValidation()
        }
    }
}


//MARK:-
//MARK:- Text Fields delegate :

extension OtpVerificationVC : UITextFieldDelegate{
    
    // text fields delegates :
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var shouldProcess = false
        var shouldMoveToNextField = false
        
        let insertStringLength = string.count
        if(insertStringLength == 0){ //backspace
            shouldProcess = true //Process if the backspace character was pressed
        }
            
        else {
            if(textField.text?.count == 0) {
                shouldProcess = true //Process if there is only 1 character right now
            }
        }
        
 
        if(shouldProcess){
           
            let mstring = NSMutableString(string: textField.text!)
            if(mstring.length == 0){
            
                mstring.append(string)
                
                shouldMoveToNextField = true;
            }
            else{
               
                if(insertStringLength > 0){
                    mstring.insert(string, at: range.location)
                }
                else {
                  
                    mstring.deleteCharacters(in: range)
                }
            }
            
            //set the text now
            textField.text = mstring as String
            
            checkNextBtnEnable()
            
            if (string.count == 1){
                if textField == txtFld1 {
                    txtFld2?.becomeFirstResponder()
                }
                if textField == txtFld2 {
                    txtFld3?.becomeFirstResponder()
                }
                if textField == txtFld3 {
                    txtFld4?.becomeFirstResponder()
                }
                if textField == txtFld4 {
                    txtFld5?.becomeFirstResponder()
                }
                if textField == txtFld5 {
                    txtFld6?.becomeFirstResponder()
                }
                if textField == txtFld6 {
                    txtFld6?.resignFirstResponder()
                    textField.text? = string
                }
                
                textField.text? = string
                return false
            }else{
                if textField == txtFld1 {
                    txtFld1?.becomeFirstResponder()
                }
                if textField == txtFld2 {
                    txtFld1?.becomeFirstResponder()
                }
                if textField == txtFld3 {
                    txtFld2?.becomeFirstResponder()
                }
                if textField == txtFld4 {
                    txtFld3?.becomeFirstResponder()
                }
                if textField == txtFld5 {
                    txtFld4?.becomeFirstResponder()
                }
                if textField == txtFld6 {
                    txtFld5?.becomeFirstResponder()
                }
                
                textField.text? = string
                return false
            }
        }
    
        return false
    }
    
    
    func checkNextBtnEnable(){
        let value = "\(txtFld1.text!)" +  "\(txtFld2.text!)" + "\(txtFld3.text!)" + "\(txtFld4.text!)" + "\(txtFld5.text!)" + "\(txtFld6.text!)"
        if value.count == 6 {
            imgNext.alpha = 1.0
            btnNext.isUserInteractionEnabled = true
            
        }else{
            imgNext.alpha = 0.5
            btnNext.isUserInteractionEnabled = false
        }
    }
    
    func emptyTextField(type:Int,txtfld:UITextField){
        if type == 1 {
            let arrTxtFld = [txtFld1,txtFld2,txtFld3,txtFld4,txtFld5, txtFld6]
            var isTxtSame = false
            for i in 0..<arrTxtFld.count {
                
                let txtFldNew = arrTxtFld[i]
                if isTxtSame {
                    txtFldNew?.text = ""
                }
                
                if txtfld == txtFldNew {
                    isTxtSame = true
                }
            }
        }
    }
}


extension OtpVerificationVC {
    
    func checkUserExistOrNot(){
        // device_token
        let device_token = Constants.userDefault.object(forKey: Variables.deviceToken)
        
        otpVM.checkUser_Api(country_code: otpInfo.countryCode, phone_number: otpInfo.mobileNumber, device_token: "\(device_token ?? "")") {[weak self] (status, msg, userInfoStatus) in
            Loader.sharedInstance.stopLoader()
            if userInfoStatus == .moveToHomeScreen {
                self?.moveToHomeScreen()
            }else if userInfoStatus == .moveToRegistrationScreen {
                self?.moveToNextVC()
            }else if userInfoStatus == .blockStatus{
                self?.userBlock()
            }
        }
    }
    
    func userBlock(){
    
        AlertTheme.sharedInstance.Show(popupCategory: .attribute, message: "The account/phone number has been blocked. Please visit the FAQ page for further assistance", attributeRangeString: "FAQ", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { [weak self](clicked) in
            
            if clicked == "FAQ" {
                self?.isFaqPopupOpen.status = true
                self?.isFaqPopupOpen.faqLink = "\(self?.otpVM.faqLink ?? "")"
                
                let objFAQVC = self?.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
                AppDelegate.shared.navController = (self?.navigationController!)!
                objFAQVC.activeScreen = .BlockFAQ
                objFAQVC.strWebUrl = "\(self?.otpVM.faqLink ?? "")"
                self?.navigationController?.pushViewController(objFAQVC, animated: true)
            }
            if clicked == "Ok"{
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    //---------------------
    // delete account :
    func deleteAccount(userID: String ,phone_number : String, verificationCode : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        settingVM.deleteAccountApi(userID: userID) { [weak self](status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self?.checkOTP(verificationCode: verificationCode)
            }else{
                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.somethingWentWrong)", showMsgAt: .bottom)
            }
        }
    }
}
