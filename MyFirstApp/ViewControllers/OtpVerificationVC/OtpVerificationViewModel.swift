//
//  OtpVerificationViewModel.swift
//  MyFirstApp
//
//  Created by cis on 19/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class OtpVerificationViewModel: NSObject {
    
    enum ActionStatus {
        case moveToHomeScreen
        case moveToRegistrationScreen
        case blockStatus
        case error
    }
    
    var faqLink  = ""
    func resendOTP(strCompleteMobileNumber : String, completion: @escaping (_ verificationID: String) -> Void){
        
        PhoneAuthProvider.provider().verifyPhoneNumber(strCompleteMobileNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                SnackBar.sharedInstance.show(message: error.localizedDescription, showMsgAt: .bottom)
                
                return
            }else{
                
                SnackBar.sharedInstance.show(message: "Verification send to your mobile number", showMsgAt: .bottom)
                completion(verificationID ?? "")
            }
        }
    }
    
    
    func checkUser_Api(country_code : String, phone_number : String,device_token : String, completion: @escaping (_ result: String, _ message : String,_ action : ActionStatus) -> Void)
    {
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        
        let strService = APPURL.Urls.userLogin
        let currentDate = Date.getCurrentDate(date: Date())
        
        param.updateValue(currentDate as AnyObject, forKey:"last_login_local_time")
        param.updateValue(country_code as AnyObject, forKey:"country_code")
        param.updateValue(phone_number as AnyObject, forKey: "phone_number")
        param.updateValue(device_token as AnyObject, forKey: "device_token")
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
           
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    let data = dict.object(forKey: "data") as! NSDictionary
                     
                    // check user is blocked or not:
                    if  1 == data.object(forKey: "blocked_indicator") as? Int { // user block
                        self.faqLink = "\(data.object(forKey: "FAQ") ?? "")"
                        completion("success","" , .blockStatus) // user is blocked
                        return
                    }
                    
                    
                    let email  = "\(data.object(forKey: "email") ?? "")"
                    let name  = "\(data.object(forKey: "name") ?? "")"
                    let phoneNumber = "\(data.object(forKey: "phone_number") ?? "")"
                    let picture = "\(data.object(forKey: "picture") ?? "")"
                    let userID = data.object(forKey: "user_id") as! Int
                    let date_format = data.object(forKey: "date_format") as! Int
                    
                    let country_code = "\(data.object(forKey: "country_code") ?? "")"
                    
                    Constants.userDefault.set(email, forKey: Variables.email)
                    Constants.userDefault.set(name, forKey: Variables.name)
                    Constants.userDefault.set(phoneNumber, forKey: Variables.phone_number)
                    Constants.userDefault.set(picture, forKey: Variables.picture)
                    Constants.userDefault.set(userID, forKey: Variables.user_id)
                    Constants.userDefault.set(country_code, forKey: Variables.country_code)
                    Constants.userDefault.set(date_format, forKey: Variables.date_format)
                    
                    completion("success","" , .moveToHomeScreen) // user is already registered:
                    
                }else if dict.object(forKey: "code") as! Int == 400 {//code : 400 , user is not registered
                    
                    completion("success",data.message, .moveToRegistrationScreen) //User is not registered
                    
                }else { // error :
                    SnackBar.sharedInstance.show(message: data.message, showMsgAt: .bottom)
                    completion("error", data.message, .error)
                }
            }
            else
            {
                SnackBar.sharedInstance.show(message: data.message, showMsgAt: .bottom)
                completion("error", data.message, .error)
            }
        }
    }
}

