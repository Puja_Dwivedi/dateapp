//
//  TellUsVC.swift
//  MyFirstApp
//
//  Created by cis on 04/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class TellUsVC: UIViewController {
    
    enum ActiveTag {
        case zero
        case first
        case secoand
        case thirld
        case fourth
        case none
    }
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var view0: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    
    @IBOutlet weak var collec0: UICollectionView!
    @IBOutlet weak var collec1: UICollectionView!
    @IBOutlet weak var collec2: UICollectionView!
    @IBOutlet weak var collec3: UICollectionView!
    @IBOutlet weak var collec4: UICollectionView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var createView: UIViewClass!
    @IBOutlet weak var lblPage: UILabel!
    
    
    //MARK:-
    //MARK:- Variables :
    
    var arr0 = ["woman","man"] // zero
    var arr1 = ["men","women","both"] // first value
    
    var arr2 = ["asian","black","hispanic","indian","white","other"]// secand value
    var arr3 = ["blonde","brunette","grey","red","bald","other"]
    var arr4 = ["beard","glasses","piercings","tatoos"]
    
    
    var selectedValue = (zeroValue: "",firstValue : "", secandValue : "",thirldValue : "",fourthValue: ["",""])
    var activetag = ActiveTag.zero
    var tellUsVM = TellUSViewModel()
    var registrationInfo = (name: "", countryCode: "" , mobileNumber : "", countryName : "")
    var screenWidth = UIScreen.main.bounds.width - 30
    
    //MARK:-
    //MARK:- App flow :
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedValue.fourthValue.removeAll()
        initView()
    }
    
    
    //MARK:-
    //MARK:- Action Methods :
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.popLikeNavigationAnimation()
    }
    
    @IBAction func actionCreateAcc(_ sender: UIButton) {
        makeRequest()
    }
    
    @IBAction func actionPreview(_ sender: UIButton) {
        
        switch activetag {
        
        case .first :
            // btnPrevious.isHidden = true
            activetag = .zero
            collec0.reloadData()
            self.scrollView.scrollTo(horizontalPage: 0)
            break
            
        case .secoand :
            activetag = .first
            collec1.reloadData()
            self.scrollView.scrollTo(horizontalPage: 1)
            break
            
        case .thirld :
            activetag = .secoand
            collec2.reloadData()
            self.scrollView.scrollTo(horizontalPage: 2)
            break
            
        case .fourth :
            btnNext.isHidden = false
            activetag = .thirld
            collec3.reloadData()
            self.scrollView.scrollTo(horizontalPage: 3)
            break
            
        default:
            break
        }
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
        switch activetag {
        
        case .zero:
            //btnPrevious.isHidden = false
            activetag = .first
            lblPage.text = "2/5"
            collec1.reloadData()
            self.scrollView.scrollTo(horizontalPage: 1)
            break
            
        case .first:
            // btnPrevious.isHidden = false
            activetag = .secoand
            lblPage.text = "3/5"
            collec2.reloadData()
            self.scrollView.scrollTo(horizontalPage: 2)
            break
            
        case .secoand :
            activetag = .thirld
            lblPage.text = "4/5"
            collec3.reloadData()
            self.scrollView.scrollTo(horizontalPage: 3)
            break
            
        case .thirld :
            btnNext.isHidden = true
            lblPage.text = "5/5"
            createView.isHidden = false
            activetag = .fourth
            collec4.reloadData()
            self.scrollView.scrollTo(horizontalPage: 4)
            break
            
        default:
            break
        }
    }
    
    
    //MARK:-
    //MARK:- Methods :
    
    func initView(){
        lblPage.text = "1/5"
        
        // btnPrevious.isHidden = true
        createView.isHidden = true
        collec0.register(UINib(nibName: "TellUsCell", bundle: .main), forCellWithReuseIdentifier: "TellUsCell")
        
        //.. first collection view :
        collec1.register(UINib(nibName: "TellUsCell", bundle: .main), forCellWithReuseIdentifier: "TellUsCell")
        
        //.. secand collection view :
        collec2.register(UINib(nibName: "TellUsCell", bundle: .main), forCellWithReuseIdentifier: "TellUsCell")
        
        //.. thirld collection view :
        collec3.register(UINib(nibName: "TellUsCell", bundle: .main), forCellWithReuseIdentifier: "TellUsCell")
        
        
        //.. fourth collection view :
        collec4.register(UINib(nibName: "TellUsCell", bundle: .main), forCellWithReuseIdentifier: "TellUsCell")
    }
    
    
    func makeRequest(){
        
        var param = [String : AnyObject]()
        var characteristicKeyIncrement = 0
        
        param.updateValue(registrationInfo.name as AnyObject, forKey: "name")
        param.updateValue(registrationInfo.countryCode as AnyObject, forKey: "country_code")
        param.updateValue(registrationInfo.mobileNumber as AnyObject, forKey: "phone_number")
        param.updateValue(registrationInfo.countryName as AnyObject, forKey: "country")
        
        //1. i am a :
        if selectedValue.zeroValue != "" {
            characteristicKeyIncrement = characteristicKeyIncrement + 1
            param.updateValue(selectedValue.zeroValue as AnyObject, forKey: "characteristic_\(characteristicKeyIncrement)")
        }
        
        
        //2. i am interested in :
        if "both" == selectedValue.firstValue {
            // partner_preference_1
            param.updateValue("woman" as AnyObject, forKey: "partner_preference_1")
            param.updateValue("man" as AnyObject, forKey: "partner_preference_2")
        }else{
            
            if "men" == selectedValue.firstValue {
                // partner_preference_1
                param.updateValue("man" as AnyObject, forKey: "partner_preference_1")
            }else if "women" == selectedValue.firstValue{
                // partner_preference_1
                param.updateValue("woman" as AnyObject, forKey: "partner_preference_1")
            }
        }
        
        
        //3. I identify as:
        if selectedValue.secandValue != "" && selectedValue.secandValue != "other"{
            characteristicKeyIncrement = characteristicKeyIncrement + 1
            
            param.updateValue(selectedValue.secandValue as AnyObject, forKey: "characteristic_\(characteristicKeyIncrement)")
        }
        
        
        //4. my hair color is :
        if  selectedValue.thirldValue != "" && selectedValue.thirldValue != "other"{
            characteristicKeyIncrement = characteristicKeyIncrement + 1
            
            param.updateValue(selectedValue.thirldValue as AnyObject, forKey: "characteristic_\(characteristicKeyIncrement)")
        }
        
        
        // 5. i have
        for (_, value) in selectedValue.fourthValue.enumerated() {
            characteristicKeyIncrement = characteristicKeyIncrement + 1
            param.updateValue(value as AnyObject, forKey: "characteristic_\(characteristicKeyIncrement)")
        }
        
        // deveice toiken :
        // device_token
        let device_token = Constants.userDefault.object(forKey: Variables.deviceToken)
        let strDToken = "\(device_token ?? "")"
        param.updateValue( strDToken as AnyObject, forKey: "device_token")
        
        callRegister(param: param)
    }
    
    func moveToHomeScreen(){
        SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.registrationSuccessfully)", showMsgAt: .bottom)
        let objRootVC = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objRootVC, animated: true)
    }
}


/*
 MARK:- collectionview delegate :
 */
extension TellUsVC : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch activetag {
        
        case .zero:
            return arr0.count
            
        case .first:
            return arr1.count
            
        case .secoand:
            return arr2.count
            
        case .thirld:
            return arr3.count
            
        case .fourth:
            return arr4.count
            
        default:
            break
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TellUsCell", for: indexPath) as! TellUsCell
        
        switch activetag {
        
        case .zero:
            cell.lblTitle.text = arr0[indexPath.row]
            
            if selectedValue.zeroValue == arr0[indexPath.row] {
                // selected view :
                cell.unSelectedView.isHidden = true
                cell.selectedView.isHidden = false
                cell.lblTitle.textColor = #colorLiteral(red: 0.3215686275, green: 0.09019607843, blue: 0.7254901961, alpha: 1)
            }else{
                // unSelected view :
                cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.selectedView.isHidden = true
                cell.unSelectedView.isHidden = false
            }
            
            break
            
        case .first:
            cell.lblTitle.text = arr1[indexPath.row]
            
            if selectedValue.firstValue == arr1[indexPath.row] {
                // selected view :
                cell.unSelectedView.isHidden = true
                cell.selectedView.isHidden = false
                cell.lblTitle.textColor = #colorLiteral(red: 0.3215686275, green: 0.09019607843, blue: 0.7254901961, alpha: 1)
            }else{
                // unSelected view :
                cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.selectedView.isHidden = true
                cell.unSelectedView.isHidden = false
            }
            break
            
        case .secoand:
            cell.lblTitle.text = arr2[indexPath.row]
            
            if selectedValue.secandValue == arr2[indexPath.row] {
                //selected view :
                
                UIView.performWithoutAnimation {
                    cell.unSelectedView.isHidden = true
                    cell.selectedView.isHidden = false
                }
                cell.lblTitle.textColor = #colorLiteral(red: 0.3215686275, green: 0.09019607843, blue: 0.7254901961, alpha: 1)
            }else{
                // unSelected view :
                UIView.performWithoutAnimation {
                    cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    cell.selectedView.isHidden = true
                    
                    cell.unSelectedView.isHidden = false
                }
            }
            
            break
            
        case .thirld:
            cell.lblTitle.text = arr3[indexPath.row]
            
            if selectedValue.thirldValue == arr3[indexPath.row] {
                // selected view :
                cell.lblTitle.textColor = #colorLiteral(red: 0.3215686275, green: 0.09019607843, blue: 0.7254901961, alpha: 1)
                
                UIView.performWithoutAnimation {
                    cell.unSelectedView.isHidden = true
                    
                    cell.selectedView.isHidden = false
                }
                
            }else{
                // unSelected view :
                UIView.performWithoutAnimation {
                    cell.selectedView.isHidden = true
                    cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    cell.unSelectedView.isHidden = false
                }
            }
            
            break
            
        case .fourth:
            cell.lblTitle.text = arr4[indexPath.row]
            
            if (selectedValue.fourthValue.firstIndex(of: arr4[indexPath.row]) != nil) {
                // selected view :
                cell.lblTitle.textColor = #colorLiteral(red: 0.3215686275, green: 0.09019607843, blue: 0.7254901961, alpha: 1)
                
                UIView.performWithoutAnimation {
                    cell.unSelectedView.isHidden = true
                    
                    cell.selectedView.isHidden = false
                }
                
            }else{
                // unSelected view :
                UIView.performWithoutAnimation {
                    cell.selectedView.isHidden = true
                    cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    cell.unSelectedView.isHidden = false
                }
            }
            break
            
        default:
            break
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch activetag {
        
        case .zero:
            if selectedValue.zeroValue == ""  {
                selectedValue.zeroValue = arr0[indexPath.row]
                
            }else if selectedValue.zeroValue == arr0[indexPath.row]{
                selectedValue.zeroValue = ""
                
            }else if selectedValue.zeroValue != arr0[indexPath.row]{
                selectedValue.zeroValue = arr0[indexPath.row]
            }
            
            UIView.performWithoutAnimation {
                self.collec0.reloadData()
            }
            
            break
            
        case .first:
            if selectedValue.firstValue == ""  {
                selectedValue.firstValue = arr1[indexPath.row]
                
            }else if selectedValue.firstValue == arr1[indexPath.row]{
                selectedValue.firstValue = ""
                
            }else if selectedValue.firstValue != arr1[indexPath.row]{
                selectedValue.firstValue = arr1[indexPath.row]
            }
            
            UIView.performWithoutAnimation {
                self.collec1.reloadData()
            }
            
            break
            
        case .secoand :
            if selectedValue.secandValue == ""  {
                selectedValue.secandValue = arr2[indexPath.row]
                
            }else if selectedValue.secandValue == arr2[indexPath.row]{
                selectedValue.secandValue = ""
                
            }else if selectedValue.secandValue != arr2[indexPath.row]{
                selectedValue.secandValue = arr2[indexPath.row]
            }
            UIView.performWithoutAnimation {
                self.collec2.reloadData()
            }
            break
            
        case .thirld :
            if selectedValue.thirldValue == ""  {
                selectedValue.thirldValue = arr3[indexPath.row]
                
            }else if selectedValue.thirldValue == arr3[indexPath.row]{
                selectedValue.thirldValue = ""
                
            }else if selectedValue.thirldValue != arr3[indexPath.row]{
                selectedValue.thirldValue = arr3[indexPath.row]
            }
            UIView.performWithoutAnimation {
                self.collec3.reloadData()
            }
            
            break
            
        case .fourth :
            if selectedValue.fourthValue.count == 0  {
                selectedValue.fourthValue.append(arr4[indexPath.row])
                
            }else if let index = selectedValue.fourthValue.firstIndex(of: arr4[indexPath.row]){
                selectedValue.fourthValue.remove(at: index)
                
            }else{
                selectedValue.fourthValue.append(arr4[indexPath.row])
            }
            UIView.performWithoutAnimation {
                self.collec4.reloadData()
            }
            
            break
            
        default:
            break
        }
        
    }
}
extension UIScrollView {
    
    func scrollTo(horizontalPage: Int? = 0, verticalPage: Int? = 0, animated: Bool? = true) {
        var frame: CGRect = self.frame
        frame.origin.x = frame.size.width * CGFloat(horizontalPage ?? 0)
        frame.origin.y = frame.size.width * CGFloat(verticalPage ?? 0)
        self.scrollRectToVisible(frame, animated: animated ?? true)
    }
}


/*
 MARK:- API's
 */
extension TellUsVC {
    
    func callRegister(param : [String : AnyObject])
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        tellUsVM.CallRegisterAPI(dict: param) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.moveToHomeScreen()
            }
        }
    }
}
