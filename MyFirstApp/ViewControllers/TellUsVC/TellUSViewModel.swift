//
//  TellUSViewModel.swift
//  MyFirstApp
//
//  Created by cis on 19/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class TellUSViewModel: NSObject {
    
    var onCloser : ((String, _ message : String)->Void)!
    
    func CallRegisterAPI(dict : [String : AnyObject], completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        onCloser = completion
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        //param.updateValue(userId as AnyObject, forKey: KEY.UserDetails.ProfileId)
        let strService = APPURL.Urls.userRegistration
        param = dict
        
        let currentDate = Date.getCurrentDate(date: Date())
        param.updateValue(currentDate as AnyObject, forKey: "last_login_local_time")
        //{"page":"1","pagination_limit":"10"}
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                //  completion("success", data.message, .moveToHomeScreen, "")
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    let data = dict.object(forKey: "data") as! NSDictionary
                    self.storeData(data: data)
                }else if dict.object(forKey: "code") as! Int == 400 {//code : 400 , user is not registered
                    
                    SnackBar.sharedInstance.show(message: data.message, showMsgAt: .bottom)
                    self.onCloser("error", data.message)
                    
                }else { // error :
                    SnackBar.sharedInstance.show(message: data.message, showMsgAt: .bottom)
                    self.onCloser("error", data.message)
                }
            }
            else
            {
                SnackBar.sharedInstance.show(message: data.message, showMsgAt: .bottom)
                self.onCloser("error", data.message)
            }
        }
    }
    
    /*
     user is already registerd
     */
    func storeData(data : NSDictionary){
        
       let email  = "\(data.object(forKey: "email") ?? "")"
       let name  = "\(data.object(forKey: "name") ?? "")"
       let phoneNumber = "\(data.object(forKey: "phone_number") ?? "")"
       let picture = "\(data.object(forKey: "picture") ?? "")"
       let userID = data.object(forKey: "user_id") as! Int
       let date_format = data.object(forKey: "date_format") as! Int
       
       let country_code = "\(data.object(forKey: "country_code") ?? "")"
       
       Constants.userDefault.set(email, forKey: Variables.email)
       Constants.userDefault.set(name, forKey: Variables.name)
       Constants.userDefault.set(phoneNumber, forKey: Variables.phone_number)
       Constants.userDefault.set(picture, forKey: Variables.picture)
       Constants.userDefault.set(userID, forKey: Variables.user_id)
       Constants.userDefault.set(country_code, forKey: Variables.country_code)
       Constants.userDefault.set(date_format, forKey: Variables.date_format)
                           
                           /// register on firebase:
                           // let FCMToken = "\(Constants.userDefault.object(forKey: Variables.deviceToken) ?? "")"
                      //     let userName = "\(Constants.userDefault.object(forKey: Variables.name) ?? "")"/
                      //     let emailID =  "\(Constants.userDefault.object(forKey: Variables.email) ?? "")"
                      //     let user_ID =  Constants.userDefault.object(forKey: Variables.user_id) as? UInt64 ?? 0
                       //    let userImg =  "\(Constants.userDefault.object(forKey: Variables.picture) ?? "")"
                           
                        //   let user = HSUser.init(isName: userName, isEmail: emailID , isID: user_ID , isPicture: userImg )
                        //   HSRealTimeMessagingLIbrary.function_CreatAndUpdateUser(user)
        
        onCloser("success","")
    }
}
