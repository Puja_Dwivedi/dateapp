//
//  ChatViewModel.swift
//  MyFirstApp
//
//  Created by cis on 04/03/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ChatViewModel: NSObject {
    
    var arrChatList = NSMutableArray()
    
    func getChatList_API( completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.getChatList
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userid as AnyObject, forKey: "user_id")
        
        //  print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    let arrData = RV_CheckDataType.getArray(anyArray: data.response.value(forKey: KEY.ServiceKeys.dataResponse) as AnyObject)
                    if arrData.count > 0
                    {
                        self.arrChatList = arrData.mutableCopy() as! NSMutableArray
                        
                    }
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    completion("success", message)
                    
                }else { // error :
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
}
