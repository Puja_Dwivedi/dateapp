//
//  ChatVC.swift
//  MyFirstApp
//
//  Created by cis on 04/03/21.
//  Copyright © 2021 cis. All rights reserved.
//
 
import UIKit
import SDWebImage
import VisualEffectView
 
class ChatVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var viewPlaceholder: UIView!
    
    //MARK:-
    //MARK:- Variables:
    
    var chatVM = ChatViewModel()
    var countryDialCode = 0 //Constants.userDefault.object(forKey: Variables.date_format) as! Int
    var refreshControl: UIRefreshControl!
    var dateFormateStatus : Int? = nil // 1 for us , 0 for non us,
    /* (1) US: MM/DD/YYYY
     (2) Non US : DD/MM/YYY
     */
    
    //MARK:-
    //MARK:- App Flow:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
          
    }
    
    override func viewWillAppear(_ animated: Bool) {
          countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
      initView()
        
    }
    

    //MARK:-
    //MARK:- Methods:
    
    func initView(){
        
        tblView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right:0)
        
        let nib = UINib.init(nibName: "ChatCell", bundle: nil)
        tblView.register(nib, forCellReuseIdentifier: "ChatCell")
        
        tblView.estimatedRowHeight = 100
        tblView.rowHeight = UITableView.automaticDimension
        
        viewPlaceholder.isHidden = true
        getChatList_Data()
    }
    
    @objc func refresh(_ sender: Any) {
        getChatList_Data()
    }
    
    @IBAction func actionmenu(_ sender: UIButton) {
        viewMoreMatch.sharedInstance.Show(arr: ["Show my profile","remove","report"], y: btnMenu.frame.origin.y, strMatch_id: "") { [weak self](result) in
            
            if result == "Show my profile" {
                self?.moveToProfile()
            }else if result == "remove" {
                print("Remove")
            }else if result == "report"{
                print("Report")
            }
        }
    }
    
    func moveToProfile(){
    }
}

//MARK:-
//MARK:- Tableview deeletes:

extension ChatVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatVM.arrChatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var picture = ""

        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        let dict = chatVM.arrChatList[indexPath.row] as! NSDictionary
        
        if let image = dict.object(forKey: "post_image") as? String {
            if image == "" {
                let picture_img = dict.object(forKey: "picture") as? String ?? "--"
                picture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(picture_img)"
            } else {
                picture = "\(APPURL.baseAPI.imageURL)post_pictures/\(image)"
            }
        } else {
            let picture_img = dict.object(forKey: "picture") as? String ?? "--"
            picture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(picture_img)"
        }
        
//        if let image = dict.object(forKey: "picture") as? String {
//            if image == "" {
//                let picture_img = dict.object(forKey: "post_image") as? String ?? "--"
//                picture = "\(APPURL.baseAPI.imageURL)post_pictures/\(picture_img)"
//            } else {
//                picture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(image)"
//            }
//        }
        
        let profile_visibility = dict.object(forKey: "profile_visibility") as! Int

        if profile_visibility == 1 { // show blur image
         for tempView in cell.img.subviews {
                    tempView.removeFromSuperview()
                  }

            for tempView in cell.lblTitle.subviews{
                tempView.removeFromSuperview()
            }
            
            // for image:
            cell.lblTitle.text = "\(dict.object(forKey: "name") ?? "--")"
            cell.img.sd_setImage(with: URL(string: picture), placeholderImage: UIImage(named: "img_profile_placeholder"))

            cell.lblTitle.unblur()

        }else if profile_visibility == 0 // show genric name: like Mike
        {
            // for image :
            for tempView in cell.img.subviews{
                    tempView.removeFromSuperview()
            }
            
            cell.lblTitle.text = "Fallon"
            cell.img.image = UIImage(named: "profile_Placeholder")

            let visualEffectView = VisualEffectView(frame: cell.img.bounds)

            // Configure the view with tint color, blur radius, etc
            visualEffectView.colorTint = .clear
            visualEffectView.colorTintAlpha = 0.1
            visualEffectView.blurRadius = 3
            visualEffectView.scale = 1

            cell.img.addSubview(visualEffectView)

            for tempView in cell.lblTitle.subviews{
                tempView.removeFromSuperview()
            }
             // for label/Users/cis/Desktop/Screen Recording 2022-07-21 at 12.17.37 PM.mov
            cell.lblTitle.updateConstraints()
            cell.lblTitle.layoutIfNeeded()
            cell.lblTitle.blur()
        }
        
        var strDate = ""
        if  countryDialCode == 1 { // for US : 12 hours time foemate // for US : 12 hours time foemate
            strDate = convertDateFormaterForUS("\(dict.object(forKey: "date") ?? "")")
        }else{
            strDate = convertDateFormaterForNonUS("\(dict.object(forKey: "date") ?? "")")
        }
        
         if let event_type = dict.object(forKey: "event_subtype") as?  Int{
             
             if event_type == 3 { // place
                 
                cell.lblSubTitle.text = "\(dict.object(forKey: "main_text") ?? ""), \(dict.object(forKey: "formatted_address") ?? "")\n\(strDate)"
                
             }else if event_type == 4 { // street
                 
                cell.lblSubTitle.text = "\(dict.object(forKey: "main_text") ?? ""), \(dict.object(forKey: "secondary_text") ?? "")\n\(strDate)"
             }else{
                cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? "")\n\(strDate)"
             }
             
         }else{
            cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? "")\n\(strDate)"
         }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var picture = ""

        let dict = chatVM.arrChatList[indexPath.row] as! NSDictionary
        let otherUser_id = dict.object(forKey: "user_id") as? Int
        let OtherName  = "\(dict.object(forKey: "name") ?? "")"
        let match_id = dict.object(forKey: "match_id") as? Int
        let profile_visibility = dict.object(forKey: "profile_visibility") as! Int
        let profile_indicator = dict.object(forKey: "profile_indicator") as! Int
        let event_id = dict.object(forKey: "event_id") as? Int
        
        
        if let image = dict.object(forKey: "picture") as? String {
            if image == "" {
                let picture_img = dict.object(forKey: "post_image") as? String ?? "--"
                picture = "\(APPURL.baseAPI.imageURL)post_pictures/\(picture_img)"
            } else {
                picture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(image)"
            }
        }
        
        
//        let picture = "http://3.137.46.73:3000/images/profile_pictures/\(dict.object(forKey: "picture") ?? "--")"
        
        let uuserID  : UInt64 = Constants.userDefault.object(forKey: Variables.user_id) as! UInt64
        
        let chatID = HSRealTimeMessagingLIbrary.function_CreatChatID(uuserID, UInt64(otherUser_id ?? 0))
        
        let ChatDetails = HSChatUser.init(isChatID: chatID, isID: UInt64((otherUser_id) ?? 0), isName: OtherName, isTime: "", isLastMessage: "")
        let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
        
        vc.chatDetail = ChatDetails
        vc.comeFromScreenStatus = .fromChat
        
        vc.profile_visibility = profile_visibility
        vc.profile_indicator = profile_indicator
        
        vc.strprofilePicture = picture
        vc.matchInfo.match_id =  match_id ?? 0
        vc.matchInfo.user_id = otherUser_id ?? 0
        vc.matchInfo.event_id = event_id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func convertDateFormaterForUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    func convertDateFormaterForNonUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
}

/*
 MARK:- API
 */
extension ChatVC {
    
    func getChatList_Data()
    {
        self.chatVM.arrChatList.removeAllObjects()
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        chatVM.getChatList_API { (status, message) in
            Loader.sharedInstance.stopLoader()
            self.refreshControl.endRefreshing()
            
            if status == "success"
            {
                if self.chatVM.arrChatList.count > 0 {
                    self.viewPlaceholder.isHidden = true
                    self.tblView.isHidden = false
                    self.tblView.reloadData()
                    
                }else{
                    self.viewPlaceholder.isHidden = false
                    self.tblView.isHidden = true
                }
            }
            else
            {
                if self.chatVM.arrChatList.count > 0 {
                    self.viewPlaceholder.isHidden = true
                    self.tblView.isHidden = false
                    self.tblView.reloadData()
                    
                }else{
                    self.viewPlaceholder.isHidden = false
                    self.tblView.isHidden = true
                }
                
                if message == "No Data Available." || message == "No more data."
                {
                    // SnackBar.sharedInstance.show(message: message)
                }
                else
                {
                    //  SnackBar.sharedInstance.show(message: message)
                }
            }
        }
    }
}


