//
//  LanguageVC.swift
//  MyFirstApp
//
//  Created by cis on 29/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class LanguageVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet weak var tblview: UITableView!
    
    
    //MARK:-
    //MARK:- variables
    
    var arrLanguage = ["English"]
    var selectedLanguage = "English"
    
    
    //MARK:-
    //MARK:- App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-
    //MARK:- Methods:
    
    func initView(){
        tblview.register(UINib(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "CountryCell")
        tblview.rowHeight = UITableView.automaticDimension;
        tblview.estimatedRowHeight = 20.0
    }
}



extension LanguageVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLanguage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryCell
        cell.lblTitle.text =  arrLanguage[indexPath.row]
        
        if arrLanguage[indexPath.row] == selectedLanguage{
            cell.imgSelect.isHidden = false
        }else{
            cell.imgSelect.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedLanguage = arrLanguage[indexPath.row]
        tableView.reloadData()
    }
}
