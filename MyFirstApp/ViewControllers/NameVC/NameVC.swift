//
//  NameVC.swift
//  MyFirstApp
//
//  Created by cis on 02/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class NameVC: UIViewController {

    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var txtFldName: UITextField!
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var btnNext_bottom: NSLayoutConstraint!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgNext: UIImageView!
    
    //MARK:-
    //MARK:- variables:
    var info = (countryCode : "", mobileNumber : "", countryName : "")
    var tellUsVM = TellUSViewModel()
    
    //MARK:-
    //MARK:- App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFldName.text = ""
        imgNext.alpha = 0.5
        btnNext.isUserInteractionEnabled = false
        
        txtFldName.becomeFirstResponder() //  keyboard open
        txtFldName.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(NameVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(NameVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    //MARK:-
    //MARK:- Action methods :
    
    @IBAction func actionBack(_ sender: UIButton) {
        moveToTheLoginScreen()
    }
    
    
    @IBAction func actionNext(_ sender: UIButton) {
    checkValidation()
    }
    
    
    //MARK:-
    //MARK:- Methods:
    func checkValidation(){
        self.view.endEditing(true)
        let strName  = txtFldName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard strName.count > 0 else {
            lblLine.layer.shake(); return }
        
        makeRequest(name: strName)
    }
    
    
    func moveToTheLoginScreen(){
        for controller in AppDelegate.shared.navController.viewControllers as Array {
            
            if controller.isKind(of: SplashVC.self) {
                Constants.userDefault.removeObject(forKey:Variables.email)
                Constants.userDefault.removeObject(forKey:Variables.name)
                Constants.userDefault.removeObject(forKey:Variables.phone_number)
                Constants.userDefault.removeObject(forKey:Variables.picture)
                Constants.userDefault.removeObject(forKey:Variables.user_id)
                
                AppDelegate.shared.navController.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    
    @objc func keyboardWillShow(notification: Notification) {
         if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
             if self.view.frame.origin.y == 0{
                 UIView.animate(withDuration: 0.5) {
                     self.btnNext_bottom.constant =  keyboardSize.height + 5
                     self.view.layoutIfNeeded()
                 }
             }
         }
     }

     @objc func keyboardWillHide(notification: Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
             UIView.animate(withDuration: 0.5) {
                 self.btnNext_bottom.constant = 5
                 self.view.layoutIfNeeded()
             }
         }
     }
}

extension NameVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let str = textField.text
        if str?.count == 0 && string == " " {
            return false
        } else if (str?.count)! > 0 {
            if str?.last == " " && string == " " {
                return false
            } else if str?.containsWhitespace ?? false && string == " "{// one space found only, allow user to only one space.
                 return false
                
            }else{// range will be nil if no whitespace is found
                let currentString: NSString = (textField.text ?? "") as NSString
                    let newString: NSString =
                        currentString.replacingCharacters(in: range, with: string) as NSString
                 let value =  "\(newString)".replace(string: " ", replacement: "")
                  if value.count >= 3{
                     imgNext.alpha = 1.0
                      btnNext.isUserInteractionEnabled = true
                      
                      }else{
                      
                      imgNext.alpha = 0.5
                      btnNext.isUserInteractionEnabled = false
                  }
            }
    }
        return true
}
}
extension String {
    var containsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
}


//MARK:-
//MARK:- API calling

extension NameVC {
    
    func makeRequest(name: String){
        
        var param = [String : AnyObject]()
        param.updateValue(name as AnyObject, forKey: "name")
        param.updateValue(info.countryCode as AnyObject, forKey: "country_code")
        param.updateValue(info.mobileNumber as AnyObject, forKey: "phone_number")
        param.updateValue(info.countryName as AnyObject, forKey: "country")
        
        // deveice token :
        // device_token
        let device_token = Constants.userDefault.object(forKey: Variables.deviceToken)
        let strDToken = "\(device_token ?? "")"
          param.updateValue( strDToken as AnyObject, forKey: "device_token")
        
        callRegister(param: param)
    }
    
    
    func callRegister(param : [String : AnyObject])
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        tellUsVM.CallRegisterAPI(dict: param) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.moveToHomeScreen()
            }
        }
    }
    
    func moveToHomeScreen(){
        SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.registrationSuccessfully)", showMsgAt: .bottom)
        let objRootVC = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objRootVC, animated: true)
    }
}
