
//
//  CreatePostView1.swift
//  MyFirstApp
//
//  Created by cis on 30/03/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import SearchTextField

enum CreatePostActiveScreen: Int {
    case  airplane //1
    case  subway   //2
    case  place    //3
    case  street   //4
    case  bus      //5
    case  train    //6
    case  none
}

var SelectedCityFullNameCreatePost = ""
var currentCountryCode = ""

class CreatePostView1: UIView , UIGestureRecognizerDelegate{
    
    enum ActiveTime  {
        case today
        case yesterday
    }
    
    enum ActionScreenUpdate {
        case new
        case update
    }
    
    enum ActivePlaceTextFldOption {
        case txtFlight
        case txtLocation
        case subAtrributes
        case non
    }

    
    //MARK:-
    //MARK:- IBOutlets:
    
    @IBOutlet var rootScrollView: UIScrollView!
    @IBOutlet var viewDate: UIView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var viewTabAirplane1Container: UIView!
    @IBOutlet weak var viewTabSubway2Container: UIView!
    @IBOutlet weak var viewTabPlace3Container: UIView!
    @IBOutlet weak var viewTabStreet4Container: UIView!
    @IBOutlet weak var viewTabBus5Container: UIView!
    @IBOutlet weak var viewTabTrain6Container: UIView!
    
    @IBOutlet weak var viewGradientToday: UIView!
    @IBOutlet weak var viewGradientYesterday: UIView!
    
    @IBOutlet weak var lblFlightTitle: UILabel!
    @IBOutlet weak var txtFldFlight: SearchTextField!
    
    @IBOutlet weak var lblFlightTitle2: UILabel!
    @IBOutlet weak var lblFlightLine2: UILabel!
    @IBOutlet weak var txtFldFlight2: SearchTextField!
    @IBOutlet weak var btnCanceltxtFlight2: UIButton!
    @IBOutlet weak var ViewPlaceFilterContainer2: UIStackView!
    @IBOutlet weak var lblSelectedFilterTitle2: UILabel!
    @IBOutlet weak var viewSelectedFilterContainer2: UIView!



    
    @IBOutlet weak var lblAirpot: UILabel!
    @IBOutlet weak var txtFldAirpot: UITextField!
    
    @IBOutlet weak var lblDateTitle: UILabel!
    
    @IBOutlet weak var txtFldSubAtrributes: UITextField!
    @IBOutlet weak var viewSubAtrributes: UIView!
    
    //Autocomplete :
    @IBOutlet weak var tblViewTicketAutoComplete: UITableView!
    @IBOutlet weak var tblViewTicketAutoComplete_h: NSLayoutConstraint!
    
    @IBOutlet var lblTitle_TopConst: NSLayoutConstraint!
    @IBOutlet var viewBase2Container: UIView!
    @IBOutlet var viewBaseContainer: UIView!
    @IBOutlet var headerTotle_topCons: NSLayoutConstraint!
    @IBOutlet weak var airport_topCons: NSLayoutConstraint!
    @IBOutlet weak var rootContainer: UIView!
    @IBOutlet weak var lblFlightLine: UILabel!
    @IBOutlet weak var lblLocationLine: UILabel!
    @IBOutlet weak var lblSubAtrributeTitle: UILabel!
    @IBOutlet weak var lblSubAtributeLine: UILabel!
    
    @IBOutlet var viewStreet: UIView!
    @IBOutlet var viewPlace: UIView!
    @IBOutlet var viewPlane: UIView!
    @IBOutlet var viewSubway: UIView!
    @IBOutlet var viewBus: UIView!
    @IBOutlet weak var lblTodayTitle1: GradientLabel!
    @IBOutlet weak var lblYesterdayTitle1: GradientLabel!
    
    @IBOutlet var viewPlaceContainer: UIView!
    @IBOutlet weak var bottomSpace_cons: NSLayoutConstraint!
    @IBOutlet weak var viewLocationContainer: UIView!
    @IBOutlet weak var lblPlaceAttribute: UILabel!
    @IBOutlet weak var btnCanceltxtFlight: UIButton!
    
    //---------: Place filter container :---------
    @IBOutlet var lblTitleSelcected: UILabel!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet weak var ViewPlaceFilterContainer: UIStackView!
    @IBOutlet weak var lblSelectedFilterTitle: UILabel!
    @IBOutlet weak var viewSelectedFilterContainer: UIView!
    @IBOutlet var lblTodayDate: GradientLabel!
    @IBOutlet var lblStep1: UILabel!
    @IBOutlet var lblYesterdayDate: GradientLabel!
    @IBOutlet var lblTdate: UILabel!
    @IBOutlet var lblYdate: UILabel!
    @IBOutlet var viewOuterDate: UIView!
    //----------------------------------------------
    
    
    //MARK:- Variables:
    var activeScreen = CreatePostActiveScreen.airplane // default
    var timeInterVal = ActiveTime.today
    var activePlaceTextFld = ActivePlaceTextFldOption.non
    var countryDialCode = 0 //Constants.userDefault.object(forKey: Variables.date_format) as! Int
    
    var location = Location()
    var actionScreenUpdate = ActionScreenUpdate.new
    var selectedCountry = ""
    var currentStateLocationName = ""
    var stateNameLong = ""
   
    var defaultCityName = ""
    var createPostVM = CreatePostView1ViewModel()
    
    var presentViewController : UIViewController? = nil
    var onCloser : ((CreatePostActiveScreen,String,String, String,String,NSDictionary,NSDictionary,Bool,String,String)-> Void)!
    let dropDownHeight = UIScreen.main.bounds.height/3.2
    var placeAPIDetailData1 = NSDictionary()
    var placeAPIDetailData2 = NSDictionary()
    var isCountryFound = false

    var headerTabViews: [UIView] = []
    var tabViews: [UIView] = []

    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        DeviceSize.screen_Height > 736.0 ? (lblTitle_TopConst.constant = 300.0) : (lblTitle_TopConst.constant = 450.0)
        rootScrollView.isScrollEnabled = false
        lblPlaceAttribute.isHidden = true
        location.setupLocationManager()
        location.delegate = self
//        btnBack.underline()
        bottomSpace_cons.constant = safeAreaInsets.bottom
        self.airport_topCons.constant = 10
        lblTodayDate.text = retrieveTodayDate()
        lblTdate.text = retrieveTodayDate()
        
        
//        lblYesterdayDate.text = retrieveYesterDayDate()
//        lblYdate.text = retrieveYesterDayDate()
        
        headerTabViews.append(viewTabAirplane1Container)
        headerTabViews.append(viewTabBus5Container)
        headerTabViews.append(viewTabPlace3Container)
        headerTabViews.append(viewTabStreet4Container)
        headerTabViews.append(viewTabTrain6Container)
        headerTabViews.append(viewTabSubway2Container)
        
        tabViews.append(viewPlane)
        tabViews.append(viewSubway)
        tabViews.append(viewPlace)
        tabViews.append(viewStreet)
        tabViews.append(viewBus)
        
        viewPlane.addShadowToView()
        viewSubway.addShadowToView()
        viewPlace.addShadowToView()
        viewStreet.addShadowToView()
        viewBus.addShadowToView()
        
        viewDate.addShadowToView()
        viewOuterDate.addShadowToView()
        AppDelegate.shared.activeCheckin = .airplane
        
//        backgroundColor = UIColor.black.withAlphaComponent(0.8)
        tblViewTicketAutoComplete.delegate = self
        tblViewTicketAutoComplete.dataSource =  self
        txtFldFlight.delegate = self
        txtFldFlight2.delegate = self

        txtFldSubAtrributes.delegate = self
        
        let nib = UINib.init(nibName: "autoCompleteCell", bundle: nil)
        self.tblViewTicketAutoComplete.register(nib, forCellReuseIdentifier: "autoCompleteCell")
        
        //self.lblTime.text = getTimeInterval()
        let widthCalclulate = (UIScreen.main.bounds.width - 60)/2
        let newView = viewGradientToday
        let newViewGradientToday = CGRect(x: viewGradientToday.bounds.origin.x, y: viewGradientToday.bounds.origin.y, width: widthCalclulate, height: viewGradientToday.bounds.height)
        newView?.frame = newViewGradientToday
//        newView?.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft ,.bottomLeft], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 22.5)
        
        let newView1 = viewGradientYesterday
        let newGradientYesterday = CGRect(x: viewGradientYesterday.bounds.origin.x, y: viewGradientYesterday.bounds.origin.y, width: widthCalclulate, height: viewGradientYesterday.bounds.height)
        newView1?.frame = newGradientYesterday
//        newView1?.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topRight ,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 22.5)
        
        lblTodayTitle1.gradientColors = [#colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1), #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)]
        lblTodayDate.gradientColors = [#colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1), #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1)]
        
        lblYesterdayTitle1.gradientColors =  [#colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1), #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)]
        lblYesterdayDate.gradientColors =  [#colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1), #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)]
        setTxtHint()
        getPlaceFilterOption()
        deselectViews()
        viewTabAirplane1Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
        viewPlane.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
    }
    
    func deselectViews() {
        headerTabViews.forEach { $0.backgroundColor = .white }
        tabViews.forEach { $0.backgroundColor = .white }
    }
    
    //MARK:-
    //MARK:- Main Method
    
    func Show(viewController : UIViewController,actionScreenUpdate : ActionScreenUpdate,activeScreen : CreatePostActiveScreen, strFlight : String, strSubAttribute : String,strLocation : String,timeStatus : String,placeAPIData1 : NSDictionary, placeAPIData2 : NSDictionary,isCountryFound: Bool,country : String, state : String, onCompletion: @escaping (CreatePostActiveScreen,String,String,String,String,NSDictionary,NSDictionary,Bool,String,String)-> Void)-> UIView{
        
        let customPopUp = Bundle.main.loadNibNamed("CreatePostView1", owner: self, options: nil)?[0] as! CreatePostView1
        customPopUp.countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        customPopUp.activeScreen = activeScreen
        customPopUp.isCountryFound = isCountryFound
        
        customPopUp.selectedCountry = country
        customPopUp.currentStateLocationName = state
        
        viewController.tabBarController?.tabBar.backgroundColor = .white
        customPopUp.btnCanceltxtFlight.isHidden = true
        customPopUp.btnCanceltxtFlight2.isHidden = true

        
        if SelectedCityFullNameCreatePost != "" {
            customPopUp.txtFldAirpot.text = SelectedCityFullNameCreatePost
        }else{
            customPopUp.txtFldAirpot.text = strLocation
        }
        
        customPopUp.defaultCityName = strLocation
        
        if placeAPIData1 != NSDictionary() {
            customPopUp.placeAPIDetailData1 = placeAPIData1
        }
        
        if placeAPIData2 != NSDictionary() {
            customPopUp.placeAPIDetailData2 = placeAPIData2
            currentCountryCode = customPopUp.getCountryCodeFrom(dict: placeAPIData2)
        }
        
        let tap = UITapGestureRecognizer(target: customPopUp, action: #selector(customPopUp.acitonCloseKeyboard))
        customPopUp.rootContainer.isUserInteractionEnabled = true
        customPopUp.rootContainer.addGestureRecognizer(tap)
        
        customPopUp.txtFldFlight.text = strFlight
        customPopUp.txtFldFlight2.text = strFlight

        customPopUp.txtFldSubAtrributes.text = strSubAttribute
        customPopUp.actionScreenUpdate = actionScreenUpdate
        
        if timeStatus == "today"{
            customPopUp.timeInterVal = .today
        }else{
            customPopUp.timeInterVal = .yesterday
        }
        
        customPopUp.frame = UIScreen.main.bounds
        customPopUp.onCloser = onCompletion
        customPopUp.setupAutoCompleteView()
        customPopUp.setTimeInterValInfo()
        
        customPopUp.presentViewController = viewController
        viewController.view.addSubview(customPopUp)
        
        customPopUp.intiViewWithoutAnimation()
        customPopUp.showWithAnimation()
        return customPopUp
    }

    func updateInfo(activeScreen : CreatePostActiveScreen, strFlight : String, strSubAttribute : String,strLocation : String,timeStatus : String, placeAPIData1 : NSDictionary, placeAPIData2 : NSDictionary, isCountryFound : Bool,country: String, state : String){
        self.activeScreen = activeScreen
        //  self.presentViewController = viewController
        txtFldFlight.text = strFlight
        txtFldFlight2.text = strFlight

        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        txtFldSubAtrributes.text = strSubAttribute
        self.selectedCountry = country
        self.currentStateLocationName = state
        self.stateNameLong = getStateNameFrom(dict: placeAPIData2).longName
        self.defaultCityName = strLocation
        
        self.placeAPIDetailData1 = placeAPIData1
        self.placeAPIDetailData2 = placeAPIData2
        self.isCountryFound = isCountryFound
        
        currentCountryCode = getCountryCodeFrom(dict: placeAPIData2)
        
        if SelectedCityFullNameCreatePost != "" {
            txtFldAirpot.text = SelectedCityFullNameCreatePost
        }else{
            txtFldAirpot.text = strLocation
        }
        
        if timeStatus == "today"{
            timeInterVal = .today
        }else{
            timeInterVal = .yesterday
        }
        
        location.setupLocationManager()
        location.delegate = self
        
        intiViewWithoutAnimation()
        showWithAnimation()
    }
    
    
    //MARK:-"
    //MARK:- Action Methods:
    
    @IBAction func actionCancel(_ sender: UIButton) {
        viewDismissKeyboard()
        removeWithAnimation()
        NotificationCenter.default.post(name: NSNotification.Name("moveToRoot"),object: nil)
//        NotificationCenter.default.post(name: NSNotification.Name("moveToSelectType"),object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        AppDelegate.shared.selectedPostId = 0
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewDismissKeyboard()
            self.removeWithAnimation()
            NotificationCenter.default.post(name: NSNotification.Name("moveToSelectPostType"),object: nil)
            Loader.sharedInstance.stopLoader()
        }
    }
    
    
    @objc func acitonCloseKeyboard() {
        viewDismissKeyboard()
    }
    
    //MARK:Place:---------------------------
    //----------: Selected remove place filter :-----------------
    @IBAction func actionBtnCloseFilter(_ sender: UIButton) {
        lblSelectedFilterTitle.text = ""
        lblSelectedFilterTitle2.text = ""
        Singleton.shared.selectedFilterPlace.selectedValue = ""
        viewSelectedFilterContainer.isHidden = true
        viewSelectedFilterContainer2.isHidden = true

    }
    
    @IBAction func actionBtnOpenFilter(_ sender: UIButton) {
    // open filter option :
        
        // add value when a user selects filter option from the given list.
        FilterView.Show(previousSelectedValue: Singleton.shared.selectedFilterPlace.selectedValue, arrFilterOption: Singleton.shared.selectedFilterPlace.arrFilterOption) { [weak self](desc,key) in
           
            Singleton.shared.selectedFilterPlace.selectedValue = key
            self?.lblSelectedFilterTitle.text = desc
            self?.lblSelectedFilterTitle2.text = desc
            self?.viewSelectedFilterContainer.isHidden = false
            self?.viewSelectedFilterContainer2.isHidden = false

        }
    }
    //-----------------------------------------------------------
    
    
    @IBAction func actionCancelTxtFlight(_ sender: UIButton) {
        txtFldFlight.text = ""
        txtFldFlight2.text = ""

        btnCanceltxtFlight.isHidden = true
        btnCanceltxtFlight2.isHidden = true

        self.tblViewTicketAutoComplete_h.constant = 0
        //  viewDismissKeyboard()
    }
    
    @IBAction func actionCloseDD(_ sender: UIButton) {
        viewDismissKeyboard()
    }
    
    @IBAction func actionTabAirplane(_ sender: UIButton) {
        AppDelegate.shared.activeCheckin = .airplane
        UIView.performWithoutAnimation{
//            lblHeaderTitle.text = "Where do you want to check in?"
            txtFldFlight.placeholder = "DL1234, AA2345…"
            txtFldFlight2.placeholder = "DL1234, AA2345…"

            self.lblTitleSelcected.text = "On a Plane."
            self.lblFlightTitle.text = "Enter the flight #"
            self.lblFlightTitle2.text = "Enter the flight #"
            self.layoutIfNeeded()
        }
        self.txtFldAirpot.text = ""
        self.btnCanceltxtFlight.isHidden = true
        self.btnCanceltxtFlight2.isHidden = true

        self.endEditing(true)
        viewDismissKeyboard()
        
        setHeightOfAutoCompleteTableview(withAnimation: true)
        activeScreen = .airplane
        intiViewWithAnimation()
    }
    
    @IBAction func actionTabSubway(_ sender: UIButton) {
        AppDelegate.shared.activeCheckin = .subway
        UIView.performWithoutAnimation{
//            lblHeaderTitle.text = "Where do you want to check in?"
            txtFldFlight.placeholder = "Red Line, Grand Canyon Rail…"
            txtFldFlight2.placeholder = "Red Line, Grand Canyon Rail…"

            self.layoutIfNeeded()
        }
        self.txtFldAirpot.text = defaultCityName
        self.btnCanceltxtFlight.isHidden = true
        self.btnCanceltxtFlight2.isHidden = true

        self.endEditing(true)
        viewDismissKeyboard()
        setHeightOfAutoCompleteTableview(withAnimation: false)
        activeScreen = .subway
        intiViewWithAnimation()
    }
    
    @IBAction func actionTabPlace(_ sender: UIButton) {
        AppDelegate.shared.activeCheckin = .place
        UIView.performWithoutAnimation{
//            lblHeaderTitle.text = "Where do you want to check in?"
            txtFldFlight.placeholder = "Starbucks, Central Park..."
            txtFldFlight2.placeholder = "Starbucks, Central Park..."

            self.layoutIfNeeded()
        }
        self.txtFldAirpot.text = ""
        self.btnCanceltxtFlight.isHidden = true
        self.btnCanceltxtFlight2.isHidden = true

        self.endEditing(true)
        viewDismissKeyboard()
        
        setHeightOfAutoCompleteTableview(withAnimation: true)
        
        activeScreen = .place
        intiViewWithAnimation()
    }
    
    
    @IBAction func actionTabStreet(_ sender: UIButton) {
        AppDelegate.shared.activeCheckin = .street
        UIView.performWithoutAnimation{
//            lblHeaderTitle.text = "Where do you want to check in?"
            txtFldFlight.placeholder = "8th Steet, Park Ave..."
            txtFldFlight2.placeholder = "8th Steet, Park Ave..."

            self.layoutIfNeeded()
        }
        self.txtFldAirpot.text = ""
        self.btnCanceltxtFlight.isHidden = true
        self.btnCanceltxtFlight2.isHidden = true

        self.endEditing(true)
        viewDismissKeyboard()
        
        setHeightOfAutoCompleteTableview(withAnimation: true)
        
        activeScreen = .street
        intiViewWithAnimation()
        
    }
    
    
    @IBAction func actionTabBus(_ sender: UIButton) {
        AppDelegate.shared.activeCheckin = .bus
        UIView.performWithoutAnimation{
//            lblHeaderTitle.text = "Where do you want to check in?"
            txtFldFlight.placeholder = "M22, Badger Bus..."
            txtFldFlight2.placeholder = "M22, Badger Bus..."

            self.layoutIfNeeded()
        }
        self.txtFldAirpot.text = defaultCityName
        self.btnCanceltxtFlight.isHidden = true
        self.btnCanceltxtFlight2.isHidden = true

        self.endEditing(true)
        viewDismissKeyboard()
        setHeightOfAutoCompleteTableview(withAnimation: false)
        
        activeScreen = .bus
        intiViewWithAnimation()
    }
    
    @IBAction func actionTabTrain(_ sender: UIButton) {
        AppDelegate.shared.activeCheckin = .train
//        UIView.performWithoutAnimation{
//            lblHeaderTitle.text = "Where do you want to check in?"
//            self.layoutIfNeeded()
//        }
        self.txtFldAirpot.text = defaultCityName
        self.btnCanceltxtFlight.isHidden = true
        self.btnCanceltxtFlight2.isHidden = true

        self.endEditing(true)
        viewDismissKeyboard()
        
        self.setHeightOfAutoCompleteTableview(withAnimation: true)
        activeScreen = .train
        intiViewWithAnimation()
    }
    
    
    @IBAction func actionBtnToday(_ sender: UIButton) {
        viewDismissKeyboard()
//        setHeightOfAutoCompleteTableview(withAnimation: true)
        timeInterVal = .today
        lblTodayDate.text = retrieveTodayDate()
        lblTdate.text = retrieveTodayDate()
        
        if activeScreen == .airplane{
            lblDateTitle.text = "Select your flight's departure date."
            
        }else{
            lblDateTitle.text = "Select your check-in date."
        }
        
        setTimeInterValInfo()
    }
    
    @IBAction func actionBtnYesterday(_ sender: UIButton) {
        viewDismissKeyboard()
//        setHeightOfAutoCompleteTableview(withAnimation: true)
        timeInterVal = .yesterday
        lblTodayDate.text = retrieveYesterDayDate()
        lblTdate.text = retrieveYesterDayDate()
        if activeScreen == .airplane{
            lblDateTitle.text = "Select your flight's departure date."
        }else{
            lblDateTitle.text = "Select your check-in date."
        }
        setTimeInterValInfo()
    }
    
    
    @IBAction func actionSubAtrribute(_ sender: UITextField) {
        activePlaceTextFld = .subAtrributes
        if activeScreen == .street {
            txtFldSubAtrributes.resignFirstResponder()
            
            PlaceViewAPI.sharedInstance.Show(activeAction: .streetCreate) { [weak self](city, country, shortCountryName,address,placeID, selectedItemDict)  in
                self?.txtFldSubAtrributes.text = city
                UserDefaults.standard.set(placeID, forKey: "placeId")
            }
        }
    }
    
    
    @IBAction func actionLocation(_ sender: UITextField) {
        txtFldAirpot.resignFirstResponder()
        activePlaceTextFld = .txtLocation
        
       
        PlaceViewAPI.sharedInstance.Show(activeAction: .city) { [weak self](city, country, shortCountryName,address,placeID, selectedItemDict)  in
            self?.txtFldAirpot.text = city
            self?.placeAPIDetailData1 = selectedItemDict
            self?.defaultCityName = city
            self?.txtFldFlight.text = ""
            self?.txtFldFlight2.text = ""

            self?.btnCanceltxtFlight.isHidden = true
            self?.btnCanceltxtFlight2.isHidden = true

            UserDefaults.standard.set(placeID, forKey: "placeId")
            // call API to get the state name:
            PlaceDetailClass.getDetailByPlaceID(placeID: placeID) { (country, shortName,stateNameLong, formatedAddress,detailData)  in
                self?.selectedCountry = country
                self?.currentStateLocationName = shortName
                self?.stateNameLong = stateNameLong
                self?.placeAPIDetailData2 = detailData
                currentCountryCode = self?.getCountryCodeFrom(dict: detailData) ?? ""
            }
        }
    }
    
    
    @IBAction func actionFlightLocation(_ sender: UITextField) {
        //active place only for place , street
        
        if activeScreen == CreatePostActiveScreen.place {
            activePlaceTextFld = .txtFlight
            txtFldFlight.resignFirstResponder()
            txtFldFlight2.resignFirstResponder()

            //API 1:
            PlaceViewAPI.sharedInstance.Show(activeAction: .placeCreate) {  [weak self](city, country, shortCountryName,address,placeID, selectedItemDict)  in
                self?.txtFldFlight.text = city
                self?.txtFldFlight2.text = city

                self?.placeAPIDetailData1 = selectedItemDict
                UserDefaults.standard.set(selectedItemDict, forKey: "placeAPIDetailData1")
                UserDefaults.standard.set(placeID, forKey: "placeId")
                // get formated address from 2nd API:
                // call API to get the state name:
                PlaceDetailClass.getDetailByPlaceID(placeID: placeID) { (country, shortName,stateNameLong, formatedAddress,dictItem)   in
                    self?.viewPlaceContainer.isHidden = false
                    self?.txtFldSubAtrributes.text = formatedAddress
                    self?.lblPlaceAttribute.isHidden = false
                    self?.lblPlaceAttribute.text = formatedAddress
                    self?.placeAPIDetailData2 = dictItem
                    UserDefaults.standard.set(dictItem, forKey: "placeAPIDetailData2")
                    currentCountryCode = (self?.getCountryCodeFrom(dict: dictItem) ?? "") // get country code
                }
            }
            
        }else if activeScreen == CreatePostActiveScreen.street {
            activePlaceTextFld = .txtFlight
            txtFldFlight.resignFirstResponder()
            txtFldFlight2.resignFirstResponder()

            PlaceViewAPI.sharedInstance.Show(activeAction: .streetCreate) {  [weak self](city, country, shortCountryName,address,placeID, selectedItemDict)  in
                
                self?.txtFldFlight.text = city
                self?.txtFldFlight2.text = city

                self?.placeAPIDetailData1 = selectedItemDict
                UserDefaults.standard.set(selectedItemDict, forKey: "placeAPIDetailData1")
                UserDefaults.standard.set(placeID, forKey: "placeId")
                // get formated address from 2nd API:
                // call API to get the state name:
                PlaceDetailClass.getDetailByPlaceID(placeID: placeID) { (country, shortName,stateNameLong, formatedAddress,dictItem)   in
                    self?.placeAPIDetailData2 = dictItem
                    UserDefaults.standard.set(dictItem, forKey: "placeAPIDetailData2")
                    currentCountryCode = self?.getCountryCodeFrom(dict: dictItem) ?? ""
                }
            }
            
        }
    }
    
    
    @IBAction func actionNext(_ sender: UIButton) {
       checkValidation()
    }
    
    func checkValidation(){
        var strFlight = ""
        viewDismissKeyboard()
        setHeightOfAutoCompleteTableview(withAnimation: true)
        if activeScreen == .subway || activeScreen == .bus {
            strFlight  = txtFldFlight.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            UserDefaults.standard.set(txtFldFlight.text, forKey: "attribute_text")
            UserDefaults.standard.set(txtFldAirpot.text, forKey: "attribute_city")
        } else {
            strFlight  = txtFldFlight2.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            UserDefaults.standard.removeObject(forKey: "attribute_text")
            UserDefaults.standard.removeObject(forKey: "attribute_city")
        }
        
        var strLocation  = txtFldAirpot.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strSubAttribute  = txtFldSubAtrributes.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        var strTitle = ""
        
        switch activeScreen {
        case .airplane:
            strTitle = AlertMessages.shareInstance.flightTitleAlert
            break
            
        case .subway:
            strTitle = AlertMessages.shareInstance.subwayTitleAlert
            break
            
        case .place:
            strTitle = AlertMessages.shareInstance.placeTitleAlert
            break
            
        case .street:
            strTitle = AlertMessages.shareInstance.streetTitleAlert
            break
            
        case .bus:
            strTitle = AlertMessages.shareInstance.busTitleAlert
            break
            
        case .train:
            strTitle = AlertMessages.shareInstance.trainTitleAlert
            break
            
        default:
            break
        }
        
        
        guard strFlight.count > 0  else {
            SnackBar.sharedInstance.show(message: strTitle, showMsgAt: .bottom);
            return }
        
        if activeScreen == .airplane {
            guard strFlight.count > 2 else {
                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.flightMesage)", showMsgAt: .bottom);
                return
            }
        }
        
        if activeScreen != .place && activeScreen != .street && activeScreen != .airplane{
            guard strLocation.count > 0  else {
                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.enterField) \(lblAirpot.text!) \(AlertMessages.shareInstance.field).", showMsgAt: .bottom);return }
        }
        
        if activeScreen ==  .train {
            guard strSubAttribute.count > 0  else { lblSubAtributeLine.layer.shake(); return }
        }
        
        if activeScreen == .bus || activeScreen == .subway {
            if selectedCountry.lowercased() == "United States".lowercased() || selectedCountry.lowercased() == "US".lowercased(){
                SelectedCityFullNameCreatePost = strLocation
                strLocation = currentStateLocationName // use state name insted of country.
            }else{
                strLocation = selectedCountry
            }
            
        }else{
            SelectedCityFullNameCreatePost = ""
        }
        
        var time  = ""
        if timeInterVal == .today { time = "today" }else{ time = "yesterday" }
        UserDefaults.standard.set(strFlight, forKey: "attribute")
        //Place and street empty
        if activeScreen == .place || activeScreen == .street  || activeScreen == .airplane{
            // value2 = location , value3 = subatribute
            self.removeWithAnimationWithValue(value1: strFlight, value2: "", value3: "", value4: time)
            
        }else if activeScreen == .bus || activeScreen == .subway{
            call_checkCounty(value1: strFlight, value2: strLocation, value3: strSubAttribute, value4: time)
        }
    }
    
    @objc func moveToStep2(_ notification: Notification){
        checkValidation()
    }
    
    func retrieveTodayDate()->String{
        let strTime = Date().string(format: "dd/MM/yyyy") // today
        
        if countryDialCode == 1 { // for US : 12 hours time foemate
            return  convertDateFormaterForUS(strTime)
        }else{
            return convertDateFormaterForNonUS(strTime)
        }
    }
    
    
    func retrieveYesterDayDate()-> String{
        let strTime = (Date() - 1).string(format: "dd/MM/yyyy") // today
        
        if countryDialCode == 1 { // for US : 12 hours time foemate
            return  convertDateFormaterForUS(strTime)
        }else{
            return convertDateFormaterForNonUS(strTime)
        }
    }
    
    
    
    //MARK:-
    //MARK:- Methods
    
    func setTxtHint(){
        
        switch activeScreen {
        case .airplane:
           
            txtFldFlight.placeholder = "DL1234, AA2345…"
            txtFldFlight2.placeholder = "DL1234, AA2345…"

            break;
            
        case .subway:
            txtFldFlight.placeholder = "Red Line, Grand Canyon Rail…"
            txtFldFlight2.placeholder = "Red Line, Grand Canyon Rail…"

            break;
            
        case .place:
            txtFldFlight.placeholder = "Starbucks, Central Park..."
            txtFldFlight2.placeholder = "Starbucks, Central Park..."

            break;
            
        case .street:
            txtFldFlight.placeholder = "8th Steet, Park Ave..."
            txtFldFlight2.placeholder = "8th Steet, Park Ave..."

            break;
            
        case .bus:
            txtFldFlight.placeholder = "M22, Badger Bus..."
            txtFldFlight2.placeholder = "M22, Badger Bus..."

            break;
            
        case .train:
           
            break;
        case .none:
            break;
        }
    }
    
    func intiViewWithAnimation(){
        lblPlaceAttribute.isHidden = true
        viewPlaceContainer.isHidden = true
        deselectViews()
        viewDismissKeyboard()
        txtFldFlight.text = ""
        txtFldFlight2.text = ""

        //txtFldAirpot.text = ""
        txtFldSubAtrributes.text = ""
        
        viewTabAirplane1Container.isHidden = true
        viewTabSubway2Container.isHidden = true
        viewTabPlace3Container.isHidden = true
        viewTabStreet4Container.isHidden = true
        viewTabBus5Container.isHidden = true
        viewTabTrain6Container.isHidden = true
        
        
        //-------: Place filter :--------
        ViewPlaceFilterContainer.isHidden = true
        ViewPlaceFilterContainer2.isHidden = true

        viewSelectedFilterContainer.isHidden = true
        viewSelectedFilterContainer2.isHidden = true

        if activeScreen != .place {Singleton.shared.selectedFilterPlace.selectedValue = ""}
        //-------------------------------
        if let id = UserDefaults.standard.value(forKey: "eventType") as? Int {
            if id == 1 {
                self.lblHeaderTitle.text = "Tell us where it happened."
            } else {
                self.lblHeaderTitle.text = "Where do you want to check in?"
            }
        }
        
        switch activeScreen {
        case .airplane:
            
            self.viewLocationContainer.alpha = 0
            self.viewSubAtrributes.alpha = 0
            
//            self.viewTabAirplane1Container.isHidden = false
//            self.airport_topCons.constant = 10
//            self.viewTabAirplane1Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//            self.viewPlane.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//            self.layoutIfNeeded()
//            self.viewSubAtrributes.isHidden = false
            
            UIView.performWithoutAnimation {
                self.viewTabAirplane1Container.isHidden = false
                self.airport_topCons.constant = 10
                self.viewTabAirplane1Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                self.viewPlane.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                self.layoutIfNeeded()
            }
            
            self.viewSubAtrributes.isHidden = false
            
//            UIView.animate(withDuration: 0.5, animations: {
//                self.viewTabAirplane1Container.isHidden = false
//                self.airport_topCons.constant = -70.5
//                self.viewTabAirplane1Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.viewPlane.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.layoutIfNeeded()
//            }) { (_) in
//                self.viewSubAtrributes.isHidden = false
//            }
            
            //----- : Remove Place data
            placeAPIDetailData1 = NSDictionary()
            placeAPIDetailData2 = NSDictionary()
            //-------
//            lblHeaderTitle.text = "Where do you want to check in?"
            lblFlightTitle.text = "Enter the flight #"
            lblFlightTitle2.text = "Enter the flight #"
            self.lblTitleSelcected.text = "On a Plane."
            lblAirpot.text = "Destination"
            //  lblDateTitle.text = "Departure date"
            lblSubAtrributeTitle.text = "Crossing (optional)"
            
            if timeInterVal == .today{
                lblDateTitle.text = "Select your flight's departure date."
            }else{
                
                lblDateTitle.text = "Select your flight's departure date."
            }
            viewBaseContainer.isHidden = true
            viewBase2Container.isHidden = false
            break;
            
        case .subway:
            self.viewTabSubway2Container.isHidden = false
            self.airport_topCons.constant = 10
            self.viewSubAtrributes.alpha = 1
            self.viewLocationContainer.alpha = 1
            self.viewTabSubway2Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.viewSubway.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.layoutIfNeeded()
            self.viewSubAtrributes.isHidden = false
            
//            UIView.animate(withDuration: 0.5, animations: {
//                self.viewTabSubway2Container.isHidden = false
//                self.airport_topCons.constant = -45
//                self.viewSubAtrributes.alpha = 1
//                self.viewLocationContainer.alpha = 1
//                self.viewTabSubway2Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.viewSubway.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.layoutIfNeeded()
//            }) { (_) in
//                self.viewSubAtrributes.isHidden = false
//            }
            
            //----- : Remove Place data
            placeAPIDetailData1 = NSDictionary()
            placeAPIDetailData2 = NSDictionary()
            //-------
            self.lblTitleSelcected.text = "On a train, subway, tram, light rail, trolley or streetcar."
            lblFlightTitle.text = "Enter the line."
            lblFlightTitle2.text = "Enter the line."
            lblAirpot.text = "City"
            // lblDateTitle.text = "Date"
            lblSubAtrributeTitle.text = "Crossing (optional)"
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            viewBaseContainer.isHidden = false
            viewBase2Container.isHidden = true
            break;
            
        case .place:
            //-------: Place filter :--------
            ViewPlaceFilterContainer.isHidden = false
            ViewPlaceFilterContainer2.isHidden = false

            if Singleton.shared.selectedFilterPlace.selectedValue != ""{
                viewSelectedFilterContainer.isHidden = false
                viewSelectedFilterContainer2.isHidden = false

                lblSelectedFilterTitle.text = Singleton.shared.selectedFilterPlace.selectedValue
                lblSelectedFilterTitle2.text = Singleton.shared.selectedFilterPlace.selectedValue

            }
            //-------------------------------
            
            self.viewSubAtrributes.alpha = 0
            self.viewLocationContainer.alpha = 0
            
            UIView.performWithoutAnimation {
                self.viewTabPlace3Container.isHidden = false
                self.airport_topCons.constant = 10
                self.viewTabPlace3Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                self.viewPlace.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                self.layoutIfNeeded()
            }
            self.viewSubAtrributes.isHidden = false
            
//            UIView.animate(withDuration: 0.5, animations: {
//                self.viewTabPlace3Container.isHidden = false
//                self.airport_topCons.constant = -70.5
//                self.viewTabPlace3Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.viewPlace.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.layoutIfNeeded()
//            }) { (_) in
//                self.viewSubAtrributes.isHidden = false
//            }
            
            lblFlightTitle.text = "Enter the name of the place."
            lblFlightTitle2.text = "Enter the name of the place."
            self.lblTitleSelcected.text = "At a specific site or location."
            lblAirpot.text = "City"
            //lblDateTitle.text = "Date"
            lblSubAtrributeTitle.text = "Crossing (optional)"
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            viewBaseContainer.isHidden = true
            viewBase2Container.isHidden = false
            break;
            
        case .street:
            self.viewSubAtrributes.alpha = 0
            self.viewLocationContainer.alpha = 0
            
            self.viewTabStreet4Container.isHidden = false
            self.viewSubAtrributes.isHidden = false
            self.viewTabStreet4Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.viewStreet.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.airport_topCons.constant = 10
            
//            UIView.animate(withDuration: 0.5) {
//                self.viewTabStreet4Container.isHidden = false
//                self.viewSubAtrributes.isHidden = false
//                self.viewTabStreet4Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.viewStreet.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.airport_topCons.constant = -70.5
//
//                self.layoutIfNeeded()
//            }
            
            lblFlightTitle.text = "Enter the name of the street."
            lblFlightTitle2.text = "Enter the name of the street."

            self.lblTitleSelcected.text = "On the road, or while walking outside."
            lblAirpot.text = "City"
            //lblDateTitle.text = "Date"
            lblSubAtrributeTitle.text = "Crossing (optional)"
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            viewBaseContainer.isHidden = true
            viewBase2Container.isHidden = false
            break;
            
        case .bus:
            
            self.viewTabBus5Container.isHidden = false
            self.airport_topCons.constant = 10
            self.viewSubAtrributes.alpha = 1
            self.viewLocationContainer.alpha = 1
            self.viewTabBus5Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.viewBus.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.layoutIfNeeded()
            self.viewSubAtrributes.isHidden = false
            
//            UIView.animate(withDuration: 0.5, animations: {
//                self.viewTabBus5Container.isHidden = false
//                self.airport_topCons.constant = -45
//                self.viewSubAtrributes.alpha = 1
//                self.viewLocationContainer.alpha = 1
//                self.viewTabBus5Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.viewBus.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.layoutIfNeeded()
//            }) { (_) in
//                self.viewSubAtrributes.isHidden = false
//            }
            
            //----- : Remove Place data
            placeAPIDetailData1 = NSDictionary()
            placeAPIDetailData2 = NSDictionary()
            //-------
            
            lblFlightTitle.text = "Enter the line."
            lblFlightTitle2.text = "Enter the line."

            self.lblTitleSelcected.text = "On a bus."
            lblAirpot.text = "City"
            //   lblDateTitle.text = "Date"
            lblSubAtrributeTitle.text = "Crossing (optional)"
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            viewBaseContainer.isHidden = false
            viewBase2Container.isHidden = true
            break;
            
        case .train:
            self.viewTabTrain6Container.isHidden = false
            self.viewTabTrain6Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.viewSubAtrributes.isHidden = false
            self.viewSubAtrributes.alpha = 1
            self.viewLocationContainer.alpha = 1
            self.airport_topCons.constant = 119.5
            self.layoutIfNeeded()
            self.viewSubAtrributes.isHidden = false
            
//            UIView.animate(withDuration: 0.5, animations: {
//                self.viewTabTrain6Container.isHidden = false
//                self.viewTabTrain6Container.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.viewSubAtrributes.isHidden = false
//                self.viewSubAtrributes.alpha = 1
//                self.viewLocationContainer.alpha = 1
//                self.airport_topCons.constant = 119.5
//                self.layoutIfNeeded()
//            }) { (_) in
//                //  self.viewSubAtrributes.isHidden = false
//            }
            
            //----- : Remove Place data
            placeAPIDetailData1 = NSDictionary()
            placeAPIDetailData2 = NSDictionary()
            //-------
            
            lblFlightTitle.text = "Route"
            lblFlightTitle2.text = "Route"

            lblAirpot.text = "City"
            // lblDateTitle.text = "Date"
            lblSubAtrributeTitle.text = "Train #"
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            break;
        case .none:
            break;
        }
    }
    
    func setTimeInterValInfo(){
        
        if timeInterVal == .today{
            viewGradientToday.isHidden = false
            viewGradientYesterday.isHidden = true
        }else{
            viewGradientToday.isHidden = true
            viewGradientYesterday.isHidden = false
        }
    }
    
    func showWithAnimation(){
        //============== Show with animation
        rootContainer.alpha = 1
        
        UIView.animate(withDuration: 0, animations: {
            self.rootContainer.alpha = 1
        }) { (_) in
        }
    }
    
    func removeWithAnimation(){
        rootContainer.alpha = 1
        
        UIView.animate(withDuration: 0.7, animations: {
            self.rootContainer.alpha = 0
            self.layoutIfNeeded(
            )
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    func removeWithAnimationWithValue(value1 : String,value2 : String,value3: String, value4 : String){
        rootContainer.alpha = 1
        
        if self.activeScreen == .bus || self.activeScreen == .subway {
            self.onCloser(self.activeScreen,value1, value2, value3, value4,self.placeAPIDetailData1,self.placeAPIDetailData2, self.isCountryFound,self.selectedCountry,self.currentStateLocationName)
        }else{
            self.onCloser(self.activeScreen,value1, value2, value3, value4,self.placeAPIDetailData1,self.placeAPIDetailData2, false,self.selectedCountry,self.currentStateLocationName)
        }
        
//        UIView.animate(withDuration: 0.7, animations: {
//            self.rootContainer.alpha = 0
//
//            self.layoutIfNeeded()
//        }) { (true) in
//            // self.removeFromSuperview()
//            self.rootContainer.alpha = 1
//
//            if self.activeScreen == .bus || self.activeScreen == .subway {
//                self.onCloser(self.activeScreen,value1, value2, value3, value4,self.placeAPIDetailData1,self.placeAPIDetailData2, self.isCountryFound,self.selectedCountry,self.currentStateLocationName)
//            }else{
//                self.onCloser(self.activeScreen,value1, value2, value3, value4,self.placeAPIDetailData1,self.placeAPIDetailData2, false,self.selectedCountry,self.currentStateLocationName)
//            }
//        }
    }
    
    func intiViewWithoutAnimation(){
        endEditing(true)
        lblPlaceAttribute.isHidden = true
        viewPlaceContainer.isHidden = true
        setHeightOfAutoCompleteTableview(withAnimation: false)
        
        viewTabAirplane1Container.isHidden = true
        viewTabSubway2Container.isHidden = true
        viewTabPlace3Container.isHidden = true
        viewTabStreet4Container.isHidden = true
        viewTabBus5Container.isHidden = true
        viewTabTrain6Container.isHidden = true
        
        txtFldFlight.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        txtFldFlight2.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)

        //-------: Place filter :--------
        ViewPlaceFilterContainer.isHidden = true
        ViewPlaceFilterContainer2.isHidden = true

        viewSelectedFilterContainer.isHidden = true
        viewSelectedFilterContainer2.isHidden = true

        if activeScreen != .place {Singleton.shared.selectedFilterPlace.selectedValue = ""}
        //-------------------------------
        
        if let id = UserDefaults.standard.value(forKey: "eventType") as? Int {
            if id == 1 {
                self.lblHeaderTitle.text = "Tell us where it happened."
            } else {
                self.lblHeaderTitle.text = "Where do you want to check in?"
            }
        }
        
        
        switch activeScreen {
        case .airplane:
//            UIView.performWithoutAnimation {
//                lblHeaderTitle.text = "Where do you want to check in?"
//                self.layoutIfNeeded()
//            }
            self.viewTabAirplane1Container.isHidden = false
            self.airport_topCons.constant = 10
            self.viewSubAtrributes.alpha = 1
            self.viewLocationContainer.alpha = 0
            self.viewSubAtrributes.isHidden = true
            self.lblTitleSelcected.text = "On a Plane."
            lblFlightTitle.text = "Enter the flight #"
            lblFlightTitle2.text = "Enter the flight #"

            lblAirpot.text = "Destination"
            // lblDateTitle.text = "Departure date"
//            lblSubAtrributeTitle.text = "Crossing (optional)"
            
            if timeInterVal == .today{
                lblDateTitle.text = "Select your flight's departure date."
            }else{
                
                lblDateTitle.text = "Select your flight's departure date."
            }
            
            //----- : Remove Place data
            placeAPIDetailData1 = NSDictionary()
            placeAPIDetailData2 = NSDictionary()
            //-------
            self.setHeightOfAutoCompleteTableview(withAnimation: false)
            viewBaseContainer.isHidden = true
            viewBase2Container.isHidden = false
            break;
            
        case .subway:
//            UIView.performWithoutAnimation {
//                lblHeaderTitle.text = "Where do you want to check in?"
//                self.layoutIfNeeded()
//            }
            
            self.viewTabSubway2Container.isHidden = false
            self.airport_topCons.constant = 10
            self.viewSubAtrributes.alpha = 1
            self.viewLocationContainer.alpha = 1
            self.viewSubAtrributes.isHidden = false
            
            //----- : Remove Place data
            placeAPIDetailData1 = NSDictionary()
            placeAPIDetailData2 = NSDictionary()
            //-------
            self.lblTitleSelcected.text = "On a train, subway, tram, light rail, trolley or streetcar."
            lblFlightTitle.text = "Enter the line."
            lblFlightTitle2.text = "Enter the line."

            lblAirpot.text = "City"
            //lblDateTitle.text = "Date"
            lblSubAtrributeTitle.text = "Crossing (optional)"
            
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            viewBaseContainer.isHidden = false
            viewBase2Container.isHidden = true
            break;
            
        case .place:
            //-------: Place filter :--------
            ViewPlaceFilterContainer.isHidden = false
            ViewPlaceFilterContainer2.isHidden = false

            if Singleton.shared.selectedFilterPlace.selectedValue != "" {
                viewSelectedFilterContainer.isHidden = false
                viewSelectedFilterContainer2.isHidden = false

                lblSelectedFilterTitle.text = Singleton.shared.selectedFilterPlace.selectedValue
                lblSelectedFilterTitle2.text = Singleton.shared.selectedFilterPlace.selectedValue

            }
            //-------------------------------
            
//            UIView.performWithoutAnimation {
//                lblHeaderTitle.text = "Where do you want to check in?"
//                self.layoutIfNeeded()
//            }
            
            self.viewTabPlace3Container.isHidden = false
            self.airport_topCons.constant = 10
            self.viewSubAtrributes.alpha = 0
            self.viewLocationContainer.alpha = 0
            self.viewSubAtrributes.isHidden = false
            
            // formated address:
            if  let formatedAdd =  placeAPIDetailData2.object(forKey: "formatted_address") as? String  {
                self.lblPlaceAttribute.isHidden = false
                self.lblPlaceAttribute.text = formatedAdd
            }
            
            lblFlightTitle.text = "Enter the name of the place."
            lblFlightTitle2.text = "Enter the name of the place."

            self.lblTitleSelcected.text = "At a specific site or location."
            lblAirpot.text = "City"
            // lblDateTitle.text = "Date"
            self.setHeightOfAutoCompleteTableview(withAnimation: false)
            
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            viewBaseContainer.isHidden = true
            viewBase2Container.isHidden = false
            break;
            
        case .street:
//            UIView.performWithoutAnimation {
//                lblHeaderTitle.text = "Where do you want to check in?"
//                self.layoutIfNeeded()
//            }
            self.viewTabStreet4Container.isHidden = false
            self.viewSubAtrributes.isHidden = false
            self.viewSubAtrributes.alpha = 0
            self.viewLocationContainer.alpha = 0
            self.airport_topCons.constant = 10
            
            lblFlightTitle.text = "Enter the name of the street."
            lblFlightTitle2.text = "Enter the name of the street."

            self.lblTitleSelcected.text = "On the road, or while walking outside."
            lblAirpot.text = "City"
            
            self.setHeightOfAutoCompleteTableview(withAnimation: false)
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            viewBaseContainer.isHidden = true
            viewBase2Container.isHidden = false
            break;
            
            
        case .bus:
//            UIView.performWithoutAnimation {
//                lblHeaderTitle.text = "Where do you want to check in?"
//                self.layoutIfNeeded()
//            }
            self.viewTabBus5Container.isHidden = false
            self.airport_topCons.constant = 10
            self.viewSubAtrributes.alpha = 1
            self.viewLocationContainer.alpha = 1
            self.viewSubAtrributes.isHidden = false
            
            //----- : Remove Place data
            placeAPIDetailData1 = NSDictionary()
            placeAPIDetailData2 = NSDictionary()
            //-------
            
            lblFlightTitle.text = "Enter the line."
            lblFlightTitle2.text = "Enter the line."

            self.lblTitleSelcected.text = "On a bus."
            lblAirpot.text = "City"
            // lblDateTitle.text = "Date"
            
            lblSubAtrributeTitle.text = "Crossing (optional)"
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            viewBaseContainer.isHidden = false
            viewBase2Container.isHidden = true
            break;
            
        case .train:
//            UIView.performWithoutAnimation {
//                lblHeaderTitle.text = "Where do you want to check in?"
//                self.layoutIfNeeded()
//            }
            
            self.viewTabTrain6Container.isHidden = false
            self.viewSubAtrributes.isHidden = false
            self.viewSubAtrributes.alpha = 1
            self.viewLocationContainer.alpha = 1
            self.airport_topCons.constant = 119.5
            self.layoutIfNeeded()
            
            //----- : Remove Place data
            placeAPIDetailData1 = NSDictionary()
            placeAPIDetailData2 = NSDictionary()
            //-------
            
            lblFlightTitle.text = "Route"
            lblFlightTitle2.text = "Route"

            lblAirpot.text = "City"
            //lblDateTitle.text = "Date"
            lblSubAtrributeTitle.text = "Train #"
            
            if timeInterVal == .today{
                lblDateTitle.text = "Select your check-in date."
            }else{
                
                lblDateTitle.text = "Select your check-in date."
            }
            break;
        case .none:
            break;
        }
    }
    
    
    func convertDateFormaterForUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.string(from: date!)
    }
    
    func convertDateFormaterForNonUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
}


//MARK:-
//MARK:- place api :

extension CreatePostView1 : GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
        switch activePlaceTextFld{
        
        case .txtLocation:
            txtFldAirpot.text = place.name
            break;
        
        default:
            break;
        }
        presentViewController?.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        presentViewController?.dismiss(animated: true, completion: nil)
    }
}


extension CreatePostView1 {
    
    func getCountryCodeFrom(dict : NSDictionary)-> String{
        
        if let arrComponents = dict.object(forKey: "address_components") as? [Any]{
            
            for dictItem in arrComponents{
                
                if let dictComponent = dictItem as? NSDictionary{
                    
                    if let arrType = dictComponent.object(forKey: "types") as? [Any] {
                        
                        for type in arrType {
                            
                            if "\(type)" == "country" {
                                return "\(dictComponent.object(forKey: "short_name") ?? "")"
                            }
                        }
                    }
                }
            }
        }
        return ""
    }
    
    func getStateNameFrom(dict : NSDictionary)-> (shortname: String,longName: String){
        
        if let arrComponents = dict.object(forKey: "address_components") as? [Any]{
            
            for dictItem in arrComponents{
                
                if let dictComponent = dictItem as? NSDictionary{
                    
                    if let arrType = dictComponent.object(forKey: "types") as? [Any] {
                        
                        for type in arrType {
                            
                            if "\(type)" == "administrative_area_level_1" {
                                return(shortname: "\(dictComponent.object(forKey: "short_name") ?? "")",longName: "\(dictComponent.object(forKey: "long_name") ?? "")")
                                 
                            }
                        }
                    }
                }
            }
        }
        return (shortname: "",longName: "")
    }

}



/*
 MARK:- Country code delegate :
 */
extension CreatePostView1 : LocationDelegate {
    
    func didReceivedCurrentLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        location.getCitName(lat: latitude, long: longitude) { [self] (cityName,countryName,stateName,countryCode)   in
            
            if self.actionScreenUpdate == .new {
                self.selectedCountry = countryName
                self.currentStateLocationName = stateName
                self.defaultCityName = cityName
                currentCountryCode = countryCode
            }
            
            
            if self.defaultCityName.count == 0 {
                self.selectedCountry = countryName
                self.currentStateLocationName = stateName
                self.defaultCityName = cityName
                self.txtFldAirpot.text = cityName
            }
            
            if self.actionScreenUpdate == .new{ // only at update
                self.txtFldAirpot.text = cityName
            }
        }
    }
    
    
    func didReceivedtLocationError(error: String) {
        print(error)
    }
}


// keyboard hide with value:
extension CreatePostView1 {
    
    func viewDismissKeyboard() {
        //  endEditing(true)
        removeAllSuggestionList()
        setHeightOfAutoCompleteTableview(withAnimation: true)
    }
    
    func removeAllSuggestionList() {
        createPostVM.arrAutoComplete.removeAll()
    }
}

//MARK:-
//MARK:- Auto complete , Drop down list:

extension CreatePostView1 : UITableViewDelegate, UITableViewDataSource{
    
    func setHeightOfAutoCompleteTableview(withAnimation : Bool){
        
        if withAnimation{
            tblViewTicketAutoComplete.reloadData()
            tblViewTicketAutoComplete.updateConstraints()
            tblViewTicketAutoComplete.layoutIfNeeded()
            
            if self.tblViewTicketAutoComplete.contentSize.height > dropDownHeight {
                UIView.animate(withDuration: 0.2) {
                    self.tblViewTicketAutoComplete_h.constant = self.dropDownHeight
                    self.layoutIfNeeded()
                }
            }else{
                UIView.animate(withDuration: 0.2) {
                    self.tblViewTicketAutoComplete_h.constant = self.tblViewTicketAutoComplete.contentSize.height
                    self.layoutIfNeeded()
                }
            }
        }else{// without animation :
            
            tblViewTicketAutoComplete.reloadData()
            tblViewTicketAutoComplete.updateConstraints()
            tblViewTicketAutoComplete.layoutIfNeeded()
            
            if self.tblViewTicketAutoComplete.contentSize.height > dropDownHeight {
                self.tblViewTicketAutoComplete_h.constant = dropDownHeight
            }else{
                self.tblViewTicketAutoComplete_h.constant = self.tblViewTicketAutoComplete.contentSize.height
            }
        }
        
        // load at the end of the tableview
        let lastRow: Int = self.tblViewTicketAutoComplete.numberOfRows(inSection: 0) - 1
        let indexPath = IndexPath(row: lastRow, section: 0);
        if indexPath.row > 0 {
            self.tblViewTicketAutoComplete.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    @objc func dismissMyKeyboard(){
       
        endEditing(true)
        setHeightOfAutoCompleteTableview(withAnimation: true)
    }
    
    
    func setupAutoCompleteView(){
        NotificationCenter.default.addObserver(self,selector:#selector(moveToStep2(_:)),name: NSNotification.Name ("moveToStep2"),object: nil)
        // registe cell :
        let nib = UINib.init(nibName: "autoCompleteCell", bundle: nil)
        tblViewTicketAutoComplete.register(nib, forCellReuseIdentifier: "autoCompleteCell")
        
        tblViewTicketAutoComplete.estimatedRowHeight = 50
        tblViewTicketAutoComplete.rowHeight = UITableView.automaticDimension
        
        tblViewTicketAutoComplete_h.constant = 0
        createPostVM.arrAutoComplete.removeAll()
    }
    
    // delegates :--------------------------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return createPostVM.arrAutoComplete.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "autoCompleteCell", for: indexPath) as! autoCompleteCell
        let indexFromBottom = createPostVM.arrAutoComplete.count - indexPath.row - 1
        
        cell.lblTitle.text = createPostVM.arrAutoComplete[indexFromBottom].attribute_text
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexFromBottom = createPostVM.arrAutoComplete.count - indexPath.row - 1
        let value  = createPostVM.arrAutoComplete[indexFromBottom].attribute_text
        txtFldFlight.text = value
        txtFldFlight2.text = value

        self.tblViewTicketAutoComplete_h.constant = 0
    }
    
    func call_dropDownList(serchText : String, eventType: String)
    {
        var tempStateLongName = ""
        var tempStateShortName = ""
        if selectedCountry.lowercased() == "United States".lowercased() || selectedCountry.lowercased() == "US".lowercased(){
       
        tempStateLongName = stateNameLong
        tempStateShortName = currentStateLocationName
        
        }else{
            tempStateLongName =  selectedCountry
            tempStateShortName = ""
        }
        
        
        createPostVM.arrAutoComplete.removeAll()
        createPostVM.getAtrributeList(event_type: eventType, country: selectedCountry, state_short_name: tempStateShortName, state_long_name: tempStateLongName, search_text: serchText) { [weak self](status, message) in
            
            if status == "success"{
              
                if self?.txtFldFlight.text?.count ?? 0 > 0 || self?.txtFldFlight2.text?.count ?? 0 > 0 {
                    self?.setHeightOfAutoCompleteTableview(withAnimation: true)
                }
               
            }else{ // getting error
                
                //SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.somethingWentWrong, showMsgAt: .bottom)
            }
        }
    }
}

extension CreatePostView1 : UITextFieldDelegate {
    
    @objc func handleTextChange(_ textChange: UITextField) {
    
        if activeScreen == .subway || activeScreen == .bus{
            if textChange.text?.count ?? 0 > 0 {
                call_dropDownList(serchText: textChange.text ?? "", eventType: "\(activeScreen.rawValue + 1)")
                
            }else{
               // print("textChange : \(textChange.text!)")
                self.tblViewTicketAutoComplete_h.constant = 0
                createPostVM.arrAutoComplete.removeAll()
                self.setHeightOfAutoCompleteTableview(withAnimation: true)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 40
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if activeScreen == .subway || activeScreen == .bus{
            if newString.length > 0 {
                btnCanceltxtFlight.isHidden = false
                btnCanceltxtFlight2.isHidden = false

            }else{
                btnCanceltxtFlight.isHidden = true
                btnCanceltxtFlight2.isHidden = true

            }
        }else{
            btnCanceltxtFlight.isHidden = true
            btnCanceltxtFlight2.isHidden = true

        }
        
        guard newString.length <= maxLength else{ return false}
        return true
    }
}


/*
 MARK:- API
 */

extension CreatePostView1{
    
    func call_checkCounty(value1: String, value2: String, value3: String, value4: String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        createPostVM.checkCountryCode(country_name: selectedCountry) { [weak self](status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success200"{
                self?.isCountryFound = true
                self?.removeWithAnimationWithValue(value1: value1, value2: value2, value3: value3, value4: value4)
                
            }else if status  == "success400"{
                self?.isCountryFound = false
                AlertTheme.sharedInstance.Show(popupCategory: .normal, message: AlertMessages.shareInstance.OnlyPublicPostsAvailableInYourArea, attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["Continue","Cancel"], isBottomTxt: "") { (clicked) in
                    
                    if clicked == "Continue"{
                        self?.removeWithAnimationWithValue(value1: value1, value2: value2, value3: value3, value4: value4)
                    }
                }
                
            }else{ // getting error
                self?.isCountryFound = false
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.somethingWentWrong, showMsgAt: .bottom)
            }
            UserDefaults.standard.set(self?.isCountryFound, forKey: "isCountryFound")
        }
    }
}

extension CreatePostView1 {
    
    func getPlaceFilterOption(){
      
        createPostVM.getFilterOptionData { (status, message) in }
            //---------------------------------------------------
        }
    }

