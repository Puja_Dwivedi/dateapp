//
//  SelectPostTypeVC.swift
//  MyFirstApp
//
//  Created by cis on 21/06/23.
//  Copyright © 2023 cis. All rights reserved.
//

import UIKit
import SDWebImage

protocol PostTypeSelectionDelegate{
    func postTypeSelected(_ id: Int)
}

class SelectPostTypeVC: UIViewController {
    
    var delegate: PostTypeSelectionDelegate?

    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var imgProfile: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppDelegate.shared.selectedPostId == 0 {
            initView()
            getProfileInfo()
        } else {
            selectedPostType(id: AppDelegate.shared.selectedPostId)
        }
    }
    
    @IBAction func actionProfileImageButton(_ sender: UIButton) {
        let obj  = ImagePickerView()
        obj.show(self, preview: (isPreview: false, image: UIImage())) { [weak self](selectedImage) in
            let imgData = selectedImage.image?.jpegData(compressionQuality: 1.0)!
            self?.imgProfile.image = selectedImage.image!
            self?.imgProfile.cornerRadius = (self?.imgProfile.bounds.height ?? 0.0)/2
            if selectedImage.image == UIImage(named: "img_profile_placeholder") {
                UserDefaults.standard.removeObject(forKey: "ProfileImage")
            } else {
                UserDefaults.standard.set(imgData, forKey: "ProfileImage")
            }
           
           // self?.setProfileImage(imageData: imgData!, image: selectedImage.image!)
        }
    }
    
    @IBAction func actionBackButton(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        self.navigationController?.pushViewController(vc, animated: false)
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
//        Loader.sharedInstance.showLoader(msg: "Loading...")
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            UserDefaults.standard.set(false, forKey: "isBackTapped")
////            self.removeViewController()
////            self.removeSpecificViewControllerFromStack()
//            self.navigationController?.popViewController(animated: false)
//            Loader.sharedInstance.stopLoader()
//        }
    }
    
    @IBAction func actionPostButton(_ sender: UIButton) {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        AppDelegate.shared.selectedPostId = 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.selectedPostType(id: 1)
            Loader.sharedInstance.stopLoader()
        }
    }
    
    @IBAction func actionCheckinButton(_ sender: UIButton) {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        AppDelegate.shared.selectedPostId = 2
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.selectedPostType(id: 2)
            Loader.sharedInstance.stopLoader()
        }
        
    }
    
    func selectedPostType(id: Int) {
        UserDefaults.standard.set(id, forKey: "eventType")
        self.delegate?.postTypeSelected(id)
        self.navigationController?.popViewController(animated: false)
    }
    
    func initView() {
        self.lbltitle.text = "Great. Let's get started."
        self.imgProfile.cornerRadius = self.imgProfile.bounds.height/2
        self.imgLogo.cornerRadius = self.imgLogo.bounds.height/2
        self.imgLogo.addShadow()
        self.imgProfile.addShadow()
    }
    
}

extension SelectPostTypeVC {
    func getProfileInfo()
    {
        let user_id  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        
        Loader.sharedInstance.showLoader(msg: "Loading...")
        ProfileViewModel().getProfileApi(user_id: user_id) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                print("set all the information")
                let item = dictData.object(forKey: "data") as! NSDictionary
                let profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(item.object(forKey: "picture") as! String)"
                // set info:
                self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                self.imgProfile.sd_setImage(with: URL(string: profilePicture), placeholderImage: UIImage(named: "img_profile_placeholder"))
                self.imgProfile.cornerRadius = (self.imgProfile.bounds.height)/2
                let imgData = self.imgProfile.image?.jpegData(compressionQuality: 1.0)!
                UserDefaults.standard.set(imgData, forKey: "ProfileImage")

            }else{
                //SnackBar.sharedInstance.show(message: message)
            }
        }
    }
}
