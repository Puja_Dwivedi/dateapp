//
//  CreatePostTagsClass.swift
//  MyFirstApp
//
//  Created by cis on 16/11/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

//MARK:-
//MARK:- Tags with Textfilds:

extension CreatePostVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let searchText  = textField.text! + string
        
        //print("searchText : \(searchText), \(textField.text), \(string)")
        let maxLength = 21
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        // guard newString.length <= maxLength else{ return false}
        
        if newString.length == maxLength && string == " "{
            print("add space")
        }else{
            guard newString.length < maxLength else{ return false}
        }
        // My attributes :
        if textField == txtFldMyAttributes {
            isEmptyBackFrom = false
            //Hide remove space :
            if range.location == 0 && string == " " {
                txtFldMyAttributes.text = ""
                return false;
            }else if (textField.text!.count) > 0 {
                if textField.text!.last == " " && string == " " {
                    return false
                }
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            // add new element :
            if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                txtFldMyAttributes.text = ""
                isEmptyBackFrom = true
                arrTagsMyAttributes.append(searchText.replace(string: " ", replacement: ""))
                createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
                return false
            }
            return true
            
            // My preferences:-------------------------------------------------------------------------------
        }else if textField == txtFldMyPreferences {
            isEmptyBackTo = false
            //Hide remove space :
            if range.location == 0 && string == " " {
                txtFldMyPreferences.text = ""
                return false;
            }else if (textField.text!.count) > 0 {
                if textField.text!.last == " " && string == " " {
                    return false
                }
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            // add new element :
            if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                txtFldMyPreferences.text = ""
                isEmptyBackTo = true
                arrTagsMyPreferences.append(searchText.replace(string: " ", replacement: ""))
                createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
                // createTagCloud(OnView: scrollViewTxtView, withArray: tagsArray)
                return false
            }
            return true
        }
        return true
    }
}
    //MARK: - UITextViewDelegate
extension CreatePostVC : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == txtFldTitle{
            
            let currentText = textView.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
            
            return updatedText.count <= 100 // Change limit based on your requirement.
        }
        return true
    }
}
    
extension CreatePostVC : PinTexFieldDelegate{
    //MARK:- PinTexFieldDelegate
    func didPressBackspace(textField : PinTextField){
        print("back prashed")
        
        // My attibutes :
        if textField == txtFldMyAttributes {
            
            // for removed :
            if arrTagsMyAttributes.count > 0 && (txtFldMyAttributes.text?.isEmpty == true) {
                
                if isEmptyBackFrom {
                    arrTagsMyAttributes.remove(at: arrTagsMyAttributes.count - 1)
                    createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
                }
                isEmptyBackFrom = true
            }
            
            // My Preferences :
        }else if textField == txtFldMyPreferences{
            
            // for removed :
            if arrTagsMyPreferences.count > 0 && (txtFldMyPreferences.text?.isEmpty ?? false){
                
                if isEmptyBackTo {
                    arrTagsMyPreferences.remove(at: arrTagsMyPreferences.count - 1)
                    createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
                }
                isEmptyBackTo = true
            }
        }
    }
}
    
    
    //My Attributes support methods : for Tag with textfield:
    //MARK:- CreateTagsForAttributes
extension CreatePostVC {
    func createTagsForAttributes(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let height = 30
        let spaceHeight : CGFloat = 20
        let screenWidth = UIScreen.main.bounds.size.width - 60.0
        
        for str in data  {
            
            let startstring = str
            let width = startstring.widthOfString(usingFont: UIFont(name:"IBM Plex Sans SemiBold", size: 15.0)!)
            
            // edit :
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            // let checkWholeWidth = CGFloat(xPos) +0 CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            
            if checkWholeWidth > screenWidth {
                //we are exceeding size need to change xpos
                xPos = 5.0
                ypos = ypos + spaceHeight + 8.0 + 11 // here 11 is the buttom space
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 35.0 , height: CGFloat(height)))
            //  bgView.layer.cornerRadius = 14.5
            bgView.backgroundColor = .clear//UIColor(red: 33.0/255.0, green: 135.0/255.0, blue:199.0/255.0, alpha: 1.0)
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.blue,UIColor.MyTheme.SecondColor.green], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 17.0, y: -2, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "IBM Plex Sans SemiBold", size: 15.0)
            textlable.text = startstring
            // textlable.textColor = #colorLiteral(red: 0.4509803922, green: 0.7529411765, blue: 1, alpha: 1)
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.blue.cgColor,UIColor.MyTheme.SecondColor.green.cgColor]
            bgView.addSubview(textlable)
            
            //when close buttton enable
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 23.0, height: 23.0)
                button.backgroundColor = UIColor.clear
                // button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
                button.setImage(#imageLiteral(resourceName: "closeTag"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagMyAttibutes(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldMyAttributes.isHidden = false
            
            if arrTagsMyAttributes.count == 12 {
                txtFldMyAttributes_y.constant = ypos
                txtFldMyAttributes_x.constant = xPos
            }else{
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    //we are exceeding size need to change xpos
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldMyAttributes_y.constant = ypos
                    txtFldMyAttributes_x.constant = xPos
                }else{
                    txtFldMyAttributes_y.constant = ypos
                    txtFldMyAttributes_x.constant = xPos
                }
            }
            
            txtFrom_width.constant = screenWidth - xPos + 50// here 30 is padding....
        }else{
            txtFldMyAttributes.isHidden = true
        }
        
        if arrTagsMyAttributes.count <= 12 {
            self.txtFldMyAttributes.alpha = 1
            scrollViewMyAttibutes_h.constant = ypos + 34 + 10//Here 50 is the height of the txtview, and 10 is the bottom space of
            // the scrollview
            if arrTagsMyAttributes.count == 12 {
                self.txtFldMyAttributes.alpha = 0
                self.txtFldMyAttributes.resignFirstResponder()
                self.view.endEditing(true)
            }
            
        }else{
            self.txtFldMyAttributes.alpha = 0
            self.txtFldMyAttributes.resignFirstResponder()
            self.view.endEditing(true)
            scrollViewMyAttibutes_h.constant = ypos//Here 50 is the height of the txtview, and 10 is the bottom space of
            // the scrollview
        }
    }
    
    @objc func removeTagMyAttibutes(_ sender: AnyObject) {
        
        arrTagsMyAttributes.remove(at: (sender.tag - 1))
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
    }
    
    
    //My Preerences support methods :
    //MARK:- createTagsForPreferences:
    
    func createTagsForPreferences(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let height = 30
        let spaceHeight : CGFloat = 20
        let screenWidth = UIScreen.main.bounds.size.width - 60.0
        
        for str in data  {
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name:"IBM Plex Sans SemiBold", size: 15.0)!)
            // edit :
            
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            // let checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            
            if checkWholeWidth > screenWidth{
                //we are exceeding size need to change xpos
                xPos = 0.0
                ypos = ypos + spaceHeight + 8.0 + 11
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 35.0 , height: CGFloat(height)))
            //  bgView.layer.cornerRadius = 14.5
            bgView.backgroundColor = .clear//UIColor(red: 33.0/255.0, green: 135.0/255.0, blue:199.0/255.0, alpha: 1.0)
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.blue,UIColor.MyTheme.SecondColor.green], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 17.0, y: -2, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "IBM Plex Sans SemiBold", size: 15.0)
            textlable.text = startstring
            textlable.gradientColors =  [UIColor.MyTheme.FirstColor.blue.cgColor,UIColor.MyTheme.SecondColor.green.cgColor]
            bgView.addSubview(textlable)
            
            //when close buttton enable
            
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                //
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 23.0, height: 23.0)
                button.backgroundColor = UIColor.clear
                button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
                
                button.setImage(#imageLiteral(resourceName: "closeTag"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagPreferences(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldMyPreferences.isHidden = false
            
            if arrTagsMyPreferences.count == 12 {
                txtFldMyPreferences_y.constant = ypos
                txtFldMyPreferences_x.constant = xPos
            }else{
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    //we are exceeding size need to change xpos
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldMyPreferences_y.constant = ypos
                    txtFldMyPreferences_x.constant = xPos
                    
                }else{
                    txtFldMyPreferences_y.constant = ypos
                    txtFldMyPreferences_x.constant = xPos
                }
            }
            txtTo_width.constant = screenWidth - xPos + 50// here 30 is padding....
        }else{
            txtFldMyPreferences.isHidden = true
        }
        
        if arrTagsMyPreferences.count <= 12 {
            self.txtFldMyPreferences.alpha = 1
            scrollViewMyPreferences_h.constant = ypos + 34 + 10 //Here 50 is the height of the txtview, and 10 is the bottom space of the scrollview
            
            if arrTagsMyPreferences.count == 12 {
                self.txtFldMyPreferences.alpha = 0
                self.txtFldMyPreferences.resignFirstResponder()
                self.view.endEditing(true)
            }
        }else{
            self.txtFldMyPreferences.alpha = 0
            self.txtFldMyPreferences.resignFirstResponder()
            self.view.endEditing(true)
            self.txtFldMyPreferences.alpha = 0
            scrollViewMyPreferences_h.constant = ypos //Here 50 is the height of the txtview, and 10 is the bottom space of the scrollview
        }
    }
    
    @objc func removeTagPreferences(_ sender: AnyObject) {
        arrTagsMyPreferences.remove(at: (sender.tag - 1))
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
    }
}

