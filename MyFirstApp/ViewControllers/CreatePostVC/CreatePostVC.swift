//
//  CreatePostVC.swift
//  MyFirstApp
//
//  Created by cis on 05/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import SearchTextField

struct CreatePostModelInfoClass {
    var activeScreen = CreatePostActiveScreen.airplane
    var flight = ""
    var location = ""
    var subAtrribute = ""
    var time = ""
    var arrPrefrenceTags = [""]
    var placeAPIData1 = NSDictionary()
    var placeAPIData2 = NSDictionary()
    var isCountryFound = false
    var country = ""
    var state = ""
}


extension CreatePostVC : CallBackMakeRequestDelegate{
    func call_preCreatePostVM(param: [String: AnyObject]){
        self.call_preCreatePost(param: param)
    }
    func call_checkPlacePostVM(preParam: [String : AnyObject], mainCreatePostParam: [String : AnyObject]){
        self.call_checkPlacePost(preParam: preParam, mainCreatePostParam: mainCreatePostParam)
    }
    func call_createPostVM(param: [String : AnyObject], displayLoader: Bool){
        self.call_createPost(param: param, displayLoader: displayLoader)
    }
    
}

class CreatePostVC: UIViewController{
    //MARK:- IBOutlet
    //MARK:-
    @IBOutlet weak var txtFldTitle: UItextView!
    @IBOutlet weak var viewOffTimePost: UIView!
    @IBOutlet weak var timePostLeftCons: NSLayoutConstraint!

    @IBOutlet weak var viewOffMatchCriteria: UIView!
    @IBOutlet weak var matchCriteriaLeftCons: NSLayoutConstraint!
    @IBOutlet weak var imgActiveCreate: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewToggleContainer: UIView!
    // Tags with textFields:--------
    
    //1. My attribtues:
    @IBOutlet weak var scrollViewMyAttibutes: UIScrollView!
    @IBOutlet weak var scrollViewMyAttibutes_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes: PinTextField!
    @IBOutlet weak var txtFldMyAttributes_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes_x: NSLayoutConstraint!
    var arrTagsMyAttributes = [String]()
    
    //2.My Preferences
    @IBOutlet weak var scrollViewMyPreferences: UIScrollView!
    @IBOutlet weak var scrollViewMyPreferences_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences: PinTextField!
    @IBOutlet weak var txtFldMyPreferences_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences_x: NSLayoutConstraint!
    var arrTagsMyPreferences = [String]()
    var activePlaceTextFld = ActivePlaceTextFldOption.non
    
    @IBOutlet weak var rootScrollView: UIScrollView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var txtFrom_width: NSLayoutConstraint!
    @IBOutlet weak var txtTo_width: NSLayoutConstraint!
    
    // Display picture:c
    @IBOutlet weak var viewDisplayPictureIndicator: UIView!
    @IBOutlet weak var displayPictureIndicatorLeftCons: NSLayoutConstraint!
    //public post
    @IBOutlet weak var viewPublicIndicatorPost: UIView!
    @IBOutlet weak var publicIndicatorPostLeftCons: NSLayoutConstraint!
    
    @IBOutlet weak var viewMatchCriteriaCantainer: UIView!
    @IBOutlet weak var btnPostPublic: UIButton!
    
    //MARK:- Variables
    //MARK:-
    
    // toggles:
    var backCreateDelegate : BackCreatePostDelegate?
    var timePostActiveStatus  = 0
    var matchCriteriaActiveStatus  = 0
    var timeInterVal = ActiveTime.today
    var createPostTimeIntervalID : Int?
    
    var viewHoursVM = ViewHoursViewModel() // for getting array of ids :
    var createPostVM = CreatePostViewModel()
    
    var arrAllAutoComplete = [String]()
    var arrSearchAutoComplete = [String]()
    
    // manage the back space of the tags.
    var isEmptyBackFrom = true
    var isEmptyBackTo = true
    var countryDialCode = 0//Constants.userDefault.object(forKey: Variables.date_format) as! Int
    
    // create view popus:
    var objCreateView1 = CreatePostView1()
    var objCreateView2 = CreatePostStep2View()
    
    // get this info from previous screen :
    var getCreatePostInfo = CreatePostModelInfoClass()//(activeScreen : CreatePostActiveScreen.airplane ,flight : "", location : "", subAtrribute : "" , time : "", arrPrefrenceTags : [""], placeAPIData1 : NSDictionary(), placeAPIData2 : NSDictionary(), isCountryFound : false, country : "", state : "")
    
    var publicIndicatorActiveStatus  = 0
    var displayPictureIndicatorStatus = 0
    var defaultUserCurrentLocation = ""
    var isCheckTest = false
    
    // for popup:
    var notification_public = 0;
    var notification_private = 0;
    
    
    //MARK:- App Flow
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFldMyAttributes.delegate = self
        txtFldMyPreferences.delegate = self
        txtFldTitle.delegate = self
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        //      let time =   "26/08/2021 01:00:00 59+0530".localToUTC(incomingFormat: "dd/MM/yyyy HH:mm:ss ssZZZ", outGoingFormat: "dd/MM/yyyy HH:mm:ss ssZZZ")
        //        print("UTC date time : \(time)")
        
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        txtFldMyAttributes.text = ""
        txtFldMyPreferences.text = ""
        
        arrTagsMyAttributes.removeAll()
        arrTagsMyPreferences.removeAll()
        initView()
        
        getProfileInfo() // api call :
    }
    
    
    //MARK:- Action Methods
    //MARK:-
    
    @IBAction func actionBack(_ sender: UIButton) {
        getCreatePostInfo.arrPrefrenceTags = arrTagsMyPreferences
        
        backCreateDelegate?.loadCreatePost1Info(activeScreen: getCreatePostInfo.activeScreen, flight: getCreatePostInfo.flight, location: getCreatePostInfo.location, subAtrribute: getCreatePostInfo.subAtrribute, timeStatus: getCreatePostInfo.time, arrPrefrenceTags: getCreatePostInfo.arrPrefrenceTags,placeAPIData1: getCreatePostInfo.placeAPIData1,placeAPIData2: getCreatePostInfo.placeAPIData2, isCountryFound: getCreatePostInfo.isCountryFound,country: getCreatePostInfo.country, state: getCreatePostInfo.state)
        self.navigationController?.popViewController(animated: true)
    }
    
     
    @IBAction func actionCreatePost(_ sender: UIButton) {
     checkValidation()
    }
    
    
    @IBAction func actionToggleDisplayPicture(_ sender: UIButton) {
        
        if displayPictureIndicatorStatus  == 1{ // on to off :
            displayPictureIndicatorStatus = 0
            
        }else{ // off to on :
            displayPictureIndicatorStatus = 1
        }
        displayIndicatorActiveStatuInfo()
    }
    
    
    @IBAction func actionTogglePublicIndicator(_ sender: UIButton) {
        
        if publicIndicatorActiveStatus  == 1{ // on to off :
            publicIndicatorActiveStatus = 0
            
        }else{ // off to on :
            publicIndicatorActiveStatus = 1
        }
        publicIndicatorActiveStatuInfo(isDisableActive: false)
    }

    @IBAction func actionOpenTime(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        if countryDialCode == 1{
            ViewHours.Show(hoursFormate: .twelve, previousSelectedTime: self.lblTime.text!) { [weak self](hours,id) in
                self?.lblTime.text =  hours
                self?.createPostTimeIntervalID = id
            }
        }else{
            ViewHours.Show(hoursFormate: .twentyFour, previousSelectedTime: self.lblTime.text! ) { [weak self](hours,id) in
                self?.lblTime.text =  hours
                self?.createPostTimeIntervalID = id
            }
        }
    }
    
    
    @IBAction func actionToggleTimePost(_ sender: UIButton) {
        if timePostActiveStatus  == 1{ // on to off :
            timePostActiveStatus = 0
            
        }else{ // off to on :
            timePostActiveStatus = 1
        }
        setTimePostActiveStatusInfo()
    }
    
    
    @IBAction func actionToggleMatchCriteria(_ sender: UIButton) {
        if matchCriteriaActiveStatus  == 1{ // on to off :
            matchCriteriaActiveStatus = 0
            
        }else{ // off to on :
            matchCriteriaActiveStatus = 1
        }
        matchCriteriaActiveStatuInfo()
    }
    
    
    // Action headers:
    @IBAction func actionHeader(_ sender: UIButton) {
        
        objCreateView1 = objCreateView1.Show(viewController: self, actionScreenUpdate: .update, activeScreen: getCreatePostInfo.activeScreen, strFlight: getCreatePostInfo.flight, strSubAttribute: getCreatePostInfo.subAtrribute, strLocation: getCreatePostInfo.location, timeStatus: getCreatePostInfo.time,placeAPIData1 : getCreatePostInfo.placeAPIData1, placeAPIData2: getCreatePostInfo.placeAPIData2, isCountryFound: getCreatePostInfo.isCountryFound,country: getCreatePostInfo.country,state: getCreatePostInfo.state) { [weak self](activeStatus, flight, location, strAtribute, time,placeAPIData1,placeAPIData2, isCountryFound,country,state)  in
            
            self?.objCreateView2 =  self?.objCreateView2.Show(activeScreen: activeStatus, strHeaderTitle: "Describe my crush", isbackButtonActive: false, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self!.arrTagsMyPreferences), viewController: self!, attribute_text: flight, attribute_city: location, totalCount: 12) { (arrSelectedTags) in
                
                self?.objCreateView1.removeWithAnimation()
                self?.objCreateView2.removeWithAnimation()
                
                self?.getCreatePostInfo.activeScreen = activeStatus
                self?.getCreatePostInfo.flight = flight
                self?.getCreatePostInfo.location = location
                self?.getCreatePostInfo.subAtrribute = strAtribute
                self?.getCreatePostInfo.time = time
                self?.getCreatePostInfo.placeAPIData1 = placeAPIData1
                self?.getCreatePostInfo.placeAPIData2 = placeAPIData2
                self?.getCreatePostInfo.isCountryFound = isCountryFound
                self?.getCreatePostInfo.country = country
                self?.getCreatePostInfo.state = state
                
                self?.arrTagsMyPreferences = arrSelectedTags
                self?.createTagsForPreferences(OnView: (self?.scrollViewMyPreferences)!, withArray: self!.arrTagsMyPreferences, isCancelButtonVisible: true)
                self?.initView()
            } as! CreatePostStep2View
            
        } as! CreatePostView1
    }
    
    
    @IBAction func actionMyMatchTagSuggestion(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.objCreateView2 =  self.objCreateView2.Show(activeScreen: getCreatePostInfo.activeScreen, strHeaderTitle: "My preferences", isbackButtonActive: true, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self.arrTagsMyPreferences), viewController: self, attribute_text: getCreatePostInfo.flight, attribute_city: getCreatePostInfo.location, totalCount: 12) { [weak self] (arrSelectedTags) in
            
            self?.objCreateView2.removeWithAnimation()
            self?.arrTagsMyPreferences = arrSelectedTags
            self?.createTagsForPreferences(OnView: (self?.scrollViewMyPreferences)!, withArray: self!.arrTagsMyPreferences, isCancelButtonVisible: true)
            self?.initView()
        } as! CreatePostStep2View
    }
    
    
    @IBAction func actionMyTagsSuggestion(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.objCreateView2 =  self.objCreateView2.Show(activeScreen: getCreatePostInfo.activeScreen, strHeaderTitle: "My traits", isbackButtonActive: true, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self.arrTagsMyAttributes), viewController: self, attribute_text: getCreatePostInfo.flight, attribute_city: getCreatePostInfo.location, totalCount: 12) { [weak self](arrSelectedTags) in
            
            self?.objCreateView2.removeWithAnimation()
            
            self?.arrTagsMyAttributes = arrSelectedTags
            self?.createTagsForAttributes(OnView: (self?.scrollViewMyAttibutes)!, withArray: self!.arrTagsMyAttributes, isCancelButtonVisible: true)
            
            self?.initView()
        } as! CreatePostStep2View
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    func initView(){
        
        // set the info from previous view: =====================4==========
        
        //====================================================================
        //setTimeInterValInfo()
        setHeaderInfo()
        setTimePostActiveStatusInfo()
        matchCriteriaActiveStatuInfo()
        
        let strTimeInterval = createPostVM.getTimeInterval()
        lblTime.text = strTimeInterval
        
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        if countryDialCode == 1 { // for US : 12 hours time foemate
            
            for value in viewHoursVM.arrIDs{
                
                if value.timeOfTwelve == strTimeInterval {
                    createPostTimeIntervalID = value.id
                    break
                }
            }
        }else{ // for non us : 24 hours time interval
            
            for value in viewHoursVM.arrIDs {
                if value.timeOfTwentyFour == strTimeInterval {
                    createPostTimeIntervalID = value.id
                    break
                }
            }
        }
        
        viewMatchCriteriaCantainer.isHidden = false
        
        
        switch getCreatePostInfo.activeScreen {
        case .airplane:
            imgActiveCreate.image = #imageLiteral(resourceName: "airplaneIcon")
            timePostActiveStatus = 0
            matchCriteriaActiveStatus = 0
            
            publicIndicatorActiveStatus = 0
            publicIndicatorActiveStatuInfo(isDisableActive: false)
            break;
            
        case .subway:
            imgActiveCreate.image = #imageLiteral(resourceName: "train 1")
            matchCriteriaActiveStatus = 1
            timePostActiveStatus = 1
            
            if getCreatePostInfo.isCountryFound {
                publicIndicatorActiveStatus = 0
                publicIndicatorActiveStatuInfo(isDisableActive: false)
            }else{
                publicIndicatorActiveStatus = 1
                publicIndicatorActiveStatuInfo(isDisableActive: true)
            }
            
            matchCriteriaActiveStatus = 1
            
            break;
            
        case .place:
            imgActiveCreate.image = #imageLiteral(resourceName: "food-stall 1")
            matchCriteriaActiveStatus = 1
            timePostActiveStatus = 1
            publicIndicatorActiveStatus = 0
            publicIndicatorActiveStatuInfo(isDisableActive: false)
            break;
            
            
        case .street:
            imgActiveCreate.image = #imageLiteral(resourceName: "map 1")
            matchCriteriaActiveStatus = 1
            timePostActiveStatus = 1
            publicIndicatorActiveStatus = 0
            publicIndicatorActiveStatuInfo(isDisableActive: false)
            break;
            
            
        case .bus:
            imgActiveCreate.image = #imageLiteral(resourceName: "bus-stop 1")
            matchCriteriaActiveStatus = 1
            timePostActiveStatus = 1
            
            if getCreatePostInfo.isCountryFound {
                publicIndicatorActiveStatus = 0
                publicIndicatorActiveStatuInfo(isDisableActive: false)
            }else{
                publicIndicatorActiveStatus = 1
                publicIndicatorActiveStatuInfo(isDisableActive: true)
            }
            break;
            
            
        case .train:
            imgActiveCreate.image = #imageLiteral(resourceName: "train (1) 2")
            timePostActiveStatus = 1
            publicIndicatorActiveStatus = 0
            publicIndicatorActiveStatuInfo(isDisableActive: false)
            break;
            
        case .none:
            break
        }
        setTimePostActiveStatusInfo()
        matchCriteriaActiveStatuInfo()
    }
    
    func setHeaderInfo(){
        var strTime = ""
        if getCreatePostInfo.time == "today"{
            strTime = Date().string(format: "dd/MM/yyyy") // today
        }else{
            
            let yesterDay :Date = Date() - 1// Jan 21, 2019
            strTime = yesterDay.string(format: "dd/MM/yyyy")
        }
        
        var strDate = ""
        if countryDialCode == 1 { // for US : 12 hours time foemate
            strDate = createPostVM.convertDateFormaterForUS(strTime)
        }else{
            strDate = createPostVM.convertDateFormaterForNonUS(strTime)
        }
        
        if getCreatePostInfo.subAtrribute == ""{
            
            if getCreatePostInfo.activeScreen == .place { // subAtrribute is always empty
                //place end:
                lblHeaderTitle.text =  createPostVM.headerTitleforPlace( getCreatePostInfo : getCreatePostInfo,strDate: strDate) //placeHeaderTitle
                
            }else if getCreatePostInfo.activeScreen == .subway{
                
                lblHeaderTitle.text = createPostVM.headerTitleforPlaceSubway( getCreatePostInfo : getCreatePostInfo,strDate: strDate)
            }else if getCreatePostInfo.activeScreen == .street{ // subAtrribute is always empty

                lblHeaderTitle.text = createPostVM.headerTitleforPlaceStreet( getCreatePostInfo : getCreatePostInfo,strDate: strDate)
                
            }else if getCreatePostInfo.activeScreen == .bus{
                lblHeaderTitle.text = createPostVM.headerTitleforPlaceBus( getCreatePostInfo : getCreatePostInfo,strDate: strDate)
                
            }else {
                // lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCre24atePostInfo.location). \(strDate)"}
                var headerTitle = ""
                
                if "\(getCreatePostInfo.flight)".count > 0 {
                    
                    if headerTitle.count > 0{
                        headerTitle = "\(headerTitle) \(getCreatePostInfo.flight)"
                    }else{
                        headerTitle = "\(getCreatePostInfo.flight)"
                    }
                }
                
                if "\(strDate)".count > 0 {
                    
                    if headerTitle.count > 0 {
                        headerTitle = "\(headerTitle). \(strDate)"
                    }else{
                        headerTitle = "\(strDate)"
                    }
                }
                
                lblHeaderTitle.text = headerTitle
            }
            
        }else{
            
            if getCreatePostInfo.activeScreen == .place {
                //  lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCreatePostInfo.subAtrribute). \(strDate)"
                
            }else if getCreatePostInfo.activeScreen == .street{
                //   lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCreatePostInfo.subAtrribute). \(strDate)"
                
            }else if getCreatePostInfo.activeScreen == .bus{
                
                var busHeaderTitle = ""
                if "\(getCreatePostInfo.flight)".count > 0 {
                    
                    if busHeaderTitle.count > 0 {
                        busHeaderTitle = "\(busHeaderTitle) \(getCreatePostInfo.flight)"
                    }else{
                        busHeaderTitle = "\(getCreatePostInfo.flight)"
                    }
                }
                
                
                if "\(strDate)".count > 0 {
                    
                    if  busHeaderTitle.count > 0 {
                        busHeaderTitle = "\(busHeaderTitle). \(strDate)"
                    }else{
                        busHeaderTitle =  "\(strDate)"
                    }
                }
                
                lblHeaderTitle.text = busHeaderTitle
                
            }else{
                //  lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCreatePostInfo.subAtrribute). \(getCreatePostInfo.location). \(strDate)"
                var headerTitle = ""
                
                if "\(getCreatePostInfo.flight)".count > 0 {
                    if  headerTitle.count > 0 {
                        headerTitle = "\(headerTitle) \(getCreatePostInfo.flight)"
                    }else{
                        headerTitle = "\(getCreatePostInfo.flight)"
                    }
                }
                
                if "\(getCreatePostInfo.subAtrribute)".count > 0 && headerTitle.count > 0{
                    
                    if headerTitle.count > 0 {
                        headerTitle = "\(headerTitle). \(getCreatePostInfo.subAtrribute)"
                    }else{
                        headerTitle = "\(getCreatePostInfo.subAtrribute)"
                    }
                }
                
                if "\(strDate)".count > 0  {
                    
                    if headerTitle.count > 0 {
                        headerTitle = "\(headerTitle). \(strDate)"
                    }else{
                        headerTitle = "\(strDate)"
                    }
                }
                //       lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCreatePostInfo.subAtrribute). \(strDate)"
            }
        }
    }
    
    func resetAllFields(){
        
        txtFldTitle.text = ""
        arrTagsMyAttributes.removeAll()
        arrTagsMyPreferences.removeAll()
        
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
    }
    
    
    func displayIndicatorActiveStatuInfo(){
        // set notification :asdasd
        if  displayPictureIndicatorStatus == 0 {
            UIView.animate(withDuration: 0.3) {
                self.displayPictureIndicatorLeftCons.constant = 1
                self.viewDisplayPictureIndicator.alpha = 1
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.displayPictureIndicatorLeftCons.constant = 28
                self.viewDisplayPictureIndicator.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    func publicIndicatorActiveStatuInfo(isDisableActive: Bool){
        // set notification :
        
        if isDisableActive {
            btnPostPublic.isUserInteractionEnabled = false
            
            if publicIndicatorActiveStatus == 0 {
                UIView.animate(withDuration: 0.3) {
                    self.publicIndicatorPostLeftCons.constant = 1
                    self.viewPublicIndicatorPost.alpha = 1
                    self.view.layoutIfNeeded()
                }
            }else{
                UIView.animate(withDuration: 0.3) {
                    self.publicIndicatorPostLeftCons.constant = 28
                    self.viewPublicIndicatorPost.alpha = 1
                    self.view.layoutIfNeeded()
                }
            }
        }else{ // active:
            // button active:
            btnPostPublic.isUserInteractionEnabled = true
            if  publicIndicatorActiveStatus == 0 {
                UIView.animate(withDuration: 0.3) {
                    self.publicIndicatorPostLeftCons.constant = 1
                    self.viewPublicIndicatorPost.alpha = 1
                    self.view.layoutIfNeeded()
                }
            }else{
                UIView.animate(withDuration: 0.3) {
                    self.publicIndicatorPostLeftCons.constant = 28
                    self.viewPublicIndicatorPost.alpha = 0
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func checkValidation(){
        
        let strTitle  = txtFldTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strTickets  = getCreatePostInfo.flight//txtF0ldTicket.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        _  = getCreatePostInfo.location//txtFldLocation.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strSubAttributes = getCreatePostInfo.subAtrribute//txtFldSubAtrributes.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        // strTitle optional:
        guard strTitle.count > 0 else { SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.pleaseEnterTitle)", showMsgAt: .bottom); return}
        guard arrTagsMyAttributes.count > 0 else { SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.pleaseAddAtLeastOneTagInMyTags)", showMsgAt: .bottom); return}
        
        guard arrTagsMyPreferences.count > 0 else { SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.pleaseAddAtLeastOneTagInMyMatchTags)", showMsgAt: .bottom); return}
    
        /*
         Condition for popup:
         */
        
        //1. If toggle Make post public/searchable is On and notification_public = 1:
        if publicIndicatorActiveStatus == 1 && notification_public == 1 {
            //Display popup message 1
            
            AlertWithCheckBoxView.Show(alertTitle: AlertMessages.shareInstance.yourPostIsPublic, btn1Title: "Continue", btn2Title: "Return to post") { [weak self] (result,doNotShowMsg) in

                if result == "Continue" {
                    
                    if doNotShowMsg{
                        self?.makeRequest(strTitle:strTitle , strTickets: strTickets, strSubAttributes: strSubAttributes, notification_private  : self?.notification_private ?? 1, notification_public: 0)
                    }else{
                        self?.makeRequest(strTitle:strTitle , strTickets: strTickets, strSubAttributes: strSubAttributes, notification_private  : self?.notification_private ?? 1, notification_public: self?.notification_public ?? 1)
                    }
                }
            }
            
        //If toggle Make post public/searchable is Off and notification_private = 1:
        }else if publicIndicatorActiveStatus == 0 && notification_private == 1{
           // Display popup message 2
            
            AlertWithCheckBoxView.Show(alertTitle: AlertMessages.shareInstance.yourPostIsPrivate, btn1Title: "Continue", btn2Title: "Return to post") { [weak self] (result,doNotShowMsg) in

                if result == "Continue" {
                    
                    if doNotShowMsg{
                        self?.makeRequest(strTitle:strTitle , strTickets: strTickets, strSubAttributes: strSubAttributes, notification_private  : 0, notification_public: self?.notification_public ?? 1)
                    }else{
                        self?.makeRequest(strTitle:strTitle , strTickets: strTickets, strSubAttributes: strSubAttributes, notification_private  : self?.notification_private ?? 1, notification_public: self?.notification_public ?? 1)
                    }
                }
            }
            // else condition
        }else{
            makeRequest(strTitle:strTitle , strTickets: strTickets, strSubAttributes: strSubAttributes,  notification_private  : self.notification_private, notification_public: self.notification_public)
        }
    }
   
    func makeRequest(strTitle : String,strTickets : String , strSubAttributes : String,  notification_private  : Int, notification_public: Int){
        
        
        //sdfndsfndsfdnsfks
         //makeRequest
        createPostVM.makeRequest(delegate: self,
                                 strTitle : strTitle,
                                 strTickets : strTickets ,
                                 strSubAttributes : strSubAttributes,
                                 notification_private  : notification_private,
                                 notification_public: notification_public,
                                 publicIndicatorActiveStatus : publicIndicatorActiveStatus,
                                 displayPictureIndicatorStatus : displayPictureIndicatorStatus,
                                 getCreatePostInfo : getCreatePostInfo,
                                 arrTagsMyAttributes : arrTagsMyAttributes,
                                 arrTagsMyPreferences :arrTagsMyPreferences ,
                                 createPostTimeIntervalID : createPostTimeIntervalID!,
                                 matchCriteriaActiveStatus : matchCriteriaActiveStatus, event_type: 1, automatchActiveStatus: 0)
    }
    
    func setInfo(dict : NSDictionary){
        let item = dict.object(forKey: "data") as! NSDictionary
        
        for index in 1...12 {
            let key = "characteristic_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                let value = "\(item.object(forKey: key) as! String)"
                arrTagsMyAttributes.append(value.lowercased())
            }
        }
    
        // for notification popups.
        if let publicValue = item.object(forKey: "notification_public") as? Int {
            notification_public = publicValue
        }
        
        if let privateValue = item.object(forKey: "notification_private") as? Int {
        notification_private = privateValue
        }
        // partner Prefreence :
        // assign from the view :
        arrTagsMyPreferences = getCreatePostInfo.arrPrefrenceTags
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
    }
    
    func setTimePostActiveStatusInfo(){
        // set notification :
        if timePostActiveStatus == 0 {
            UIView.animate(withDuration: 0.3) {
                self.timePostLeftCons.constant = 1
                self.viewOffTimePost.alpha = 1
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.timePostLeftCons.constant = 28
                self.viewOffTimePost.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    func matchCriteriaActiveStatuInfo(){
        // set notification :
        if  matchCriteriaActiveStatus == 0 {
            UIView.animate(withDuration: 0.3) {
                self.matchCriteriaLeftCons.constant = 1
                self.viewOffMatchCriteria.alpha = 1
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.matchCriteriaLeftCons.constant = 28
                self.viewOffMatchCriteria.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
    }
}



/*
 MARK:- API's
 */
extension CreatePostVC {
    
    // get profile info:
    func getProfileInfo()
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        createPostVM.getProfileApi() { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.setInfo(dict: dictData)
            }else{
                
                // SnackBar.sharedInstance.show(message: message)
            }
        }
    }
}

/*
 MARK:- API's
 */

extension CreatePostVC {
    
    func call_createPost(param : [String : AnyObject], displayLoader : Bool)
    {
        if displayLoader {
            Loader.sharedInstance.showLoader(msg: "Loading...")
        }
        
        createPostVM.callCreatePost_Api(requestParam : param) { (status, message, postId) in
            
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.resetAllFields()
                self.backCreateDelegate?.backFromCreatePost()
                self.navigationController?.popViewController(animated: true)
                //successSnackBar.sharedInstance.show(message: message)
            }else if status == "error"{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    // check place and atreet post :
    
    func call_checkPlacePost(preParam : [String : AnyObject], mainCreatePostParam : [String : AnyObject])
    {
        
        Loader.sharedInstance.showLoader(msg: "Loading...")
        createPostVM.callCheckPlace_Api(requestParam : preParam) { [weak self](status, message) in
            
            if status == "success"
            {
                self?.call_createPost(param: mainCreatePostParam, displayLoader: false)
                
            }else if status == "error"{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    
    // first call for plane category only.
    func call_preCreatePost(param : [String : AnyObject])
    {
        /*
         code 400 :
         Something went wrong
         
         code 401  :
         1) Third party is not calling.
         2) some issue in third party api
         
         code 402 :
         Record not found in  third party api
         
         code 200:
         Record is found
         */
        
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        createPostVM.callPreCreatePost_Api(requestParam : param) { [weak self] (status, message, popupMsg)  in
            
            switch status{
            
            case "success200":
                
                if popupMsg == "" {
                    // call api :
                    self?.call_createPost(param: param, displayLoader: false)
                }else{
                    Loader.sharedInstance.stopLoader()
                    AlertTheme.sharedInstance.Show(popupCategory: .normal, message: popupMsg, attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
                        if clicked == "Ok" {
                            self?.call_createPost(param: param, displayLoader: false)
                            // action logout perform
                        }
                    }
                }
                
                break
                
            case "success400":
                Loader.sharedInstance.stopLoader()
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                break
                
            case "success401":
                self?.call_createPost(param: param, displayLoader: false)
                break
                
            case "success402":
                Loader.sharedInstance.stopLoader()
                
                AlertTheme.sharedInstance.Show(popupCategory: .normal,message: popupMsg, attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["Yes, post","Cancel"], isBottomTxt: "") { [weak self] (clicked) in
                    if clicked == "Yes, post" {
                        self?.call_createPost(param: param, displayLoader: false)
                        // action logout perform
                    }else{
                        self?.backCreateDelegate?.backFromCreatePost()
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
                break
                
            default:
                break;
                
            }
        }
    }
}
