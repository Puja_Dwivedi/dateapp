//
//  CreatePostStep4VC.swift
//  MyFirstApp
//
//  Created by cis on 05/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import SearchTextField

extension CreatePostStep4VC : CallBackMakeRequestDelegate{
    func call_preCreatePostVM(param: [String: AnyObject]){
        self.call_preCreatePost(param: param)
    }
    func call_checkPlacePostVM(preParam: [String : AnyObject], mainCreatePostParam: [String : AnyObject]){
        self.call_checkPlacePost(preParam: preParam, mainCreatePostParam: mainCreatePostParam)
    }
    func call_createPostVM(param: [String : AnyObject], displayLoader: Bool){
        self.call_createPost(param: param, displayLoader: displayLoader)
    }
    
}


class CreatePostStep4VC: UIViewController, UITextFieldDelegate, UITextViewDelegate{
    
    //MARK:- IBOutlet
    //MARK:-
    @IBOutlet var viewPostToggle: UIView!
    @IBOutlet var viewToggle: UIView!
    @IBOutlet var viewTimeToggle: UIView!
    @IBOutlet weak var rootScrollView: UIScrollView!
    @IBOutlet weak var viewOffTimePost: UIView!
    @IBOutlet weak var timePostLeftCons: NSLayoutConstraint!
    // Display picture:c
    @IBOutlet var btnBack: UIButton!
    @IBOutlet weak var viewDisplayPictureIndicator: UIView!
    @IBOutlet weak var displayPictureIndicatorLeftCons: NSLayoutConstraint!
    //public post
    @IBOutlet weak var viewPublicIndicatorPost: UIView!
    @IBOutlet weak var publicIndicatorPostLeftCons: NSLayoutConstraint!
    @IBOutlet weak var btnPostPublic: UIButton!
    @IBOutlet weak var viewOffAutomatch: UIView!
    @IBOutlet var viewAutomatchToggle: UIView!
    @IBOutlet weak var automatchLeftCons: NSLayoutConstraint!

    
    
    //MARK:- Variables
    //MARK:-
    var arrTagsMyPreferences = [String]()
    var arrTagsMyAttributes = [String]()
    var objCreateView1 = CreatePostView1()
    
    // toggles:
    var publicIndicatorActiveStatus  = 1
    var displayPictureIndicatorStatus = 1
    var timePostActiveStatus  = 0
    var automatchActiveStatus  = 0
    
    var publicIndicator = 1
    var displayPicture = 1
    
    // for popup:
    var notification_public = 0;
    var notification_private = 0;
    
    var createPostVM = CreatePostViewModel()
    var getCreatePostInfo = CreatePostModelInfoClass()
    
    var timeInterVal = ActiveTime.today
    var createPostTimeIntervalID : Int? = 0
    var matchCriteriaActiveStatus  = 0
    
    var backCreateDelegate : BackCreatePostDelegate?

    
    //MARK:- App Flow
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCreatePostInfo.activeScreen = AppDelegate.shared.activeCheckin
        getCreatePostInfo.isCountryFound = UserDefaults.standard.bool(forKey: "isCountryFound")
        if let placeData1 = UserDefaults.standard.dictionary(forKey: "placeAPIDetailData1") as? NSDictionary {
            getCreatePostInfo.placeAPIData1 = placeData1
        }
        if let placeData2 = UserDefaults.standard.dictionary(forKey: "placeAPIDetailData2") as? NSDictionary {
            getCreatePostInfo.placeAPIData2 = placeData2
        }
        setViewFromBack()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
//        btnBack.underline()
        super.viewWillAppear(animated)
        DeviceSize.screen_Height > 736.0 ? (rootScrollView.isScrollEnabled = false) : (rootScrollView.isScrollEnabled = true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UserDefaults.standard.set(publicIndicator, forKey: "PublicIndicator")
        UserDefaults.standard.set(displayPicture, forKey: "DisplayPicture")
        UserDefaults.standard.set(timePostActiveStatus, forKey: "TimePostActiveStatus")
        UserDefaults.standard.set(automatchActiveStatus, forKey: "AutomatchActiveStatus")

    }
    
    func getReversedValue(value: Int) -> Int {
        if value == 1 {
            return 0
        }
        return 1
    }
    
    func setViewFromBack() {
        if let publicIndicator = UserDefaults.standard.value(forKey: "PublicIndicator") as? Int {
            publicIndicatorActiveStatus = getReversedValue(value: publicIndicator)
            if AppDelegate.shared.activeCheckin == .subway || AppDelegate.shared.activeCheckin == .bus {
                self.publicIndicatorActiveStatuInfo(isDisableActive: !self.getCreatePostInfo.isCountryFound)
            } else {
                self.publicIndicatorActiveStatuInfo(isDisableActive: false)
            }
        }
        
        if let displayPicture = UserDefaults.standard.value(forKey: "DisplayPicture") as? Int {
            displayPictureIndicatorStatus = getReversedValue(value: displayPicture)
            self.displayIndicatorActiveStatuInfo()
        }
        if let timePostStatus = UserDefaults.standard.value(forKey: "TimePostActiveStatus") as? Int {
            timePostActiveStatus = timePostStatus
            setTimePostActiveStatusInfo()
        }
        if let automatchStatus = UserDefaults.standard.value(forKey: "AutomatchActiveStatus") as? Int {
            automatchActiveStatus = automatchStatus
            setAutomatchActiveStatusInfo()
        }
    }
    
    
    //MARK:- Action Methods
    //MARK:-
    
    @IBAction func actionToggleDisplayPicture(_ sender: UIButton) {
        
        if displayPictureIndicatorStatus  == 1{ // on to off :
            displayPictureIndicatorStatus = 0
            
        }else{ // off to on :
            displayPictureIndicatorStatus = 1
        }
        UIView.animate(withDuration: 0.3) {
            self.displayIndicatorActiveStatuInfo()
        }
        print("Picture")
    }
    
    
    @IBAction func actionTogglePublicIndicator(_ sender: UIButton) {
        if publicIndicatorActiveStatus  == 1{ // on to off :
            publicIndicatorActiveStatus = 0
            
        }else{ // off to on :
            publicIndicatorActiveStatus = 1
        }
        UIView.animate(withDuration: 0.3) {
            if AppDelegate.shared.activeCheckin == .subway || AppDelegate.shared.activeCheckin == .bus {
                self.publicIndicatorActiveStatuInfo(isDisableActive: !self.getCreatePostInfo.isCountryFound)
            } else {
                self.publicIndicatorActiveStatuInfo(isDisableActive: false)
            }
            
        }
        print("Public")
        
    }
    
    @IBAction func actionToggleTimePost(_ sender: UIButton) {
        if timePostActiveStatus  == 1{ // on to off :
            timePostActiveStatus = 0
            
        }else{ // off to on :
            timePostActiveStatus = 1
        }
        UIView.animate(withDuration: 0.3) {
            self.setTimePostActiveStatusInfo()
        }
        print("Time")
        
    }
    
    @IBAction func actionToggleAutomatch(_ sender: UIButton) {
        if automatchActiveStatus  == 1{ // on to off :
            automatchActiveStatus = 0
            
        }else{ // off to on :
            automatchActiveStatus = 1
        }
        UIView.animate(withDuration: 0.3) {
            self.setAutomatchActiveStatusInfo()
        }
        print("Automatch")
        
    }
    
    
    @IBAction func actionCancel(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        self.navigationController?.pushViewController(vc, animated: false)
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)

    }
    
    @IBAction func actionBack(_ sender: UIButton) {
//        UserDefaults.standard.set(arrTagsMyPreferences, forKey: "MyPreferences")
//        UserDefaults.standard.set(arrTagsMyAttributes, forKey: "MyAttributes")
        AppDelegate.shared.isBackPressedFromTag = false
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func actionCreatePost(_ sender: UIButton) {
        checkValidation()
    }
    
    func checkValidation() {
        /*
         Condition for popup:
         */
        guard let strTitle  = UserDefaults.standard.value(forKey: "tell_us_your_mind") as? String else { return }
        guard let timeInterval = UserDefaults.standard.value(forKey: "PostTimeId") as? Int else { return }
        let strTickets  = ""
        let strSubAttributes = ""
        createPostTimeIntervalID = timeInterval
        makeRequest(strTitle:strTitle , strTickets: strTickets, strSubAttributes: strSubAttributes,  notification_private  : self.notification_private, notification_public: self.notification_public)
    }
    
    func resetAllFields(){
        
    }
    
    
    // MARK :- Toggle methods
    
    func setAutomatchActiveStatusInfo(){
        // set notification :
        if automatchActiveStatus == 0 {
            self.automatchLeftCons.constant = 1
            self.viewOffAutomatch.alpha = 1
            self.viewAutomatchToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
            self.view.layoutIfNeeded()
        }else{
            self.automatchLeftCons.constant = 28
            self.viewOffAutomatch.alpha = 0
            self.viewAutomatchToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.view.layoutIfNeeded()
        }
        
    }
    
    func setTimePostActiveStatusInfo(){
        // set notification :
        if timePostActiveStatus == 0 {
            self.timePostLeftCons.constant = 1
            self.viewOffTimePost.alpha = 1
            self.viewTimeToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
            self.view.layoutIfNeeded()
//            UIView.animate(withDuration: 0.3) {
//                self.timePostLeftCons.constant = 1
//                self.viewOffTimePost.alpha = 1
//                self.viewTimeToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
//                self.view.layoutIfNeeded()
//            }
        }else{
            self.timePostLeftCons.constant = 28
            self.viewOffTimePost.alpha = 0
            self.viewTimeToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.view.layoutIfNeeded()
//            UIView.animate(withDuration: 0.3) {
//                self.timePostLeftCons.constant = 28
//                self.viewOffTimePost.alpha = 0
//                self.viewTimeToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.view.layoutIfNeeded()
//            }
        }
        
    }
    
    func displayIndicatorActiveStatuInfo(){
        // set notification :asdasd
        if  displayPictureIndicatorStatus == 0 {
            self.displayPictureIndicatorLeftCons.constant = 1
            self.viewDisplayPictureIndicator.alpha = 1
            self.viewToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
            self.view.layoutIfNeeded()
            self.displayPicture = 1
//            UIView.animate(withDuration: 0.3) {
//                self.displayPictureIndicatorLeftCons.constant = 1
//                self.viewDisplayPictureIndicator.alpha = 1
//                self.viewToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
//                self.view.layoutIfNeeded()
//                self.displayPicture = 1
//            }
        }else{
            self.displayPictureIndicatorLeftCons.constant = 28
            self.viewToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            self.viewDisplayPictureIndicator.alpha = 0
            self.view.layoutIfNeeded()
            self.displayPicture = 0
//            UIView.animate(withDuration: 0.3) {
//                self.displayPictureIndicatorLeftCons.constant = 28
//                self.viewToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                self.viewDisplayPictureIndicator.alpha = 0
//                self.view.layoutIfNeeded()
//                self.displayPicture = 0
//            }
        }
    }
    
    func publicIndicatorActiveStatuInfo(isDisableActive: Bool){
        // set notification :
        
        if isDisableActive {
            btnPostPublic.isUserInteractionEnabled = false
            self.publicIndicator = 1
            if publicIndicatorActiveStatus == 0 {
                self.publicIndicatorPostLeftCons.constant = 1
                self.viewPublicIndicatorPost.alpha = 1
                self.viewPostToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
                self.publicIndicator = 1
                self.view.layoutIfNeeded()
//                UIView.animate(withDuration: 0.3) {
//                    self.publicIndicatorPostLeftCons.constant = 1
//                    self.viewPublicIndicatorPost.alpha = 1
//                    self.viewPostToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
//                    self.view.layoutIfNeeded()
//                }
            }else{
                self.publicIndicatorPostLeftCons.constant = 1
                self.viewPublicIndicatorPost.alpha = 1
                self.viewPostToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
                self.publicIndicator = 1
                self.view.layoutIfNeeded()
//                UIView.animate(withDuration: 0.3) {
//                    self.publicIndicatorPostLeftCons.constant = 28
//                    self.viewPublicIndicatorPost.alpha = 1
//                    self.viewPostToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
//                    self.view.layoutIfNeeded()
//                }
            }
        }else{ // active:
            // button active:
            self.publicIndicator = 0
            btnPostPublic.isUserInteractionEnabled = true
            if  publicIndicatorActiveStatus == 0 {
                self.publicIndicatorPostLeftCons.constant = 1
                self.viewPublicIndicatorPost.alpha = 1
                self.viewPostToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
                self.view.layoutIfNeeded()
                self.publicIndicator = 1
//                UIView.animate(withDuration: 0.3) {
//                    self.publicIndicatorPostLeftCons.constant = 1
//                    self.viewPublicIndicatorPost.alpha = 1
//                    self.viewPostToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
//                    self.view.layoutIfNeeded()
//                    self.publicIndicator = 1
//                }
            }else{
                self.publicIndicatorPostLeftCons.constant = 28
                self.viewPostToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                self.viewPublicIndicatorPost.alpha = 0
                self.view.layoutIfNeeded()
                self.publicIndicator = 0
//                UIView.animate(withDuration: 0.3) {
//                    self.publicIndicatorPostLeftCons.constant = 28
//                    self.viewPostToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
//                    self.viewPublicIndicatorPost.alpha = 0
//                    self.view.layoutIfNeeded()
//                    self.publicIndicator = 0
//                }
            }
        }
    }
    
    func makeRequest(strTitle : String,strTickets : String , strSubAttributes : String,  notification_private  : Int, notification_public: Int){
        guard let eventType = UserDefaults.standard.value(forKey: "eventType") as? Int else { return }
        createPostVM.makeRequest(delegate: self,
                                         strTitle : strTitle,
                                         strTickets : strTickets ,
                                         strSubAttributes : strSubAttributes,
                                         notification_private  : notification_private,
                                         notification_public: notification_public,
                                         publicIndicatorActiveStatus : publicIndicator,
                                         displayPictureIndicatorStatus : displayPicture,
                                         getCreatePostInfo : getCreatePostInfo,
                                         arrTagsMyAttributes : arrTagsMyAttributes,
                                         arrTagsMyPreferences :arrTagsMyPreferences ,
                                         createPostTimeIntervalID : createPostTimeIntervalID!,
                                 matchCriteriaActiveStatus : timePostActiveStatus, event_type: eventType, automatchActiveStatus: automatchActiveStatus)
    }
    
    
}


/*
 MARK:- API's
 */

extension CreatePostStep4VC {
    
    func moveToRootVC() {
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func call_uploadImage(image: Data, postId: Int) {
//        Loader.sharedInstance.showLoader(msg: "Loading...")
//        let imgData = image.jpegData(compressionQuality: 1.0)!
//        var param = [String : AnyObject]()
//        param.updateValue(postId as AnyObject, forKey:"post_id")
        
        CreatePostStep4VM().updatePostImage(postId: postId, imageData: image) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                print("Uploaded Image")
                self.moveToRootVC()
                
            }else{
                
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    
    func call_createPost(param : [String : AnyObject], displayLoader : Bool)
    {
        if displayLoader {
            Loader.sharedInstance.showLoader(msg: "Loading...")
        }

        createPostVM.callCreatePost_Api(requestParam : param) { (status, message, postId) in


            if status == "success"
            {
                self.resetAllFields()
                self.backCreateDelegate?.backFromCreatePost()
                if let image = UserDefaults.standard.value(forKey: "ProfileImage") as? Data {
                    self.call_uploadImage(image: image, postId: postId)
                } else {
                    Loader.sharedInstance.stopLoader()
                    self.moveToRootVC()
                }
                
                //successSnackBar.sharedInstance.show(message: message)
            }else if status == "error"{
                Loader.sharedInstance.stopLoader()
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
//     check place and atreet post :
    
    func call_checkPlacePost(preParam : [String : AnyObject], mainCreatePostParam : [String : AnyObject])
    {

        Loader.sharedInstance.showLoader(msg: "Loading...")
        createPostVM.callCheckPlace_Api(requestParam : preParam) { [weak self](status, message) in

            if status == "success"
            {
                self?.call_createPost(param: mainCreatePostParam, displayLoader: false)

            }else if status == "error"{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    
//     first call for plane category only.
    func call_preCreatePost(param : [String : AnyObject])
    {
        /*
         code 400 :
         Something went wrong

         code 401  :
         1) Third party is not calling.
         2) some issue in third party api

         code 402 :
         Record not found in  third party api

         code 200:
         Record is found
         */

        Loader.sharedInstance.showLoader(msg: "Loading...")

        createPostVM.callPreCreatePost_Api(requestParam : param) { [weak self] (status, message, popupMsg)  in

            switch status{

            case "success200":

                if popupMsg == "" {
                    // call api :
                    self?.call_createPost(param: param, displayLoader: false)
                }else{
                    Loader.sharedInstance.stopLoader()
                    AlertTheme.sharedInstance.Show(popupCategory: .normal, message: popupMsg, attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
                        if clicked == "Ok" {
                            self?.call_createPost(param: param, displayLoader: false)
                            // action logout perform
                        }
                    }
                }

                break

            case "success400":
                Loader.sharedInstance.stopLoader()
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                break

            case "success401":
                self?.call_createPost(param: param, displayLoader: false)
                break

            case "success402":
                Loader.sharedInstance.stopLoader()

                AlertTheme.sharedInstance.Show(popupCategory: .normal,message: popupMsg, attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["Yes, post","Cancel"], isBottomTxt: "") { [weak self] (clicked) in
                    if clicked == "Yes, post" {
                        self?.call_createPost(param: param, displayLoader: false)
                        // action logout perform
                    }else{
                        self?.backCreateDelegate?.backFromCreatePost()
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
                break

            default:
                break;

            }
        }
    }
}
