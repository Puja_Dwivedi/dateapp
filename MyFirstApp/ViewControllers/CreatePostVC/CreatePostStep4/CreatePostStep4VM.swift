//
//  CreatePostStep4VM.swift
//  MyFirstApp
//
//  Created by cis on 28/06/23.
//  Copyright © 2023 cis. All rights reserved.
//

import Foundation

class CreatePostStep4VM: NSObject {
    
    // Updating profile values :
    func updatePostImage(postId: Int, imageData : Data, completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var message  = ""
        let param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
//        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = "\(APPURL.Urls.updatePostImage)/\(postId)"
        
        let arrDocuments = NSMutableArray()
        arrDocuments.setValue(String(format: "pictureImage%02d", 1), forKey: "picture")
        arrDocuments.setValue(imageData, forKey: "picture")
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.formRequestWith(strUrl: strService, imageData: imageData, imageName: "picture", parameters: param, headers: header) { (result, data) in
            
            let dict = data.response
            
            if let statusCode = dict.object(forKey: "code") as? Int  { // user is already register :
                
                if statusCode == 200 {
                    let dict = data.response
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    completion("success", message, dict)
                }else{
                    print("data : \(data)")
                    if data.message == "" {
                        if data.message == "" {
                            message = "\(dict.object(forKey: "message") ?? "")"
                            
                        }else{
                            message = data.message
                        }
                        
                    }else{
                        message = data.message
                    }
                    completion("error", message, NSDictionary())
                }
            }else{
                print("data : \(data)")
                if data.message == "" {
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                }else{
                    message = data.message
                }
                completion("error", message, NSDictionary())
            }
        }
    }
}
