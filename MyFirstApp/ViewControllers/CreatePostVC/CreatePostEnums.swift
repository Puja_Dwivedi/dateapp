//
//  CreatePostEnums.swift
//  MyFirstApp
//
//  Created by cis on 16/11/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

enum ActiveTime  {
    case today
    case yesterday
}

enum ActivePlaceTextFldOption {
    case txtFlight
    case  txtLocation
    case subAtrributes
    case non
}
