//
//  CreatePostStep3ViewModel.swift
//  MyFirstApp
//
//  Created by cis on 06/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class CreatePostStep3ViewModel: NSObject {
    var makeRequestDelegate : CallBackMakeRequestDelegate?
    
    var arrSuggestedAttributeList = [DropDownAtributesList]()
    
    func getProfileApi(completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var message  = ""
        let param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.getUserDetails + "/\(userID)"
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                //  completion("success", data.message, .moveToHomeScreen, "")
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message, dict)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message, NSDictionary())
                }else { // error :
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("error", message, NSDictionary())
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                completion("error", message, NSDictionary())
            }
        }
    }
    
    
    func callCreatePost_Api(requestParam : [String : AnyObject], completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        //param.updateValue(userId as AnyObject, forKey: KEY.UserDetails.ProfileId)
        let strService = APPURL.Urls.createPOst
        param =  requestParam
        
        //{"page":"1","pagination_limit":"10"}
        
        print("header \(header) \n API: \(strService) \n request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                //  completion("success", data.message, .moveToHomeScreen, "")
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                }else { // error :
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                completion("error", message)
            }
        }
    }
    
    
    func callCheckPlace_Api(requestParam : [String : AnyObject], completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        //param.updateValue(userId as AnyObject, forKey: KEY.UserDetails.ProfileId)
        let strService = APPURL.Urls.insertPlaceData
        param =  requestParam
        
        //{"page":"1","pagination_limit":"10"}
        
        print("header \(header) \n API: \(strService) \n request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                //  completion("success", data.message, .moveToHomeScreen, "")
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                }else { // error :
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                completion("error", message)
            }
        }
    }
    
    /*
     code 400 :
     Something went wrong
     
     code 401  :
     1) Third party is not calling.
     2) some issue in third party api
     
     code 402 :
     Record not found in  third party api
     
     code 200:
     Record is found
     */
    
    func callPreCreatePost_Api(requestParam : [String : AnyObject], completion: @escaping (_ result: String, _ message : String,_ popupMsg : String) -> Void)
    {
        var message  = ""
        var popUpMsg = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        //param.updateValue(userId as AnyObject, forKey: KEY.UserDetails.ProfileId)
        let strService = APPURL.Urls.preCreatePOst
        param =  requestParam
        //{"page":"1","pagination_limit":"10"}
        
        print("header \(header) \n API: \(strService) \n request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                //  completion("success", data.message, .moveToHomeScreen, "")
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    if let msg =  dict.object(forKey: "popup_message") as? String {
                        popUpMsg = msg
                    }
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success200", message, popUpMsg)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                    if data.message == "" {
                        message = AlertMessages.shareInstance.somethingWrongTryAgain
                    }else{
                        message = data.message
                    }
                    
                    completion("success400", message, popUpMsg)
                    
                }else if dict.object(forKey: "code") as! Int == 401 { // error :
                    
                    if data.message == "" {
                        message = AlertMessages.shareInstance.someIssueAPICalling
                    }else{
                        message = data.message
                    }
                    
                    completion("success401", message, popUpMsg)
                    
                }else if dict.object(forKey: "code") as! Int == 402{
                    
                    if let msg =  dict.object(forKey: "popup_message") as? String {
                        popUpMsg = msg
                    }
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    completion("success402", message, popUpMsg)
                }
            }
            else
            {
                if data.message == "" {
                    message = AlertMessages.shareInstance.somethingWrongTryAgain
                }else{
                    message = data.message
                }
                
                completion("error", message, popUpMsg)
            }
        }
    }
    
    
    //make request :
    
    func makeRequest(delegate :CallBackMakeRequestDelegate,
                        strTitle : String,
                     strTickets : String ,
                     strSubAttributes : String,
                     notification_private  : Int,
                     notification_public: Int,
                     publicIndicatorActiveStatus : Int,
                     displayPictureIndicatorStatus : Int,
                     getCreatePostInfo : CreatePostModelInfoClass,
                     arrTagsMyAttributes : [String],
                     arrTagsMyPreferences : [String] ,
                     createPostTimeIntervalID : Int,
                     matchCriteriaActiveStatus : Int){
        self.makeRequestDelegate = delegate
        let userID = Constants.userDefault.object(forKey: Variables.user_id)
        
        // create request :
        var param = [String : AnyObject]()
        param.updateValue(strTitle as AnyObject, forKey:"description")
        param.updateValue( userID as AnyObject, forKey:"user_id")
        
        param.updateValue( strTickets as AnyObject, forKey:"attribute")
        
        //Popup
        param.updateValue( notification_private as AnyObject, forKey:"notification_private")
        param.updateValue( notification_public as AnyObject, forKey:"notification_public")
        
        // add new two request param:
        param.updateValue(publicIndicatorActiveStatus as AnyObject, forKey:"public_indicator")
        param.updateValue(displayPictureIndicatorStatus as AnyObject ,forKey: "picture_display")
        
        let strCurrentLocalDateTime = Date.getCurrentDate(date: Date())
        param.updateValue(strCurrentLocalDateTime as AnyObject ,forKey: "post_create_locat_time")
        
        
        if getCreatePostInfo.time == "today"{
            //
            let strLocalDate = Date.getCurrentDate(date: Date())
            param.updateValue( strLocalDate as AnyObject, forKey:"date")
            
        }else{
            let yesterDayDate : Date = Date() - 1
            let strLocalYesterDayDate = Date.getCurrentDate(date: yesterDayDate)
          
            param.updateValue( strLocalYesterDayDate as AnyObject, forKey:"date")
           
        }
        
        param.updateValue( "" as AnyObject, forKey:"city")
        
        if getCreatePostInfo.activeScreen == .subway || getCreatePostInfo.activeScreen == .bus {
            
            if getCreatePostInfo.isCountryFound {
                param.updateValue( getCreatePostInfo.country as AnyObject, forKey:"country_name")
                
            }else{
                param.updateValue( "" as AnyObject, forKey:"country_name")
            }
            
        }else if getCreatePostInfo.activeScreen == .place || getCreatePostInfo.activeScreen == .street{
            
            param.updateValue( retrieveLastOffset(placeAPIDetailData1: getCreatePostInfo.placeAPIData1) as AnyObject, forKey:"country_name")
        }
        
        //param.updateValue( 0 as AnyObject, forKey:"min_user_attribute")
        //param.updateValue( arrTagsMyPreferences.count as AnyObject, forKey:"min_partner_attribute")
        
        param.updateValue( arrTagsMyAttributes.count as AnyObject, forKey:"min_user_attribute")
        param.updateValue( arrTagsMyPreferences.count as AnyObject, forKey:"min_partner_attribute")
        
        switch getCreatePostInfo.activeScreen {
        case .airplane:
            param.updateValue( 1 as AnyObject ,forKey:"event_subtype")
            break;
            
        case .subway:
            param.updateValue( 2 as AnyObject ,forKey:"event_subtype")
            break;
            
        case .place:
            param.updateValue( 3 as AnyObject ,forKey:"event_subtype")
            break;
            
        case .street:
            param.updateValue( 4 as AnyObject ,forKey:"event_subtype")
            break;
            
        case .bus:
            param.updateValue( 5 as AnyObject ,forKey:"event_subtype")
            break;
            
        case .train:
            param.updateValue( 6 as AnyObject ,forKey:"event_subtype")
            break;
        case .none:
            break;
        }
        
        // from :
        var strUser_attribute_fav = ""
        for index in 0..<12{
            let key  = "user_attribute_\(index + 1)"
            
            if arrTagsMyAttributes.count > index {
                
                if strUser_attribute_fav == "" {
                    strUser_attribute_fav = arrTagsMyAttributes[index]
                }else{
                    strUser_attribute_fav = strUser_attribute_fav + "," + arrTagsMyAttributes[index]
                }
                
                param.updateValue( arrTagsMyAttributes[index] as AnyObject ,forKey: key)
            }else{
                param.updateValue( "" as AnyObject ,forKey: key )
            }
        }
        
        param.updateValue( strUser_attribute_fav as AnyObject ,forKey: "user_attribute_fav")
        
        // To :
        var strPartner_attribute_fav = ""
        for (index,_) in arrTagsMyPreferences.enumerated() {
            let key = "partner_attribute_\(index + 1)"
            
            if strPartner_attribute_fav == "" {
                strPartner_attribute_fav =  arrTagsMyPreferences[index]
            }else{
                strPartner_attribute_fav = strPartner_attribute_fav + "," + arrTagsMyPreferences[index]
            }
            param.updateValue( arrTagsMyPreferences[index] as AnyObject ,forKey: key)
        }
        
        param.updateValue( strPartner_attribute_fav as AnyObject ,forKey: "partner_attribute_fav")
        
        //sub_attribute:
        if strSubAttributes != "" {
            param.updateValue( strSubAttributes as AnyObject ,forKey: "sub_attribute")
        }else{
            param.updateValue( "" as AnyObject ,forKey: "sub_attribute")
        }
        
        //new param:
        param.updateValue(createPostTimeIntervalID as AnyObject ,forKey: "time_interval")
        param.updateValue("1" as AnyObject ,forKey: "time_display")
        param.updateValue(matchCriteriaActiveStatus as AnyObject ,forKey: "time_required")
        
        //  if getCreatePostInfo.activeScreen == .airplane{
//        if matchCriteriaActiveStatus == 0 {
//            param.updateValue("0" as AnyObject ,forKey: "time_required")
//        }else if matchCriteriaActiveStatus == 1{
//            param.updateValue("1" as AnyObject ,forKey: "time_required")
//        }
//
        
        if getCreatePostInfo.activeScreen == .airplane {
            makeRequestDelegate?.call_preCreatePostVM(param: param)
        }else if getCreatePostInfo.activeScreen == .place || getCreatePostInfo.activeScreen == .street{
            
            if let place_id = getCreatePostInfo.placeAPIData1.object(forKey: "place_id") as? String {
                param.updateValue(place_id as AnyObject ,forKey: "attribute")
                
            }else{
                param.updateValue("" as AnyObject ,forKey: "attribute")
            }
            param.updateValue( "" as AnyObject ,forKey: "sub_attribute")
            //   param.updateValue( "" as AnyObject, forKey:"city")
            
            makeRequestDelegate?.call_checkPlacePostVM(preParam: makeRequestForPlacestreet( getCreatePostInfo : getCreatePostInfo), mainCreatePostParam: param)
            
        }else{
            print("request param : \(param)")
            makeRequestDelegate?.call_createPostVM(param: param, displayLoader: true)
        }
    }
    
    func retrieveLastOffset(placeAPIDetailData1 : NSDictionary)->String{
        if let arrTerms  = placeAPIDetailData1.object(forKey: "terms") as? [Any]{
            if let lastValue = arrTerms.last as? NSDictionary {
                return "\(lastValue.object(forKey: "value") ?? "")"
            }
        }
        return ""
    }
    
    func makeRequestForPlacestreet( getCreatePostInfo : CreatePostModelInfoClass)-> [String : AnyObject]{
        var placeAPIDetailData = [String : AnyObject]()
        
        
        //1. place_id ----------
        if let place_id = getCreatePostInfo.placeAPIData1.object(forKey: "place_id") as? String {
            placeAPIDetailData.updateValue(place_id as AnyObject ,forKey: "place_id")
        }else{
            placeAPIDetailData.updateValue("" as AnyObject ,forKey: "place_id")
        }
        
        
        //2. description --------
        if let description = getCreatePostInfo.placeAPIData1.object(forKey: "description") as? String {
            
            placeAPIDetailData.updateValue(description as AnyObject ,forKey: "description")
        }else{
            placeAPIDetailData.updateValue("" as AnyObject ,forKey: "description")
        }
        
        
        // main_text , secondary_text
        let dictFormate = getCreatePostInfo.placeAPIData1.object(forKey: "structured_formatting") as! NSDictionary
        //3. main_text -----------
        if let main_text = dictFormate.object(forKey: "main_text") as? String {
            placeAPIDetailData.updateValue(main_text as AnyObject ,forKey: "main_text")
        }else{
            placeAPIDetailData.updateValue("" as AnyObject ,forKey: "main_text")
        }
        
        
        //4.secondary_text
        if let secondary_text = dictFormate.object(forKey: "secondary_text") as? String {
            placeAPIDetailData.updateValue(secondary_text as AnyObject ,forKey: "secondary_text")
        }else{
            placeAPIDetailData.updateValue("" as AnyObject ,forKey: "secondary_text")
        }
        
        
        //5. types
        let arrType = getCreatePostInfo.placeAPIData1.object(forKey: "types") as! [String]
        if arrType.count > 0 {
            placeAPIDetailData.updateValue((arrType as NSArray).componentsJoined(by: ",") as AnyObject ,forKey: "types")
            
        }else{
            placeAPIDetailData.updateValue("" as AnyObject ,forKey: "types")
        }
        
        
        // 2nd API: --------------------------------------------------->
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "sublocality_level_1")
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "sublocality")
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "locality")
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "administrative_area_level_2")
        
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "postal_code")
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "street_number")
        
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "neighborhood")
        
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "route_short")
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "route_long")
        
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "administrative_area_level_1_short")
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "administrative_area_level_1_long")
        
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "country_short")
        placeAPIDetailData.updateValue("" as AnyObject ,forKey: "country_long")
        
        
        if let arrComponents = getCreatePostInfo.placeAPIData2.object(forKey: "address_components") as? [Any]{
            
            for dictItem in arrComponents{
                
                if let dictComponent = dictItem as? NSDictionary{
                    
                    if let arrType = dictComponent.object(forKey: "types") as? [Any] {
                        
                        for type in arrType {
                            
                            switch("\(type)"){
                            
                            //6.
                            case "sublocality_level_1":
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "sublocality_level_1")
                                
                                break
                                
                            //7.
                            case "sublocality" :
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "sublocality")
                                break
                                
                            //8.
                            case "locality":
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "locality")
                                
                                break
                                
                            //9.
                            case "administrative_area_level_2":
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "administrative_area_level_2")
                                break
                                
                            //10. short_name long_name
                            case "administrative_area_level_1":
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "short_name") ?? "")" as AnyObject ,forKey: "administrative_area_level_1_short")
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "administrative_area_level_1_long")
                                break
                                
                            //11.
                            case "country":
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "short_name") ?? "")" as AnyObject ,forKey: "country_short")
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "country_long")
                                break
                                
                                
                            case "postal_code":
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "postal_code")
                                break
                                
                            case "street_number":
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "street_number")
                                break
                                
                            case "route":
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "short_name") ?? "")" as AnyObject ,forKey: "route_short")
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "route_long")
                                break
                                
                            case "neighborhood":
                                placeAPIDetailData.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "neighborhood")
                                break
                                
                            // let postCode = addressComponent.name
                            //       print("postCode : \(postCode)")
                            //   print("------------------------------------------")
                            
                            default:
                                break
                            }
                        }
                    }
                }
            }
        }
        
        placeAPIDetailData.updateValue("\(getCreatePostInfo.placeAPIData2.object(forKey: "formatted_address") ?? "")" as AnyObject ,forKey: "formatted_address")
        
        return placeAPIDetailData
    }
    
    
    
    //------------------------------------------
    func convertDateFormaterForUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.string(from: date!)
    }
    
    
    func convertDateFormaterForNonUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    func getTimeStatus() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en-US")
        formatter.dateFormat = "a"
        let time12 = formatter.string(from: date)
        return time12
    }
    
    
    func getTimeInterval()-> String{
      //  let calendar = Calendar.current
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as? Int ?? 0
        
        if countryDialCode == 1{ // for us date formate : 12 hours
              let calendar = Calendar.current
            let date = Date()
            let formatter = DateFormatter()
            
            formatter.dateStyle = .none
            formatter.timeStyle = .short
            formatter.locale = Locale(identifier: "en_US_POSIX")
            
            formatter.dateFormat = "hh a" // "a" prints "pm" or "am"
            let hourString = formatter.string(from:date) // "12 AM"
            let minutes = calendar.component(.minute, from: date)
            let hours = hourString.components(separatedBy: " ")
            
            var minValue = ""
            if hours.count > 1 {
                minValue = hours[1]
            } else {
                minValue = getTimeStatus()
            }

            if 0 <= minutes && 15 >= minutes {
                return "\(hours[0]):00-15 \(minValue)"
            }else if 15 <= minutes && 30 >= minutes{
                return "\(hours[0]):15-30 \(minValue)"
            }else if  30 <= minutes && 45 >= minutes{
                return "\(hours[0]):30-45 \(minValue)"
            }else if  45 <= minutes {
                return "\(hours[0]):45-00 \(minValue)"
            }
        }
        
        else{// for non US : date formate 24 hours :s
            let today = Date()
            // 2. Pick the date components
            let hour   = (Calendar.current.component(.hour, from: today))
            let minutes = (Calendar.current.component(.minute, from: today))
            if 0 <= minutes && 15 >= minutes {
                return "\(String(format: "%02d", hour)):00-15"
            }else if 15 <= minutes && 30 >= minutes{
                return "\(String(format: "%02d", hour)):15-30"
            }else if  30 <= minutes && 45 >= minutes{
                return "\(String(format: "%02d", hour)):30-45"
            }else if  45 <= minutes {
                return "\(String(format: "%02d", hour)):45-00"
            }
        }
        return ""
    }
    
    
    //---------------------------------------

    func headerTitleforPlace( getCreatePostInfo : CreatePostModelInfoClass,strDate: String)-> String{
        var placeHeaderTitle  = ""
        
        let dictFormate = getCreatePostInfo.placeAPIData1.object(forKey: "structured_formatting") as! NSDictionary
        
        // formated address:
        if  let formatedAdd =  getCreatePostInfo.placeAPIData2.object(forKey: "formatted_address") as? String  {
            
            if "\(dictFormate.object(forKey: "main_text") ?? "--")".count > 0 {
                
                if placeHeaderTitle.count > 0 {
                    placeHeaderTitle = "\(placeHeaderTitle) \(dictFormate.object(forKey: "main_text") ?? "--")"
                }else{
                    placeHeaderTitle = "\(dictFormate.object(forKey: "main_text") ?? "--")"
                }
            }
            
            if formatedAdd.count > 0  {
                if placeHeaderTitle.count > 0 {
                    placeHeaderTitle = "\(placeHeaderTitle), \(formatedAdd)"
                }else{
                    placeHeaderTitle = "\(formatedAdd)"
                }
            }
            
            if strDate.count > 0  {
                if placeHeaderTitle.count > 0  {
                    placeHeaderTitle = "\(placeHeaderTitle). \(strDate)"
                }else{
                    placeHeaderTitle = "\(strDate)"
                }
            }
        }
        return placeHeaderTitle
    }
    
    
    func headerTitleforPlaceSubway( getCreatePostInfo : CreatePostModelInfoClass,strDate: String)->String{
        var subwayHeaderTitle = ""
        
        if "\(getCreatePostInfo.flight)".count > 0 {
            
            if  subwayHeaderTitle.count > 0 {
                subwayHeaderTitle = "\(subwayHeaderTitle) \(getCreatePostInfo.flight)"
            }else{
                subwayHeaderTitle = "\(getCreatePostInfo.flight)"
            }
        }
        
        if "\(SelectedCityFullNameCreatePost)".count > 0 {
            //lblHeaderTitle.text = "\(lblHeaderTitle.text ?? ""). \(SelectedCityFullNameCreatePost)"
            
            if subwayHeaderTitle.count > 0 {
                subwayHeaderTitle = "\(subwayHeaderTitle). \(SelectedCityFullNameCreatePost)"
            }else{
                subwayHeaderTitle = "\(SelectedCityFullNameCreatePost)"
            }
        }
        
        if "\(strDate)".count > 0   {
            //lblHeaderTitle.text = "\(lblHeaderTitle.text ?? ""). \(strDate)"
            
            if subwayHeaderTitle.count > 0 {
                subwayHeaderTitle = "\(subwayHeaderTitle). \(strDate)"
            }else{
                subwayHeaderTitle = "\(strDate)"
            }
        }
        return subwayHeaderTitle
         
    }
    
    func headerTitleforPlaceStreet( getCreatePostInfo : CreatePostModelInfoClass,strDate: String)->String{
        
        var streetHeaderTitle  = ""
        
        //lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(strDate)"
        
        let dictFormate = getCreatePostInfo.placeAPIData1.object(forKey: "structured_formatting") as! NSDictionary
        
        if "\(dictFormate.object(forKey: "main_text") ?? "--")".count > 0  {
            //  lblHeaderTitle.text = "\(lblHeaderTitle.text ?? "") \(0dictFormate.object(forKey: "main_text") ?? "--")"
            
            if streetHeaderTitle.count > 0 {
                streetHeaderTitle = "\(streetHeaderTitle) \(dictFormate.object(forKey: "main_text") ?? "--")"
            }else{
                streetHeaderTitle = "\(dictFormate.object(forKey: "main_text") ?? "--")"
            }
        }
        
        if "\(dictFormate.object(forKey: "secondary_text") ?? "--")".count > 0 {
            //   lblHeaderTitle.text = "\(lblHeaderTitle.text ?? ""), \(dictFormate.object(forKey: "secondary_text") ?? "--")"
            
            if streetHeaderTitle.count > 0 {
                streetHeaderTitle = "\(streetHeaderTitle), \(dictFormate.object(forKey: "secondary_text") ?? "--")"
            }else{
                streetHeaderTitle = "\(dictFormate.object(forKey: "secondary_text") ?? "--")"
            }
        }
        
        
        if strDate.count > 0  {
            //    lblHeaderTitle.text = "\(lblHeaderTitle.text ?? ""). \(strDate)"
            
            if streetHeaderTitle.count > 0 {
                streetHeaderTitle = "\(streetHeaderTitle). \(strDate)"
            }else{
                streetHeaderTitle = "\(strDate)"
            }
        }
        
        return streetHeaderTitle
    }
    
    func headerTitleforPlaceBus( getCreatePostInfo : CreatePostModelInfoClass,strDate: String)->String{
        var busHeaderTitle = ""
        
        if "\(getCreatePostInfo.flight)".count > 0  {
            // lblHeaderTitle.text = "\(lblHeaderTitle.text ?? "") \(getCreatePostInfo.flight)"
            
            if busHeaderTitle.count > 0 {
                busHeaderTitle = "\(busHeaderTitle) \(getCreatePostInfo.flight)"
            }else{
                busHeaderTitle = "\(getCreatePostInfo.flight)"
            }
        }
        
        if "\(strDate)".count > 0  {
            
            if busHeaderTitle.count > 0 {
                busHeaderTitle = "\(busHeaderTitle). \(strDate)"
            }else{
                busHeaderTitle = "\(strDate)"
            }
        }
        
        return busHeaderTitle
    }
}

