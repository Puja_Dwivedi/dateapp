//
//  CreatePostStep3VC.swift
//  MyFirstApp
//
//  Created by cis on 05/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import SearchTextField


class CreatePostStep3VC: UIViewController {
    //MARK:- IBOutlet
    //MARK:-
    // Tags with textFields:--------
    
    //1. My attribtues:
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet weak var scrollViewMyAttibutes: UIScrollView!
    @IBOutlet weak var scrollViewMyAttibutes_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes: PinTextField!
    @IBOutlet weak var txtFldMyAttributes_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes_x: NSLayoutConstraint!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnYou: UIButton!
    @IBOutlet var btnMe: UIButton!
    var arrTagsMyAttributes = [String]()
    
    //2.My Preferences
    @IBOutlet weak var scrollViewMyPreferences: UIScrollView!
    @IBOutlet weak var scrollViewMyPreferences_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences: PinTextField!
    @IBOutlet weak var txtFldMyPreferences_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences_x: NSLayoutConstraint!
    var arrTagsMyPreferences = [String]()
    var activePlaceTextFld = ActivePlaceTextFldOption.non
    
    @IBOutlet weak var rootScrollView: UIScrollView!
    
    @IBOutlet weak var txtFrom_width: NSLayoutConstraint!
    @IBOutlet weak var txtTo_width: NSLayoutConstraint!
    
    
    //MARK:- Variables
    //MARK:-
    
    // toggles:
    var backCreateDelegate : BackCreatePostDelegate?
    var timeInterVal = ActiveTime.today
    var createPostTimeIntervalID : Int?
    
    var viewHoursVM = ViewHoursViewModel() // for getting array of ids :
    var createPostVM = CreatePostViewModel()
    
    var arrAllAutoComplete = [String]()
    var arrSearchAutoComplete = [String]()
    
    // manage the back space of the tags.
    var isEmptyBackFrom = true
    var isEmptyBackTo = true
    var countryDialCode = 0//Constants.userDefault.object(forKey: Variables.date_format) as! Int
    
    // create view popus:
    var objCreateView1 = CreatePostView1()
    var objCreateView2 = CreatePostView2()
    
    // get this info from previous screen :
    var getCreatePostInfo = CreatePostModelInfoClass()//(activeScreen : CreatePostActiveScreen.airplane ,flight : "", location : "", subAtrribute : "" , time : "", arrPrefrenceTags : [""], placeAPIData1 : NSDictionary(), placeAPIData2 : NSDictionary(), isCountryFound : false, country : "", state : "")
    
    var defaultUserCurrentLocation = ""
    var isCheckTest = false
    
    // for popup:
    var notification_public = 0;
    var notification_private = 0;
    
    
    //MARK:- App Flow
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppDelegate.shared.isBackPressedFromTag {
            self.navigationController?.popViewController(animated: false)
        } else {
            initViewSetup()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.rootScrollView.scrollToTop()
        UserDefaults.standard.set(arrTagsMyPreferences, forKey: "MyPreferences")
        UserDefaults.standard.set(arrTagsMyAttributes, forKey: "MyAttributes")
        //        UserDefaults.standard.set(createPostTimeIntervalID, forKey: "PostTimeId")
    }
    
    func initViewSetup() {
        //        DeviceSize.screen_Height > 736.0 ? (rootScrollView.isScrollEnabled = false) : (rootScrollView.isScrollEnabled = true)
        self.btnBack.isUserInteractionEnabled = true
        UserDefaults.standard.set(true, forKey: "MoveFromStep3")
        txtFldMyAttributes.delegate = self
        txtFldMyPreferences.delegate = self
        setInitialText()
        Loader.sharedInstance.showLoader(msg: "Loading...")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.setTags()
            Loader.sharedInstance.stopLoader()
        }
    }
    
    func setTags() {
        getTags()
    }
    
    func getTags() {
        //      let time =   "26/08/2021 01:00:00 59+0530".localToUTC(incomingFormat: "dd/MM/yyyy HH:mm:ss ssZZZ", outGoingFormat: "dd/MM/yyyy HH:mm:ss ssZZZ")
        //        print("UTC date time : \(time)")
        if let myAttr = UserDefaults.standard.value(forKey: "MyAttributes") as? [String] {
            self.arrTagsMyAttributes = myAttr
            createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
        }
        
        if let myPref = UserDefaults.standard.value(forKey: "MyPreferences") as? [String] {
            self.arrTagsMyPreferences = myPref
            createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
        }
        
        if self.arrTagsMyAttributes == [] && self.arrTagsMyPreferences == [] {
            txtFldMyAttributes.text = ""
            txtFldMyPreferences.text = ""
            
            arrTagsMyAttributes.removeAll()
            arrTagsMyPreferences.removeAll()
            initView()
            
            getProfileInfo() // api call :

        }
        
        
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
    }
    
    func checkValidation() {
        guard arrTagsMyPreferences.count > 0 else { SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.pleaseAddAtLeastOneTagInMyMatchTags)", showMsgAt: .bottom); return}
        
        guard arrTagsMyAttributes.count > 0 else { SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.pleaseAddAtLeastOneTagInMyTags)", showMsgAt: .bottom); return}
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostStep4VC") as! CreatePostStep4VC
        vc.arrTagsMyPreferences = arrTagsMyPreferences
        vc.arrTagsMyAttributes = arrTagsMyAttributes
        vc.objCreateView1 = objCreateView1
        vc.notification_public = notification_public
        vc.notification_private = notification_private
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    //MARK:- Action Methods
    //MARK:-
    
    @IBAction func actionNext(_ sender: UIButton) {
       checkValidation()
    }
    
    
    @IBAction func actionCancel(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        self.navigationController?.pushViewController(vc, animated: false)
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        AppDelegate.shared.isBackPressedFromTag = true
        self.btnBack.isUserInteractionEnabled = false
        Loader.sharedInstance.showLoader(msg: "Loading...")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.navigationController?.popViewController(animated: false)
            Loader.sharedInstance.stopLoader()
        }
    }
    
    @IBAction func actionMyMatchTagSuggestion(_ sender: UIButton) {
        self.view.endEditing(true)
        var attribute_Text = ""
        var attribute_City = ""
        
        if let attributeText = UserDefaults.standard.value(forKey: "attribute_text") as? String {
            attribute_Text = attributeText
        }
        
        if let attributeCity = UserDefaults.standard.value(forKey: "attribute_city") as? String {
            attribute_City = attributeCity
        }
        
        self.objCreateView2 =  self.objCreateView2.Show(activeScreen: getCreatePostInfo.activeScreen, strHeaderTitle: "You", isbackButtonActive: true, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self.arrTagsMyPreferences), viewController: self, attribute_text: attribute_Text, attribute_city: attribute_City, totalCount: 12) { [weak self] (arrSelectedTags) in
            
            self?.objCreateView2.removeWithAnimation()
            self?.arrTagsMyPreferences = arrSelectedTags
            self?.createTagsForPreferences(OnView: (self?.scrollViewMyPreferences)!, withArray: self!.arrTagsMyPreferences, isCancelButtonVisible: true)
            self?.initView()
        } as! CreatePostView2
    }
    
    
    @IBAction func actionMyTagsSuggestion(_ sender: UIButton) {
        self.view.endEditing(true)
        
        var attribute_Text = ""
        var attribute_City = ""
        
        if let attributeText = UserDefaults.standard.value(forKey: "attribute_text") as? String {
            attribute_Text = attributeText
        }
        
        if let attributeCity = UserDefaults.standard.value(forKey: "attribute_city") as? String {
            attribute_City = attributeCity
        }
        
        self.objCreateView2 =  self.objCreateView2.Show(activeScreen: getCreatePostInfo.activeScreen, strHeaderTitle: "Me", isbackButtonActive: true, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self.arrTagsMyAttributes), viewController: self, attribute_text: attribute_Text, attribute_city: attribute_City, totalCount: 12) { [weak self](arrSelectedTags) in
            
            self?.objCreateView2.removeWithAnimation()
            
            self?.arrTagsMyAttributes = arrSelectedTags
            self?.createTagsForAttributes(OnView: (self?.scrollViewMyAttibutes)!, withArray: self!.arrTagsMyAttributes, isCancelButtonVisible: true)
//
            self?.initView()
        } as! CreatePostView2
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    func initView(){
        
        // set the info from previous view: =====================4==========
        
        //====================================================================
        //setTimeInterValInfo()
        
        setHeaderInfo()
        txtFldMyAttributes.delegate = self
        txtFldMyPreferences.delegate = self

    }
    
    func setInitialText() {
        if let id = UserDefaults.standard.value(forKey: "eventType") as? Int {
            if id == 1 { //Post
                self.lblTitle.text = "Give us specific details about your looks and unique traits."
            } else { // Checkin
                self.lblTitle.text = "In a few words, tell us how you look and who you’re looking to network with."
            }
        }
        
    }
    
    func setHeaderInfo(){
        var strTime = ""
        if getCreatePostInfo.time == "today"{
            strTime = Date().string(format: "dd/MM/yyyy") // today
        }else{
            
            let yesterDay :Date = Date() - 1// Jan 21, 2019
            strTime = yesterDay.string(format: "dd/MM/yyyy")
        }
        
        var strDate = ""
        if countryDialCode == 1 { // for US : 12 hours time foemate
            strDate = createPostVM.convertDateFormaterForUS(strTime)
        }else{
            strDate = createPostVM.convertDateFormaterForNonUS(strTime)
        }
        
        if getCreatePostInfo.subAtrribute == ""{
            
            if getCreatePostInfo.activeScreen == .place { // subAtrribute is always empty
                //place end:
//                lblHeaderTitle.text =  createPostVM.headerTitleforPlace( getCreatePostInfo : getCreatePostInfo,strDate: strDate) //placeHeaderTitle
                
            }else if getCreatePostInfo.activeScreen == .subway{
                
//                lblHeaderTitle.text = createPostVM.headerTitleforPlaceSubway( getCreatePostInfo : getCreatePostInfo,strDate: strDate)
            }else if getCreatePostInfo.activeScreen == .street{ // subAtrribute is always empty

//                lblHeaderTitle.text = createPostVM.headerTitleforPlaceStreet( getCreatePostInfo : getCreatePostInfo,strDate: strDate)
                
            }else if getCreatePostInfo.activeScreen == .bus{
//                lblHeaderTitle.text = createPostVM.headerTitleforPlaceBus( getCreatePostInfo : getCreatePostInfo,strDate: strDate)
                
            }else {
                // lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCre24atePostInfo.location). \(strDate)"}
                var headerTitle = ""
                
                if "\(getCreatePostInfo.flight)".count > 0 {
                    
                    if headerTitle.count > 0{
                        headerTitle = "\(headerTitle) \(getCreatePostInfo.flight)"
                    }else{
                        headerTitle = "\(getCreatePostInfo.flight)"
                    }
                }
                
                if "\(strDate)".count > 0 {
                    
                    if headerTitle.count > 0 {
                        headerTitle = "\(headerTitle). \(strDate)"
                    }else{
                        headerTitle = "\(strDate)"
                    }
                }
                
            }
            
        }else{
            
            if getCreatePostInfo.activeScreen == .place {
                //  lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCreatePostInfo.subAtrribute). \(strDate)"
                
            }else if getCreatePostInfo.activeScreen == .street{
                //   lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCreatePostInfo.subAtrribute). \(strDate)"
                
            }else if getCreatePostInfo.activeScreen == .bus{
                
                var busHeaderTitle = ""
                if "\(getCreatePostInfo.flight)".count > 0 {
                    
                    if busHeaderTitle.count > 0 {
                        busHeaderTitle = "\(busHeaderTitle) \(getCreatePostInfo.flight)"
                    }else{
                        busHeaderTitle = "\(getCreatePostInfo.flight)"
                    }
                }
                
                
                if "\(strDate)".count > 0 {
                    
                    if  busHeaderTitle.count > 0 {
                        busHeaderTitle = "\(busHeaderTitle). \(strDate)"
                    }else{
                        busHeaderTitle =  "\(strDate)"
                    }
                }
                
//                lblHeaderTitle.text = busHeaderTitle
                
            }else{
                //  lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCreatePostInfo.subAtrribute). \(getCreatePostInfo.location). \(strDate)"
                var headerTitle = ""
                
                if "\(getCreatePostInfo.flight)".count > 0 {
                    if  headerTitle.count > 0 {
                        headerTitle = "\(headerTitle) \(getCreatePostInfo.flight)"
                    }else{
                        headerTitle = "\(getCreatePostInfo.flight)"
                    }
                }
                
                if "\(getCreatePostInfo.subAtrribute)".count > 0 && headerTitle.count > 0{
                    
                    if headerTitle.count > 0 {
                        headerTitle = "\(headerTitle). \(getCreatePostInfo.subAtrribute)"
                    }else{
                        headerTitle = "\(getCreatePostInfo.subAtrribute)"
                    }
                }
                
                if "\(strDate)".count > 0  {
                    
                    if headerTitle.count > 0 {
                        headerTitle = "\(headerTitle). \(strDate)"
                    }else{
                        headerTitle = "\(strDate)"
                    }
                }
                //       lblHeaderTitle.text = "\(getCreatePostInfo.flight). \(getCreatePostInfo.subAtrribute). \(strDate)"
            }
        }
    }
    
    func resetAllFields(){
        
        arrTagsMyAttributes.removeAll()
        arrTagsMyPreferences.removeAll()
        
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
    }
    
    
        
    func setInfo(dict : NSDictionary){
        let item = dict.object(forKey: "data") as! NSDictionary
        
        for index in 1...12 {
            let key = "characteristic_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                let value = "\(item.object(forKey: key) as! String)"
                arrTagsMyAttributes.append(value.lowercased())
            }
        }
        
        for index in 1...5 {
            let key = "partner_preference_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                let value = "\(item.object(forKey: key) as! String)"
                arrTagsMyPreferences.append(value.lowercased())
            }
        }
    
        // for notification popups.
        if let publicValue = item.object(forKey: "notification_public") as? Int {
            notification_public = publicValue
        }
        
        if let privateValue = item.object(forKey: "notification_private") as? Int {
            notification_private = privateValue
        }
        // partner Prefreence :
        // assign from the view :
//        arrTagsMyPreferences = getCreatePostInfo.arrPrefrenceTags
        if arrTagsMyPreferences.count > 0 {
            if arrTagsMyPreferences[0] == "" {
                self.arrTagsMyPreferences.remove(at: 0)
            }
            createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
        }
        if arrTagsMyAttributes.count > 0 {
            createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)

        }
    }
    
}



/*
 MARK:- API's
 */
extension CreatePostStep3VC {
    
    // get profile info:
    func getProfileInfo()
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        createPostVM.getProfileApi() { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.setInfo(dict: dictData)
            }else{
                
                // SnackBar.sharedInstance.show(message: message)
            }
        }
    }
}

extension CreatePostStep3VC : PinTexFieldDelegate{
    //MARK:- PinTexFieldDelegate
    func didPressBackspace(textField : PinTextField){
        print("back prashed")
        
        // My attibutes :
        if textField == txtFldMyAttributes {
            
            // for removed :
            if arrTagsMyAttributes.count > 0 && (txtFldMyAttributes.text?.isEmpty == true) {
                
                if isEmptyBackFrom {
                    arrTagsMyAttributes.remove(at: arrTagsMyAttributes.count - 1)
                    createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: true)
                }
                isEmptyBackFrom = true
            }
            
            // My Preferences :
        }else if textField == txtFldMyPreferences{
            
            // for removed :
            if arrTagsMyPreferences.count > 0 && (txtFldMyPreferences.text?.isEmpty ?? false){
                
                if isEmptyBackTo {
                    arrTagsMyPreferences.remove(at: arrTagsMyPreferences.count - 1)
                    createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: true)
                }
                isEmptyBackTo = true
            }
        }
    }
}


