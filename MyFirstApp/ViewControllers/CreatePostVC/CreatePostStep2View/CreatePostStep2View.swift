

//
//  CreatePostView2.swift
//  MyFirstApp
//
//  Created by cis on 24/05/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import SDWebImage


class CreatePostStep2View: UIView {
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet var lblTopConstraint: NSLayoutConstraint!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet weak var mainViewContainer: UIView!
            
    @IBOutlet var constraintTextViewHeight: NSLayoutConstraint!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var lblTextCount: UILabel!
    
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var txtViewDesc: UITextView!
    
    private var isOversized = false {
            didSet {
                txtViewDesc.isScrollEnabled = isOversized
            }
        }
    
    //MARK:-
    //MARK:- variables:

    var activeScreen = CreatePostActiveScreen.airplane // default
    var totalCount = 0
    var activeIndex = 0
    var placeholder = "Start writing here..."
    private let maxHeight: CGFloat = 70
    var lineCount = 0
    let placeholderColor = AppDelegate.shared.textFieldPlaceholderColor
    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        let window = UIApplication.shared.windows[0]
        
        backgroundColor = UIColor.white
      
        
//        let layout = TagFlowLayout()
//        layout.estimatedItemSize = CGSize(width: 140, height: 40)
//        self.constraintTextViewHeight.constant = maxHeight
        
       
    }
    
    private var previousRect:CGRect = CGRect.zero
    
    private func checkIfReturnOrLineWrap(textView:UITextView) {
        let currentRect = textView.caretRect(for:textView.endOfDocument)
        if (currentRect.origin.y != previousRect.origin.y && (previousRect.origin.y != CGFloat.infinity || currentRect.origin.y > 0) || currentRect.origin.y == CGFloat.infinity) && (self.lblTopConstraint.constant == 2.5 && self.lineCount > 2) {
            //React to the line change!
//            self.lblTopConstraint.constant = self.lblTopConstraint.constant + 5.0
            self.constraintTextViewHeight.constant = self.constraintTextViewHeight.constant + 14.0
            
        }
        previousRect = currentRect
    }
    
    //MARK:-
    //MARK:- Main Method
    
    func Show(activeScreen : CreatePostActiveScreen,strHeaderTitle : String,isbackButtonActive : Bool,isTabBarAvailable : Bool, isAvailable : (isTxtPreviousTag : Bool, arrTxtTags : [String]), viewController : UIViewController , attribute_text : String, attribute_city : String ,totalCount : Int, onCompletion: @escaping ([String])-> Void)-> UIView{
        
        let customPopUp = Bundle.main.loadNibNamed("CreatePostStep2View", owner: self, options: nil)?[0] as! CreatePostStep2View
        viewController.tabBarController?.tabBar.backgroundColor = .white
        
        customPopUp.frame = UIScreen.main.bounds
//        customPopUp.onCloser = onCompletion
//        customPopUp.txtFldMyPreferences.text = ""
//        customPopUp.info.attribute_text = attribute_text
//        customPopUp.info.attribute_city = attribute_city
//        customPopUp.lblHeaderTitle.text = strHeaderTitle
//        customPopUp.activeScreen = activeScreen
//        customPopUp.totalCount = totalCount
//
//        customPopUp.isAvailable = isAvailable
//        customPopUp.isTabBarAvailable = isTabBarAvailable
//
//        customPopUp.post2VM.arrCategory.removeAll()
//        customPopUp.post2VM.arrTag.removeAll()
//        customPopUp.post2VM.arrTextTags.removeAll()
//        customPopUp.arrTxtTagsMyPreferences.removeAll()
//        customPopUp.menuBarView.currentIndex = 0
//        customPopUp.menuBarView.collView.scrollToTop()
//        customPopUp.menuBarView.collView.reloadData()
//
//        if isbackButtonActive{
//            customPopUp.isbackButtonActive = true
//            customPopUp.btnNext.isHidden = true
//        }else{
//            customPopUp.isbackButtonActive = false
//            customPopUp.btnNext.isHidden = false
//        }
        
        customPopUp.initView()
        viewController.view.addSubview(customPopUp)
        
        customPopUp.showWithAnimation()
        return customPopUp
    }
    
    func initView(){
        self.lblTextCount.textColor = .darkGray
//        btnBack.underline()
        self.lblTopConstraint.constant = 2.5
        self.constraintTextViewHeight.constant = 30
        if let desc = UserDefaults.standard.value(forKey: "tell_us_your_mind") as? String {
            self.txtViewDesc.text = desc
            txtViewDesc.textColor = .black
            self.lblTextCount.text = "\(self.txtViewDesc.text.count)/100"
        } else {
            txtViewDesc.text = placeholder
            txtViewDesc.textColor = placeholderColor
            self.lblTextCount.text = "0/100"
        }
        NotificationCenter.default.post(name: NSNotification.Name("hideTabs"),object: nil)
        
        if let id = UserDefaults.standard.value(forKey: "eventType") as? Int {
            if id == 1 {
                self.lblTitle.text = "Write Something cute to your crush."
                self.lblSubtitle.text = "Make them feel special."
            } else {
                self.lblTitle.text = "Craft a compelling message. "
                self.lblSubtitle.text = "Stand out from the crowd."
            }
        }
    }
    
    
    func showWithAnimation(){
        //============== Show with animation
        
        mainViewContainer.alpha = 1
        
//        UIView.animate(withDuration: 0.7, animations: {
//            self.mainViewContainer.alpha = 1
//            self.layoutIfNeeded()
//        }) { (true) in
//            
//            UIView.animate(withDuration: 0.5) {
//                self.mainViewContainer.alpha = 1
//                // self.layoutIfNeeded()
//            }
//        }
    }
    
    
    func removeWithAnimation(){
        mainViewContainer.alpha = 1
        
        UIView.animate(withDuration: 0.7, animations: {
            self.mainViewContainer.alpha = 0
            
            self.layoutIfNeeded()
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    
    func updateInfo(isTabBarAvailable : Bool , isAvailable: (isTxtPreviousTag: Bool, arrTxtTags: [String]), attribute_text: String, attribute_city: String){
        

        
    }
    
    //MARK:-
    //MARK:- Action Methods :
    
    @IBAction func actionCancel(_ sender: UIButton) {
        self.removeFromSuperview()
        NotificationCenter.default.post(name: NSNotification.Name("moveToRootStep2AVC"),object: nil)
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.removeFromSuperview()
        if txtViewDesc.text != "Start writing here..." {
            UserDefaults.standard.set(self.txtViewDesc.text, forKey: "tell_us_your_mind")
        }
        NotificationCenter.default.post(name: NSNotification.Name("moveToStep2AVC"),object: nil)
//        NotificationCenter.default.post(name: NSNotification.Name("moveToStep1"),object: nil)
//        self.removeFromSuperview()
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        self.checkValidation()
    }
    
    func checkValidation() {
        var strTitle = ""
        if txtViewDesc.text.isEmpty || txtViewDesc.text == "Start writing here..." {
            strTitle = AlertMessages.shareInstance.messageLessTenAlert
        } else if txtViewDesc.text.count < 10 {
            strTitle = AlertMessages.shareInstance.messageLessTenAlert
        } else if txtViewDesc.text.count > 100 {
            strTitle = AlertMessages.shareInstance.messageMoreHundTitleAlert
        } else {
            UserDefaults.standard.set(self.txtViewDesc.text, forKey: "tell_us_your_mind")
            self.removeFromSuperview()
            NotificationCenter.default.post(name: NSNotification.Name("moveToStep3"),object: nil)
        }
        
        if strTitle.count > 0 {
            SnackBar.sharedInstance.show(message: strTitle, showMsgAt: .bottom);
        }
    }
    
}

extension CreatePostStep2View: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == placeholderColor {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Start writing here..."
            textView.textColor = placeholderColor
            placeholder = ""
        } else {
            placeholder = textView.text
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.lblTextCount.text = "\(self.txtViewDesc.text.count)/100"
        self.lblTextCount.textColor = .darkGray
//        if self.txtViewDesc.text.count > 100 {
//            self.lblTextCount.textColor = .red
//        } else {
//            self.lblTextCount.textColor = .darkGray
//        }
        
        if textView.contentSize.height >= maxHeight {
            isOversized = true
        }
        checkIfReturnOrLineWrap(textView: textView)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if txtViewDesc.text != "Start writing here..." {
            UserDefaults.standard.removeObject(forKey: "tell_us_your_mind")
        }
        self.lineCount = self.lineCount + 1
        checkIfReturnOrLineWrap(textView: textView)
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if newText.count > 100 {
            return false
        }
        return true
    }
}










