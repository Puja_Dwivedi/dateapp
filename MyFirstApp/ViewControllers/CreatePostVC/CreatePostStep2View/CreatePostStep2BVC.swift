//
//  CreatePostStep2BVC.swift
//  MyFirstApp
//
//  Created by cis on 04/07/23.
//  Copyright © 2023 cis. All rights reserved.
//

import UIKit

class CreatePostStep2BVC: UIViewController {

    @IBOutlet var viewStep2: CreatePostStep2View!
    
    var createPostTimeIntervalID : Int?
    var viewHoursVM = ViewHoursViewModel()
    var objCreateView2 = CreatePostStep2View()
    var activeScreen = CreatePostActiveScreen.airplane
    var slideActiveRow   : Int? = nil
    var countryDialCode = 0 //Constants.userDefault.object(forKey: Variables.date_format) as! Int
    var timeInterVal = ActiveTime.today
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,selector:#selector(moveToStep2AVC(_:)),name: NSNotification.Name ("moveToStep2AVC"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(moveToRootStep2AVC(_:)),name: NSNotification.Name ("moveToRootStep2AVC"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(createPost(_:)),name: NSNotification.Name ("moveToStep3"),object: nil)
        //moveToStep2AVC
    }
    
    @objc func createPost(_ notification: Notification){
        let objStep1 = CreatePostView1()
        objStep1.removeFromSuperview()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostStep3VC") as! CreatePostStep3VC
        vc.objCreateView1 = objStep1
        AppDelegate.shared.isBackPressedFromTag = false
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initView()
    }
    
    private func initView() {
        self.objCreateView2 =   self.objCreateView2.Show(activeScreen: activeScreen , strHeaderTitle: "Describe my crush", isbackButtonActive: false, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: false, arrTxtTags: [String()]), viewController: self, attribute_text: "", attribute_city: "", totalCount: 12) { (arrSelectedTags) in
            
            self.moveToCreatePostScreen(activeScreen: self.activeScreen , flight: "", location: "", subAttribute: "", timeStatus: "", arrSelectedPreferenceTag: [],placeAPIData1 : [:], placeAPIData2 : [:], isCountryFound: false,country: "",state: "")
        } as! CreatePostStep2View
    }
    
    @objc func moveToStep2AVC(_ notification: Notification){
//        self.navigationController?.removeViewController(CreatePostStep2BVC.self)
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func moveToRootStep2AVC(_ notification: Notification){
//        self.objCreateView2.removeFromSuperview()
//        NotificationCenter.default.post(name: NSNotification.Name("moveToRoot"),object: nil)
//        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
        UserDefaults.standard.set(false, forKey: "isBackTapped")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        self.navigationController?.pushViewController(vc, animated: false)
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
    }
    
    func moveToCreatePostScreen(activeScreen : CreatePostActiveScreen, flight : String, location : String ,subAttribute : String,timeStatus : String, arrSelectedPreferenceTag : [String],placeAPIData1 : NSDictionary, placeAPIData2 : NSDictionary, isCountryFound : Bool,country : String, state: String){
        slideActiveRow = nil
        let objCreatePostVC = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
        objCreatePostVC.getCreatePostInfo.activeScreen = activeScreen
        objCreatePostVC.getCreatePostInfo.arrPrefrenceTags = arrSelectedPreferenceTag
        objCreatePostVC.getCreatePostInfo.flight = flight
        objCreatePostVC.getCreatePostInfo.location = location
        objCreatePostVC.getCreatePostInfo.subAtrribute = subAttribute
        objCreatePostVC.getCreatePostInfo.time = timeStatus
        objCreatePostVC.getCreatePostInfo.placeAPIData1 = placeAPIData1
        objCreatePostVC.getCreatePostInfo.placeAPIData2 = placeAPIData2
        objCreatePostVC.getCreatePostInfo.isCountryFound = isCountryFound
        objCreatePostVC.getCreatePostInfo.country = country
        objCreatePostVC.getCreatePostInfo.state = state
      //  objCreatePostVC.backCreateDelegate = self
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objCreatePostVC, animated: true)
    }
}
