//
//  CreatePostStep1ViewModel.swift
//  MyFirstApp
//
//  Created by cis on 11/03/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

class CreatePostStep1ViewModel: NSObject {

    func checkCountryCode( country_name : String ,completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        //currentPage = pageNumber
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        //param.updateValue(userId as AnyObject, forKey: KEY.UserDetails.ProfileId)
        let strService = APPURL.Urls.checkCountryExist
    //    let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
      //  param.updateValue(userid as AnyObject, forKey: "user_id")
        param.updateValue(country_name as AnyObject, forKey: "country_name")
        
    
        
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                //  completion("success", data.message, .moveToHomeScreen, "")
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
           
                    
                    completion("success200", message)
                    
                }else if dict.object(forKey: "code") as! Int == 400  {
                    completion("success400", message)
                    
                }else{ // error :
                    if data.message == "" {
                        message = AlertMessages.shareInstance.somethingWentWrong
                    }else{
                        message = data.message
                    }
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = AlertMessages.shareInstance.somethingWentWrong
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    //--------------------------//
    var arrAutoComplete = [DropDownAtributesList]()
    func getAtrributeList( event_type: String,country : String,state_short_name : String,state_long_name : String , search_text : String,  completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        //currentPage = pageNumber
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.getAttributeList
        
        //Request param:
        param.updateValue(event_type as AnyObject, forKey: "event_type")
        param.updateValue(country as AnyObject, forKey: "country")
        param.updateValue(state_short_name as AnyObject, forKey: "state_short_name")
        param.updateValue(state_long_name as AnyObject, forKey: "state_long_name")
        param.updateValue(search_text as AnyObject, forKey: "search_text") // 0 for iOS
        param.updateValue(0 as AnyObject, forKey: "device_type")
        
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
             //   print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { //
                    
                    let arr = dict.object(forKey: "data") as! [Any]
                    self.arrAutoComplete =  self.getAttributeList(arrResponseData: arr)
                    
                    completion("success", message)
                    
                }else{ // error :
                    if data.message == "" {
                        message = AlertMessages.shareInstance.somethingWentWrong
                    }else{
                        message = data.message
                    }
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = AlertMessages.shareInstance.somethingWentWrong
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    
    func getAttributeList(arrResponseData : [Any])->[DropDownAtributesList]{
        var arr = [DropDownAtributesList]()
        
        for item in arrResponseData{
            let dict = item as! NSDictionary
            
            let attribute_text  = "\(dict.object(forKey: "attribute_text") ?? "")"
            let attribute_city  = "\(dict.object(forKey: "attribute_city") ?? "")"
            let attribute_type  = dict.object(forKey: "attribute_type") as! Int
          //  let attribute_country = "\(dict.object(forKey: "attribute_country") ?? "")"
            arr.append(DropDownAtributesList(attribute_city: attribute_city, attribute_text: attribute_text, attribute_type: attribute_type))
        }
        return arr
    }
    
    
    // Get filter array option :-------------------------------------------------------------------
    func getFilterOptionData(completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        let strService = APPURL.Urls.getPlaceAllowed
       
        print("strService \(strService)")
        
        RV_GetPostMethod.getService_API(API: strService) { (result, data)  in
            
            if result == "success"
            {
                let dict = data as NSDictionary
                
                if dict.object(forKey: "code") as! Int == 200 { //
                let arr = dict.object(forKey: "data") as! [Any]
                    if arr.count > 0 {
                        Singleton.shared.selectedFilterPlace.arrFilterOption = arr
                        completion("success", "Record found")
                    }else{
                        Singleton.shared.selectedFilterPlace.arrFilterOption.removeAll()
                        completion("success", "No Record found")
                    }
                    
                }else{ // error :
                    completion("error", "something went wron, Please try again")
                }
        }
    }
}
}
