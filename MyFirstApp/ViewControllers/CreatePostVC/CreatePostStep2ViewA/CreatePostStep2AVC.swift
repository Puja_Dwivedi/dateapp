//
//  CreatePostStep2AVC.swift
//  MyFirstApp
//
//  Created by cis on 03/07/23.
//  Copyright © 2023 cis. All rights reserved.
//

import UIKit

class CreatePostStep2AVC: UIViewController {
    
    @IBOutlet weak var viewGradientToday: UIView!
    @IBOutlet weak var viewGradientYesterday: UIView!
    @IBOutlet var lblYesterdayDate: GradientLabel!
    @IBOutlet var lblTdate: UILabel!
    @IBOutlet var lblYdate: UILabel!
    @IBOutlet var viewOuterDate: UIView!
    @IBOutlet var lblTodayDate: GradientLabel!
    @IBOutlet weak var lblTodayTitle1: GradientLabel!
    @IBOutlet weak var lblYesterdayTitle1: GradientLabel!
    @IBOutlet var viewDate: UIView!
    @IBOutlet weak var lblDateTitle: UILabel!
    @IBOutlet weak var lblPlaceAttribute: UILabel!
    
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var viewTime: UIView!
    @IBOutlet weak var lblTime: UILabel!


    var createPostTimeIntervalID : Int?
    var viewHoursVM = ViewHoursViewModel()
    var objCreateView2 = CreatePostStep2View()
    var activeScreen = CreatePostActiveScreen.airplane
    var slideActiveRow   : Int? = nil
    var countryDialCode = 0 //Constants.userDefault.object(forKey: Variables.date_format) as! Int
    var timeInterVal = ActiveTime.today
    var timeSelected = ""



    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if timeInterVal == ActiveTime.today {
            UserDefaults.standard.set("today", forKey: "TimeInterval")
        } else {
            UserDefaults.standard.set("yesterday", forKey: "TimeInterval")
        }
        UserDefaults.standard.set(lblTime.text, forKey: "TimeSelected")
        UserDefaults.standard.set(createPostTimeIntervalID, forKey: "PostTimeId")
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
        self.navigationController?.pushViewController(vc, animated: false)
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
    }

    @IBAction func btnNextTapped(_ sender: UIButton) {
//        self.objCreateView2 =   self.objCreateView2.Show(activeScreen: activeScreen , strHeaderTitle: "Describe my crush", isbackButtonActive: false, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: false, arrTxtTags: [String()]), viewController: self, attribute_text: "", attribute_city: "", totalCount: 12) { (arrSelectedTags) in
//
//            self.moveToCreatePostScreen(activeScreen: self.activeScreen , flight: "", location: "", subAttribute: "", timeStatus: "", arrSelectedPreferenceTag: [],placeAPIData1 : [:], placeAPIData2 : [:], isCountryFound: false,country: "",state: "")
//        } as! CreatePostStep2View
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostStep2BVC") as! CreatePostStep2BVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "isBackTapped")
        self.navigationController?.popViewController(animated: false)
        NotificationCenter.default.post(name: NSNotification.Name("moveToStep1"),object: nil)
    }
    
    @IBAction func actionBtnToday(_ sender: UIButton) {
        timeInterVal = .today
        lblTodayDate.text = retrieveTodayDate()
        lblTdate.text = retrieveTodayDate()
        setTimeInterValInfo()
    }
    
    @IBAction func actionBtnYesterday(_ sender: UIButton) {
        timeInterVal = .yesterday
        lblYesterdayDate.text = retrieveYesterDayDate()
        lblYdate.text = retrieveYesterDayDate()
        setTimeInterValInfo()
    }
    
    @IBAction func actionOpenTime(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        if countryDialCode == 1{
            CreatePostViewHours.Show(hoursFormate: .twelve, previousSelectedTime: self.lblTime.text!) { [weak self](hours,id) in
                self?.lblTime.text =  hours
                self?.createPostTimeIntervalID = id
            }
        }else{
            CreatePostViewHours.Show(hoursFormate: .twentyFour, previousSelectedTime: self.lblTime.text! ) { [weak self](hours,id) in
                self?.lblTime.text =  hours
                self?.createPostTimeIntervalID = id
            }
        }
    }
    
    private func initView() {
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        lblPlaceAttribute.isHidden = true
        lblTodayDate.text = retrieveTodayDate()
        lblTdate.text = retrieveTodayDate()
        lblYesterdayDate.text = retrieveYesterDayDate()
        lblYdate.text = retrieveYesterDayDate()
        viewDate.addShadowToView()
        viewOuterDate.addShadowToView()
        lblTodayTitle1.gradientColors = [#colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1), #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)]
        lblTodayDate.gradientColors = [#colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1), #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)]
        
        lblYesterdayTitle1.gradientColors =  [#colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1), #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)]
        lblYesterdayDate.gradientColors =  [#colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1), #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)]
        viewTime.addShadowToView()
        setTimeInterValInfo()
        setInitialText()
        let strTimeInterval = CreatePostViewModel().getTimeInterval()
        lblTime.text = strTimeInterval
        
        if countryDialCode == 1 { // for US : 12 hours time foemate
            
            for value in viewHoursVM.arrIDs{
                
                if value.timeOfTwelve == strTimeInterval {
                    createPostTimeIntervalID = value.id
                    break
                }
            }
        }else{ // for non us : 24 hours time interval
            
            for value in viewHoursVM.arrIDs {
                if value.timeOfTwentyFour == strTimeInterval {
                    createPostTimeIntervalID = value.id
                    break
                }
            }
        }
        setTimeIntervalAfterNext()
    }
    
    func setTimeIntervalAfterNext() {
        if (UserDefaults.standard.string(forKey: "TimeInterval") == "yesterday") {
            timeInterVal = ActiveTime.yesterday
        } else {
            timeInterVal = ActiveTime.today
        }
        setTimeInterValInfo()
        
        if let timeInterval = UserDefaults.standard.value(forKey: "TimeSelected") as? String {
            lblTime.text = timeInterval
        }
    }
    
    func setInitialText() {
        if let id = UserDefaults.standard.value(forKey: "eventType") as? Int {
            if id == 1 { //Post
                if activeScreen == .airplane{
                    lblDateTitle.text = "Select your flight’s departure date."
                    lblSubtitle.text = "Select the time of the encounter."
                }else{
                    lblDateTitle.text = "Select the day of the encounter."
                    lblSubtitle.text = "Select the time."
                }
            } else { // Checkin
                if activeScreen == .airplane{
                    lblDateTitle.text = "Select your flight’s departure date."
                    lblSubtitle.text = "Select your flight’s departure time."
                }else{
                    lblDateTitle.text = "Select your check-in date."
                    lblSubtitle.text = "Select your check-in time."
                }
            }
        }
        
    }
    
    func setTimeInterValInfo(){
        
        if timeInterVal == .today{
            viewGradientToday.isHidden = false
            viewGradientYesterday.isHidden = true
            
            lblYesterdayDate.isHidden = true
            lblYdate.isHidden = false
            lblTdate.isHidden = true
            lblTodayDate.isHidden = false
        }else{
            viewGradientToday.isHidden = true
            viewGradientYesterday.isHidden = false
            
            lblYesterdayDate.isHidden = false
            lblYdate.isHidden = true
            lblTdate.isHidden = false
            lblTodayDate.isHidden = true 
        }
    }
    
    func retrieveTodayDate()->String{
        let strTime = Date().string(format: "dd/MM/yyyy") // today
        
        if countryDialCode == 1 { // for US : 12 hours time foemate
            return  convertDateFormaterForUS(strTime)
        }else{
            return convertDateFormaterForNonUS(strTime)
        }
    }
    
    
    func retrieveYesterDayDate()-> String{
        let strTime = (Date() - 1).string(format: "dd/MM/yyyy") // today
        
        if countryDialCode == 1 { // for US : 12 hours time foemate
            return  convertDateFormaterForUS(strTime)
        }else{
            return convertDateFormaterForNonUS(strTime)
        }
    }
    
    
    func convertDateFormaterForUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.string(from: date!)
    }
    
    func convertDateFormaterForNonUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    
    func moveToCreatePostScreen(activeScreen : CreatePostActiveScreen, flight : String, location : String ,subAttribute : String,timeStatus : String, arrSelectedPreferenceTag : [String],placeAPIData1 : NSDictionary, placeAPIData2 : NSDictionary, isCountryFound : Bool,country : String, state: String){
        slideActiveRow = nil
        let objCreatePostVC = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
        objCreatePostVC.getCreatePostInfo.activeScreen = activeScreen
        objCreatePostVC.getCreatePostInfo.arrPrefrenceTags = arrSelectedPreferenceTag
        objCreatePostVC.getCreatePostInfo.flight = flight
        objCreatePostVC.getCreatePostInfo.location = location
        objCreatePostVC.getCreatePostInfo.subAtrribute = subAttribute
        objCreatePostVC.getCreatePostInfo.time = timeStatus
        objCreatePostVC.getCreatePostInfo.placeAPIData1 = placeAPIData1
        objCreatePostVC.getCreatePostInfo.placeAPIData2 = placeAPIData2
        objCreatePostVC.getCreatePostInfo.isCountryFound = isCountryFound
        objCreatePostVC.getCreatePostInfo.country = country
        objCreatePostVC.getCreatePostInfo.state = state
      //  objCreatePostVC.backCreateDelegate = self
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objCreatePostVC, animated: false)
    }
}

//extension CreatePostStep2AVC: BackCreatePostDelegate {
//    func backFromCreatePost() {
//        <#code#>
//    }
//
//    func loadCreatePost1Info(activeScreen: CreatePostActiveScreen, flight: String, location: String, subAtrribute: String, timeStatus: String, arrPrefrenceTags: [String], placeAPIData1: NSDictionary, placeAPIData2: NSDictionary, isCountryFound: Bool, country: String, state: String) {
//        <#code#>
//    }
//
//}
