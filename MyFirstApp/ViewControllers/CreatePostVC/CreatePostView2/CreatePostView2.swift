

//
//  CreatePostView2.swift
//  MyFirstApp
//
//  Created by cis on 24/05/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import SDWebImage

class CreatePostView2: UIView {
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var mainViewContainer: UIView!
    @IBOutlet weak var menuBarView: MenuTabsView!
    @IBOutlet weak var lblTagsRemaining: UILabel!
    @IBOutlet weak var tagCollectionView: UICollectionView!
    @IBOutlet weak var tagCollectionView_h: NSLayoutConstraint!
    
    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
    
    //My Preferences:
    @IBOutlet weak var scrollViewMyPreferences: UIScrollView!
    @IBOutlet weak var scrollViewMyPreferences_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences: PinTextField!
    @IBOutlet weak var txtFldMyPreferences_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences_x: NSLayoutConstraint!
    @IBOutlet weak var txtTo_width: NSLayoutConstraint!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblTagCategoryDes: UILabel!
    
    @IBOutlet weak var txtViewCatDes: UItextView!
    
    
    @IBOutlet weak var bottomSpace_cons: NSLayoutConstraint!
    var arrTxtTagsMyPreferences = [String]()
    
    //MARK:-
    //MARK:- variables:
    
    var onCloser : (([String])-> Void)!
    var isEmptyBackTo = true
    var post2VM = CreatePostView2ViewModel()
    var info = ( attribute_city : "", attribute_text : "")
    var isAvailable = (isTxtPreviousTag : false, arrTxtTags : [""])
    var topArea : CGFloat = 0.0
    var isTabBarAvailable : Bool = false
    var isbackButtonActive : Bool = false
    
    var activeScreen = CreatePostActiveScreen.airplane // default
    var totalCount = 0
    var activeIndex = 0
    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        let window = UIApplication.shared.windows[0]
        topArea = window.safeAreaInsets.top
        
        bottomSpace_cons.constant = safeAreaInsets.bottom
        backgroundColor = UIColor.white
        txtFldMyPreferences.delegate = self
        
        tagCollectionView.dataSource = self
        tagCollectionView.delegate = self
        
        let layout = TagFlowLayout()
        layout.estimatedItemSize = CGSize(width: 140, height: 40)
        
        tagCollectionView.collectionViewLayout = layout
        tagCollectionView.register(UINib(nibName: "tagPostCell", bundle: .main), forCellWithReuseIdentifier: "tagPostCell")
        
        let remaingTag = (totalCount - arrTxtTagsMyPreferences.count)
        if remaingTag == 1 {
            lblTagsRemaining.text = "\(remaingTag) tag left"
        }else{
            lblTagsRemaining.text = "\(remaingTag) tags left"
        }
    }
    
    //MARK:-
    //MARK:- Main Method
    
    func Show(activeScreen : CreatePostActiveScreen,strHeaderTitle : String,isbackButtonActive : Bool,isTabBarAvailable : Bool, isAvailable : (isTxtPreviousTag : Bool, arrTxtTags : [String]), viewController : UIViewController , attribute_text : String, attribute_city : String ,totalCount : Int, onCompletion: @escaping ([String])-> Void)-> UIView{
        
        let customPopUp = Bundle.main.loadNibNamed("CreatePostView2", owner: self, options: nil)?[0] as! CreatePostView2
        viewController.tabBarController?.tabBar.backgroundColor = .white
        
        customPopUp.frame = UIScreen.main.bounds
        customPopUp.onCloser = onCompletion
        customPopUp.txtFldMyPreferences.text = ""
        customPopUp.info.attribute_text = attribute_text
        customPopUp.info.attribute_city = attribute_city
        customPopUp.lblHeaderTitle.text = strHeaderTitle
        customPopUp.activeScreen = activeScreen
        customPopUp.totalCount = totalCount
        
        customPopUp.isAvailable = isAvailable
        customPopUp.isTabBarAvailable = isTabBarAvailable
        
        customPopUp.post2VM.arrCategory.removeAll()
        customPopUp.post2VM.arrTag.removeAll()
        customPopUp.post2VM.arrTextTags.removeAll()
        customPopUp.arrTxtTagsMyPreferences.removeAll()
        customPopUp.menuBarView.currentIndex = 0
        customPopUp.menuBarView.collView.scrollToTop()
        customPopUp.menuBarView.collView.reloadData()
        
        if isbackButtonActive{
            customPopUp.isbackButtonActive = true
            customPopUp.btnNext.isHidden = true
        }else{
            customPopUp.isbackButtonActive = false
            customPopUp.btnNext.isHidden = false
        }
        
        customPopUp.initView()
        viewController.view.addSubview(customPopUp)
        
        customPopUp.showWithAnimation()
        return customPopUp
    }
    
    
    func updateInfo(isTabBarAvailable : Bool , isAvailable: (isTxtPreviousTag: Bool, arrTxtTags: [String]), attribute_text: String, attribute_city: String){
        
        self.info.attribute_text = attribute_text
        self.info.attribute_city = attribute_city
        self.isTabBarAvailable = isTabBarAvailable
        self.isAvailable = isAvailable
        
        self.post2VM.arrCategory.removeAll()
        self.post2VM.arrTag.removeAll()
        self.post2VM.arrTextTags.removeAll()
        self.arrTxtTagsMyPreferences.removeAll()
        self.menuBarView.currentIndex = 0
        self.menuBarView.collView.scrollToTop()
        self.menuBarView.collView.reloadData()
        
        self.initView()
    }
    
    //MARK:-
    //MARK:- Action Methods :
    
    @IBAction func actionBack(_ sender: UIButton) {
        
        if isbackButtonActive{
            validation()
        }else{
            NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
            self.removeFromSuperview()
        }
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        validation()
    }
    
    @IBAction func actionSuggestion(_ sender: UIButton) {
        TagAddView.sharedInstance.Show(title: getSuggestTitle()) { [weak self](strSuggestion) in
             print("Suggestion : \(strSuggestion)")
            guard strSuggestion != "" else {
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.pleaseEnterDes, showMsgAt: .bottom)
                return
            }
            // api calling:
            self?.addTagDes_API(strDes: strSuggestion, tag_category_name: self?.post2VM.arrCategory[self?.activeIndex ?? 0].title ?? "", attribute_id: "\(self?.post2VM.arrCategory[self?.activeIndex ?? 0].id ?? 0)", event_id: "\(self?.post2VM.arrCategory[self?.activeIndex ?? 0].event_id ?? 0)")
//            self?.callReport(to_user: toUserId, report_reason: strReason)
        }
    }
    
    
    @IBAction func actionSendDes(_ sender: UIButton) {
        self.endEditing(true)
        
        guard txtViewCatDes.text != "" else {
            SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.pleaseEnterDes, showMsgAt: .bottom)
            return
        }
        
        //
        // api calling:
        addTagDes_API(strDes: txtViewCatDes.text, tag_category_name: post2VM.arrCategory[activeIndex].title ?? "", attribute_id: "\(post2VM.arrCategory[activeIndex].id ?? 0)", event_id: "\(post2VM.arrCategory[activeIndex].event_id ?? 0)")
    }
    
    
    //MARK:-
    //MARK:- Methods :
    
    func initView(){
        NotificationCenter.default.post(name: NSNotification.Name("hideTabs"),object: nil)
        getAllValueFirstTimeLoad_API(isFirstTimeLoad: true)
    }
    
    func getSuggestTitle() -> String {
        if post2VM.arrCategory.count > 0 {
            if let strDes = post2VM.arrCategory[activeIndex].des {
                return strDes
            }else{
                return "---"
            }
        }else{
            return "Error: Something went wrong, please try again later."
        }
    }
    
    
    func showWithAnimation(){
        //============== Show with animation
        
        mainViewContainer.alpha = 1
        
        UIView.animate(withDuration: 0.7, animations: {
            self.mainViewContainer.alpha = 1
            self.layoutIfNeeded()
        }) { (true) in
            
            UIView.animate(withDuration: 0.5) {
                self.mainViewContainer.alpha = 1
                // self.layoutIfNeeded()
            }
        }
    }
    
    
    func removeWithAnimation(){
        mainViewContainer.alpha = 1
        
        UIView.animate(withDuration: 0.7, animations: {
            self.mainViewContainer.alpha = 0
            
            self.layoutIfNeeded()
        }) { (true) in
            NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
            self.removeFromSuperview()
        }
    }
    
    
    
    func removeWithAnimationWithValue(arrValue : [String]){
        NotificationCenter.default.post(name: NSNotification.Name("showTabs"),object: nil)
        self.onCloser(arrValue)
    }
    
    
    
    func validation(){
        
        if isbackButtonActive == false{
            guard arrTxtTagsMyPreferences.count > 0 else {
                self.endEditing(true)
                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.pleaseSelectAtleastOneTag)", showMsgAt: .bottom); return
            }
        }
        removeWithAnimationWithValue(arrValue: arrTxtTagsMyPreferences)
    }
}

extension CreatePostView2: MenuBarDelegate {
    
    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {
        let catId = post2VM.arrCategory[index].id!
        txtViewCatDes.text = ""
        activeIndex = index
        lblTagCategoryDes.text = post2VM.arrCategory[index].des
        // call api :
        getTagByCategory_Api(tag_category_id: catId, isFirstTimeLoad: false)
    }
}


// -----------------------------------------
// textfield text :


//MARK:-
//MARK:- Tags with Textfilds:

extension CreatePostView2 :  UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let searchText  = textField.text! + string
        
        let maxLength = 21
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if newString.length == maxLength && string == " "{
            print("add space")
        }else{
            guard newString.length < maxLength else{ return false}
        }
        
        if textField == txtFldMyPreferences {
            isEmptyBackTo = false
            //Hide remove space :
            if range.location == 0 && string == " " {
                txtFldMyPreferences.text = ""
                return false;
            }else if (textField.text!.count) > 0 {
                if textField.text!.last == " " && string == " " {
                    return false
                }
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if !checkIsTagRepeated(str: str, strArr: arrTxtTagsMyPreferences) {
                // add new element :
                if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                    txtFldMyPreferences.text = ""
                    isEmptyBackTo = true
                    if setTagFromText(tag: str) {
                        arrTxtTagsMyPreferences.append(searchText.replace(string: " ", replacement: ""))
                        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTxtTagsMyPreferences, isCancelButtonVisible: true)
                    } else {
                        arrTxtTagsMyPreferences.append(searchText.replace(string: " ", replacement: ""))
                        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTxtTagsMyPreferences, isCancelButtonVisible: true)
                    }

                    // createTagCloud(OnView: scrollViewTxtView, withArray: tagsArray)
                    return false
                }
            }
            return true
        }
        return true
    }
}


extension CreatePostView2 : PinTexFieldDelegate{
    func didPressBackspace(textField : PinTextField){
        print("back prashed")
        
        if textField == txtFldMyPreferences{
            
            // for removed :
            if arrTxtTagsMyPreferences.count > 0 && (txtFldMyPreferences.text?.isEmpty ?? false){
                
                if isEmptyBackTo {
                    arrTxtTagsMyPreferences.remove(at: arrTxtTagsMyPreferences.count - 1)
                    createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTxtTagsMyPreferences, isCancelButtonVisible: true)
                }
                isEmptyBackTo = true
            }
        }
    }
}


//My Preerences support methods :
extension CreatePostView2 {
    
    func createTagsForPreferences(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let height = 30
        let spaceHeight : CGFloat = 20
        let screenWidth = UIScreen.main.bounds.size.width - 100.0
        // let containerHeight = (UIScreen.main.bounds.size.height / 2) - 145
        
        for str in data  {
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name:"Poppins-Regular", size: 14.0)!)
            // edit :
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable an0d cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            if checkWholeWidth > screenWidth{
                //we are exceeding size need to change xpos
                xPos = 0.0
                ypos = ypos + spaceHeight + 8.0 + 11
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 35.0 , height: CGFloat(height)))
            
            bgView.backgroundColor = .clear
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 14.0, y: -0.5, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "Poppins-Regular", size: 14.0)
            textlable.text = startstring
            textlable.gradientColors =  [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
            bgView.addSubview(textlable)
            
            //when close buttton enable
            
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 28.0, height: 28.0)
                button.backgroundColor = UIColor.clear
                // button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
                button.setImage(#imageLiteral(resourceName: "cross_icon"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagPreferences(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldMyPreferences.isHidden = false
            
            if arrTxtTagsMyPreferences.count == totalCount {
                txtFldMyPreferences_y.constant = ypos
                txtFldMyPreferences_x.constant = xPos
            }else{
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldMyPreferences_y.constant = ypos
                    txtFldMyPreferences_x.constant = xPos
                    self.layoutIfNeeded()
                    
                }else{
                    
                    txtFldMyPreferences_y.constant = ypos
                    txtFldMyPreferences_x.constant = xPos
                    self.layoutIfNeeded()
                }
            }
            txtTo_width.constant = screenWidth - xPos + 50//
        }else{
            txtFldMyPreferences.isHidden = true
        }
        
        if arrTxtTagsMyPreferences.count <= totalCount {
            self.txtFldMyPreferences.alpha = 1
            UIView.animate(withDuration: 0.5) {
                
                self.scrollViewMyPreferences_h.constant = ypos + 34 + 10 //Here 50 is the height of the txtview, and 10 is the bottom space of the scrollview
                self.layoutIfNeeded()
            }
            
            if arrTxtTagsMyPreferences.count == totalCount {
                self.txtFldMyPreferences.alpha = 0
                self.txtFldMyPreferences.resignFirstResponder()
                self.endEditing(true)
            }
        }else{
            self.txtFldMyPreferences.alpha = 0
            self.txtFldMyPreferences.resignFirstResponder()
            self.endEditing(true)
            self.txtFldMyPreferences.alpha = 0
            UIView.animate(withDuration: 0.5) {
                self.scrollViewMyPreferences_h.constant = ypos
                self.layoutIfNeeded()
            }
        }
        
        let remaingTag = (totalCount - arrTxtTagsMyPreferences.count)
        if remaingTag == 1 {
            lblTagsRemaining.text = "\(remaingTag) tag left"
            
        }else{
            lblTagsRemaining.text = "\(remaingTag) tags left"
        }
    }
    
    @objc func removeTagPreferences(_ sender: AnyObject) {
        
        if let  index = post2VM.arrTag.firstIndex(where: { $0.title?.lowercased() == arrTxtTagsMyPreferences[(sender.tag - 1)].lowercased() }){
            post2VM.arrTag[index].isSelected = false
            tagCollectionView.reloadData()
        }
        
        arrTxtTagsMyPreferences.remove(at: (sender.tag - 1))
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTxtTagsMyPreferences, isCancelButtonVisible: true)
    }
    
    func setInfo(isFirstTimeLoad : Bool){
        
        menuBarView.dataArray = post2VM.arrCategory.map { ($0.title ?? "-") }
        menuBarView.isSizeToFitCellsNeeded = true
        menuBarView.menuDelegate = self
        
        self.txtViewCatDes.text = ""
        activeIndex = 0
        
        if post2VM.arrCategory.count > 0 {
            if let strDes = post2VM.arrCategory[activeIndex].des {
                lblTagCategoryDes.text = strDes
            }else{
                lblTagCategoryDes.text = "---"
            }
        }else{
            lblTagCategoryDes.text = "Error: Something went wrong, please try again later."
        }
        
        
        arrTxtTagsMyPreferences = post2VM.arrTextTags
        
        if isAvailable.isTxtPreviousTag {
            arrTxtTagsMyPreferences = isAvailable.arrTxtTags
            
        }
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTxtTagsMyPreferences, isCancelButtonVisible: true)
        
        setTagInfo(isFirstTimeLoad : isFirstTimeLoad)
    }
    
    func setTagInfo(isFirstTimeLoad : Bool){
        // tags :
        
        tagCollectionView.reloadData()
        tagCollectionView.updateConstraints()
        tagCollectionView.layoutIfNeeded()
        tagCollectionView_h.constant = self.tagCollectionView.contentSize.height
    }
    
    func setTagFromText(tag: String) -> Bool {
        let tagEntered = tag.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()
        for i in 0..<post2VM.arrTag.count {
            if let arrTag = post2VM.arrTag[i].title?.lowercased() {
                if tagEntered == arrTag {
                    post2VM.arrTag[i].isSelected = true
                    self.tagCollectionView.reloadData()
                    return true
                }
            }
        }
        return false
    }
}


extension CreatePostView2 : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return post2VM.arrTag.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tagPostCell", for: indexPath) as! tagPostCell
        
        cell.lblTitle.text = post2VM.arrTag[indexPath.row].title
        cell.lblTitle.preferredMaxLayoutWidth = collectionView.frame.width - 32
        
        if arrTxtTagsMyPreferences.firstIndex(where: { $0.lowercased() == post2VM.arrTag[indexPath.row].title?.lowercased() }) != nil{
            post2VM.arrTag[indexPath.row].isSelected = true
        }
        
        if post2VM.arrTag[indexPath.row].img ?? "" == "" {
            cell.img.isHidden = true
            
        }else{ 
            cell.img.isHidden = false
            let urlImgString = "\(APPURL.baseAPI.imageURL)tag_pictures/\(post2VM.arrTag[indexPath.row].img ?? "")"
            cell.img.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.img.sd_setImage(with: URL(string: urlImgString), placeholderImage: UIImage(named: "imagePlaceholder"))
        }
        
        cell.viewContainer.backgroundColor = UIColor.white
        cell.lblTitle.textColor =  #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
        
        
        if post2VM.arrTag[indexPath.row].isSelected ?? false {
            cell.viewContainer.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            cell.lblTitle.textColor = .white
            cell.viewInnerTag.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
        }else{
            cell.viewContainer.backgroundColor = .white
            cell.lblTitle.textColor =  #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
            cell.viewInnerTag.backgroundColor = .white
        }
        cell.viewInnerTag.addShadowToView()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 200, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if post2VM.arrTag[indexPath.row].isSelected ?? false{
            post2VM.arrTag[indexPath.row].isSelected = false
            if let  index = arrTxtTagsMyPreferences.firstIndex(where: { $0 == post2VM.arrTag[indexPath.row].title }){
                arrTxtTagsMyPreferences.remove(at: index)
            }
        }else{
            guard arrTxtTagsMyPreferences.count < totalCount else {
                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.completedAllTags)", showMsgAt: .bottom); return
            }
            post2VM.arrTag[indexPath.row].isSelected = true
            arrTxtTagsMyPreferences.append(post2VM.arrTag[indexPath.row].title ?? "--")
        }
        
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTxtTagsMyPreferences, isCancelButtonVisible: true)
        collectionView.reloadData()
    }
    
}


/*
 MARK:- API's
 */

extension CreatePostView2 {
    
    func getAllValueFirstTimeLoad_API(isFirstTimeLoad : Bool)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        var strEventID = ""
        if activeScreen == CreatePostActiveScreen.none {
            strEventID = "0"
        }else{
            strEventID = "\(AppDelegate.shared.activeCheckin.rawValue + 1)"
        }
        
        post2VM.getAllData_Api( event_id: strEventID, attribute_text: info.attribute_text, attribute_city: info.attribute_city) { (status, message) in
            
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                UIView.performWithoutAnimation {
                    self.setInfo(isFirstTimeLoad: isFirstTimeLoad)
                }
                
            }else if status == "error"{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    func getTagByCategory_Api(tag_category_id : Int,isFirstTimeLoad : Bool)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        post2VM.getTagByCategory_Api(tag_category_id: tag_category_id) { (status, message) in
            
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.setTagInfo(isFirstTimeLoad: isFirstTimeLoad)
            }else if status == "error"{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    func addTagDes_API(strDes : String, tag_category_name : String,attribute_id : String, event_id : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        post2VM.sendDesCategoryInfo_API(tag_suggestion_text: strDes, tag_category_name: tag_category_name, attribute_id: attribute_id, event_id: event_id) { (status, message) in
            Loader.sharedInstance.stopLoader()
            if status == "success"
            {
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                self.txtViewCatDes.text = ""
            }else{
                
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}
