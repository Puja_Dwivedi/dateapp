//
//  MenuTabsView.swift
//

import UIKit

protocol MenuBarDelegate {
    func menuBarDidSelectItemAt(menu: MenuTabsView,index: Int)
}

class MenuTabsView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
   lazy var collView: UICollectionView = {
    
        let layOut = UICollectionViewFlowLayout()
        let cv = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: layOut)
        cv.showsHorizontalScrollIndicator = false
        layOut.scrollDirection = .horizontal
        cv.backgroundColor = .white
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    var isSizeToFitCellsNeeded: Bool = false {
        didSet{
            self.collView.reloadData()
        }
    }

    var dataArray: [String] = [] {
        didSet{
            self.collView.reloadData()
        }
    }
    
    var menuDidSelected: ((_ collectionView: UICollectionView, _ indexPath: IndexPath)->())?

    var menuDelegate: MenuBarDelegate?
    var currentIndex = 0
  //  var cellId = "BasicCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        customIntializer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
        customIntializer()
    }
    
    
    private func customIntializer () {
        
        collView.register(UINib(nibName: "BasicTagsCell", bundle: nil), forCellWithReuseIdentifier: "BasicTagsCell")
       // collView.register(BasicTagsCell.self, forCellWithReuseIdentifier: "BasicTagsCell")
        addSubview(collView)
        addConstraintsWithFormatString(formate: "V:|[v0]|", views: collView)
        addConstraintsWithFormatString(formate: "H:|[v0]|", views: collView)
        backgroundColor = .clear
    }
    
    
    //MARK: CollectionView Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BasicTagsCell", for: indexPath) as! BasicTagsCell
            let item = dataArray[indexPath.row]
            cell.lblTitle.text = item
        
            if self.currentIndex == indexPath.row {
                
                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
                       cell.viewBackground.backgroundColor = #colorLiteral(red: 0.156845212, green: 0.1568739414, blue: 0.1568388939, alpha: 1)
                      cell.lblTitle.textColor = UIColor.white
                   }, completion:nil)
                
            }else{
                cell.viewBackground.backgroundColor = .clear
                cell.lblTitle.textColor = #colorLiteral(red: 0.156845212, green: 0.1568739414, blue: 0.1568388939, alpha: 1)
            }
        
        cell.viewBackground.cornerRadius = cell.viewBackground.bounds.height/2 - 2.0
    
            return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isSizeToFitCellsNeeded {

            let str = dataArray[indexPath.row]
            
               let label = UILabel(frame: CGRect.zero)
                label.text = str
                label.sizeToFit()
                return CGSize(width: label.frame.width + 20, height: self.frame.height)
        }
        
        return CGSize.init(width: (self.frame.width - 10)/CGFloat(dataArray.count), height: self.frame.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        currentIndex = indexPath.row
        menuDelegate?.menuBarDidSelectItemAt(menu: self, index: currentIndex)
        collectionView.reloadData()
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
