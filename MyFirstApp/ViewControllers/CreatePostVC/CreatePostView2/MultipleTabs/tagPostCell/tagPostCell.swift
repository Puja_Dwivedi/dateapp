//
//  tagPostCell.swift
//  MyFirstApp
//
//  Created by cis on 25/05/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class tagPostCell: UICollectionViewCell {

    @IBOutlet var viewInnerTag: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
