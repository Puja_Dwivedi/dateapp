//
//  ContactUSViewModel.swift
//  MyFirstApp
//
//  Created by cis on 12/07/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ContactUSViewModel: NSObject {
    
    var arrQuestion = [Any]()
    
    override init() {
    }
    
    func getContactList_Api(type_id : String,subtype_id : String, completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        param.updateValue(type_id as AnyObject, forKey:"type_id")
        param.updateValue(subtype_id as AnyObject, forKey:"subtype_id")
        
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.getAboutURL
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                self.arrQuestion = dict.object(forKey: "data") as! [Any]
                
                completion("success", message)
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
}
