//
//  ContactUSVC.swift
//  MyFirstApp
//
//  Created by cis on 12/07/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ContactUSVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet weak var tblView: UITableView!
    
    //MARK:-
    //MARK:- App flow
    
    let contactVM = ContactUSViewModel()
    
    //MARK:-
    //MARK:- App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-
    //MARK:- Methods:
    
    func initView(){
        
        let nib = UINib.init(nibName: "ContactUSCell", bundle: nil)
        tblView.register(nib, forCellReuseIdentifier: "ContactUSCell")
        tblView.estimatedRowHeight = 20
        tblView.rowHeight = UITableView.automaticDimension
        getContactList_API()
    }
    
    func moveToNextScreen(index : Int){
        let itemDict = contactVM.arrQuestion[index] as! NSDictionary
        
        let objAskQuestionVC = self.storyboard?.instantiateViewController(withIdentifier: "AskQuestionVC") as! AskQuestionVC
        AppDelegate.shared.navController = self.navigationController!
        objAskQuestionVC.infoIdict = itemDict
        self.navigationController?.pushViewController(objAskQuestionVC, animated: true)
    }
}


//MARK:-
//MARK:- Tableview deeletes:

extension ContactUSVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactVM.arrQuestion.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUSCell", for: indexPath) as! ContactUSCell
        
        let itemDict = contactVM.arrQuestion[indexPath.row] as! NSDictionary
        let strType_description = "\(itemDict.object(forKey: "type_description") ?? "--")"
        
        cell.lblTitle.text = strType_description
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        moveToNextScreen(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
}

//MARK:-
//MARK:- API:

extension ContactUSVC {
    
    func getContactList_API()
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        contactVM.getContactList_Api(type_id: "2", subtype_id: "0") { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.tblView.reloadData()
            }else{
                
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}

