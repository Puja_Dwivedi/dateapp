//
//  RootVC.swift
//  MyFirstApp
//
//  Created by cis on 19/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class RootVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.removeUserDefaults()
        NotificationCenter.default.addObserver(self,selector:#selector(hideTabs(_:)),name: NSNotification.Name ("hideTabs"),object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(showTabs(_:)),name: NSNotification.Name ("showTabs"),object: nil)
    }
    
    @objc func showTabs(_ notification: Notification){
        self.tabBar.isHidden = false
    }
    
    @objc func hideTabs(_ notification: Notification){
        self.tabBar.isHidden = true
    }
}
