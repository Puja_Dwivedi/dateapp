//
//  LocationVC.swift
//  MyFirstApp
//
//  Created by cis on 12/07/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation

class LocationVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet:
    
    @IBOutlet weak var lblLocation: UILabel!
    
    //MARK:-
    //MARK:- IBOutlet:
    
    let locationVM = LocationViewModel()
    var isChangeAnything = false
    
    //MARK:-
    //MARK:- App flow:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblLocation.text = settingInfo.location
        lblLocation.text = "Location"
        lblLocation.textColor = AppDelegate.shared.textFieldPlaceholderColor
    }
    
    //MARK:-
    //MARK:- Action methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        if isChangeAnything {
            updateLocation_API(location: lblLocation.text!)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @IBAction func actionClear(_ sender: UIButton) {
        isChangeAnything = true
        lblLocation.text = "Location"
        lblLocation.textColor = AppDelegate.shared.textFieldPlaceholderColor
    }
    
    @IBAction func actionChangeLocation(_ sender: UIButton) {
        
        PlaceViewAPI.sharedInstance.Show(activeAction: .city) { [weak self](city, country, shortCountryName,address,placeID,selectedItemDict)  in
            self?.lblLocation.text =  city
            self?.lblLocation.textColor = .black
            self?.isChangeAnything = true
        }
    }
}

//MARK:-
//MARK:- API:
extension LocationVC{
    
    func updateLocation_API(location : String )
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        locationVM.updateLocationApi(location: location) { (status, message, dictResult) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                settingInfo.location = location
                self.navigationController?.popViewController(animated: true)
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}
