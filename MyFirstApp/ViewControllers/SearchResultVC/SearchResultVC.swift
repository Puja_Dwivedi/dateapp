//
//  SearchResultVC.swift
//  MyFirstApp
//
//  Created by cis on 24/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import SDWebImage

class SearchResultVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var viewNoRecordFound: UIView!
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var btnQuestion: UIButton!
    
    @IBOutlet weak var scrollViewAllTags: UIScrollView!
    @IBOutlet weak var scrollViewAllTags_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldAllTags: PinTextField!
    @IBOutlet weak var txtFldMyAllTags_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldAllTags_x: NSLayoutConstraint!
    @IBOutlet weak var txtTo_width: NSLayoutConstraint!
    var arrAllTags = [String]()
    var isEmptyBackFrom = true
    
    
    //MARK:-
    //MARK:- Variables
    
    var searchVM = SearchViewModel() // for api calling.
    var requestParam : [String : AnyObject]?
    var arrSearchResult = [Any]()
    var backDelegate : BackDeleteFromSearchResult?
    var dateFormateStatus : Int? = nil // 1 for us , 0 for non us,
    var countryDialCode = 0
    var info = (activeScreen : CreatePostActiveScreen.airplane,attribute_text : "" , attribute_city : "",placeEnable : false, trainValue : "",timeIntervalActive : false, placeAPIDetailData1 : NSDictionary(),placeAPIDetailData2 : NSDictionary())
    
    // create view popus:
    var objCreateView2 = CreatePostView2()
    var viewHoursVM = ViewHoursViewModel() // for getting array of ids :
    
    
    //MARK:-
    //MARK:- App Flow
    
    override func viewDidLoad() {
        arrAllTags.removeAll()
        
        if arrSearchResult.count > 0 {
            scrollViewAllTags.isHidden = false
            btnQuestion.isHidden = false
        }else{
            scrollViewAllTags.isHidden = true
            btnQuestion.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        // print("requestParam : \(requestParam?.count)")
        initView()
    }
    
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        backDelegate?.refreshScreen(placeAPIDetailData1: info.placeAPIDetailData1, placeAPIDetailData2: info.placeAPIDetailData2)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:-
    //MARK:- Methods:
    
    func initView(){
        
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        if arrSearchResult.count > 0 {
            tblview.isHidden = false
            viewNoRecordFound.isHidden = true
        }else{
            tblview.isHidden = true
            viewNoRecordFound.isHidden = false
        }
        
        txtFldAllTags.delegate = self
        let nib = UINib.init(nibName: "SearchResultCell", bundle: nil)
        tblview.register(nib, forCellReuseIdentifier: "SearchResultCell")
        tblview.estimatedRowHeight = 200
        tblview.rowHeight = UITableView.automaticDimension
        tblview.reloadData()
    }
    
    
    @IBAction func actionHintTag(_ sender: UIButton) {
        self.objCreateView2 =  self.objCreateView2.Show(activeScreen: info.activeScreen, strHeaderTitle: "Select tags", isbackButtonActive: true, isTabBarAvailable: false, isAvailable: (isTxtPreviousTag: true, arrTxtTags: self.arrAllTags), viewController: self, attribute_text: info.attribute_text, attribute_city: info.attribute_city, totalCount: 12) { [weak self] (arrSelectedTags) in
            
            self?.objCreateView2.removeWithAnimation()
            self?.arrAllTags = arrSelectedTags
            self?.createTagsForPreferences(OnView: (self?.scrollViewAllTags)!, withArray: self!.arrAllTags, isCancelButtonVisible: true)
        } as! CreatePostView2
    }
    
    
    
    func getTimeIntervalValue(id : Int)-> (strTime : String, id : Int){
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        guard id != 0 else { return (strTime : "----", id : 0)}
        
        let index : Int =  viewHoursVM.arrIDs.firstIndex(where: {$0.id == id})!
        
        if countryDialCode == 1{ // for us date formate : 12 hours
            return (strTime : viewHoursVM.arrIDs[index].timeOfTwelve, id : viewHoursVM.arrIDs[index].id)
        }else{
            return (strTime : viewHoursVM.arrIDs[index].timeOfTwentyFour, id : viewHoursVM.arrIDs[index].id)
        }
    }
    
    func makeRequestForPlacestreet(request Param : [String : AnyObject])-> [String : AnyObject]{
        
        var param =   Param
        if let dictFormate = info.placeAPIDetailData1.object(forKey: "structured_formatting") as? NSDictionary{
            //3. main_text -----------
            if let main_text = dictFormate.object(forKey: "main_text") as? String {
                param.updateValue(main_text as AnyObject ,forKey: "main_text")
            }else{
                param.updateValue("" as AnyObject ,forKey: "main_text")
            }
            
            //4.secondary_text
            if let secondary_text = dictFormate.object(forKey: "secondary_text") as? String {
                param.updateValue(secondary_text as AnyObject ,forKey: "secondary_text")
            }else{
                param.updateValue("" as AnyObject ,forKey: "secondary_text")
            }
        }else{
            param.updateValue("" as AnyObject ,forKey: "main_text")
            param.updateValue("" as AnyObject ,forKey: "secondary_text")
        }
        
        
        //5. types
        let arrType = info.placeAPIDetailData1.object(forKey: "types") as? [String]
        if arrType?.count ?? 0 > 0 {
            param.updateValue((arrType! as NSArray).componentsJoined(by: ",") as AnyObject ,forKey: "types")
            
        }else{
            param.updateValue("" as AnyObject ,forKey: "types")
        }
        
        
        // 2nd API: --------------------------------------------------->
        
        param.updateValue("" as AnyObject ,forKey: "administrative_area_level_2")
        
        if let arrComponents = info.placeAPIDetailData2.object(forKey: "address_components") as? [Any]{
            
            for dictItem in arrComponents{
                
                if let dictComponent = dictItem as? NSDictionary{
                    
                    if let arrType = dictComponent.object(forKey: "types") as? [Any] {
                        
                        for type in arrType {
                            
                            if "\(type)" == "administrative_area_level_2" {
                                param.updateValue("\(dictComponent.object(forKey: "long_name") ?? "")" as AnyObject ,forKey: "administrative_area_level_2")
                            }
                        }
                    }
                }
            }
        }else{
            param.updateValue(""  as AnyObject,forKey: "administrative_area_level_2")
        }
        
        if let formattedAddress = info.placeAPIDetailData2.object(forKey: "formatted_address") as? String  {
            param.updateValue(formattedAddress as AnyObject ,forKey: "formatted_address")
        }else{
            param.updateValue("" as AnyObject ,forKey: "formatted_address")
        }
        
        return param
    }
}


//MARK:-
//MARK:- Tableview deeletes:

extension SearchResultVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath) as! SearchResultCell
        var profilePicture = ""
        let dict = arrSearchResult[indexPath.row] as! NSDictionary
        
        if info.timeIntervalActive{
            cell.lblTime.isHidden = true
            
            
        }else{
            cell.lblTime.isHidden = false
            if let time_interval = dict.object(forKey: "time_interval") as? Int {
                let time  =  getTimeIntervalValue(id: time_interval)
                cell.lblTime.text = "\(time.strTime)"
            }else{
                cell.lblTime.isHidden = true
            }
        }
        
        cell.lblTitle.text = "\(dict.object(forKey: "description") ?? "")"
        
        //1. This should only be displayed if field is blank in selection screen
        if info.attribute_text == "" {
            cell.lblSubTitle.isHidden = false
            cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? "")"
        }else{
            cell.lblSubTitle.isHidden = true
        }
        
        
        let pictureDisplay = dict.object(forKey: "picture_display") as! Int
        
        if pictureDisplay == 1{ //load picture
            
            
            if let image = dict.object(forKey: "post_image") as? String {
                if image == "" {
                    let picture_img = dict.object(forKey: "picture") as? String ?? "--"
                    profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(picture_img)"
                } else {
                    profilePicture = "\(APPURL.baseAPI.imageURL)post_pictures/\(image)"
                }
            } else {
                let picture_img = dict.object(forKey: "picture") as? String ?? "--"
                profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(picture_img)"
            }
            
//            if let image = dict.object(forKey: "picture") as? String {
//                if image == "" {
//                    let picture_img = dict.object(forKey: "post_image") as? String ?? "--"
//                    profilePicture = "\(APPURL.baseAPI.imageURL)post_pictures/\(picture_img)"
//                } else {
//                    profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(image)"
//                }
//            }
//
            
         //   let profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(dict.object(forKey: "picture") as! String)"
            cell.imgPost.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgPost.sd_setImage(with: URL(string: profilePicture), placeholderImage: UIImage(named: "img_profile_placeholder"))
            
        }else if pictureDisplay == 0{
            cell.imgPost.image = UIImage(named: "11placeHolder")
        }
        
        switch  "\(dict.object(forKey: "event_subtype") ?? "")"{
        case "1": //airplane
            break
            
        case "2": //subway
            break
            
            
        case "3": //place
            
            if "\(dict.object(forKey: "attribute") ?? "")" == ""{
                
                cell.lblSubTitle.text = "\(dict.object(forKey: "main_text") ?? ""), \(dict.object(forKey: "formatted_address") ?? "")"
            }else {
                
                if info.placeEnable{
                    cell.lblSubTitle.isHidden = true
                }else{
                    cell.lblSubTitle.isHidden = false
                    cell.lblSubTitle.text = "\(dict.object(forKey: "main_text") ?? ""), \(dict.object(forKey: "formatted_address") ?? "")"
                }
            }
            
            break
            
        case "4": //street
            
            if "\(dict.object(forKey: "sub_attribute") ?? "")" != ""{
                
                cell.lblSubTitle.text = "\(dict.object(forKey: "main_text") ?? ""), \(dict.object(forKey: "secondary_text") ?? "")"
            }else{
                cell.lblSubTitle.text = "\(dict.object(forKey: "main_text") ?? ""), \(dict.object(forKey: "secondary_text") ?? "")"
            }
            break
            
        case "5": //bus
            break
            
        case "6": //train
            
            if info.trainValue == "" || info.attribute_text == ""{
                cell.lblSubTitle.isHidden = false
                
                if "\(dict.object(forKey: "sub_attribute") ?? "")" != ""{
                    cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? ""). \(dict.object(forKey: "sub_attribute") ?? "")"
                }else{
                    cell.lblSubTitle.text = "\(dict.object(forKey: "attribute") ?? "")"
                }
            }else{
                cell.lblSubTitle.isHidden = true
            }
            break
            
        default:
            break
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var picture = ""
        let dict = arrSearchResult[indexPath.row] as! NSDictionary
        let userID = Constants.userDefault.object(forKey: Variables.user_id)
        
        guard "\(userID ?? "")" != "\(dict.object(forKey: "user_id") ?? "")" else {
            SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.Thatsyourpost, showMsgAt: .bottom)
            
            return
        }
        
        let objCreatePostVC = self.storyboard?.instantiateViewController(withIdentifier: "RespondSearchResultVC") as! RespondSearchResultVC
        
        objCreatePostVC.to_post_id = "\(dict.object(forKey: "post_id") ?? "")"
        objCreatePostVC.to_user_id = "\(dict.object(forKey: "user_id") ?? "")"
        objCreatePostVC.event_id  = "\(dict.object(forKey: "event_id") ?? "")"
        
        objCreatePostVC.chatInfo.otherUserId = dict.object(forKey: "user_id") as! Int
        objCreatePostVC.chatInfo.name = "\(dict.object(forKey: "name") ?? "")"
     //   objCreatePostVC.chatInfo.profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(dict.object(forKey: "picture") ?? "--")"
        
        if let image = dict.object(forKey: "picture") as? String {
            if image == "" {
                let picture_img = dict.object(forKey: "post_image") as? String ?? "--"
                picture = "\(APPURL.baseAPI.imageURL)post_pictures/\(picture_img)"
            } else {
                picture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(image)"
            }
        }
        
        objCreatePostVC.chatInfo.profilePicture = picture
        
        objCreatePostVC.dictResult = arrSearchResult[indexPath.row] as! NSDictionary
        self.navigationController?.pushViewController(objCreatePostVC, animated: true)
    }
}


///MARK:-
//MARK:- Tags with Textfilds:

extension SearchResultVC :  UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //print("searchText : \(searchText), \(textField.text), \(string)")
        let searchText  = textField.text! + string
        let maxLength = 21
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if newString.length == maxLength && string == " "{
            print("add space")
        }else{
            guard newString.length < maxLength else{ return false}
        }
        
        // My attributes :
        isEmptyBackFrom = false
        //Hide remove space :
        if range.location == 0 && string == " " {
            txtFldAllTags.text = ""
            return false;
        }else if (textField.text!.count) > 0 {
            if textField.text!.last == " " && string == " " {
                return false
            }
        }
        
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if !checkIsTagRepeated(str: str, strArr: arrAllTags) {
            // add new element :
            if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                txtFldAllTags.text = ""
                isEmptyBackFrom = true
                arrAllTags.append(searchText.replace(string: " ", replacement: ""))
                createTagsForPreferences(OnView: scrollViewAllTags, withArray: arrAllTags, isCancelButtonVisible: true)
                return false
            }
        }
        return true
    }
}

extension SearchResultVC : PinTexFieldDelegate{
    
    func didPressBackspace(textField : PinTextField){
        
        // My attibutes :
        if textField == txtFldAllTags {
            
            // for removed :
            if arrAllTags.count > 0 && (txtFldAllTags.text?.isEmpty == true) {
                
                if isEmptyBackFrom {
                    arrAllTags.remove(at: arrAllTags.count - 1)
                    createTagsForPreferences(OnView: scrollViewAllTags, withArray: arrAllTags, isCancelButtonVisible: true)
                }
                isEmptyBackFrom = true
            }
        }
    }
}

//My Preerences support methods :
extension SearchResultVC {
    
    func createTagsForPreferences(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let height = 30
        let spaceHeight : CGFloat = 20
        let screenWidth = UIScreen.main.bounds.size.width - 100.0
        
        for str in data  {
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name: "Poppins-Regular", size: 14.0)!)
            // edit :
            
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            if checkWholeWidth > screenWidth{
                //we are exceeding size need to change xpos
                xPos = 0.0
                ypos = ypos + spaceHeight + 8.0 + 11
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 35.0 , height: CGFloat(height)))
            //  bgView.layer.cornerRadius = 14.5
            bgView.backgroundColor = .clear//UIColor(red: 33.0/255.0, green: 135.0/255.0, blue:199.0/255.0, alpha: 1.0)
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 14.0, y: -0.5, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "Poppins-Regular", size: 14.0)
            textlable.text = startstring
            
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
            bgView.addSubview(textlable)
            
            //when close buttton enable
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                //
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 28.0, height: 28.0)
                button.backgroundColor = UIColor.clear
                button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
                
                button.setImage(#imageLiteral(resourceName: "cross_icon"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagPreferences(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldAllTags.isHidden = false
            
            if arrAllTags.count == 10 {
                txtFldMyAllTags_y.constant = ypos
                txtFldAllTags_x.constant = xPos
            }else{
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    //we are exceeding size need to change xpos
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldMyAllTags_y.constant = ypos
                    txtFldAllTags_x.constant = xPos
                    
                }else{
                    txtFldMyAllTags_y.constant = ypos
                    txtFldAllTags_x.constant = xPos
                }
            }
            txtTo_width.constant = screenWidth - xPos + 50// here 30 is padding....
        }else{
            txtFldAllTags.isHidden = true
        }
        
        if arrAllTags.count <= 10 {
            self.txtFldAllTags.alpha = 1
            scrollViewAllTags_h.constant = ypos + 34 + 10 //Here 50 is the height of the txtview, and 10 is the bottom space of the scrollview
            
            if arrAllTags.count == 10 {
                self.txtFldAllTags.alpha = 0
                self.txtFldAllTags.resignFirstResponder()
                self.view.endEditing(true)
            }
        }else{
            self.txtFldAllTags.alpha = 0
            self.txtFldAllTags.resignFirstResponder()
            self.view.endEditing(true)
            self.txtFldAllTags.alpha = 0
            scrollViewAllTags_h.constant = ypos //Here 50 is the height of the txtview, and 10 is the bottom space of the scrollview
        }
        
       // if arrAllTags.count > 0 {
            // api call search update:
            let strSearchTag : String = arrAllTags.joined(separator: ",")
            
            requestParam?["search_tag"] = strSearchTag as AnyObject
            let updateRequestParam = makeRequestForPlacestreet(request: requestParam!)
            requestParam = updateRequestParam
            
            call_searchPost(param: requestParam!)
     //   }
    }
    
    @objc func removeTagPreferences(_ sender: AnyObject) {
        arrAllTags.remove(at: (sender.tag - 1))
        createTagsForPreferences(OnView: scrollViewAllTags, withArray: arrAllTags, isCancelButtonVisible: true)
    }
}


/*
 MARK:- API's
 */

extension SearchResultVC {
    
    func call_searchPost(param : [String : AnyObject])
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        searchVM.callSearchPost_Api(requestParam : param) { [weak self](status, message) in
            
            Loader.sharedInstance.stopLoader()
            
            self?.arrSearchResult = self!.searchVM.arrPostSearchData
            
            if self!.searchVM.arrPostSearchData.count > 0 { }else{
//                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.noDataFound)", showMsgAt: .mid)
            }
            self?.initView()
        }
    }
}

