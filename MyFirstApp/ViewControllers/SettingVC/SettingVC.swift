//
//  SettingVC.swift
//  MyFirstApp
//
//  Created by cis on 29/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet var viewToggle: UIView!
    @IBOutlet weak var notificationLeftCons: NSLayoutConstraint!
    @IBOutlet weak var viewOffTime: UIView!
    @IBOutlet weak var lblBuildVersion: UILabel!
    
    //MARK:-
    //MARK:- Variables:
    
    var settingProfileInfo = (name : "", image : UIImage())
    var settingVM = SettingViewModel()
    
    //MARK:-
    //MARK:- App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        initView()
    }
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionEmail(_ sender: UIButton) {
        let objEmailVC = self.storyboard?.instantiateViewController(withIdentifier: "EmailVC") as! EmailVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objEmailVC, animated: true)
    }
    
    
    @IBAction func actionLocation(_ sender: UIButton) {
        let objLocationVC = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objLocationVC, animated: true)
    }
    
    
    @IBAction func actionNotification(_ sender: UIButton) {
        
        //notification: 1 or 0   -  {on/off}
        if  notificationActiveStatus == 1 { // set to off
            self.viewToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
            setNotification(notificationStatus: 0)
            
        }else{ // set to on :
            self.viewToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
            setNotification(notificationStatus: 1)
        }
    }
    
    
    @IBAction func actionLanguage(_ sender: UIButton) {
        let objLanguageVC = self.storyboard?.instantiateViewController(withIdentifier: "LanguageVC") as! LanguageVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objLanguageVC, animated: true)
    }
    
    @IBAction func actionCountry(_ sender: UIButton) {
        let objCountryVC = self.storyboard?.instantiateViewController(withIdentifier: "CountryVC") as! CountryVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objCountryVC, animated: true)
    }
    
    
    @IBAction func actionContactInfo(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            // contact us screen
            let objContactUSVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactUSVC") as! ContactUSVC
            AppDelegate.shared.navController = self.navigationController!
            self.navigationController?.pushViewController(objContactUSVC, animated: true)
            break;
            
        case 2:
            let objFAQVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
            AppDelegate.shared.navController = self.navigationController!
            objFAQVC.activeScreen = .FAQ
            self.navigationController?.pushViewController(objFAQVC, animated: true)
            break;
            
        case 3:
            let objTermsVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
            AppDelegate.shared.navController = self.navigationController!
            objTermsVC.activeScreen = .TermsAndConditions
            self.navigationController?.pushViewController(objTermsVC, animated: true)
            break;
            
        case 4:
            let objPrivacyVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
            AppDelegate.shared.navController = self.navigationController!
            objPrivacyVC.activeScreen = .PrivacyPolicy
            self.navigationController?.pushViewController(objPrivacyVC, animated: true)
            break;
            
        case 5:
            let objCookieVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
            AppDelegate.shared.navController = self.navigationController!
            objCookieVC.activeScreen = .CookiePolicy
            self.navigationController?.pushViewController(objCookieVC, animated: true)
            break;
            
        default:
            print(";)")
        }
    }
    
    
    @IBAction func actionLogOut(_ sender: UIButton) {
        AlertTheme.sharedInstance.Show(popupCategory: .normal,message: "\(AlertMessages.shareInstance.areYouSureWantToLogout)", attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["\(AlertMessages.shareInstance.yesLogOut)","\(AlertMessages.shareInstance.cancel)"], isBottomTxt: "") { [weak self](clicked) in
            if clicked == "\(AlertMessages.shareInstance.yesLogOut)" {
                // action logout perform
               
                self?.api_logout()
              
            }
        }
    }
    
    
    @IBAction func actionDeleteAccount(_ sender: UIButton) {
        
          AlertTheme.sharedInstance.Show(popupCategory : .normal,message: "Are you sure you want to delete your account?", attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["\(AlertMessages.shareInstance.yesDelete)","\(AlertMessages.shareInstance.cancel)"], isBottomTxt: "") { [weak self] (clicked) in
              if clicked == "\(AlertMessages.shareInstance.yesDelete)" {
                  print("delete account")
                  // action logout perform
                  self?.deleteAccount()
              }
          }
//
//        AlertTheme.sharedInstance.Show(popupCategory : .normal,message: "\(AlertMessages.shareInstance.areYouSureWantToDelete) \(recreate_days) \(AlertMessages.shareInstance.deactivatingIt)", attributeRangeString: "", isCancelButtonVisible: true, arrBtn: ["\(AlertMessages.shareInstance.yesDelete)","\(AlertMessages.shareInstance.cancel)"], isBottomTxt: "") { [weak self] (clicked) in
//            if clicked == "\(AlertMessages.shareInstance.yesDelete)" {
//                print("delete account")
//                // action logout perform
//                self?.deleteAccount()
//            }
//        }
    }
    
    
    //MARK:-
    //MARK:- Methods:
    
    func initView(){
        lblBuildVersion.text = UIApplication.appVersion
        setNotificationInfo()
    }

    func setNotificationInfo(){
        // set notification :
        if  notificationActiveStatus == 0 {
            UIApplication.shared.unregisterForRemoteNotifications() // disabled notification.
            
            UIView.animate(withDuration: 0.3) {
                self.viewToggle.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
                self.notificationLeftCons.constant = 1
                self.viewOffTime.alpha = 1
                self.view.layoutIfNeeded()
            }
        }else{
            
            UIApplication.shared.registerForRemoteNotifications() // enabled notification.
            UIView.animate(withDuration: 0.3) {
                self.viewToggle.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                self.notificationLeftCons.constant = 28
                self.viewOffTime.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    func moveToTheWelcomeScreen(){
        for controller in AppDelegate.shared.navController.viewControllers as Array {
            
            if controller.isKind(of: WelcomePageVC.self) {
                Constants.userDefault.removeObject(forKey:Variables.email)
                Constants.userDefault.removeObject(forKey:Variables.name)
                Constants.userDefault.removeObject(forKey:Variables.phone_number)
                Constants.userDefault.removeObject(forKey:Variables.picture)
                Constants.userDefault.removeObject(forKey: Variables.country_code)
                Constants.userDefault.removeObject(forKey:Variables.user_id)
               
                
                AppDelegate.shared.navController.popToViewController(controller, animated: false)
                break
            }
        }
    }
}


extension SettingVC {
    
    // get profile info:
    func setNotification(notificationStatus : Int)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        settingVM.setNotificationUpdateApi(notificationStatus: notificationStatus) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                notificationActiveStatus = notificationStatus
                self.setNotificationInfo()
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    
    // get profile info:
    func api_logout()
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        settingVM.logoutApi { (status, message, dictData) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.moveToTheWelcomeScreen()
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    // delete account :
    func deleteAccount()
    {
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        Loader.sharedInstance.showLoader(msg: "Loading...")
        settingVM.deleteAccountApi(userID: userID) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.moveToTheWelcomeScreen()
                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.accountDeactivatedSuccuessfully)", showMsgAt: .bottom)
            }else{
                SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.somethingWentWrong)", showMsgAt: .bottom)
            }
        }
    }
}
 
