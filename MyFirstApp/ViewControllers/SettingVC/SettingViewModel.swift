//
//  SettingViewModel.swift
//  MyFirstApp
//
//  Created by cis on 30/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class SettingViewModel: NSObject {
    
    // Updating notification values :
    
    //notification: 1 or 0   -  {on/off}
    func setNotificationUpdateApi(notificationStatus : Int, completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        param.updateValue(notificationStatus as AnyObject, forKey:"notification")
        
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.setNotificationUpdate + "/\(userID)"
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("success", message, dict)
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("error", message, NSDictionary())
            }
        }
    }
    
    
    func logoutApi(completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
       
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.logout
        param.updateValue(userID as AnyObject, forKey:"user_id")
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("success", message, dict)
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                completion("error", message, NSDictionary())
            }
        }
    }
    
    
    // delete account :
    func deleteAccountApi(userID : String, completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        let param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
       
        let strService = APPURL.Urls.deleteUserAccount + "/\(userID)"
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                let dict = data.response
                
                completion("success", data.message, dict)
            }
            else
            {
                completion("error", data.message, NSDictionary())
            }
        }
    }
}
