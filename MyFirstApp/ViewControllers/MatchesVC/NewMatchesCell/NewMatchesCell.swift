//
//  NewMatchesCell.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class NewMatchesCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var viewStory: UIView!
    @IBOutlet weak var btnProfileOption: UIButton!
    @IBOutlet weak var imgProfileOption: UIImageView!
    
    @IBOutlet weak var lblNextTitle: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var imgChatOption: UIImageView!
    @IBOutlet weak var btnChatOption: UIButton!
    
    @IBOutlet weak var btnMore: UIButton!
    
    @IBOutlet weak var viewMainCantainer: UIViewClass!
    
    @IBOutlet weak var viewInfoContainer: UIView!
    @IBOutlet weak var btnInfo1: UIButton!
    @IBOutlet var viewStatus: UIView!
    @IBOutlet weak var btnInfo2: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        viewStory.addShadowToView()
        viewStatus.addShadowToView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
