
//
//  MatchesViewModel.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class MatchesViewModel: NSObject {
    
    var arrMatches = NSMutableArray()
    
    func getMatchData( post_id : Int, sort_type : Int,completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.getMatchListByPostId
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userid as AnyObject, forKey: "user_id")
        param.updateValue(post_id as AnyObject, forKey: "post_id")
        param.updateValue(sort_type as AnyObject, forKey: "sort_type")
        
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    let arrData = RV_CheckDataType.getArray(anyArray: data.response.value(forKey: KEY.ServiceKeys.dataResponse) as AnyObject)
                    if arrData.count > 0
                    {
                        self.arrMatches = arrData.mutableCopy() as! NSMutableArray
                    }
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("success", message)
                    
                }else { // error :
                    self.arrMatches.removeAllObjects()
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                }
            }
            else
            {
                self.arrMatches.removeAllObjects()
                let dict = data.response
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    
    func removeMatchAPI( match_id : Int,index : Int, like_id : String, completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        //currentPage = pageNumber
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.removeMatch
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userid as AnyObject, forKey: "user_id")
        param.updateValue(match_id as AnyObject, forKey: "match_id")
        param.updateValue(like_id as AnyObject, forKey: "like_id")
        
        
        print("strSe2r2v2ice \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    
                    if self.arrMatches.count > 0 {
                        self.arrMatches.removeObject(at: index)
                    }
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("success", message)
                    
                }else { // error :
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                }
            }
            else
            {   let dict = data.response
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    
    func reportPost_Api(from_user : String, to_user : String, report_reason : String , completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.reportUser
        
        param.updateValue(from_user as AnyObject, forKey: "from_user")
        param.updateValue(to_user as AnyObject, forKey: "to_user")
        param.updateValue(report_reason as AnyObject, forKey: "report_reason")
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else { // error :
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                }
            }
            else
            {   let dict = data.response
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    
    func profileIndicatorUpdateAPI( match_id : Int,profile_indicator : Int, completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.updateProfileIndicatorAPI
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userid as AnyObject, forKey: "user_id")
        param.updateValue(profile_indicator as AnyObject, forKey: "profile_indicator")
        param.updateValue(match_id as AnyObject, forKey: "match_id")
        
        print("strService \(strService) \n param \(param)")
        
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("success", message)
                    
                }else { // error :
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                }
            }
            else
            {   let dict = data.response
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
}
