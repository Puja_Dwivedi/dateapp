//
//  MatchesVC.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

protocol BackToPostVCDelegate {
    func backToPostVc()
}

extension MatchesVC : BackToPostVCDelegate {
    func backToPostVc(){
        self.navigationController?.popViewController(animated: true)
    }
}


class MatchesVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet:
    @IBOutlet weak var tblViewMatches: UITableView!
    @IBOutlet weak var viewSort: UIView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    //----------------------------------------------
  
    //MARK:-
    //MARK:- IBOutlet:
    let matchesVM = MatchesViewModel()
    var profileVM = ProfileViewModel()
    var selectedValue = "Newest to Oldest"
    var post_id = 0
    var refreshControl: UIRefreshControl!
    
    var info1 = (title: "" , url : "")
    var info2 = (title: "" , url : "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblViewMatches.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        initView()
    }
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOpenSortBy(_ sender: UIButton) {
        let frame =  viewSort.frame.origin
        let window = UIApplication.shared.windows[0]
        let topPadding = window.safeAreaInsets.top
        ssSortView.sharedInstance.Show(x: frame.x, y: frame.y - topPadding, selectedValue: selectedValue) { [weak self](value) in
            self?.selectedValue = value
            self?.initView()
        }
    }
    
    //MARK:-
    //MARK:- Methods:
    
    
    func callURL(selectInfoPage : String){
        if let url = URL(string: selectInfoPage), UIApplication.shared.canOpenURL(url) {
           if #available(iOS 10.0, *) {
              UIApplication.shared.open(url, options: [:], completionHandler: nil)
           } else {
              UIApplication.shared.openURL(url)
           }
        }
    }
    
    func initView(){
        getInfoList()
    
        matchesVM.arrMatches.removeAllObjects()
        tblViewMatches.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right:0)
        
        let nib = UINib.init(nibName: "MatchesCell", bundle: nil)
        tblViewMatches.register(nib, forCellReuseIdentifier: "MatchesCell")
        
        tblViewMatches.estimatedRowHeight = 100
        tblViewMatches.rowHeight = UITableView.automaticDimension
        
        lblNoDataFound.isHidden = true
        tblViewMatches.isHidden = true
        // after response of API
        
        getInitialMatchData()
    }
    
    func setUnderlineAttributeString(title : String) -> NSAttributedString? {
       let attributes : [NSAttributedString.Key : Any] = [
           NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0),
           NSAttributedString.Key.foregroundColor : UIColor.gray,
           NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
       ]
       let attributedString = NSAttributedString(string: title, attributes: attributes)
       return attributedString
     }
    
    
    func getInfoList(){
    
        for item in Singleton.shared.InfoPage{
            if let dict = item as? NSDictionary {
             
                if (dict.object(forKey: "type_id") as? Int) != nil  {
                    if let subtype_id = dict.object(forKey: "subtype_id") as? Int{
                        switch subtype_id {
                        case 8:
                            if let output = dict.object(forKey: "output") as? String {
                                if output.count > 0 {
                            
                                    info1.title = "\(dict.object(forKey: "subtype_description") ?? "--")".replacingOccurrences(of: "\n", with: "")
                                    info1.url = output
                                
                                  
                                  //  print("lblInfoPage1.text : \(lblInfoPage1.text)")
                                }
                            }
                         
                            break
                            
                        case 9:
                            
                            if let output = dict.object(forKey: "output") as? String {
                                if output.count > 0 {
                                   
                                    info2.title = "\(dict.object(forKey: "subtype_description") ?? "--")".replacingOccurrences(of: "\n", with: "")
                                    info2.url = output
                                    
                                 //   print("lblInfoPage2.text : \(lblInfoPage2.text)")
                                }
                            }
                            break
                        
                            
                        default:
                            break
                        }
                    }
                }
        }
    }
        tblViewMatches.reloadData()
    }
    
    func getInitialMatchData(){
        var sortTypeValue = 0
        if selectedValue == "Newest to Oldest" {
            sortTypeValue = 1
        }else if selectedValue == "Oldest to Newest" {
            sortTypeValue = 2
        }else if selectedValue == "Matches" {
            sortTypeValue = 3
        }
        
        getMatchData(sort_type: sortTypeValue)
    }
    
    @objc func refresh(_ sender: Any) {
        getInitialMatchData()
    }
}

//MARK:-
//MARK:- Tableview deeletes:

extension MatchesVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if matchesVM.arrMatches.count > 0 {
            return matchesVM.arrMatches.count + 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesCell", for: indexPath) as! MatchesCell
        
        if matchesVM.arrMatches.count > indexPath.row{
            cell.viewMainCantainer.isHidden = false
            cell.viewInfoContainer.isHidden = true
       
        
        let dict = matchesVM.arrMatches[indexPath.row] as! NSDictionary
        
        cell.lblTitle.text = "\(dict.object(forKey: "description") ?? "")"
        
        cell.lblNextTitle.text = "The story"
        cell.btnNext.tag = indexPath.row
        cell.btnNext.addTarget(self, action: #selector(self.actionNext(sender:)), for: .touchUpInside)
        
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(self.actionMoreOption(sender: )), for: .touchUpInside)
        
        cell.btnProfileOption.tag = indexPath.row
        cell.btnProfileOption.addTarget(self, action: #selector(self.actionProfile(sender:)), for: .touchUpInside)
        
        cell.btnChatOption.tag = indexPath.row
        cell.btnChatOption.addTarget(self, action: #selector(self.actionChatOption), for: .touchUpInside)
        
        
        // profile:
        if let item  = dict.object(forKey: "profile_visibility") as? Int{// hide
            
            if item == 0 {//  1 visible , 0 desble
                
                cell.btnProfileOption.isUserInteractionEnabled = false
                cell.imgProfileOption.image = #imageLiteral(resourceName: "desableProfile Icon")
            }else if item == 1 {// show
                cell.btnProfileOption.isUserInteractionEnabled = true
                cell.imgProfileOption.image = #imageLiteral(resourceName: "profile_Placeholder@1x.png")
            }
        }else{
            cell.btnProfileOption.isUserInteractionEnabled = false
            cell.imgProfileOption.image = #imageLiteral(resourceName: "desableProfile Icon")
        }
        
        
        //Chat :
        if let item = dict.object(forKey: "match_id") as? Int {// hide
            if item == 0 {
                cell.btnChatOption.isUserInteractionEnabled = false
                cell.imgChatOption.image = #imageLiteral(resourceName: "notActiveChatIcon")
            }else{// show
                cell.btnChatOption.isUserInteractionEnabled = true
                cell.imgChatOption.image = #imageLiteral(resourceName: "activeChatIcon")
            }
        }else{
            cell.btnChatOption.isUserInteractionEnabled = false
            cell.imgChatOption.image = #imageLiteral(resourceName: "notActiveChatIcon")
        }
        return cell
            
      //----------------------------------------------------------
        }else{
            cell.viewMainCantainer.isHidden = true
            cell.viewInfoContainer.isHidden = false
            
            cell.btnInfo1.setAttributedTitle(setUnderlineAttributeString(title: info1.title), for: .normal)
            cell.btnInfo1.tag = 1
            cell.btnInfo1.addTarget(self, action: #selector(self.actionInfoButton(sender:)), for: .touchUpInside)
          
            cell.btnInfo2.setAttributedTitle(setUnderlineAttributeString(title: info2.title), for: .normal)
            cell.btnInfo2.tag = 2
            cell.btnInfo2.addTarget(self, action: #selector(self.actionInfoButton(sender:)), for: .touchUpInside)
        }
        return cell
    }
    
    @objc func actionInfoButton(sender: UIButton){
        switch sender.tag {
        case 1:
            callURL(selectInfoPage: info1.url)
            break;
            
        case 2:
            callURL(selectInfoPage: info2.url)
            break;
            
            
        default:
            break;
        }
        
    }
    
    
    @objc func actionProfile(sender : UIButton){
        let dict = matchesVM.arrMatches[sender.tag] as! NSDictionary
        let user_id = dict.object(forKey: "user_id") as! Int
        getProfileInfo(openUser_id: "\(user_id)")
    }
    
    func moveToProfile(index : Int){
        let dict = matchesVM.arrMatches[index] as! NSDictionary
        let user_id = dict.object(forKey: "user_id") as! Int
        let objProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        objProfileVC.isComeFromMatchIfo.status = true
        objProfileVC.isComeFromMatchIfo.user_id =  user_id
        self.navigationController?.pushViewController(objProfileVC, animated: true)
    }
    
    
    func changeStatus(index : Int){
        
        let dict = matchesVM.arrMatches[index] as! NSDictionary
        
        let profile_indicator = dict.object(forKey: "profile_indicator") as? Int // hide
        let match_id = dict.object(forKey: "match_id") as? Int // hide
        guard profile_indicator != nil else { return }
        
        if profile_indicator == 0{ //hide to show
            updateStatusOfProfileIndicator(profileIndicator  : 1, match_id : match_id ?? 0, index : index)
            
        }else if profile_indicator == 1{ // show to hide
            
            updateStatusOfProfileIndicator(profileIndicator  : 0, match_id : match_id ?? 0, index : index)
        }
    }
    
    
    @objc func actionNext(sender : UIButton){
        let dict = matchesVM.arrMatches[sender.tag] as! NSDictionary
        
        let like_id = dict.object(forKey: "like_id") as! Int
        let event_type = dict.object(forKey: "event_subtype") as! Int
        let objDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        objDetailVC.detailRequestInfo.like_id = like_id
        objDetailVC.event_type = event_type
        self.navigationController?.pushViewController(objDetailVC, animated: true)
        
    }
    
    @objc func actionMoreOption(sender : UIButton){
        let dict = matchesVM.arrMatches[sender.tag] as! NSDictionary
        
        var profile_indicator : Int? = nil
        if let item = dict.object(forKey: "profile_indicator") as? Int {// hide
            profile_indicator = item
        }
        
        var strMsg = ""
        if profile_indicator == 0{
            strMsg =  "Share my profile"
        }else if profile_indicator == 1{
            strMsg = "Profile shared"
        }
        
        let rectOfCellInTableView = tblViewMatches.rectForRow(at: IndexPath(item: sender.tag, section: 0))
        let rectOfCellInSuperview = tblViewMatches.convert(rectOfCellInTableView, to: tblViewMatches.superview)
        let YValue = rectOfCellInSuperview.origin.y + 50
        
        viewMoreMatch.sharedInstance.Show(arr: [strMsg,"Remove match","Report"], y: YValue, strMatch_id: "\(profile_indicator ?? 0)") { [weak self](result) in
            
            switch  result {
            
            case strMsg:
                
                guard strMsg != "Profile shared" else { return }
                
                AlertTheme.sharedInstance.Show(popupCategory: .normal,message: "\(AlertMessages.shareInstance.YourProfileWillBe)", attributeRangeString: "", isCancelButtonVisible: true, arrBtn: ["Got it, share","Cancel"], isBottomTxt: "") { [weak self](clicked) in
                    if clicked == "Got it, share" {
                        self?.changeStatus(index: sender.tag)
                    }
                }
                
                break;
                
            case "Remove match":
                print("Remove match")
                
                if let match_id = dict.object(forKey: "match_id") as? Int {
                    let like_id = "\(dict.object(forKey: "like_id") ?? "")"
                    self?.removeMatch_api(index: sender.tag, match_id: match_id, like_id: like_id)
                }else{
                    let like_id = "\(dict.object(forKey: "like_id") ?? "")"
                    self?.removeMatch_api(index: sender.tag, match_id: 0, like_id: like_id)
                }
                break;
                
            case "Report":
                let to_user_id = "\(dict.object(forKey: "user_id") ?? "")"
                self?.callReportView(toUserId: to_user_id)
                break;
                
            default:
                break
            }
        }
    }
    
    
    func callReportView(toUserId: String){
        ReportReasonView.sharedInstance.Show { [weak self](strReason) in
            self?.callReport(to_user: toUserId, report_reason: strReason)
        }
    }
    
    
    @objc func actionChatOption(sender : UIButton){
        print("actionChatOption button tag : \(sender.tag)")
        
        let dict = matchesVM.arrMatches[sender.tag] as! NSDictionary
        let otherUser_id = dict.object(forKey: "user_id") as? Int
        let OtherName  = "\(dict.object(forKey: "name") ?? "")"
        let match_id = dict.object(forKey: "match_id") as? Int
        let profile_visibility = dict.object(forKey: "profile_visibility") as? Int
        let profile_indicator = dict.object(forKey: "profile_indicator") as? Int
        let event_id = dict.object(forKey: "event_id") as? Int
        
        let picture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(dict.object(forKey: "picture") ?? "--")"
        
        let uuserID  : UInt64 = Constants.userDefault.object(forKey: Variables.user_id) as! UInt64
        
        let chatID = HSRealTimeMessagingLIbrary.function_CreatChatID(uuserID, UInt64(otherUser_id ?? 0))
        
        let ChatDetails = HSChatUser.init(isChatID: chatID, isID: UInt64((otherUser_id) ?? 0), isName: OtherName, isTime: "", isLastMessage: "")
        let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
        
        vc.chatDetail = ChatDetails
        vc.comeFromScreenStatus = .fromMatch
        vc.backToPostVCDelegate = self
        
        vc.profile_visibility = profile_visibility ?? 0
        vc.profile_indicator = profile_indicator ?? 0
        
        vc.strprofilePicture = picture
        vc.matchInfo.match_id =  match_id ?? 0
        vc.matchInfo.user_id = otherUser_id ?? 0
        vc.matchInfo.event_id = event_id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

/*
 MARK:- API
 */

extension MatchesVC {
    
    func getMatchData(sort_type : Int)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        matchesVM.getMatchData(post_id: post_id, sort_type : sort_type) { (status, message) in
            Loader.sharedInstance.stopLoader()
            self.refreshControl.endRefreshing()
            
            if status == "success"
            {
                if self.matchesVM.arrMatches.count > 0 {
                    self.lblNoDataFound.isHidden = true
                    self.tblViewMatches.isHidden = false
                    self.tblViewMatches.reloadData()
                    
                }else{
                    self.lblNoDataFound.isHidden = false
                    self.tblViewMatches.isHidden = true
                    self.tblViewMatches.reloadData()
                    self.lblNoDataFound.text = message
                }
            }
            else
            {
                if self.matchesVM.arrMatches.count > 0 {
                    self.lblNoDataFound.isHidden = true
                    self.tblViewMatches.isHidden = false
                    self.tblViewMatches.reloadData()
                    
                }else{
                    self.lblNoDataFound.isHidden = false
                    self.tblViewMatches.isHidden = true
                    self.tblViewMatches.reloadData()
                    self.lblNoDataFound.text = message
                }
                
                if message == "No Data Available." || message == "No more data."
                {
                    //  SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    //SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
    
    
    func removeMatch_api(index : Int, match_id : Int, like_id : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        matchesVM.removeMatchAPI(match_id: match_id, index: index, like_id: like_id) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.tblViewMatches.reloadData()
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                if message == "No Data Available." || message == "No more data."
                {
                    //  SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    // SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
    
    
    // get profile info:
    func getProfileInfo(openUser_id : String)
    {
        
        Loader.sharedInstance.showLoader(msg: "Loading...")
        profileVM.getProfileApi(user_id: openUser_id) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                let item = dictData.object(forKey: "data") as! NSDictionary
                
                // get info :
                let userName = item.object(forKey: "name") as? String
                
                let bio =  item.object(forKey: "Description") as! String
                let Work =  item.object(forKey: "Work") as! String
                let Education =  item.object(forKey: "Education") as! String
                let profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(item.object(forKey: "picture") as! String)"
                
                let objProfileEditVC = self.storyboard?.instantiateViewController(withIdentifier: "profileEditVC") as! profileEditVC
                objProfileEditVC.valueFromPreviousScreen.profileName = userName ?? ""
                objProfileEditVC.valueFromPreviousScreen.strProfilePicture = profilePicture
                objProfileEditVC.activeComeFromStatus = .matchScreen
                objProfileEditVC.valueFromPreviousScreen.bio = bio
                objProfileEditVC.valueFromPreviousScreen.work = Work
                objProfileEditVC.valueFromPreviousScreen.education = Education
                objProfileEditVC.actionScreen = .readOnly
                AppDelegate.shared.navController = self.navigationController!
                self.navigationController?.pushViewController(objProfileEditVC, animated: true)
                
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    
    func updateStatusOfProfileIndicator(profileIndicator  : Int, match_id : Int, index : Int){
        
        Loader.sharedInstance.showLoader(msg: "Loading...")
        matchesVM.profileIndicatorUpdateAPI(match_id: match_id, profile_indicator : profileIndicator) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                let dict = self.matchesVM.arrMatches[index] as! NSDictionary
                let newDict : NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
                newDict["profile_indicator"] = profileIndicator
                self.matchesVM.arrMatches[index] = newDict
                self.tblViewMatches.reloadData()
            }
            else
            {
                if message == "No Data Available." || message == "No more data."
                {
                    //self.profile_indicator = profileIndicator
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
    
    // Report API:
    func callReport( to_user : String, report_reason : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        matchesVM.reportPost_Api(from_user: userID, to_user: to_user, report_reason: report_reason) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            self.navigationController?.popViewController(animated: true)
            SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            
        }
    }
}

