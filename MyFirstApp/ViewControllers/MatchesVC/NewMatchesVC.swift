//
//  NewMatchesVC.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

//protocol BackToPostVCDelegate {
//    func backToPostVc()
//}

extension NewMatchesVC : BackToPostVCDelegate {
    func backToPostVc(){
        self.navigationController?.popViewController(animated: true)
    }
}


class NewMatchesVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet:
    @IBOutlet var viewVehicle: UIView!
    @IBOutlet var viewInfo: UIView!
    @IBOutlet weak var tblViewMatches: UITableView!
    @IBOutlet weak var viewSort: UIView!
    @IBOutlet var viewFilter: UIView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var contentHeightConstraintViewOuterTag: NSLayoutConstraint!

    
    //----------------------------------------------
  
    //MARK:-
    //MARK:- IBOutlet:
    let matchesVM = MatchesViewModel()
    var profileVM = ProfileViewModel()
    var selectedValue = "Newest to Oldest"
    var post_id = 0
    var refreshControl: UIRefreshControl!
    
    var info1 = (title: "" , url : "")
    var info2 = (title: "" , url : "")
    var postTitle = String()
    var postType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblViewMatches.addSubview(refreshControl)
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       getInitialMatchData()
    }
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOpenInfo(_ sender: UIButton) {
        let frame =  viewInfo.frame.origin
//        let window = UIApplication.shared.windows[0]
//        let topPadding = window.safeAreaInsets.top
        MatchInfoView.sharedInstance.Show(x: frame.x, y: frame.y, selectedValue: selectedValue) { [weak self](value) in
            self?.selectedValue = value
            self?.initView()
        }
    }
    
    
    @IBAction func actionOpenSortBy(_ sender: UIButton) {
        let frame =  viewSort.frame.origin
        let window = UIApplication.shared.windows[0]
        let topPadding = window.safeAreaInsets.top
        ssSortView.sharedInstance.Show(x: frame.x, y: frame.y - topPadding, selectedValue: selectedValue) { [weak self](value) in
            self?.selectedValue = value
            self?.initView()
        }
    }
    
    //MARK:-
    //MARK:- Methods:
    
    
    func callURL(selectInfoPage : String){
        if let url = URL(string: selectInfoPage), UIApplication.shared.canOpenURL(url) {
           if #available(iOS 10.0, *) {
              UIApplication.shared.open(url, options: [:], completionHandler: nil)
           } else {
              UIApplication.shared.openURL(url)
           }
        }
    }
    
    func initView(){
        getInfoList()
        setPostInfo()

        matchesVM.arrMatches.removeAllObjects()
        tblViewMatches.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right:0)
        
        let nib = UINib.init(nibName: "NewMatchesCell", bundle: nil)
        tblViewMatches.register(nib, forCellReuseIdentifier: "NewMatchesCell")
        
        tblViewMatches.estimatedRowHeight = 100
        tblViewMatches.rowHeight = UITableView.automaticDimension
        
        lblNoDataFound.isHidden = true
        tblViewMatches.isHidden = true
        // after response of API
        viewInfo.addShadowToView()
        viewFilter.addShadowToView()
        getInitialMatchData()
//        updateContentViewHeight()

    }
    
    func setPostInfo() {
        self.lblTitle.text = self.postTitle
        switch  postType {
        case "1": //airplane
            imgView.image = UIImage(named: "flight_post")
            break
            
        case "2": //subway
            imgView.image = UIImage(named: "train_post")
            break
            
        case "3": //place
            imgView.image = UIImage(named: "location_post")
            break
            
        case "4": //street
            imgView.image = UIImage(named: "street_post")
            break
            
        case "5": //bus
            imgView.image = UIImage(named: "bus_post")
            break
            
        case "6": //train
            imgView.image = UIImage(named: "train_post")
            break
            
        default:
            break
        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//            self.updateContent()
//        }
        
    }
    
    func setUnderlineAttributeString(title : String) -> NSAttributedString? {
       let attributes : [NSAttributedString.Key : Any] = [
           NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0),
           NSAttributedString.Key.foregroundColor : UIColor.gray,
           NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
       ]
       let attributedString = NSAttributedString(string: title, attributes: attributes)
       return attributedString
     }
    
    
    func getInfoList(){
    
        for item in Singleton.shared.InfoPage{
            if let dict = item as? NSDictionary {
             
                if (dict.object(forKey: "type_id") as? Int) != nil  {
                    if let subtype_id = dict.object(forKey: "subtype_id") as? Int{
                        switch subtype_id {
                        case 8:
                            if let output = dict.object(forKey: "output") as? String {
                                if output.count > 0 {
                            
                                    info1.title = "\(dict.object(forKey: "subtype_description") ?? "--")".replacingOccurrences(of: "\n", with: "")
                                    info1.url = output
                                
                                  
                                  //  print("lblInfoPage1.text : \(lblInfoPage1.text)")
                                }
                            }
                         
                            break
                            
                        case 9:
                            
                            if let output = dict.object(forKey: "output") as? String {
                                if output.count > 0 {
                                   
                                    info2.title = "\(dict.object(forKey: "subtype_description") ?? "--")".replacingOccurrences(of: "\n", with: "")
                                    info2.url = output
                                    
                                 //   print("lblInfoPage2.text : \(lblInfoPage2.text)")
                                }
                            }
                            break
                        
                            
                        default:
                            break
                        }
                    }
                }
        }
    }
        tblViewMatches.reloadData()
    }
    
    func getInitialMatchData(){
        var sortTypeValue = 0
        if selectedValue == "Newest to Oldest" {
            sortTypeValue = 1
        }else if selectedValue == "Oldest to Newest" {
            sortTypeValue = 2
        }else if selectedValue == "Matches" {
            sortTypeValue = 3
        }
        
        getMatchData(sort_type: sortTypeValue)
    }
    
    @objc func refresh(_ sender: Any) {
        getInitialMatchData()
    }
}

//MARK:-
//MARK:- Tableview deeletes:

extension NewMatchesVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchesVM.arrMatches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewMatchesCell", for: indexPath) as! NewMatchesCell
        
        cell.viewMainCantainer.isHidden = false
        cell.viewInfoContainer.isHidden = true
        
        guard matchesVM.arrMatches.count > 0 else { return cell }
        
        guard let dict = matchesVM.arrMatches[indexPath.row] as? NSDictionary else { return cell }
        
        cell.lblTitle.text = "\(dict.object(forKey: "description") ?? "")"
        
        cell.lblNextTitle.text = "The story"
        cell.btnNext.tag = indexPath.row
        cell.btnNext.addTarget(self, action: #selector(self.actionNext(sender:)), for: .touchUpInside)
        
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(self.actionMoreOption(sender: )), for: .touchUpInside)
        
        cell.btnProfileOption.tag = indexPath.row
        cell.btnProfileOption.addTarget(self, action: #selector(self.actionProfile(sender:)), for: .touchUpInside)
        
        cell.btnChatOption.tag = indexPath.row
        cell.btnChatOption.addTarget(self, action: #selector(self.actionChatOption), for: .touchUpInside)
        
        self.setStatusData(match_id: dict.object(forKey: "match_id") as? Int ?? -1, chat_id: dict.object(forKey: "chat_indicator") as? Int ?? -1, cell: cell)
        
        // profile:
        if let item  = dict.object(forKey: "profile_visibility") as? Int{// hide
            
            if item == 0 {//  1 visible , 0 desble
                
                cell.btnProfileOption.isUserInteractionEnabled = false
                cell.imgProfileOption.image = UIImage(named: "match_disabled_user")
            }else if item == 1 {// show
                cell.btnProfileOption.isUserInteractionEnabled = true
                cell.imgProfileOption.image = UIImage(named: "match_user")
            }
        }else{
            cell.btnProfileOption.isUserInteractionEnabled = false
            cell.imgProfileOption.image = UIImage(named: "match_disabled_user")
        }
        
        
        //Chat :
        if let item = dict.object(forKey: "match_id") as? Int {// hide
            if item == 0 {
                cell.btnChatOption.isUserInteractionEnabled = false
                cell.imgChatOption.image = UIImage(named: "match_disabled_chat")
            }else{// show
                cell.btnChatOption.isUserInteractionEnabled = true
                cell.imgChatOption.image = UIImage(named: "match_chat")
            }
        }else{
            cell.btnChatOption.isUserInteractionEnabled = false
            cell.imgChatOption.image = UIImage(named: "match_disabled_chat")
        }
        
        return cell
    }
    
    func setStatusData(match_id: Int, chat_id: Int, cell: NewMatchesCell) {
        if match_id != -1 { // Matched
            cell.viewStatus.backgroundColor = #colorLiteral(red: 0.8784313725, green: 1, blue: 0.9803921569, alpha: 1)
            cell.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7490196078, blue: 0.6196078431, alpha: 1)
            cell.lblStatus.text = "Matched"
        } else {
            if chat_id == -1 { // Pending
                cell.viewStatus.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
                cell.lblStatus.textColor = #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1)
                cell.lblStatus.text = "Pending"
            } else if chat_id == 0 { // Rejected
                cell.viewStatus.backgroundColor = #colorLiteral(red: 1, green: 0.9058823529, blue: 0.8901960784, alpha: 1)
                cell.lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
                cell.lblStatus.text = "Declined"
            } else { // Requested
                cell.viewStatus.backgroundColor = #colorLiteral(red: 1, green: 0.9647058824, blue: 0.8745098039, alpha: 1)
                cell.lblStatus.textColor = #colorLiteral(red: 0.9450980392, green: 0.6745098039, blue: 0, alpha: 1)
                cell.lblStatus.text = "Chat requested"
            }
        }
    }
    
    @objc func actionInfoButton(sender: UIButton){
        switch sender.tag {
        case 1:
            callURL(selectInfoPage: info1.url)
            break;
            
        case 2:
            callURL(selectInfoPage: info2.url)
            break;
            
            
        default:
            break;
        }
        
    }
    
    
    @objc func actionProfile(sender : UIButton){
        let dict = matchesVM.arrMatches[sender.tag] as! NSDictionary
        let user_id = dict.object(forKey: "user_id") as! Int
        getProfileInfo(openUser_id: "\(user_id)")
    }
    
    func moveToProfile(index : Int){
        let dict = matchesVM.arrMatches[index] as! NSDictionary
        let user_id = dict.object(forKey: "user_id") as! Int
        let objProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        objProfileVC.isComeFromMatchIfo.status = true
        objProfileVC.isComeFromMatchIfo.user_id =  user_id
        self.navigationController?.pushViewController(objProfileVC, animated: true)
    }
    
    
    func changeStatus(index : Int){
        
        let dict = matchesVM.arrMatches[index] as! NSDictionary
        
        let profile_indicator = dict.object(forKey: "profile_indicator") as? Int // hide
        let match_id = dict.object(forKey: "match_id") as? Int // hide
        guard profile_indicator != nil else { return }
        
        if profile_indicator == 0{ //hide to show
            updateStatusOfProfileIndicator(profileIndicator  : 1, match_id : match_id ?? 0, index : index)
            
        }else if profile_indicator == 1{ // show to hide
            
            updateStatusOfProfileIndicator(profileIndicator  : 0, match_id : match_id ?? 0, index : index)
        }
    }
    
    
    @objc func actionNext(sender : UIButton){
        let dict = matchesVM.arrMatches[sender.tag] as! NSDictionary
        
        let like_id = dict.object(forKey: "like_id") as! Int
        let event_type = dict.object(forKey: "event_subtype") as! Int
        let objDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        objDetailVC.detailRequestInfo.like_id = like_id
        objDetailVC.event_type = event_type
        self.navigationController?.pushViewController(objDetailVC, animated: true)
        
    }
    
    @objc func actionMoreOption(sender : UIButton){
        let dict = matchesVM.arrMatches[sender.tag] as! NSDictionary
        
        var profile_indicator : Int? = nil
        if let item = dict.object(forKey: "profile_indicator") as? Int {// hide
            profile_indicator = item
        }
        
        var strMsg = ""
        if profile_indicator == 0{
            strMsg =  "Share my profile"
        }else if profile_indicator == 1{
            strMsg = "Profile shared"
        }
        
        let rectOfCellInTableView = tblViewMatches.rectForRow(at: IndexPath(item: sender.tag, section: 0))
        let rectOfCellInSuperview = tblViewMatches.convert(rectOfCellInTableView, to: tblViewMatches.superview)
        let YValue = rectOfCellInSuperview.origin.y + 50
        
        ViewMoreMatchNew.sharedInstance.Show(arr: [strMsg,"Remove","Report"], y: YValue, strMatch_id: "\(profile_indicator ?? 0)") { [weak self](result) in
            
            switch  result {
            
            case strMsg:
                
                guard strMsg != "Profile shared" else { return }
                
                AlertTheme.sharedInstance.Show(popupCategory: .normal,message: "\(AlertMessages.shareInstance.YourProfileWillBe)", attributeRangeString: "", isCancelButtonVisible: true, arrBtn: ["Got it, share","Cancel"], isBottomTxt: "") { [weak self](clicked) in
                    if clicked == "Got it, share" {
                        self?.changeStatus(index: sender.tag)
                    }
                }
                
                break;
                
            case "Remove":
                print("Remove")
                
                if let match_id = dict.object(forKey: "match_id") as? Int {
                    let like_id = "\(dict.object(forKey: "like_id") ?? "")"
                    self?.removeMatch_api(index: sender.tag, match_id: match_id, like_id: like_id)
                }else{
                    let like_id = "\(dict.object(forKey: "like_id") ?? "")"
                    self?.removeMatch_api(index: sender.tag, match_id: 0, like_id: like_id)
                }
                break;
                
            case "Report":
                let to_user_id = "\(dict.object(forKey: "user_id") ?? "")"
                self?.callReportView(toUserId: to_user_id)
                break;
                
            default:
                break
            }
        }
    }
    
    
    func callReportView(toUserId: String){
        ReportReasonView.sharedInstance.Show { [weak self](strReason) in
            self?.callReport(to_user: toUserId, report_reason: strReason)
        }
    }
    
    
    @objc func actionChatOption(sender : UIButton){
        print("actionChatOption button tag : \(sender.tag)")
        
        var picture = ""

        let dict = matchesVM.arrMatches[sender.tag] as! NSDictionary
        let otherUser_id = dict.object(forKey: "user_id") as? Int
        let OtherName  = "\(dict.object(forKey: "name") ?? "")"
        let match_id = dict.object(forKey: "match_id") as? Int
        let profile_visibility = dict.object(forKey: "profile_visibility") as? Int
        let profile_indicator = dict.object(forKey: "profile_indicator") as? Int
        let event_id = dict.object(forKey: "event_id") as? Int
        
//        let picture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(dict.object(forKey: "picture") ?? "--")"
        
        if let image = dict.object(forKey: "picture") as? String {
            if image == "" {
                let picture_img = dict.object(forKey: "post_image") as? String ?? "--"
                picture = "\(APPURL.baseAPI.imageURL)post_pictures/\(picture_img)"
            } else {
                picture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(image)"
            }
        }
        
        let uuserID  : UInt64 = Constants.userDefault.object(forKey: Variables.user_id) as! UInt64
        
        let chatID = HSRealTimeMessagingLIbrary.function_CreatChatID(uuserID, UInt64(otherUser_id ?? 0))
        
        let ChatDetails = HSChatUser.init(isChatID: chatID, isID: UInt64((otherUser_id) ?? 0), isName: OtherName, isTime: "", isLastMessage: "")
        let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
        
        vc.chatDetail = ChatDetails
        vc.comeFromScreenStatus = .fromMatch
        vc.backToPostVCDelegate = self
        
        vc.profile_visibility = profile_visibility ?? 0
        vc.profile_indicator = profile_indicator ?? 0
        
        vc.strprofilePicture = picture
        vc.matchInfo.match_id =  match_id ?? 0
        vc.matchInfo.user_id = otherUser_id ?? 0
        vc.matchInfo.event_id = event_id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

/*
 MARK:- API
 */

extension NewMatchesVC {
    
    func getMatchData(sort_type : Int)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        matchesVM.getMatchData(post_id: post_id, sort_type : sort_type) { (status, message) in
            Loader.sharedInstance.stopLoader()
            self.refreshControl.endRefreshing()
            
            if status == "success"
            {
                if self.matchesVM.arrMatches.count > 0 {
                    self.lblNoDataFound.isHidden = true
                    self.tblViewMatches.isHidden = false
                    self.tblViewMatches.reloadData()
                    
                }else{
                    self.lblNoDataFound.isHidden = false
                    self.tblViewMatches.isHidden = true
                    self.tblViewMatches.reloadData()
                    self.lblNoDataFound.text = message
                }
            }
            else
            {
                if self.matchesVM.arrMatches.count > 0 {
                    self.lblNoDataFound.isHidden = true
                    self.tblViewMatches.isHidden = false
                    self.tblViewMatches.reloadData()
                    
                }else{
                    self.lblNoDataFound.isHidden = false
                    self.tblViewMatches.isHidden = true
                    self.tblViewMatches.reloadData()
                    self.lblNoDataFound.text = message
                }
                
                if message == "No Data Available." || message == "No more data."
                {
                    //  SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    //SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
    
    
    func removeMatch_api(index : Int, match_id : Int, like_id : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        matchesVM.removeMatchAPI(match_id: match_id, index: index, like_id: like_id) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.tblViewMatches.reloadData()
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                if message == "No Data Available." || message == "No more data."
                {
                    //  SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    // SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
    
    
    // get profile info:
    func getProfileInfo(openUser_id : String)
    {
        var profilePicture = ""
        Loader.sharedInstance.showLoader(msg: "Loading...")
        profileVM.getProfileApi(user_id: openUser_id) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                let item = dictData.object(forKey: "data") as! NSDictionary
                
                // get info :
                let userName = item.object(forKey: "name") as? String
                
                let bio =  item.object(forKey: "Description") as! String
                let Work =  item.object(forKey: "Work") as! String
                let Education =  item.object(forKey: "Education") as! String
                
                if let image = item.object(forKey: "post_image") as? String {
                    if image == "" {
                        let picture_img = item.object(forKey: "picture") as? String ?? "--"
                        profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(picture_img)"
                    } else {
                        profilePicture = "\(APPURL.baseAPI.imageURL)post_pictures/\(image)"
                    }
                }  else {
                    let picture_img = item.object(forKey: "picture") as? String ?? "--"
                    profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(picture_img)"
                }
                
//                let profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(item.object(forKey: "picture") as! String)"
                
                let objProfileEditVC = self.storyboard?.instantiateViewController(withIdentifier: "profileEditVC") as! profileEditVC
                objProfileEditVC.valueFromPreviousScreen.profileName = userName ?? ""
                objProfileEditVC.valueFromPreviousScreen.strProfilePicture = profilePicture
                objProfileEditVC.activeComeFromStatus = .matchScreen
                objProfileEditVC.valueFromPreviousScreen.bio = bio
                objProfileEditVC.valueFromPreviousScreen.work = Work
                objProfileEditVC.valueFromPreviousScreen.education = Education
                objProfileEditVC.actionScreen = .readOnly
                AppDelegate.shared.navController = self.navigationController!
                self.navigationController?.pushViewController(objProfileEditVC, animated: true)
                
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    
    func updateStatusOfProfileIndicator(profileIndicator  : Int, match_id : Int, index : Int){
        
        Loader.sharedInstance.showLoader(msg: "Loading...")
        matchesVM.profileIndicatorUpdateAPI(match_id: match_id, profile_indicator : profileIndicator) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                let dict = self.matchesVM.arrMatches[index] as! NSDictionary
                let newDict : NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
                newDict["profile_indicator"] = profileIndicator
                self.matchesVM.arrMatches[index] = newDict
                self.tblViewMatches.reloadData()
            }
            else
            {
                if message == "No Data Available." || message == "No more data."
                {
                    //self.profile_indicator = profileIndicator
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
    
    // Report API:
    func callReport( to_user : String, report_reason : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        matchesVM.reportPost_Api(from_user: userID, to_user: to_user, report_reason: report_reason) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            self.navigationController?.popViewController(animated: true)
            SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            
        }
    }
}

extension NewMatchesVC {
    func updateContentViewHeight() {
        let requiredHeight = calculateContentHeight()
        contentHeightConstraintViewOuterTag.constant = requiredHeight
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func calculateContentHeight() -> CGFloat {
        var totalHeight: CGFloat = 0
        for subview in viewVehicle.subviews {
            totalHeight += subview.frame.height
        }
        return totalHeight
    }
    
    func updateContent() {
        updateContentViewHeight()
    }
}
