//
//  detailTagViewCell.swift
//  MyFirstApp
//
//  Created by cis on 19/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class detailTagViewCell: UICollectionViewCell {

    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet weak var viewCorner: UIView!
    @IBOutlet var viewInnerTag: UIView!
    @IBOutlet var viewTag: UIViewClass!
    @IBOutlet weak var imgStatus: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
