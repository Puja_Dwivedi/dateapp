//
//  DetailViewModel.swift
//  MyFirstApp
//
//  Created by cis on 20/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class DetailViewModel: NSObject {
    
    var dictData = NSDictionary()
    
    func getMatchDetailData( like_id : Int,completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.getMatchDetails
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userid as AnyObject, forKey: "user_id")
        param.updateValue(like_id as AnyObject, forKey: "like_id")
        
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    self.dictData = dict
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    
                    completion("success", message)
                    
                }else { // error :
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                }
            }
            else
            {   let dict = data.response
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
    
    
    func setMatchAcceptStatus( like_id : Int,chat_indicator : Int,completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        //currentPage = pageNumber
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.updateChatAgreeStatus
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userid as AnyObject, forKey: "user_id")
        param.updateValue(like_id as AnyObject, forKey: "like_id")
        param.updateValue(chat_indicator as AnyObject, forKey: "chat_indicator")
        
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else { // error :
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                }
            }
            else
            {   let dict = data.response
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                }else{
                    message = data.message
                }
                completion("error", message)
            }
        }
    }
}
