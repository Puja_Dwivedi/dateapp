//
//  DetailVC.swift
//  MyFirstApp
//
//  Created by cis on 16/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet var viewReject: UIView!
    @IBOutlet var viewAccept: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet var viewTop: UIViewClass!
    @IBOutlet var imgTopArrow: UIImageView!
    @IBOutlet var imgBottomArrow: UIImageView!
    @IBOutlet var viewBottom: UIViewClass!
    
    //MARK: First View :
    @IBOutlet weak var lblRightChatFirstView: UILabel!
    @IBOutlet weak var lblLeftChatFirstView: UILabel!
    
    //MARK: Secoand View :
    @IBOutlet weak var collectionView1SecoandView: UICollectionView!
    @IBOutlet weak var collectionView1SecoandView_h: NSLayoutConstraint!
    @IBOutlet weak var imgDropDownSecoandView1: UIImageView!
    
    
    @IBOutlet weak var collectionView2SecoandView: UICollectionView!
    @IBOutlet weak var collectionView2SecoandView_h: NSLayoutConstraint!
    @IBOutlet weak var imgDropDownSecoandView2: UIImageView!
    @IBOutlet weak var imgSecondBG: UIImageView!
    @IBOutlet var imgFourthBG: UIImageView!
    
    var arr1TagSecoandView = [(title : String,isIconDone : Bool)]()
    var arr2SecoandView = [ String]()
    
    //thirld view:
    @IBOutlet weak var collectionView1ThirldView: UICollectionView!
    @IBOutlet weak var collectionView1ThirldView_h: NSLayoutConstraint!
    @IBOutlet weak var imgDropDownThirldView1: UIImageView!
    
    @IBOutlet weak var collectionView2ThirldView: UICollectionView!
    @IBOutlet weak var collectionView2ThirldView_h: NSLayoutConstraint!
    @IBOutlet weak var imgDropDownThirldView2: UIImageView!
    @IBOutlet weak var imgBackgroundFirstView: UIImageView!
    @IBOutlet weak var imgThirldBG: UIImageView!
    
    var arr1ThirldTagView = [(title: String, isIconDone: Bool)]()
    var arr2ThirldView = [String]()
    
    //Fourth View:
    
    @IBOutlet weak var lblTitleFourthView: UILabel!
    
    @IBOutlet weak var viewRejectContainer: UIView!
    @IBOutlet weak var viewLikeContainer: UIView!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    @IBOutlet weak var viewMainContainer: UIViewClass!
    @IBOutlet weak var view1DDSecondScreenContainer: UIView!
    
    
    //MARK:-
    //MARK:- Variable
    
    let detailVM = DetailViewModel()
    var detailRequestInfo = (like_id : 0, user_id : 0)
    var event_type = 0
    
    // secoand view1:
    var isOpenDDSecoandView1 = true
    // secoand view 2
    var isOpenDDSecoandView2 = true
    // get value view will open or close
    var isSecondViewOPenBothDD_At_A_Time = false
    
    // thirld view1:
    var isOpenDDThirldView1 = true
    // thirld view 2
    var isOpenDDThirldView2 = true
    // get value view will open or close
    var isThirldViewOPenBothDD_At_A_Time = false
    
    //MARK:-
    //MARK:- App Flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMatchDetailData()
    }
    
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Second screen:
    @IBAction func actionDDSecoandView1(_ sender: UIButton) {
        
        if isSecondViewOPenBothDD_At_A_Time == false{ // one tag DD view display and other tag DD.
            isOpenDDSecoandView2 = true // close:
            openDDSecondView2(isAnimationStatus: false)
        }
        
        // open:
        openDDSecondView1(isAnimationStatus: false)
    }
    
    
    @IBAction func actionDDSecoandView2(_ sender: UIButton) {
        
        if isSecondViewOPenBothDD_At_A_Time == false{ // one tag DD view display and other tag DD.
            isOpenDDSecoandView1 = true // close if open
            openDDSecondView1(isAnimationStatus: false)
        }
        
        // open
        openDDSecondView2(isAnimationStatus: false)
    }
    
    //Thirld screen:
    @IBAction func actionDDThirldView1(_ sender: UIButton) {
        isOpenDDThirldView2 = true // close if open
        openDDThirldView2(isAnimationStatus: false)
        
        // open:
        openDDThirldView1(isAnimationStatus: false)
    }
    
    
    @IBAction func actionDDThirldView2(_ sender: UIButton) {
        isOpenDDThirldView1 = true // close if open
        openDDThirldView1(isAnimationStatus: false)
        
        // open :
        openDDThirldView2(isAnimationStatus: false)
    }
    
    
    @IBAction func actionAccept(_ sender: UIButton) { // 1: accept, 0 disable
        let like_id = detailVM.dictData.object(forKey: "like_id") as! Int
        setAcceptMatch(like_id: like_id, chat_indicator: 1)
    }
    
    
    @IBAction func actionReject(_ sender: UIButton) { // 1: accept, 0 disable
        let like_id = detailVM.dictData.object(forKey: "like_id") as! Int
        setAcceptMatch(like_id: like_id, chat_indicator: 0)
    }
    
    
    //MARK:-
    //MARK:- Methods
    
    func initView(){
        
        viewAccept.addShadowToView()
        viewReject.addShadowToView()
        viewTop.addShadowToView()
        viewBottom.addShadowToView()
        imgTopArrow.addShadowToImage()
        imgBottomArrow.addShadowToImage()
        
//        imgSecondBG.addShadowToImage()
//        imgThirldBG.addShadowToImage()
//        imgFourthBG.addShadowToImage()
        
        let columnLayout1 = FlowLayout(
            minimumInteritemSpacing: 5.0,
            minimumLineSpacing: -8,
            sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        )
        
        let columnLayout2 = FlowLayout(
            minimumInteritemSpacing: 5.0,
            minimumLineSpacing: -8,
            sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        )
        
        let columnLayout3 = FlowLayout(
            minimumInteritemSpacing: 5.0,
            minimumLineSpacing: -8,
            sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        )
        
        let columnLayout4 = FlowLayout(
            minimumInteritemSpacing: 5.0,
            minimumLineSpacing: -8,
            sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        )
        
        collectionView1SecoandView?.contentInsetAdjustmentBehavior = .always
        collectionView1SecoandView.collectionViewLayout = columnLayout1
        collectionView1SecoandView.register(UINib(nibName: "detailTagViewCell", bundle: .main), forCellWithReuseIdentifier: "detailTagViewCell")
        
        collectionView2SecoandView?.contentInsetAdjustmentBehavior = .always
        collectionView2SecoandView.collectionViewLayout = columnLayout2
        collectionView2SecoandView.register(UINib(nibName: "detailTagViewCell", bundle: .main), forCellWithReuseIdentifier: "detailTagViewCell")
        
        collectionView1ThirldView?.contentInsetAdjustmentBehavior = .always
        collectionView1ThirldView.collectionViewLayout = columnLayout3
        collectionView1ThirldView.register(UINib(nibName: "detailTagViewCell", bundle: .main), forCellWithReuseIdentifier: "detailTagViewCell")
        
        collectionView2ThirldView?.contentInsetAdjustmentBehavior = .always
        collectionView2ThirldView.collectionViewLayout = columnLayout4
        collectionView2ThirldView.register(UINib(nibName: "detailTagViewCell", bundle: .main), forCellWithReuseIdentifier: "detailTagViewCell")
        
        openDDThirldView1(isAnimationStatus: false)
        openDDThirldView2(isAnimationStatus: false)
        
        switch event_type {
        case 1: //airplane
            imgBackgroundFirstView.image = UIImage(named: "match_plane")
            break;
            
        case 2: //subway
            imgBackgroundFirstView.image = UIImage(named: "match_subway")
            break;
            
        case 3: //place
            imgBackgroundFirstView.image = UIImage(named: "match_place")
            break;
            
        case 4:  //street
            imgBackgroundFirstView.image = UIImage(named: "match_street")
            break;
            
        case 5: //bus
            imgBackgroundFirstView.image = UIImage(named: "match_bus")
            break;
            
        case 6: //train
            imgBackgroundFirstView.image = #imageLiteral(resourceName: "detailTrain1")
            break;
            
        default:
            break
        }
        
        setFirstViewInfo()
        setSecoandViewInfo()
        setThirldViewInfo()
        setFourthViewInfo()
        setAcceptInfoStatus()
    }
    
    
    // drop down secoand view :
    
    func openDDSecondView1(isAnimationStatus : Bool){
        
        if isOpenDDSecoandView1{ //open to off
            
            if isAnimationStatus {
                UIView.animate(withDuration: 0.5) {
                    self.collectionView1SecoandView.isHidden = true;
                    self.imgDropDownSecoandView1.transform = .identity
                    self.view.layoutIfNeeded()
                }
            }else{
                self.collectionView1SecoandView.isHidden = true;
                self.view.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.5) {
                    self.imgDropDownSecoandView1.transform = CGAffineTransform(rotationAngle: -(.pi / 2))
                        //.identity
                    self.view.layoutIfNeeded()
                }
            }
            
            isOpenDDSecoandView1 = false
        }else{ // close to open
            
            if isAnimationStatus {
                UIView.animate(withDuration: 0.5) {
                    self.collectionView1SecoandView.isHidden = false
                    self.imgDropDownSecoandView1.transform = CGAffineTransform(rotationAngle: .pi / 2)
                    self.view.layoutIfNeeded()
                }
            }else{
                self.collectionView1SecoandView.isHidden = false
                self.view.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.5) {
                    self.imgDropDownSecoandView1.transform = .identity
                    //CGAffineTransform(rotationAngle: .pi / 2)
                    self.view.layoutIfNeeded()
                }
            }
            isOpenDDSecoandView1 = true
        }
    }
    
    
    func openDDSecondView2(isAnimationStatus : Bool){
        
        if isOpenDDSecoandView2{ //open to off
            
            if isAnimationStatus {
                UIView.animate(withDuration: 0.5) {
                    self.collectionView2SecoandView.isHidden = true;
                    self.imgDropDownSecoandView2.transform = .identity
                    self.view.layoutIfNeeded()
                }
            }else{
                self.collectionView2SecoandView.isHidden = true;
                self.view.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.5) {
                    self.imgDropDownSecoandView2.transform = CGAffineTransform(rotationAngle: (.pi / 2))
                        //.identity
                    self.view.layoutIfNeeded()
                }
            }
            
            isOpenDDSecoandView2 = false
        }else{ // close to open
            
            if isAnimationStatus {
                UIView.animate(withDuration: 0.5) {
                    self.collectionView2SecoandView.isHidden = false
                    self.imgDropDownSecoandView2.transform = CGAffineTransform(rotationAngle: -(.pi / 2))
                    self.view.layoutIfNeeded()
                    
                }
            }else{
                self.collectionView2SecoandView.isHidden = false
                self.view.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.5) {
                    self.imgDropDownSecoandView2.transform = .identity
                    //CGAffineTransform(rotationAngle: -(.pi / 2))
                    self.view.layoutIfNeeded()
                }
            }
            isOpenDDSecoandView2 = true
        }
    }
    
    // drop down thirld view :
    func openDDThirldView1(isAnimationStatus : Bool){
        
        if isOpenDDThirldView1{ //open to off
            
            if isAnimationStatus {
                UIView.animate(withDuration: 0.5) {
                    self.collectionView1ThirldView.isHidden = true;
                    self.imgDropDownThirldView1.transform = .identity
                    self.view.layoutIfNeeded()
                }
            }else{
                self.collectionView1ThirldView.isHidden = true;
                self.view.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.5) {
                    self.imgDropDownThirldView1.transform = CGAffineTransform(rotationAngle: -(.pi / 2))
                        //.identity
                    self.view.layoutIfNeeded()
                }
            }
            
            isOpenDDThirldView1 = false
        }else{ // close to open
            
            if isAnimationStatus {
                UIView.animate(withDuration: 0.5) {
                    self.collectionView1ThirldView.isHidden = false
                    UIView.animate(withDuration: 0.5) {
                        self.imgDropDownThirldView1.transform = CGAffineTransform(rotationAngle: .pi / 2)
                        self.view.layoutIfNeeded()
                    }
                }
            }else{
                self.collectionView1ThirldView.isHidden = false
                self.view.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.5) {
                    self.imgDropDownThirldView1.transform = .identity
                    // CGAffineTransform(rotationAngle: .pi / 2)
                    self.view.layoutIfNeeded()
                }
            }
            isOpenDDThirldView1 = true
        }
    }
    
    
    func openDDThirldView2(isAnimationStatus : Bool){
        
        if isOpenDDThirldView2{ //open to off
            
            if isAnimationStatus {
                UIView.animate(withDuration: 0.5) {
                    self.collectionView2ThirldView.isHidden = true;
                    self.imgDropDownThirldView2.transform = .identity
                    self.view.layoutIfNeeded()
                }
            }else{
                self.collectionView2ThirldView.isHidden = true;
                self.view.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.5) {
                    self.imgDropDownThirldView2.transform = CGAffineTransform(rotationAngle: (.pi / 2))
                        // .identity
                    self.view.layoutIfNeeded()
                }
            }
            
            isOpenDDThirldView2 = false
        }else{ // close to open
            
            if isAnimationStatus {
                UIView.animate(withDuration: 0.5) {
                    self.collectionView2ThirldView.isHidden = false
                    self.imgDropDownThirldView2.transform = CGAffineTransform(rotationAngle: -(.pi / 2))
                    self.view.layoutIfNeeded()
                }
            }else{
                self.collectionView2ThirldView.isHidden = false
                self.view.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.5) {
                    self.imgDropDownThirldView2.transform = .identity
                    // CGAffineTransform(rotationAngle: -(.pi / 2))
                    self.view.layoutIfNeeded()
                }
            }
            isOpenDDThirldView2 = true
        }
    }
    
    
    func setFirstViewInfo(){
        
        let dictYou = detailVM.dictData.object(forKey: "you") as! NSDictionary
        lblRightChatFirstView.text = "\(dictYou.object(forKey: "description") ?? "")"
        
        let dictYourMatch = detailVM.dictData.object(forKey: "yourMatch") as! NSDictionary
        lblLeftChatFirstView.text = "\(dictYourMatch.object(forKey: "description") ?? "")"
    }
    
    func setSecoandViewInfo(){
        
        let dictYourMatch = detailVM.dictData.object(forKey: "yourMatch") as! NSDictionary
        let arrYourMatch = dictYourMatch.object(forKey: "yourMatch") as! [Any]
        
        // top view:
        for item in arrYourMatch{
            arr1TagSecoandView.append((title: item as! String, isIconDone: true))
        }
        
        
        let arrYourNotmatch = dictYourMatch.object(forKey: "yourNotmatch") as! [Any]
        for item in arrYourNotmatch{
            arr1TagSecoandView.append((title: item as! String, isIconDone: false))
        }
        
        let arryourMissedmatch = dictYourMatch.object(forKey: "YourMissedmatch") as! [Any]
        for item in arryourMissedmatch{
            arr2SecoandView.append(item as! String)
        }
        
        //imgSecoandView_h.constant = UIScreen.main.bounds.width - 200
        collectionView1SecoandView.reloadData()
        collectionView1SecoandView.updateConstraints()
        collectionView1SecoandView.layoutIfNeeded()
        collectionView1SecoandView_h.constant = self.collectionView1SecoandView.contentSize.height
        
        collectionView2SecoandView.reloadData()
        collectionView2SecoandView.updateConstraints()
        collectionView2SecoandView.layoutIfNeeded()
        collectionView2SecoandView_h.constant = self.collectionView2SecoandView.contentSize.height
        
        // set info:
        if arr1TagSecoandView.count > 0 {
            var isManTagFound = false
            var isWomanTagFound = false
            
            for item in arr1TagSecoandView {
                //1. man tag found
                if item.title.lowercased() == "man"{
                    isManTagFound = true
                }
                // woman tag found
                if item.title.lowercased() == "woman"{
                    isWomanTagFound = true
                }
            }
            
            if isWomanTagFound && isManTagFound{ // if one of the tags has man, display man picture
                imgSecondBG.image = UIImage(named: "match_man_woman_img2")
            }else if isWomanTagFound{ // if one of the tags has woman, display woman picture
                imgSecondBG.image = UIImage(named: "match_woman_img2")
            }else if isManTagFound { // if both tags found man and woman, display man picture
                imgSecondBG.image = UIImage(named: "match_man_img2")
            }else { // if not found display default image
                imgSecondBG.image = UIImage(named: "match_man_woman_img2")
            }
        }else{// if non of the tags have, then display man picture
            imgSecondBG.image = UIImage(named: "match_man_woman_img2")
        }
        
        
        viewMainContainer.updateConstraints()
        viewMainContainer.layoutIfNeeded()
        let mainViewContainerHeight = viewMainContainer.frame.height
        let view1TagHeight = self.collectionView1SecoandView.contentSize.height
        let view2TagHeight = self.collectionView2SecoandView.contentSize.height
        
        //1. check have space to open all tags: Both DD"
        if (mainViewContainerHeight - (view2TagHeight) >= (view1TagHeight)) {
            // Both tags can open
            
            isOpenDDSecoandView1 = false
            isOpenDDSecoandView2 = false
            openDDSecondView1(isAnimationStatus: false)
            openDDSecondView2(isAnimationStatus: false)
            isSecondViewOPenBothDD_At_A_Time = true
            
        }else{
            
            // one can display
            isSecondViewOPenBothDD_At_A_Time = false// not display both
            // default open first
            isOpenDDSecoandView1 = false
            openDDSecondView1(isAnimationStatus: false)
            
            isOpenDDSecoandView2 = true
            openDDSecondView2(isAnimationStatus: false)
            
            // tag first DD:
            if (mainViewContainerHeight) - 150 >= collectionView1SecoandView_h.constant {
                collectionView1SecoandView_h.constant = view1TagHeight
            }else{
                collectionView1SecoandView_h.constant = mainViewContainerHeight - 150
            }
            
            // second tag DD:
            if (mainViewContainerHeight) - 150 >= collectionView2SecoandView_h.constant {
                collectionView2SecoandView_h.constant = view2TagHeight
            }else{
                collectionView2SecoandView_h.constant = mainViewContainerHeight - 150
            }
        }
    }
    
    func setThirldViewInfo(){
        
        let dictYourMatch = detailVM.dictData.object(forKey: "you") as! NSDictionary
        let arrYourMatch = dictYourMatch.object(forKey: "yourMatch") as! [Any]
        
        for item in arrYourMatch{
            arr1ThirldTagView.append((title: item as! String, isIconDone: true))
        }
        
        let arrYourNotmatch = dictYourMatch.object(forKey: "yourNotmatch") as! [Any]
        for item in arrYourNotmatch{
            arr1ThirldTagView.append((title: item as! String, isIconDone: false))
        }
        
        let arryourMissedmatch = dictYourMatch.object(forKey: "YourMissedmatch") as! [Any]
        for item in arryourMissedmatch{
            arr2ThirldView.append(item as! String)
        }
        
        collectionView1ThirldView.reloadData()
        collectionView1ThirldView.updateConstraints()
        collectionView1ThirldView.layoutIfNeeded()
        collectionView1ThirldView_h.constant = self.collectionView1ThirldView.contentSize.height
        
        collectionView2ThirldView.reloadData()
        collectionView2ThirldView.updateConstraints()
        collectionView2ThirldView.layoutIfNeeded()
        collectionView2ThirldView_h.constant = self.collectionView2ThirldView.contentSize.height
        
        
        // set background:---------------------
        
        if arr1ThirldTagView.count > 0 {
            var isManTagFound = false
            var isWomanTagFound = false
            
            for item in arr1ThirldTagView {
                //1. man tag found
                if item.title.lowercased() == "man"{
                    isManTagFound = true
                }
                // woman tag found
                if item.title.lowercased() == "woman"{
                    isWomanTagFound = true
                }
            }
            
            if isWomanTagFound && isManTagFound{ // if one of the tags has man, display man picture
                imgThirldBG.image = UIImage(named: "match_man_woman_img3")
            }else if isWomanTagFound{ // if one of the tags has woman, display woman picture
                imgThirldBG.image = UIImage(named: "match_woman_img3")
            }else if isManTagFound { // if both tags found man and woman, display man picture
                imgThirldBG.image = UIImage(named: "match_man_img3")
            }else { // if not found display default image
                imgThirldBG.image = UIImage(named: "match_man_woman_img3")
            }
            
        }else{// if non of the tags have, then display man picture
            imgThirldBG.image = UIImage(named: "match_man_woman_img3")
        }
        
        viewMainContainer.updateConstraints()
        viewMainContainer.layoutIfNeeded()
        let mainViewContainerHeight = viewMainContainer.frame.height
        let view1TagHeight = self.collectionView1ThirldView.contentSize.height
        let view2TagHeight = self.collectionView2ThirldView.contentSize.height
        
        //1. check have space to open all tags: Both DD"
        if (mainViewContainerHeight - (view2TagHeight) >= (view1TagHeight)) {
            // Both tags can open
            
            isOpenDDThirldView1 = false
            isOpenDDThirldView2 = false
            openDDThirldView1(isAnimationStatus: false)
            openDDThirldView2(isAnimationStatus: false)
            isThirldViewOPenBothDD_At_A_Time = true
            
        }else{
            
            // one can display
            isThirldViewOPenBothDD_At_A_Time = false// not display both
            // default open first
            isOpenDDThirldView1 = false
            openDDThirldView1(isAnimationStatus: false)
            
            isOpenDDThirldView2 = true
            openDDThirldView2(isAnimationStatus: false)
            
            // tag first DD:
            if (mainViewContainerHeight) - 150 >= collectionView1ThirldView_h.constant {
                collectionView1ThirldView_h.constant = view1TagHeight
            }else{
                collectionView1ThirldView_h.constant = mainViewContainerHeight - 150
            }
            
            // second tag DD:
            if (mainViewContainerHeight) - 150 >= collectionView2ThirldView_h.constant {
                collectionView2ThirldView_h.constant = view2TagHeight
            }else{
                collectionView2ThirldView_h.constant = mainViewContainerHeight - 150
            }
        }
    }
    
    //Fourth view:
    func setFourthViewInfo(){
        lblTitleFourthView.text = "And now we’re here! So, What next?"
    }
    
    func setAcceptInfoStatus(){
        
        if  let chat_indicator = detailVM.dictData.object(forKey: "chat_indicator") as? Int{
            if chat_indicator == 1 { // 1: accept, 0 disable
                
                viewLikeContainer.isHidden = false
                viewRejectContainer.isHidden = true
                
                btnAccept.isUserInteractionEnabled = false
                btnReject.isUserInteractionEnabled = false
                
            }else if chat_indicator == 0{
                
                viewLikeContainer.isHidden = true
                viewRejectContainer.isHidden = false
                
                btnAccept.isUserInteractionEnabled = false
                btnReject.isUserInteractionEnabled = false
            }
        }
    }
}


extension DetailVC : UIScrollViewDelegate {
    
    func updatePageNumber() {
        let currentPage = Int(ceil(scrollView.contentOffset.x / scrollView.frame.size.width))
        pageIndicator.currentPage = currentPage
        print("current page : \(currentPage)")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updatePageNumber()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        updatePageNumber()
    }
}


//MARK:-
//MARK:- CollectionView Delegate-

extension DetailVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // secoand view :
        if collectionView == collectionView1SecoandView {
            return arr1TagSecoandView.count
        }else if collectionView == collectionView2SecoandView {
            return arr2SecoandView.count
        }else
        
        // thirld view :
        if collectionView == collectionView1ThirldView {
            return arr1ThirldTagView.count
        }else if collectionView == collectionView2ThirldView {
            return arr2ThirldView.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "detailTagViewCell", for: indexPath) as! detailTagViewCell
        
        // secoand view :
        var title = ""
        var image  = UIImage()
        
        cell.lblTitle.textColor = .black
        
//        cell.lblTitle.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
        
        // secoand view :------------------------------------------------------------------------
        if collectionView == collectionView1SecoandView {
            title = arr1TagSecoandView[indexPath.row].title
            if arr1TagSecoandView[indexPath.row].isIconDone{
                image = #imageLiteral(resourceName: "doneIConnn")
            }else{
                image = #imageLiteral(resourceName: "cancelllIcon")
            }
            
            cell.imgStatus.isHidden = false
            
        }else if collectionView == collectionView2SecoandView {
            
            title = arr2SecoandView[indexPath.row]
            cell.imgStatus.isHidden = true
            
            
        }else
        
        //Thirld view :---------------------------------------------------------------
        if collectionView == collectionView1ThirldView {
            title = arr1ThirldTagView[indexPath.row].title
            if arr1ThirldTagView[indexPath.row].isIconDone{
                image = #imageLiteral(resourceName: "doneIConnn")
            }else{
                image = #imageLiteral(resourceName: "cancelllIcon")
            }
            
            cell.imgStatus.isHidden = false
            
        }else if collectionView == collectionView2ThirldView {
            
            title = arr2ThirldView[indexPath.row]
            cell.imgStatus.isHidden = true
            //image = #imageLiteral(resourceName: "cancelllIcon")
        }
        
        cell.lblTitle.text = title
        cell.imgStatus.image = image
        
        cell.viewInnerTag.addShadowToView()
       
        return cell
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView1SecoandView.collectionViewLayout.invalidateLayout()
        collectionView2SecoandView.collectionViewLayout.invalidateLayout()
        collectionView1ThirldView.collectionViewLayout.invalidateLayout()
        collectionView2ThirldView.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }
}


/*
 MARK:-
 MARK:- API
 */

extension DetailVC {
    
    func getMatchDetailData()
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        detailVM.getMatchDetailData(like_id: detailRequestInfo.like_id) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                print("\(self.detailVM.dictData)")
                self.initView()
            }
            else
            {
                if message == "No Data Available." || message == "No more data."
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
    
    
    func setAcceptMatch(like_id : Int,chat_indicator : Int)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        detailVM.setMatchAcceptStatus(like_id: like_id, chat_indicator: chat_indicator) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                print("\(self.detailVM.dictData)")
                
                if chat_indicator == 1 { // 1: accept, 0 disable
                    
                    self.viewLikeContainer.isHidden = false
                    self.viewRejectContainer.isHidden = true
                    
                    self.btnAccept.isUserInteractionEnabled = false
                    self.btnReject.isUserInteractionEnabled = false
                    
                }else if chat_indicator == 0{
                    
                    self.viewLikeContainer.isHidden = true
                    self.viewRejectContainer.isHidden = false
                    
                    self.btnAccept.isUserInteractionEnabled = false
                    self.btnReject.isUserInteractionEnabled = false
                }
            }
            else
            {
                if message == "No Data Available." || message == "No more data."
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
}


class FlowLayout: UICollectionViewFlowLayout {
    
    required init(minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        super.init()
        
        estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
        sectionInsetReference = .fromSafeArea
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributes = super.layoutAttributesForElements(in: rect)!.map { $0.copy() as! UICollectionViewLayoutAttributes }
        guard scrollDirection == .vertical else { return layoutAttributes }
        
        
        let cellAttributes = layoutAttributes.filter({ $0.representedElementCategory == .cell })
        
        
        for (_, attributes) in Dictionary(grouping: cellAttributes, by: { ($0.center.y / 10).rounded(.up) * 10 }) {
            
            let cellsTotalWidth = attributes.reduce(CGFloat(0)) { (partialWidth, attribute) -> CGFloat in
                partialWidth + attribute.size.width
            }
            
            
            let totalInset = collectionView!.safeAreaLayoutGuide.layoutFrame.width - cellsTotalWidth - sectionInset.left - sectionInset.right - minimumInteritemSpacing * CGFloat(attributes.count - 1)
            var leftInset = (totalInset / 2 * 10).rounded(.down) / 10 + sectionInset.left
            
            for attribute in attributes {
                attribute.frame.origin.x = leftInset
                leftInset = attribute.frame.maxX + minimumInteritemSpacing
            }
        }
        return layoutAttributes
    }
}
