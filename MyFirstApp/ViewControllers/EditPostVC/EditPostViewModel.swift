//
//  EditPostViewModel.swift
//  MyFirstApp
//
//  Created by cis on 12/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class EditPostViewModel: NSObject {
    
    func getEditInfo_api(post_id : Int ,completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.getPostDetails
        param.updateValue(post_id as AnyObject, forKey:"post_id")
        param.updateValue(userID as AnyObject, forKey:"user_id")
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                completion("success", data.message, dict)
            }
            else
            {
                completion("error", data.message, NSDictionary())
            }
        }
    }
    
    func setEditInfo_Api(requestParam : [String : AnyObject], completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        //param.updateValue(userId as AnyObject, forKey: KEY.UserDetails.ProfileId)
        let strService = APPURL.Urls.editPost
        param =  requestParam
        
        
        print("header \(header) \n API: \(strService) \n request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                }else { // error :
                    
                    if data.message == "" {
                        SnackBar.sharedInstance.show(message: "Something went wrong,Please try again.", showMsgAt: .bottom)
                        completion("error", "Something went wrong,Please try again.")
                    }else{
                        SnackBar.sharedInstance.show(message: data.message, showMsgAt: .bottom)
                        completion("error", data.message)
                    }
                }
            }
            else
            {
                if data.message == "" {
                    completion("error", "Something went wrong,Please try again.")
                }else{
                    completion("error", data.message)
                }
            }
        }
    }
}
