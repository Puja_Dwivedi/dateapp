//
//  EditPostVC.swift
//  MyFirstApp
//
//  Created by cis on 10/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class EditPostVC: UIViewController, UITextFieldDelegate {
    
    enum ActiveScreen {
        case  airplane
        case  subway
        case  place
        case  street
        case  bus
        case  train
        case none
    }
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var lblInfoTitleMyTraits: GradientLabel!
    
    @IBOutlet weak var lblInfoTitleMyMatches: GradientLabel!
    
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    @IBOutlet weak var myMatchTagTopSpace: NSLayoutConstraint!
    @IBOutlet weak var myTagsTagSpace: NSLayoutConstraint!
    
    //1. My attribtues:
    @IBOutlet weak var scrollViewMyAttibutes: UIScrollView!
    @IBOutlet weak var scrollViewMyAttibutes_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes: PinTextField!
    @IBOutlet weak var txtFldMyAttributes_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes_x: NSLayoutConstraint!
    var arrTagsMyAttributes = [(title : String, isSelected : Bool)]()
    
    //2.My Preferences:
    @IBOutlet weak var scrollViewMyPreferences: UIScrollView!
    @IBOutlet weak var scrollViewMyPreferences_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences: PinTextField!
    @IBOutlet weak var txtFldMyPreferences_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences_x: NSLayoutConstraint!
    @IBOutlet weak var imgDropDownMyPreferemce: UIImageView!
    var arrTagsMyPreferences = [(title : String, isSelected : Bool)]()
    
    @IBOutlet weak var viewOffTimePost: UIView!
    @IBOutlet weak var timePostLeftCons: NSLayoutConstraint!
    
    @IBOutlet weak var viewOffMatchCriteria: UIView!
    @IBOutlet weak var matchCriteriaLeftCons: NSLayoutConstraint!
    
    @IBOutlet weak var viewPublicIndicatorPost: UIView!
    @IBOutlet weak var publicIndicatorPostLeftCons: NSLayoutConstraint!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgDropDownMyTraits: UIImageView!
    
    @IBOutlet weak var includeTimeContainerView: UIView!
    @IBOutlet weak var addTimeContainerView: UIView!
    
    // Display picture:
    @IBOutlet weak var viewDisplayPictureIndicator: UIView!
    @IBOutlet weak var displayPictureIndicatorLeftCons: NSLayoutConstraint!
    
    @IBOutlet weak var timePostViewContainer: UIView!
    
    @IBOutlet weak var btnPublicSearch: UIButton!
    
    //MARK:-
    //MARK:- Variables :
    let editVM = EditPostViewModel()
    var activeScreen = ActiveScreen.none
    
    var isOpenMatchTraits = true
    var isOpenMyTraits = true
    // toggles:
    var timePostActiveStatus  = 0
    var matchCriteriaActiveStatus  = 0
    var viewHoursVM = ViewHoursViewModel() // for getting array of ids :
    
    var publicIndicatorActiveStatus  = 0
    var displayPictureIndicatorStatus = 0
    
    var post_id = 0
    var countryDialCode = 0
    var createPostTimeIntervalID : Int?
    
    var isChangeAnything = false
    
    // for popup:
    var notification_public = 0;
    var notification_private = 0;
    
    //MARK:-
    //MARK:- App flow:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFldMyAttributes.delegate = self
        txtFldMyPreferences.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        isChangeAnything = false
        arrTagsMyAttributes.removeAll()
        arrTagsMyPreferences.removeAll()
        initView()
    }
    
    //MARK:-
    //MARK:- Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionToggleTimePost(_ sender: UIButton) {
        
        isChangeAnything = true
        
        if timePostActiveStatus  == 1{ // on to off :
            timePostActiveStatus = 0
            
        }else{ // off to on :
            timePostActiveStatus = 1
        }
        setTimePostActiveStatusInfo()
    }
    
    
    @IBAction func actionToggleMatchCriteria(_ sender: UIButton) {
        isChangeAnything = true
        
        if timePostActiveStatus  == 1{ // on to off :
            timePostActiveStatus = 0
            
        }else{ // off to on :
            timePostActiveStatus = 1
        }
        matchCriteriaActiveStatuInfo()
    }
    
    @IBAction func actionToggleDisplayPicture(_ sender: UIButton) {
        isChangeAnything = true
        
        if displayPictureIndicatorStatus  == 1{ // on to off :
            displayPictureIndicatorStatus = 0
            
        }else{ // off to on :
            displayPictureIndicatorStatus = 1
        }
        displayIndicatorActiveStatuInfo()
    }
    
    
    // Drop down manage:
    
    @IBAction func actionOpenMyMatchTraits(_ sender: UIButton) {
        openDropDownMyMatchTraits(isAnimationEnable: true)
    }
    
    @IBAction func actionOpenMyTraits(_ sender: UIButton) {
        openDropDownMyTraits(isAnimationEnable: true)
    }
    
    @IBAction func actionTogglePublicIndicator(_ sender: UIButton) {
        isChangeAnything = true
        
        if publicIndicatorActiveStatus  == 1{ // on to off :
            publicIndicatorActiveStatus = 0
            
        }else{ // off to on :
            publicIndicatorActiveStatus = 1
        }
        publicIndicatorActiveStatuInfo(isDisableActive: false)
    }
    
    @IBAction func actionUpdate(_ sender: UIButton) {
        checkValidation()
    }
    
    func checkValidation(){
        
        guard isChangeAnything else {
        SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.informationAlreadyUpdated, showMsgAt: .bottom); return
        }
        
            var mySelectedTagAtribute : Bool = false
            for item in arrTagsMyAttributes {
                
                if item.isSelected {
                    mySelectedTagAtribute = true
                    break
                }
            }
            
            var mySelectedTagPreferences : Bool = false
            for item in arrTagsMyPreferences {
                
                if item.isSelected {
                    mySelectedTagPreferences = true
                    break
                }
            }
            
            guard mySelectedTagAtribute else {
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.pleaseSelectAtLeastOneTagInMyMatchTags, showMsgAt: .bottom)
                return
            }
            guard mySelectedTagPreferences else {
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.pleaseSelectAtLeastOneTagInMyTags, showMsgAt: .bottom)
                return
            }
        
        
        
        /*
         Condition for popup:
         */
        
        //1. If toggle Make post public/searchable is On and notification_public = 1:
        if publicIndicatorActiveStatus == 1 && notification_public == 1 {
            //Display popup message 1
            
            AlertWithCheckBoxView.Show(alertTitle: AlertMessages.shareInstance.yourPostIsPublic, btn1Title: "Continue", btn2Title: "Return to post") { [weak self] (result,doNotShowMsg) in

                if result == "Continue" {
                    
                    if doNotShowMsg{
                        self?.makeEditRequest( notification_private  : self?.notification_private ?? 1, notification_public: 0)
                    }else{
                        self?.makeEditRequest(notification_private  : self?.notification_private ?? 1, notification_public: self?.notification_public ?? 1)
                    }
                }
            }
            
        //If toggle Make post public/searchable is Off and notification_private = 1:
        }else if publicIndicatorActiveStatus == 0 && notification_private == 1{
           // Display popup message 2
            
            AlertWithCheckBoxView.Show(alertTitle: AlertMessages.shareInstance.yourPostIsPrivate, btn1Title: "Continue", btn2Title: "Return to post") { [weak self] (result,doNotShowMsg) in

                if result == "Continue" {
                    
                    if doNotShowMsg{
                        self?.makeEditRequest(notification_private  : 0, notification_public: self?.notification_public ?? 1)
                    }else{
                        self?.makeEditRequest( notification_private  : self?.notification_private ?? 1, notification_public: self?.notification_public ?? 1)
                    }
                }
            }
            // else condition
        }else{
            makeEditRequest(notification_private  : self.notification_private, notification_public: self.notification_public)
        }
            
    }
    
    
    @IBAction func actionOpenTime(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        if countryDialCode == 1{
            ViewHours.Show(hoursFormate: .twelve, previousSelectedTime: self.lblTime.text!) { [weak self](hours,id) in
                self?.lblTime.text =  hours
             
                self?.createPostTimeIntervalID = id
                self?.isChangeAnything = true
            }
            
        }else{
            
            ViewHours.Show(hoursFormate: .twentyFour, previousSelectedTime: self.lblTime.text! ) { [weak self](hours,id) in
                self?.lblTime.text =  hours
              
                self?.createPostTimeIntervalID = id
                self?.isChangeAnything = true
            }
        }
    }
    
    
    //MARK:-
    //MARK:- Methods
    
    func initView(){
        
        setTimePostActiveStatusInfo()
        matchCriteriaActiveStatuInfo()
        
        // handle drop down
        isOpenMyTraits = true // default is open
        isOpenMatchTraits = true // default is open
        
        lblInfoTitleMyTraits.gradientColors = [UIColor.MyTheme.FirstColor.blue.cgColor,UIColor.MyTheme.SecondColor.green.cgColor]
        lblInfoTitleMyMatches.gradientColors = [UIColor.MyTheme.FirstColor.blue.cgColor,UIColor.MyTheme.SecondColor.green.cgColor]
        
        getEditInfo_api()
    }
    
    func setTimePostActiveStatusInfo(){
        // set notification :
        if  timePostActiveStatus == 0 {
            UIView.animate(withDuration: 0.3) {
                self.timePostLeftCons.constant = 1
                self.viewOffTimePost.alpha = 1
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.timePostLeftCons.constant = 28
                self.viewOffTimePost.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func matchCriteriaActiveStatuInfo(){
        
        if  timePostActiveStatus == 0 {
            UIView.animate(withDuration: 0.3) {
                self.matchCriteriaLeftCons.constant = 1
                self.viewOffMatchCriteria.alpha = 1
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.matchCriteriaLeftCons.constant = 28
                self.viewOffMatchCriteria.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    func publicIndicatorActiveStatuInfo(isDisableActive : Bool){
        // set notification :
        
        if isDisableActive {
            btnPublicSearch.isUserInteractionEnabled = false
            if  publicIndicatorActiveStatus == 0 {
                UIView.animate(withDuration: 0.3) {
                    self.publicIndicatorPostLeftCons.constant = 1
                    self.viewPublicIndicatorPost.alpha = 1
                    self.view.layoutIfNeeded()
                }
                
            }else{
                UIView.animate(withDuration: 0.3) {
                    self.publicIndicatorPostLeftCons.constant = 28
                    self.viewPublicIndicatorPost.alpha = 1
                    self.view.layoutIfNeeded()
                }
            }
            
        }else{
            btnPublicSearch.isUserInteractionEnabled = true
            if  publicIndicatorActiveStatus == 0 {
                UIView.animate(withDuration: 0.3) {
                    self.publicIndicatorPostLeftCons.constant = 1
                    self.viewPublicIndicatorPost.alpha = 1
                    self.view.layoutIfNeeded()
                }
                
            }else{
                UIView.animate(withDuration: 0.3) {
                    self.publicIndicatorPostLeftCons.constant = 28
                    self.viewPublicIndicatorPost.alpha = 0
                    self.view.layoutIfNeeded()
                }
            }
            
        }
        
    }
    
    func displayIndicatorActiveStatuInfo(){
        // set notification :asdasd
        if  displayPictureIndicatorStatus == 0 {
            UIView.animate(withDuration: 0.3) {
                self.displayPictureIndicatorLeftCons.constant = 1
                self.viewDisplayPictureIndicator.alpha = 1
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.displayPictureIndicatorLeftCons.constant = 28
                self.viewDisplayPictureIndicator.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    func openDropDownMyTraits(isAnimationEnable : Bool){
        if isOpenMyTraits{ //open to off
            
            if isAnimationEnable {
                UIView.animate(withDuration: 0.5) {
                    
                    self.myTagsTagSpace.constant = 20
                    self.imgDropDownMyTraits.transform = CGAffineTransform(rotationAngle: .pi / 2)
                    self.view.layoutIfNeeded()
                }
            }else{
                self.myTagsTagSpace.constant = 20
                self.imgDropDownMyTraits.transform = CGAffineTransform(rotationAngle: .pi / 2)
                self.view.layoutIfNeeded()
            }
            
            isOpenMyTraits = false
        }else{ // close to open
            
            if isAnimationEnable {
                UIView.animate(withDuration: 0.5) {
                    
                    self.myTagsTagSpace.constant = -self.scrollViewMyAttibutes_h.constant - 20
                    self.imgDropDownMyTraits.transform = .identity
                    self.view.layoutIfNeeded()
                }
            }else{
                
                self.myTagsTagSpace.constant = -self.scrollViewMyAttibutes_h.constant - 20
                self.imgDropDownMyTraits.transform = .identity
                self.view.layoutIfNeeded()
            }
            isOpenMyTraits = true
        }
    }
    
    func openDropDownMyMatchTraits(isAnimationEnable : Bool){
        
        if isOpenMatchTraits{ //open to off
            
            if isAnimationEnable{
                UIView.animate(withDuration: 0.5) {
                    
                    self.myMatchTagTopSpace.constant = 20
                    
                    self.imgDropDownMyPreferemce.transform = CGAffineTransform(rotationAngle: .pi / 2)
                    self.view.layoutIfNeeded()
                }
                
            }else{
                self.myMatchTagTopSpace.constant = 20
                
                self.imgDropDownMyPreferemce.transform = CGAffineTransform(rotationAngle: .pi / 2)
                self.view.layoutIfNeeded()
            }
            
            isOpenMatchTraits = false
        }else{ // close to open
            if isAnimationEnable{
                UIView.animate(withDuration: 0.5) {
                    self.myMatchTagTopSpace.constant = -self.scrollViewMyPreferences_h.constant - 20
                    self.imgDropDownMyPreferemce.transform = .identity
                    self.view.layoutIfNeeded()
                }
            }else{
                self.myMatchTagTopSpace.constant = -self.scrollViewMyPreferences_h.constant - 20
                self.imgDropDownMyPreferemce.transform = .identity
                self.view.layoutIfNeeded()
            }
            
            isOpenMatchTraits = true
        }
    }
    
    func makeEditRequest(notification_private  : Int, notification_public: Int){
        
        isChangeAnything = false
        
        // create request :
        var param = [String : AnyObject]()
        
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        param.updateValue(userID as AnyObject, forKey:"user_id")
        param.updateValue(post_id as AnyObject, forKey:"post_id")
        //Popup
        param.updateValue( notification_private as AnyObject, forKey:"notification_private")
        param.updateValue( notification_public as AnyObject, forKey:"notification_public")
        
        var strUser_attribute_fav = ""
        var min_user_attributeCount = 0
        
        for item in arrTagsMyAttributes {
            
            if item.isSelected {
                if strUser_attribute_fav == "" {
                    strUser_attribute_fav = item.title
                }else{
                    strUser_attribute_fav = strUser_attribute_fav + "," + item.title
                }
                
                min_user_attributeCount = min_user_attributeCount + 1
            }
        }
        
        param.updateValue(strUser_attribute_fav as AnyObject, forKey:"user_attribute_fav")
        param.updateValue(min_user_attributeCount as AnyObject, forKey:"min_user_attribute")
        
        var strPartner_attribute_fav = ""
        var min_partner_attributeCount = 0
        for item in arrTagsMyPreferences {
            
            if item.isSelected {
                if strPartner_attribute_fav == "" {
                    strPartner_attribute_fav = item.title
                }else{
                    strPartner_attribute_fav = strPartner_attribute_fav + "," + item.title
                }
                min_partner_attributeCount = min_partner_attributeCount + 1
            }
        }
        param.updateValue(strPartner_attribute_fav as AnyObject, forKey:"partner_attribute_fav")
        param.updateValue(min_partner_attributeCount as AnyObject, forKey:"min_partner_attribute")
        
        
        param.updateValue(publicIndicatorActiveStatus as AnyObject, forKey:"public_indicator")
        param.updateValue(timePostActiveStatus as AnyObject, forKey:"time_display")
        
        //if activeScreen == ActiveScreen.airplane {
        
        if timePostActiveStatus == 0 {
            param.updateValue("0" as AnyObject, forKey:"time_required")
        }else{
            param.updateValue("1" as AnyObject, forKey:"time_required")
        }
        
        //new param:
        param.updateValue(createPostTimeIntervalID as AnyObject ,forKey: "time_interval")
        param.updateValue(displayPictureIndicatorStatus as AnyObject ,forKey: "picture_display")
        print("param : \(param)")
        
        setEditInfo_api(param: param)
    }
    
    func getTimeIntervalValue(id : Int)-> (strTime : String, id : Int){
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        guard id != 0 else { return (strTime : "----", id : 0)}
        
        let index : Int =  viewHoursVM.arrIDs.firstIndex(where: {$0.id == id})!
        
        if countryDialCode == 1{ // for us date formate : 12 hours
            return (strTime : viewHoursVM.arrIDs[index].timeOfTwelve, id : viewHoursVM.arrIDs[index].id)
        }else{
            return (strTime : viewHoursVM.arrIDs[index].timeOfTwentyFour, id : viewHoursVM.arrIDs[index].id)
        }
    }
    
    func convertDateFormaterForUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.string(from: date!)
    }
    
    func convertDateFormaterForNonUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    func setInfo(dict : NSDictionary){
        
        let item = dict.object(forKey: "data") as! NSDictionary
        let title = "\(item.object(forKey: "description") ?? "")"
        let event_type =  (item.object(forKey: "event_subtype") ?? 0) as! Int
        let strUser_attribute_fav = "\(item.object(forKey: "user_attribute_fav") ?? "")"
        var arrUser_attribute_fav = strUser_attribute_fav.components(separatedBy: ",")
        
        let strPartner_attribute_fav = "\(item.object(forKey: "partner_attribute_fav") ?? "")"
        var arrPartner_attribute_fav = strPartner_attribute_fav.components(separatedBy: ",")
        
        let time_interval = item.object(forKey: "time_interval") as! Int
        timePostActiveStatus  = item.object(forKey: "time_display") as! Int
        matchCriteriaActiveStatus  = item.object(forKey: "time_required") as! Int
        publicIndicatorActiveStatus = item.object(forKey: "public_indicator") as! Int
        displayPictureIndicatorStatus = item.object(forKey: "picture_display") as! Int
        let country = "\(item.object(forKey: "country_name")  ?? "")"
        
        
        // for notification popups.
        if let publicValue = item.object(forKey: "notification_public") as? Int {
            notification_public = publicValue
        }
        
        if let privateValue = item.object(forKey: "notification_private") as? Int {
        notification_private = privateValue
        }
        
        setTimePostActiveStatusInfo()
        matchCriteriaActiveStatuInfo()
        
        displayIndicatorActiveStatuInfo()
        
        
        if event_type == 2 ||  event_type == 5 {
            if country.count > 0 {
                publicIndicatorActiveStatuInfo(isDisableActive: false)
            }else{
                publicIndicatorActiveStatuInfo(isDisableActive: true)
            }
        }else{
            publicIndicatorActiveStatuInfo(isDisableActive: false)
        }
        
        
        let time  =  getTimeIntervalValue(id: time_interval)
        createPostTimeIntervalID = time.id
        lblTime.text = time.strTime
        
        lblTitle.text =  "\(title)"
        lblSubtitle.isHidden = true
        
        switch event_type {
        case 1: //airplane
            imgType.image = #imageLiteral(resourceName: "airplaneIcon")
            activeScreen = ActiveScreen.airplane
            
            includeTimeContainerView.isHidden = true
            break;
            
        case 2: //subway
            imgType.image = #imageLiteral(resourceName: "train 1")
            activeScreen = ActiveScreen.subway
            
            includeTimeContainerView.isHidden = true
            break;
            
        case 3: //place
            imgType.image = #imageLiteral(resourceName: "food-stall 1")
            activeScreen = ActiveScreen.place
            
            includeTimeContainerView.isHidden = true
            break;
            
        case 4: //street
            imgType.image = #imageLiteral(resourceName: "map 1")
            activeScreen = ActiveScreen.street
            
            includeTimeContainerView.isHidden = true
            
            break;
            
        case 5: //bus
            imgType.image = #imageLiteral(resourceName: "bus-stop 1")
            activeScreen = ActiveScreen.bus
            
            includeTimeContainerView.isHidden = true
            break;
            
        case 6: //t0rain
            imgType.image = #imageLiteral(resourceName: "train (1) 2")
            activeScreen = ActiveScreen.none
            
            includeTimeContainerView.isHidden = true
            break;
            
        default:
            break
        }
        
        // my traits:
        for index in 1..<13 {
            let key = "user_attribute_\(index)"
            if "\(item.object(forKey: key) as! String)" != "" {
                let value = "\(item.object(forKey: key) as! String)"
            
                if let index = arrUser_attribute_fav.firstIndex(of:value) {
                    arrUser_attribute_fav.remove(at: index)
                    arrTagsMyAttributes.append((title: value, isSelected: true))
                }else{
                    arrTagsMyAttributes.append((title: value, isSelected: false))
                }
            }
        }
        
        // my matches traits :
        for index in 1...12 {
            let key = "partner_attribute_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                let value1 = "\(item.object(forKey: key) as! String)"
                if let index1 = arrPartner_attribute_fav.firstIndex(of:value1){
                    arrPartner_attribute_fav.remove(at: index1)
                    arrTagsMyPreferences.append((title: value1, isSelected: true))
                }else{
                    arrTagsMyPreferences.append((title: value1, isSelected: false))
                }
            }
        }
        
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: false)
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: false)
        
        openDropDownMyTraits(isAnimationEnable: false)
        openDropDownMyMatchTraits(isAnimationEnable: false)
    }
    
    func getTimeStatus() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en-US")
        formatter.dateFormat = "a"
        let time12 = formatter.string(from: date)
        return time12
    }
    
    
    func getTimeInterval()-> String{
      //  let calendar = Calendar.current
        let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        
        if countryDialCode == 1{ // for us date formate : 12 hours
            
//            // 1. Choose a date
//            let today = Date()
//            // 2. Pick the date components
//            var hours   = (Calendar.current.component(.hour, from: today))
//            let minutes = (Calendar.current.component(.minute, from: today))
//           // let seconds = (Calendar.current.component(.second, from: today))
//
//          //  hours = 24 // testing purpose.
//
//            var time = ""
//            if hours > 12 {
//                hours = hours - 12
//                time = "PM"
//            }else{
//                time = "AM"
//            }
//
//            if 0 <= minutes && 15 >= minutes {
//                return "\(String(format: "%02d", hours)):00-15 \(time)"
//            }else if 15 <= minutes && 30 >= minutes{
//                return "\(String(format: "%02d", hours)):15-30 \(time)"
//            }else if  30 <= minutes && 45 >= minutes{
//                return "\(String(format: "%02d", hours)):30-45 \(time)"
//            }else if  45 <= minutes {
//                return "\(String(format: "%02d", hours)):45-00 \(time)"
//            }
              let calendar = Calendar.current
            let date = Date()
            let formatter = DateFormatter()
            
            formatter.dateStyle = .none
            formatter.timeStyle = .short
            formatter.locale = Locale(identifier: "en_US_POSIX")
            
            formatter.dateFormat = "hh a" // "a" prints "pm" or "am"
            let hourString = formatter.string(from:date) // "12 AM"
            let minutes = calendar.component(.minute, from: date)
            let hours = hourString.components(separatedBy: " ")

            var minValue = ""
            if hours.count > 1 {
                minValue = hours[1]
            } else {
                minValue = getTimeStatus()
            }

            if 0 <= minutes && 15 >= minutes {
                return "\(hours[0]):00-15 \(minValue)"
            }else if 15 <= minutes && 30 >= minutes{
                return "\(hours[0]):15-30 \(minValue)"
            }else if  30 <= minutes && 45 >= minutes{
                return "\(hours[0]):30-45 \(minValue)"
            }else if  45 <= minutes {
                return "\(hours[0]):45-00 \(minValue)"
            }
            
        }else{// for non US : date formate 24 hours :
            let date = Date()
            
            let hour = Calendar.current.component(.hour, from: date)
            let minutes = Calendar.current.component(.minute, from: date)
            
            if 0 <= minutes && 15 >= minutes {
                return "\(String(format: "%02d", hour)):00-15"
            }else if 15 <= minutes && 30 >= minutes{
                return "\(String(format: "%02d", hour)):15-30"
            }else if  30 <= minutes && 45 >= minutes{
                return "\(String(format: "%02d", hour)):30-45"
            }else if  45 <= minutes {
                return "\(String(format: "%02d", hour)):45-00"
            }
        }
        return ""
    }
}


//My Attributes support methods : for Tag with textfield:
extension EditPostVC {
    
    func createTagsForAttributes(OnView view: UIView, withArray data:[(title : String, isSelected : Bool)], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let spaceHeight : CGFloat = 16
        let height = 30
        let screenWidth = UIScreen.main.bounds.size.width - 90.0
        var boderWidth = 1
        var favTagCount = 0
        
        for str in data  {
            let startstring = str.title
            let width = startstring.widthOfString(usingFont: UIFont(name:"IBM Plex Sans SemiBold", size: 15.0)!)
            
            // edit :
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            if checkWholeWidth > screenWidth {
                //we are exceeding size need to change xpos
                xPos = 5.0
                ypos = ypos + spaceHeight + 8.0 + 11 // here 11 is the buttom space
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 35.0 , height: CGFloat(height)))
            bgView.cornerRadius = 14.5
            bgView.tag = tag
            bgView.backgroundColor =  .white
            
            if str.isSelected { // for selected border : 3 , normal : 1
                boderWidth = 3
                favTagCount = favTagCount + 1
                
            }else{
                boderWidth = 1
            }
            
            bgView.gradientBorder(width: CGFloat(boderWidth), colors: [UIColor.MyTheme.FirstColor.blue,UIColor.MyTheme.SecondColor.green], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 17.0, y: -2, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "IBM Plex Sans SemiBold", size: 15.0)
            textlable.text = startstring
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.blue.cgColor,UIColor.MyTheme.SecondColor.green.cgColor]
            bgView.addSubview(textlable)
            
            let btnClickAttribute = UIButton()
            btnClickAttribute.frame = CGRect(x: 0, y: 0, width: width + space + 35.0, height: CGFloat(height))
            btnClickAttribute.backgroundColor = .clear
            btnClickAttribute.titleLabel?.text = ""
            btnClickAttribute.tag = tag - 1
            btnClickAttribute.addTarget(self, action: #selector(clickTagMyAttibutes(_:)), for: .touchUpInside)
            bgView.addSubview(btnClickAttribute)
            
            //when close buttton enable
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 23.0, height: 23.0)
                button.backgroundColor = UIColor.clear
                // button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
                button.setImage(#imageLiteral(resourceName: "closeTag"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagMyAttibutes(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldMyAttributes.isHidden = false
            txtFldMyAttributes_y.constant = ypos
            txtFldMyAttributes_x.constant = xPos
            
        }else{
            txtFldMyAttributes.isHidden = true
        }
        
        if arrTagsMyAttributes.count <= 20 {
            self.txtFldMyAttributes.alpha = 1
        }else{
            self.txtFldMyAttributes.alpha = 0
        }
        
        lblInfoTitleMyTraits.text = "Match post with others on these tags ( \(favTagCount)/\(arrTagsMyAttributes.count))"
        scrollViewMyAttibutes_h.constant = ypos + 34//Here 50 is the height of the txtview, and 10 is the bottom space of the scrollview
    }
    
    
    @objc func removeTagMyAttibutes(_ sender: AnyObject) {
        arrTagsMyAttributes.remove(at: (sender.tag - 1))
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: false)
    }
    
    @objc func clickTagMyAttibutes(_ sender: AnyObject) {
        isChangeAnything = true
        if arrTagsMyAttributes[sender.tag].isSelected {
            arrTagsMyAttributes[sender.tag].isSelected = false
        }else{
            arrTagsMyAttributes[sender.tag].isSelected = true
        }
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: false)
    }
}

//My Preerences support methods :
extension EditPostVC {
    
    func createTagsForPreferences(OnView view: UIView, withArray data:[(title : String, isSelected : Bool)], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let height = 30
        let spaceHeight : CGFloat = 16
        let screenWidth = UIScreen.main.bounds.size.width - 90.0
        var boderWidth = 1
        var favTagCount = 0
        
        for str in data  {
            let startstring = str.title
            let width = startstring.widthOfString(usingFont: UIFont(name:"IBM Plex Sans SemiBold", size: 15.0)!)
            // edit :
            
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            if checkWholeWidth > screenWidth {
                //we are exceeding size need to change xpos
                xPos = 0.0
                ypos = ypos + spaceHeight + 8.0 + 11
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 35.0 , height: CGFloat(height)))
            bgView.layer.cornerRadius = 14.5
            bgView.tag = tag
            bgView.backgroundColor = .white
            
            if str.isSelected { // for selected border : 3 , normal : 1
                boderWidth = 3
                favTagCount = favTagCount + 1
                
            }else{
                boderWidth = 1
            }
            
            bgView.gradientBorder(width: CGFloat(boderWidth), colors: [UIColor.MyTheme.FirstColor.blue,UIColor.MyTheme.SecondColor.green], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 17.0, y: -2, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "IBM Plex Sans SemiBold", size: 15.0)
            textlable.text = startstring
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.blue.cgColor,UIColor.MyTheme.SecondColor.green.cgColor]
            bgView.addSubview(textlable)
            
            let btnClickAttribute = UIButton()
            btnClickAttribute.frame = CGRect(x: 0, y: 0, width: width + space + 35.0, height: CGFloat(height))
            btnClickAttribute.backgroundColor = .clear
            btnClickAttribute.titleLabel?.text = ""
            btnClickAttribute.tag = tag - 1
            btnClickAttribute.addTarget(self, action: #selector(clickTagPreferences(_:)), for: .touchUpInside)
            bgView.addSubview(btnClickAttribute)
            
            //when close buttton enable
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                //
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 23.0, height: 23.0)
                button.backgroundColor = UIColor.clear
                button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
                
                button.setImage(#imageLiteral(resourceName: "closeTag"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagPreferences(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldMyPreferences.isHidden = false
            txtFldMyPreferences_y.constant = ypos
            txtFldMyPreferences_x.constant = xPos
            
        }else{
            txtFldMyPreferences.isHidden = true
        }
        
        if arrTagsMyPreferences.count <= 20 {
            self.txtFldMyPreferences.alpha = 1
        }else{
            self.txtFldMyPreferences.alpha = 0
        }
        
        lblInfoTitleMyMatches.text = "Match post with others on these tags (\(favTagCount)/\(arrTagsMyPreferences.count))"
        scrollViewMyPreferences_h.constant = ypos + 34  //Here 50 is the height of the txtview, and 10 is the bottom space of the scrollview
    }
    
    @objc func removeTagPreferences(_ sender: AnyObject) {
        arrTagsMyPreferences.remove(at: (sender.tag - 1))
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: false)
    }
    
    @objc func clickTagPreferences(_ sender: AnyObject) {
        isChangeAnything = true
        if arrTagsMyPreferences[sender.tag].isSelected {
            arrTagsMyPreferences[sender.tag].isSelected = false
        }else{
            arrTagsMyPreferences[sender.tag].isSelected = true
        }
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: false)
    }
}

/*
 MARK:- API's
 */
extension EditPostVC {
    
    // get profile info:
    func getEditInfo_api()
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        editVM.getEditInfo_api(post_id: post_id) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.setInfo(dict: dictData)
            }else{
            }
        }
    }
    
    // set info: time_display : req.body.time_display ,
    func setEditInfo_api(param : [String : AnyObject])
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        editVM.setEditInfo_Api(requestParam: param) { (status, message)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.navigationController?.popViewController(animated: true)
                //successSnackBar.sharedInstance.show(message: message)
            }else{
            }
        }
    }
}


class ssCustomSlider: UISlider {
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let point = CGPoint(x: bounds.minX, y: bounds.midY)
        return CGRect(origin: point, size: CGSize(width: bounds.width, height: 12))
    }
}

