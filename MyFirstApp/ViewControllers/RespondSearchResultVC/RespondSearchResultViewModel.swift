//
//  RespondSearchResultViewModel.swift
//  MyFirstApp
//
//  Created by cis on 06/03/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class RespondSearchResultViewModel: NSObject {
    
    func callSendingNote(requestParam : [String : AnyObject], completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.sendingNote
        param =  requestParam
        
        print("header \(header) \n API: \(strService) \n request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("success", message)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                    completion("error", message)
                    
                }else { // error :
                    
                    if data.message == "" {
                        message = "Something went wrong,Please try again."
                    }else{
                        message = data.message
                    }
                    completion("error", message)
                }
            }
            else
            {
                if data.message == "" {
                    message = "Something went wrong,Please try again."
                }else{
                    message = data.message
                }
                
                completion("error", message)
            }
        }
    }
}
