//
//  RespondSearchResultVC.swift
//  MyFirstApp
//
//  Created by cis on 26/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class RespondSearchResultVC: UIViewController {
    
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var txtSendNote: UITextView!
    
    @IBOutlet var viewTopHeader: UIViewClass!
    //1. My attribtues:
    @IBOutlet weak var scrollViewMyAttibutes: UIScrollView!
    @IBOutlet weak var scrollViewMyAttibutes_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes: PinTextField!
    @IBOutlet weak var txtFldMyAttributes_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyAttributes_x: NSLayoutConstraint!
    var arrTagsMyAttributes = [String]()
    var chatInfo = (otherUserId: 0 , name : "" , profilePicture : "")
    @IBOutlet weak var btnReport: UIButton!
    
    //2.My Preferences:
    
    @IBOutlet weak var scrollViewMyPreferences: UIScrollView!
    @IBOutlet weak var scrollViewMyPreferences_h: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences: PinTextField!
    @IBOutlet weak var txtFldMyPreferences_y: NSLayoutConstraint!
    @IBOutlet weak var txtFldMyPreferences_x: NSLayoutConstraint!
    
    @IBOutlet weak var txtTraits_width: NSLayoutConstraint!
    @IBOutlet weak var txtMatch_width: NSLayoutConstraint!
    
    var arrTagsMyPreferences = [String]()
    var chatObj = Class_StageTwo()
    var matchesVM = MatchesViewModel()
    
    var event_id = ""
    var to_post_id = ""
    var to_user_id = ""
    
    let responseVM =  RespondSearchResultViewModel()
    var dictResult = NSDictionary()
    var countryDialCode = 0
    
    
    //MARK:-
    //MARK:- App flow:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFldMyAttributes.delegate = self
        txtFldMyPreferences.delegate = self
//        viewTopHeader.addShadowToView()
        viewTopHeader.cornerRadius = viewTopHeader.bounds.height/2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
        arrTagsMyAttributes.removeAll()
        arrTagsMyPreferences.removeAll()
        initView()
    }
    
    func convertDateFormaterForUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    
    func convertDateFormaterForNonUS(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    //MARK:-
    //MARK:- Action Methods :
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSend(_ sender: UIButton) {
        self.view.endEditing(true)
        checkValidation()
    }
    
    @IBAction func actionReport(_ sender: UIButton) {
       
        ReportReasonView.sharedInstance.Show { [weak self](strReason) in
            // print("reason report : \(strReason)")
            self?.callReport(to_user: self!.to_user_id, report_reason: strReason)
        }
    }
    
    func checkValidation(){
        let strSendNote = txtSendNote.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard strSendNote != "" else { SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.PleaseWriteSomething, showMsgAt: .bottom); return}
        
        call_sending_note(msg: strSendNote)
    }
    
    
    
    //MARK:-
    //MARK:- Methods
    
    func initView(){
        setInfo(dict : dictResult)
    }
    
    
    func setInfo(dict : NSDictionary){
        
        let attributedString = NSAttributedString(string: NSLocalizedString("Report", comment: ""), attributes:[
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0),
            NSAttributedString.Key.foregroundColor : UIColor.gray,
            NSAttributedString.Key.underlineStyle:1.0
        ])
        
        btnReport.setAttributedTitle(attributedString, for: .normal)
        
        let item = dict
        let title  = "\(item.object(forKey: "description") ?? "")"
        let event_type =  (item.object(forKey: "event_subtype") ?? 0) as! Int
        let attribute  = "\(item.object(forKey: "attribute") ?? "")"
        let city = "\(item.object(forKey: "city") ?? "")"
        let strDate  = "\(item.object(forKey: "date") ?? "")"
        
        if let publicIndicator = item.object(forKey: "public_indicator") as? Int {
            if publicIndicator == 0 {
                // "Private post"
                btnReport.isHidden = true
            }else{
               //"Public post"
                btnReport.isHidden = false
            }
        }else{
            //"Public post"
             btnReport.isHidden = false
        }
        
        var date = ""
        if countryDialCode == 1 {  // for US : 12 hours time foemate
            date = convertDateFormaterForUS(strDate)
        }else{
            date = convertDateFormaterForNonUS(strDate)
        }
        
        
        lblTitle.text =  "\(title)"
        switch event_type {
        case 1: //airplane
            imgType.image = UIImage(named: "plane")
            
            var flightTitle = ""
            if  attribute.count > 0 {
                flightTitle = "\(attribute)"
            }
            
            if city.count > 0 {
                if flightTitle.count > 0 {
                    flightTitle = "\(flightTitle). \(city)"
                }else{
                    flightTitle = "\(city)"
                }
            }
            
            if date.count > 0 {
                if flightTitle.count > 0 {
                    flightTitle =  "\(flightTitle). \(date)"
                }else{
                    flightTitle =   "\(date)"
                }
            }
            
            lblSubtitle.text = flightTitle
            break;
            
        case 2: //subway
            imgType.image = UIImage(named: "subway")
            
            var sybwayTitle = ""
            if attribute.count > 0 {
                sybwayTitle =  "\(attribute)"
            }
            
            if date.count > 0 {
                if sybwayTitle.count > 0 {
                    sybwayTitle = "\(attribute). \(date)"
                }else{
                    sybwayTitle = "\(date)"
                }
            }
            
            lblSubtitle.text = sybwayTitle
            break;
            
        case 3: //place
            imgType.image = UIImage(named: "place")
            
            var placeTitle = ""
            if "\(item.object(forKey: "main_text") ?? "")".count > 0 {
                placeTitle = "\(item.object(forKey: "main_text") ?? "")"
            }
            
            if "\(dict.object(forKey: "formatted_address") ?? "")".count > 0 {
                if placeTitle.count > 0 {
                    placeTitle = "\(placeTitle), \(dict.object(forKey: "formatted_address") ?? "")"
                }else{
                    placeTitle = "\(dict.object(forKey: "formatted_address") ?? "")"
                }
            }
            
            
            if date.count > 0 {
                if placeTitle.count > 0 {
                    placeTitle = "\(placeTitle). \(date)"
                }else{
                    placeTitle = "\(date)"
                }
            }
            lblSubtitle.text = placeTitle
            
            break;
            
        case 4: //street
            imgType.image = UIImage(named: "street")
            
            var streetTitle = ""
            if "\(item.object(forKey: "main_text") ?? "")".count > 0 {
                streetTitle = "\(item.object(forKey: "main_text") ?? "")"
            }
            
            if "\(dict.object(forKey: "secondary_text") ?? "")".count > 0 {
                if streetTitle.count > 0 {
                    streetTitle = "\(streetTitle), \(dict.object(forKey: "secondary_text") ?? "")"
                }else{
                    streetTitle = "\(dict.object(forKey: "secondary_text") ?? "")"
                }
            }
            
            
            if date.count > 0 {
                if streetTitle.count > 0 {
                    streetTitle = "\(streetTitle). \(date)"
                }else{
                    streetTitle = "\(date)"
                }
            }
            lblSubtitle.text = streetTitle
            
            break;
            
        case 5: //bus
            imgType.image = UIImage(named: "bus")
            
            var busTitle = ""
            if  attribute.count > 0 {
                busTitle = "\(attribute)"
            }
            
            
            if date.count > 0 {
                if busTitle.count > 0 {
                    busTitle =  "\(busTitle). \(date)"
                }else{
                    busTitle =   "\(date)"
                }
            }
            
            lblSubtitle.text = busTitle
            break;
            
        case 6: //train
            imgType.image = #imageLiteral(resourceName: "train (1) 2")
            
            var trainTitle = ""
            if  attribute.count > 0 {
                trainTitle = "\(attribute)"
            }
            
            if city.count > 0 {
                if trainTitle.count > 0 {
                    trainTitle = "\(trainTitle). \(city)"
                }else{
                    trainTitle = "\(city)"
                }
            }
            
            if date.count > 0 {
                if trainTitle.count > 0 {
                    trainTitle =  "\(trainTitle). \(date)"
                }else{
                    trainTitle =   "\(date)"
                }
            }
            
            lblSubtitle.text = trainTitle
            break;
        default:
            break
        }
        
        
        // my traits:
        for index in 1..<13 {
            let key = "user_attribute_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                arrTagsMyAttributes.append("\(item.object(forKey: key) as! String)")
            }
        }
        
        // my matches traits :
        for index in 1...12 {
            let key = "partner_attribute_\(index)"
            
            if "\(item.object(forKey: key) as! String)" != "" {
                arrTagsMyPreferences.append("\(item.object(forKey: key) as! String)")
            }
        }
        
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: false)
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: false)
    }
}


//My Attributes support methods : for Tag with textfield:
extension RespondSearchResultVC {
    
    func createTagsForAttributes(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let height = 30
        let spaceHeight : CGFloat = 20
        let screenWidth = UIScreen.main.bounds.size.width - 60.0
        
        for str in data  {
            
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name: "Poppins-Regular", size: 14.0)!)
            
            // edit :
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0)
            }
            
            if checkWholeWidth > screenWidth {
                xPos = 5.0
                ypos = ypos + spaceHeight + 8.0 + 11 // here 11 is the buttom space
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 38.5 , height: CGFloat(height)))
            bgView.backgroundColor = .clear
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 14.0, y: -0.5, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "Poppins-Regular", size: 14.0)
            textlable.text = startstring
            
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
            bgView.addSubview(textlable)
            
            //when close buttton enable
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 28.0, height: 28.0)
                button.backgroundColor = UIColor.clear
                button.setImage(#imageLiteral(resourceName: "cross_icon"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagMyAttibutes(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            tag = tag  + 1
        }
        
        
        if isCancelButtonVisible{
            txtFldMyAttributes.isHidden = false
            
            if  arrTagsMyAttributes.count == 20 {
                txtFldMyAttributes_y.constant = ypos
                txtFldMyAttributes_x.constant = xPos
                
            }else{
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldMyAttributes_y.constant = ypos
                    txtFldMyAttributes_x.constant = xPos
                }else{
                    txtFldMyAttributes_y.constant = ypos
                    txtFldMyAttributes_x.constant = xPos
                }
            }
            txtTraits_width.constant = screenWidth - xPos + 50// here 30 is padding....
        }else{
            txtFldMyAttributes.isHidden = true
        }
        
        if arrTagsMyAttributes.count < 20 {
            self.txtFldMyAttributes.alpha = 1
            scrollViewMyAttibutes_h.constant = ypos + 34 + 10//Here 50 is the height of the txtview, and 10 is the bottom space of
            
            if arrTagsMyAttributes.count == 20 {
                self.txtFldMyAttributes.alpha = 0
                self.txtFldMyAttributes.resignFirstResponder()
                self.view.endEditing(true)
            }
            
            // the scrollview
        }else{
            self.txtFldMyAttributes.alpha = 0
            self.txtFldMyAttributes.resignFirstResponder()
            self.view.endEditing(true)
            scrollViewMyAttibutes_h.constant = ypos//Here 50 is the height of the txtview, and 10 is the
        }
    }
    
    @objc func removeTagMyAttibutes(_ sender: AnyObject) {
        
        arrTagsMyAttributes.remove(at: (sender.tag - 1))
        createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: false)
    }
}

//My Preerences support methods :
extension RespondSearchResultVC {
    
    func createTagsForPreferences(OnView view: UIView, withArray data:[String], isCancelButtonVisible : Bool) {
        
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 0.0
        var ypos: CGFloat = 5
        var tag: Int = 1
        let height = 30
        let spaceHeight : CGFloat = 20
        let screenWidth = UIScreen.main.bounds.size.width - 60.0
        
        for str in data  {
            
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name: "Poppins-Regular", size: 14.0)!)
            // edit :
            var checkWholeWidth : CGFloat = 0.0
            var space : CGFloat = 0.0
            if isCancelButtonVisible{
                space = 17.0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }else{
                space = 0
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) //13.0 is the width between lable and cross button and and gap to righht
            }
            
            // let checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            
            if checkWholeWidth > screenWidth{
                //we are exceeding size need to change xpos
                xPos = 0.0
                ypos = ypos + spaceHeight + 8.0 + 11
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + space + 38.5 , height: CGFloat(height)))
            //  bgView.layer.cornerRadius = 14.5
            bgView.backgroundColor = .clear
            bgView.tag = tag
            bgView.gradientBorder(width: 1, colors: [UIColor.MyTheme.FirstColor.black,UIColor.MyTheme.SecondColor.black], corners: [.topLeft ,.topRight ,.bottomLeft,.bottomRight], startPoint: .unitCoordinate(.top), endPoint: .unitCoordinate(.bottom), andRoundCornersWithRadius: 14.5)
            
            let textlable = GradientLabel(frame: CGRect(x: 14.0, y: -0.5, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "Poppins-Regular", size: 14.0)
            textlable.text = startstring
            
            textlable.gradientColors = [UIColor.MyTheme.FirstColor.black.cgColor,UIColor.MyTheme.SecondColor.black.cgColor]
            bgView.addSubview(textlable)
            
            //when close buttton enable
            if isCancelButtonVisible{
                let button = UIButton(type: .custom)
                //
                button.frame = CGRect(x: bgView.frame.size.width - 10.0 - 23.0, y: CGFloat(height/2) - 11.5, width: 28.0, height: 28.0)
                button.backgroundColor = UIColor.clear
                button.layer.cornerRadius = CGFloat(button.frame.size.width)/CGFloat(2.0)
                
                button.setImage(#imageLiteral(resourceName: "cross_icon"), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
                button.tag = tag
                button.addTarget(self, action: #selector(removeTagPreferences(_:)), for: .touchUpInside)
                bgView.addSubview(button)
            }
            
            xPos = CGFloat(xPos) + CGFloat(width) + space + CGFloat(43.0)
            view.addSubview(bgView)
            
            tag = tag  + 1
        }
        
        if isCancelButtonVisible{
            txtFldMyPreferences.isHidden = false
            
            if arrTagsMyPreferences.count == 20 {
                txtFldMyPreferences_y.constant = ypos
                txtFldMyPreferences_x.constant = xPos
            }else{
                // adjust txt search :
                if xPos + 100 > screenWidth {
                    //we are exceeding size need to change xpos
                    xPos = 0.0
                    ypos = ypos + spaceHeight + 8.0 + 11
                    
                    txtFldMyPreferences_y.constant = ypos
                    txtFldMyPreferences_x.constant = xPos
                }else{
                    txtFldMyPreferences_y.constant = ypos
                    txtFldMyPreferences_x.constant = xPos
                }
            }
            txtMatch_width.constant = screenWidth - xPos + 50// here 30 is padding....
            
        }else{
            txtFldMyPreferences.isHidden = true
        }
        
        if arrTagsMyPreferences.count < 20 {
            self.txtFldMyPreferences.alpha = 1
            scrollViewMyPreferences_h.constant = ypos + 34 + 10//Here 50 is the height of the txtview, and 10 is the bottom space of
            // the scrollview
            if arrTagsMyPreferences.count == 20 {
                self.txtFldMyPreferences.alpha = 0
                self.txtFldMyPreferences.resignFirstResponder()
                self.view.endEditing(true)
            }
        }else{
            self.txtFldMyPreferences.alpha = 0
            self.txtFldMyPreferences.resignFirstResponder()
            self.view.endEditing(true)
            scrollViewMyPreferences_h.constant = ypos//Here 50 is the height of the txtview, and 10 is the bottom space of
            
        }
    }
    
    @objc func removeTagPreferences(_ sender: AnyObject) {
        arrTagsMyPreferences.remove(at: (sender.tag - 1))
        createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: false)
    }
}


extension RespondSearchResultVC : PinTexFieldDelegate{
    func didPressBackspace(textField : PinTextField){
        print("back prashed")
        
        // My attibutes :
        if textField == txtFldMyAttributes {
            
            // for removed :
            if arrTagsMyAttributes.count > 0 && (txtFldMyAttributes.text?.isEmpty ?? false){
                
                arrTagsMyAttributes.remove(at: arrTagsMyAttributes.count - 1)
                
                createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: false)
            }
            
            // My Preferences :
        }else if textField == txtFldMyPreferences{
            
            // for removed :
            if arrTagsMyPreferences.count > 0 && (txtFldMyPreferences.text?.isEmpty ?? false){
                
                arrTagsMyPreferences.remove(at: arrTagsMyPreferences.count - 1)
                createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: false)
            }
        }
    }
}

///MARK:-
//MARK:- Tags with Textfilds:

extension RespondSearchResultVC :  UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let searchText  = textField.text! + string
        
        let maxLength = 21
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if newString.length == maxLength && string == " "{
            print("add space")
        }else{
            guard newString.length < maxLength else{ return false}
        }
        
        // My attributes :
        
        if textField == txtFldMyAttributes {
            //Hide remove space :
            if range.location == 0 && string == " " {
                txtFldMyAttributes.text = ""
                return false;
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            // add new element :
            if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                txtFldMyAttributes.text = ""
                arrTagsMyAttributes.append(searchText.replace(string: " ", replacement: ""))
                createTagsForAttributes(OnView: scrollViewMyAttibutes, withArray: arrTagsMyAttributes, isCancelButtonVisible: false)
            }
            return true
            
            // My preferences:-------------------------------------------------------------------------------
        }else if textField == txtFldMyPreferences {
            
            //Hide remove space :
            if range.location == 0 && string == " " {
                txtFldMyPreferences.text = ""
                return false;
            }
            
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            // add new element :
            if str.last == " " && searchText.replace(string: " ", replacement: "").count > 0{
                txtFldMyPreferences.text = ""
                arrTagsMyPreferences.append(searchText.replace(string: " ", replacement: ""))
                createTagsForPreferences(OnView: scrollViewMyPreferences, withArray: arrTagsMyPreferences, isCancelButtonVisible: false)
                
                return false
            }
            return true
        }
        return true
    }
}

/*
 MARK:- API's
 */
extension RespondSearchResultVC {
    
    func call_sending_note(msg : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        var param = [String : AnyObject]()
        
        let from_user_id =  "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        
        param.updateValue(event_id as AnyObject, forKey:"event_id")
        param.updateValue( to_post_id as AnyObject, forKey:"to_post_id")
        param.updateValue( from_user_id as AnyObject, forKey:"from_user_id")
        param.updateValue( to_user_id as AnyObject, forKey:"to_user_id")
        
        responseVM.callSendingNote(requestParam : param) { (status, message) in
            
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.chatObj.matchInfo.event_id = Int(self.event_id) ?? 0
                self.chatObj.sendDirectMessage(txtMsg: msg, isReciverName: self.chatInfo.name, isReciverID: Int64(self.chatInfo.otherUserId), isReciverImage: self.chatInfo.profilePicture, match_id : 0, otherUser_id : 0)
                
                // call Notification API
                pushNotificationVM.callPushNotification_Api(otherUser_id: Int(self.to_user_id) ?? 0, match_id: 0, message: msg) { (status, _) in
                    self.navigationController?.popViewController(animated: true)
                  //  successSnackBar.sharedInstance.show(message: message)
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                
                
            }else if status == "error"{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    // Report API:
    func callReport( to_user : String, report_reason : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        matchesVM.reportPost_Api(from_user: userID, to_user: to_user, report_reason: report_reason) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            self.navigationController?.popViewController(animated: true)
            SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
        }
    }
}

