


//
//  ssTagViewWithTextField.swift
//  MyFirstApp
//
//  Created by cis on 30/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

extension ssTagViewWithTextField : UITextFieldDelegate{
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let searchText  = textField.text! + string

            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)

            // for add new elements :
            if str.last! == " "{
                searchTextField.text = ""
                tagListData.append(searchText.replace(string: " ", replacement: ""))
                createTagCloud(OnView: targetView, withArray: tagListData, isCancelVisible: isCancelVisibleTarget)
            }
            return true
        }
}

class ssTagViewWithTextField: NSObject  {

    var searchTextField : UITextField!
    var tagListData = [String]()
    var targetView : UIView!
    var isCancelVisibleTarget : Bool!
    
    func createTagCloud(OnView view: UIView, withArray data:[String], isCancelVisible : Bool) {
            tagListData = data
            targetView = view
            isCancelVisibleTarget = isCancelVisible
           for tempView in view.subviews {
               if tempView.tag != 0 {
                   tempView.removeFromSuperview()
               }
           }
           
           var xPos:CGFloat = 15.0
           var ypos: CGFloat = 5
           var tag: Int = 1
        
           for str in data  {
            
            let startstring = str
            
            let width = startstring.widthOfString(usingFont: UIFont(name:"verdana", size: 13.0)!)
            
            var checkWholeWidth : CGFloat = 0.0
            
            if isCancelVisible {
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 ) + CGFloat(50.0)//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht, here 50 is cancel button width
                
            }else{
                checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(25.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            }
            
            
               if checkWholeWidth > UIScreen.main.bounds.size.width - 30.0 {
                   //we are exceeding size need to change xpos
                   xPos = 15.0
                   ypos = ypos + 29.0 + 8.0
               }
            
            
            let customView = loadView(isCancelVisible: isCancelVisible, tagIndex: tag, frame: CGRect(x: xPos, y: ypos, width: width + 17.0 + 38.5, height: 30), title: startstring)
            
//             
               xPos = CGFloat(xPos) + CGFloat(width) + CGFloat(17.0) + CGFloat(43.0)
               view.addSubview(customView)
               tag = tag  + 1
           }
        
        
        //----------------------------------------------------------------
        // adjust txt search :
        if xPos + 100 > UIScreen.main.bounds.size.width - 30.0 {
            //we are exceeding size need to change xpos
            xPos = 15.0
            ypos = ypos + 29.0 + 8.0
        }
        
        if searchTextField == nil {
            searchTextField = UITextField(frame: CGRect(x: xPos, y: ypos, width: 100, height: 30))
            
            searchTextField.becomeFirstResponder()
        }else{
            searchTextField.frame =  CGRect(x: xPos, y: ypos, width: 100, height: 30)
            searchTextField.becomeFirstResponder()
        }
        searchTextField.delegate = self
       }
    
    
    
    func loadView(isCancelVisible : Bool,tagIndex : Int, frame : CGRect,title : String)->UIView{
        
        let myView = Bundle.main.loadNibNamed("ssTagView", owner: nil, options: nil)![0] as! ssTagView
        myView.frame = frame
     
        myView.lblTitle.text = title
        
        if isCancelVisible {
               myView.btnCancel.isHidden = false
               myView.btnCancel.tag = tagIndex
               myView.btnCancel.addTarget(self, action: #selector(removeField(sender: )), for: .touchUpInside)
        }else{
            myView.btnCancel.isHidden = false
            myView.btnCancel.tag = tagIndex
        }
        
        return myView
    }
    
    @objc func removeField(sender : UIButton){
        tagListData.remove(at: sender.tag)
        createTagCloud(OnView: targetView, withArray: tagListData, isCancelVisible: isCancelVisibleTarget)
    }
}

extension String {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
}

