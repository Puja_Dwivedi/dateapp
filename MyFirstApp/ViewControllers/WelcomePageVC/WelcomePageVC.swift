//
//  WelcomePageVC.swift
//  MyFirstApp
//
//  Created by cis on 11/08/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class WelcomePageVC: UIViewController {

    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var lblMesaage: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    let text = "By tapping Sign in or register, you agree to our Terms. Learn how we process your data in our Privacy Policy and Cookies Policy"
    let attributeRangeString1 =  "Terms"
    let attributeRangeString2 = "Privacy Policy"
    let attributeRangeString3 = "Cookies Policy"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblSubtitle.text = "See each other.\nFind each other."
        addAttributeText()
    if Constants.userDefault.object(forKey: Variables.phone_number) != nil {
            self.moveToLoginScreen()
        }
    }
    
    
    @IBAction func actionTermsOfService(_ sender: UIButton) {
    let objTermsVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
              AppDelegate.shared.navController = self.navigationController!
              objTermsVC.activeScreen = .TermsAndConditions
              self.navigationController?.pushViewController(objTermsVC, animated: true)
    }
    
    @IBAction func actionPrivacyPolicy(_ sender: Any) {
    let objPrivacyVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
          AppDelegate.shared.navController = self.navigationController!
          objPrivacyVC.activeScreen = .PrivacyPolicy
          self.navigationController?.pushViewController(objPrivacyVC, animated: true)
    }

    @IBAction func ActionLogin(_ sender: UIButton) {
    let objLoginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")
           AppDelegate.shared.navController = self.navigationController!
           self.presentLikeNavigationAnimation(viewController: objLoginVC!)
    }
    
    func moveToLoginScreen(){
        let objRootVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        AppDelegate.shared.navController = self.navigationController!
        self.navigationController?.pushViewController(objRootVC, animated: false)
    }
    
    func setSpiral() {
        let image = UIImage(named: "welcome_spiral")
        let imageView = UIImageView(frame: view.bounds)
        imageView.contentMode = .scaleAspectFill
        imageView.image = image
        tiltImage(angleInDegrees: -5.5, imageView: imageView)
        view.addSubview(imageView)
    }
    
    func tiltImage(angleInDegrees: CGFloat, imageView: UIImageView) {
        let angleInRadians = angleInDegrees * CGFloat.pi / 180.0
        let transform = CGAffineTransform(rotationAngle: angleInRadians)
        imageView.transform = transform
    }
}


/*
 Add attribute text
 */

extension WelcomePageVC {
    
    func addAttributeText(){

        
        lblMesaage.text = text
        self.lblMesaage.textColor =  .white
        let underlineAttriString = NSMutableAttributedString(string: text)
       
        // terms:
        let range1 = (text as NSString).range(of: attributeRangeString1)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)


        //"Privacy Policy"
        let range2 = (text as NSString).range(of: attributeRangeString2)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
        
        //"Privacy Policy"
        let range3 = (text as NSString).range(of: attributeRangeString3)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range3)
        
        // adding item:
        //1.
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Poppins-Regular", size: 14)!, range: range1)
        
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white , range: range1)
       //2.
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Poppins-Regular", size: 14)!, range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range2)
        //3.
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Poppins-Regular", size: 14)!, range: range3)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range3)
        
        lblMesaage.attributedText = underlineAttriString
        lblMesaage.isUserInteractionEnabled = true
        lblMesaage.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
    }
    
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        
        //1.
  let numberRange1 = (text as NSString).range(of: attributeRangeString1)
  let numberRange2 = (text as NSString).range(of: attributeRangeString2)
  let numberRange3 = (text as NSString).range(of: attributeRangeString3)
        
  if gesture.didTapAttributedTextInLabel(label: lblMesaage, inRange: numberRange1) {
       // "Terms"
    let objTermsVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
    objTermsVC.activeScreen = .TermsAndConditions
    self.navigationController?.pushViewController(objTermsVC, animated: true)
    
    
  }else if gesture.didTapAttributedTextInLabel(label: lblMesaage, inRange: numberRange2) {
    //"Privacy Policy"
    let objPrivacyVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
    objPrivacyVC.activeScreen = .PrivacyPolicy
    self.navigationController?.pushViewController(objPrivacyVC, animated: true)
    
  }else if gesture.didTapAttributedTextInLabel(label: lblMesaage, inRange: numberRange3) {
    //"Cookies Policy"
    let objCookieVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
    objCookieVC.activeScreen = .CookiePolicy
    self.navigationController?.pushViewController(objCookieVC, animated: true)
  }
    }
}

