//
//  TagCell.swift
//  MyFirstApp
//
//  Created by cis on 27/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class TagCell: UICollectionViewCell {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewGradient: UIViewClass!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
