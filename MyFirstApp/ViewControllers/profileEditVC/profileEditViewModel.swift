//
//  profileEditViewModel.swift
//  MyFirstApp
//
//  Created by cis on 03/04/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ProfileEditViewModel: NSObject {
    
    func setProfileImage(imageData : Data, completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var message  = ""
        let param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.userUpdateProfilePic + "/\(userID)"
        
        let arrDocuments = NSMutableArray()
        arrDocuments.setValue(String(format: "pictureImage%02d", 1), forKey: "picture")
        arrDocuments.setValue(imageData, forKey: "picture")
        
        //  print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.formRequestWith(strUrl: strService, imageData: imageData, imageName: "picture", parameters: param, headers: header) { (result, data) in
            
            let dict = data.response
            
            if let statusCode = dict.object(forKey: "code") as? Int  { // user is already register :
                
                if statusCode == 200 {
                    let dict = data.response
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    completion("success", message, dict)
                }else{
                    print("data : \(data)")
                    if data.message == "" {
                        if data.message == "" {
                            message = "\(dict.object(forKey: "message") ?? "")"
                            
                        }else{
                            message = data.message
                        }
                    }else{
                        message = data.message
                    }
                    completion("error", message, NSDictionary())
                }
            }else{
                print("data : \(data)")
                if data.message == "" {
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    
                }else{
                    message = data.message
                }
                completion("error", message, NSDictionary())
            }
        }
    }
    
    
    // Updating profile values :
    
    func setUpdateApi(requestParam : [String : AnyObject], completion: @escaping (_ result: String, _ message : String,_ data : NSDictionary) -> Void)
    {
        var message  = ""
        let param = requestParam
        let header = RV_GetPostMethod.getHeaders()
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        let strService = APPURL.Urls.setNotificationUpdate + "/\(userID)"
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                if data.message == "" {
                    message = "\(dict.object(forKey: "message") ?? "")"
                    
                }else{
                    message = data.message
                }
                completion("success",message, dict)
            }
            else
            {
                if data.message == "" {
                    SnackBar.sharedInstance.show(message: "Something went wrong,Please try again.", showMsgAt: .bottom)
                    completion("error", "Something went wrong,Please try again.", NSDictionary())
                }else{
                    SnackBar.sharedInstance.show(message: data.message, showMsgAt: .bottom)
                    completion("error", "Something went wrong,Please try again.", NSDictionary())
                    completion("error", data.message, NSDictionary())
                }
            }
        }
    }
}
