//
//  profileEditVC.swift
//  MyFirstApp
//
//  Created by cis on 03/04/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
import SDWebImage
import VisualEffectView

class profileEditVC: UIViewController {
    
    enum ActiveScreen {
        case readOnly
        case readOnlyWithBlur
        case write
        case none
    }
    
    enum ActiveComeFromStatus {
        case matchScreen
        case profileScreen
        case none
    }
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    
    @IBOutlet var lblHeaderTitle: UILabel!
    @IBOutlet var imgEdu: UIImageView!
    @IBOutlet var imgWork: UIImageView!
    @IBOutlet var imgBio: UIImageView!
    @IBOutlet var viewCheck: UIView!
    @IBOutlet var imgChcek: UIImageView!
    @IBOutlet var SaveBtnTopConstant: NSLayoutConstraint!
    @IBOutlet weak var txtViewBio: UITextView!
    @IBOutlet weak var txtFldWork: UITextView!
    @IBOutlet weak var txtViewEducation: UITextView!
    @IBOutlet weak var imgEdit: UIImageView!
    @IBOutlet weak var btnProfileImage: UIButton!
    
    //MARK:-
    //MARK:- Variables:
    
    var isChangeAnything = false
    var profileEditVM = ProfileEditViewModel()
    var valueFromPreviousScreen  = (profileName : "", profilePicture: UIImage(), bio : "",work : "", education : "",strProfilePicture : "")
    var actionScreen = ActiveScreen.none
    var activeComeFromStatus = ActiveComeFromStatus.none
    var isComeFromMatchIfo  = (status : false, user_id : 0)

    
    //MARK:-
    //MARK:- App flow:
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        for tempView in self.lblProfileName.subviews{
            tempView.removeFromSuperview()
        }
        
        for tempView in imgProfile.subviews{
            tempView.removeFromSuperview()
        }
    }
    
    
    //MARK:-
    //MARK:- Action Methods:
    @IBAction func actionSaveButton(_ sender: UIButton) {
        if actionScreen == .readOnly || actionScreen == .readOnlyWithBlur{
            self.navigationController?.popViewController(animated: true)
        }else if actionScreen == .write {
            if isChangeAnything {
                prepairForUpdate()
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func actionBackFromEdit(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionBtnPhotoEdit(_ sender: UIButton) {
        let obj  = ImagePickerView()
        obj.show(self, preview: (isPreview: false, image: UIImage())) { [weak self](selectedImage) in
            let imgData = selectedImage.image?.jpegData(compressionQuality: 1.0)!
            self?.setProfileImage(imageData: imgData!, image: selectedImage.image!)
        }
    }
    
    
    //MARK:-
    //MARK:- Methods:
    
    func initView(){
        DeviceSize.screen_Height > 736.0 ? (self.SaveBtnTopConstant.constant = 50.0) : (self.SaveBtnTopConstant.constant = 30.0)
        viewCheck.isHidden = true
        lblProfileName.text = valueFromPreviousScreen.profileName
//        getProfileInfo()
        switch activeComeFromStatus {
        case .matchScreen:
            imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgProfile.sd_setImage(with: URL(string: valueFromPreviousScreen.strProfilePicture), placeholderImage: UIImage(named: "img_profile_placeholder"))
            lblHeaderTitle.text = ""
            break
            
        case .profileScreen:
            imgProfile.image = valueFromPreviousScreen.profilePicture
            lblHeaderTitle.text = "About Me"
            break
            
        case .none:
            imgProfile.image = #imageLiteral(resourceName: "img_profile_placeholder")
            lblHeaderTitle.text = ""
            break
        }
        
        txtViewBio.text = valueFromPreviousScreen.bio
        txtFldWork.text = valueFromPreviousScreen.work
        txtViewEducation.text = valueFromPreviousScreen.education
        
        if actionScreen == .readOnly {
            imgEdit.isHidden = true
            btnProfileImage.isUserInteractionEnabled = false
            imgProfile.isUserInteractionEnabled = false
            txtViewBio.isUserInteractionEnabled = false
            txtFldWork.isUserInteractionEnabled = false
            txtViewEducation.isUserInteractionEnabled = false
            editIconsDisplay(show: false)
            
        }else if actionScreen == .write {
            imgEdit.isHidden = false
            btnProfileImage.isUserInteractionEnabled = true
            imgProfile.isUserInteractionEnabled = true
            txtViewBio.isUserInteractionEnabled = true
            txtFldWork.isUserInteractionEnabled = true
            txtViewEducation.isUserInteractionEnabled = true
            editIconsDisplay(show: true)
            
        }else if actionScreen == .readOnlyWithBlur {
            
            imgEdit.isHidden = true
            btnProfileImage.isUserInteractionEnabled = false
            imgProfile.isUserInteractionEnabled = false
            txtViewBio.isUserInteractionEnabled = false
            txtFldWork.isUserInteractionEnabled = false
            txtViewEducation.isUserInteractionEnabled = false
            editIconsDisplay(show: false)
            
            // for label:---------
            for tempView in self.lblProfileName.subviews{
                tempView.removeFromSuperview()
            }
            
            let visualEffectView1 = VisualEffectView(frame: self.lblProfileName.bounds)
            
            // Configure the view with tint color, blur radius, etc
            visualEffectView1.colorTint = .clear
            visualEffectView1.colorTintAlpha = 0.1
            visualEffectView1.blurRadius = 5
            visualEffectView1.scale = 1
            
            self.lblProfileName.addSubview(visualEffectView1)
            
            // for image :
            for tempView in imgProfile.subviews{
                tempView.removeFromSuperview()
            }
            
            let visualEffectView = VisualEffectView(frame: imgProfile.bounds)
            
            // Configure the view with tint color, blur radius, etc
            visualEffectView.colorTint = .clear
            visualEffectView.colorTintAlpha = 0.1
            visualEffectView.blurRadius = 3
            visualEffectView.scale = 1
            
            imgProfile.addSubview(visualEffectView)
            
        }
    }
    
    func editIconsDisplay(show: Bool) {
        if show {
            imgBio.isHidden = false
            imgWork.isHidden = false
            imgEdu.isHidden = false
        } else {
            imgBio.isHidden = true
            imgWork.isHidden = true
            imgEdu.isHidden = true
        }
    }
    
    func prepairForUpdate(){
        var param = [String : AnyObject]()
        param.updateValue(txtViewBio.text! as AnyObject, forKey: "Description")
        param.updateValue(txtFldWork.text! as AnyObject, forKey: "Work")
        param.updateValue(txtViewEducation.text! as AnyObject, forKey: "Education")
        
        setUpdateProfileInfo(requestParam: param)
    }
    
//    func setInfo(dict : NSDictionary){
//        let item = dict.object(forKey: "data") as! NSDictionary
//        let profilePicture = "http://3.137.46.73:4000/images/profile_pictures/\(item.object(forKey: "picture") as! String)"
//        imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
//        imgProfile.sd_setImage(with: URL(string: profilePicture), placeholderImage: UIImage(named: "img_profile_placeholder"))
//
//    }
}


//MARK:-
//MARK:- Api calling:
extension profileEditVC {
    
//    func getProfileInfo()
//    {
//        var user_id  = ""
//        if isComeFromMatchIfo.status {
//            user_id = "\(isComeFromMatchIfo.user_id)"
//        }else{
//            user_id = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
//        }
//
//        Loader.sharedInstance.showLoader(msg: "Loading...")
//        ProfileViewModel().getProfileApi(user_id: user_id) { (status, message, dictData)  in
//            Loader.sharedInstance.stopLoader()
//
//            if status == "success"
//            {
//                print("set all the information")
//                self.setInfo(dict: dictData)
//            }
//        }
//    }
    
    // set profile info:
    
    func setUpdateProfileInfo(requestParam : [String : AnyObject])
    {
        self.view.endEditing(true)
        Loader.sharedInstance.showLoader(msg: "Loading...")
        profileEditVM.setUpdateApi(requestParam: requestParam) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                // print("Update the information")
                self.navigationController?.popViewController(animated: true)
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
    
    
    func setProfileImage(imageData : Data, image : UIImage)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        profileEditVM.setProfileImage(imageData: imageData) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                print("Update the information")
                self.imgProfile.image = image
                
            }else{
                
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}

//MARK:-
//MARK:- text field delegate :

extension profileEditVC : UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        isChangeAnything = true
        viewCheck.isHidden = false
        if textView == txtViewBio{
            return self.textLimit(existingText: textView.text,
                                  newText: text,
                                  limit: 120)
        }
        
        if textView == txtFldWork{
            return self.textLimit(existingText: textView.text,
                                  newText: text,
                                  limit: 60)
        }
        
        if textView == txtViewEducation{
            return self.textLimit(existingText: textView.text,
                                  newText: text,
                                  limit: 60)
        }
        
        return true
    }
    
    private func textLimit(existingText: String?,
                           newText: String,
                           limit: Int) -> Bool {
        let text = existingText ?? ""
        let isAtLimit = text.count + newText.count <= limit
        return isAtLimit
    }
}

