//
//  AskQuestionViewModel.swift
//  MyFirstApp
//
//  Created by cis on 28/07/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class AskQuestionViewModel: NSObject {
    
    func submitQuery_API(fromEmail : String,toEmail : String, subject : String,text : String, completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        let userid  = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        var param = AppTheme.sharedInstance.GetBasicParam()
        param.updateValue(fromEmail as AnyObject, forKey:"from")
        param.updateValue(toEmail as AnyObject, forKey:"to")
        param.updateValue(subject as AnyObject, forKey:"subject")
        param.updateValue(text as AnyObject, forKey:"text")
        param.updateValue(userid as AnyObject, forKey:"user_id")
        
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.sendContactUSURL
        
        print("header \(header) \n API: \(strService) request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                
                
                completion("success", AlertMessages.shareInstance.sendEmailSuccessfully)
            }
            else
            {
                completion("error", AlertMessages.shareInstance.somethingWentWrong)
            }
        }
    }
}
