//
//  AskQuestionVC.swift
//  MyFirstApp
//
//  Created by cis on 12/07/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class AskQuestionVC: UIViewController {
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var txtViewDes: UItextView!
    @IBOutlet var txtViewHtConstraint: NSLayoutConstraint!
    @IBOutlet var lblTopConstraint: NSLayoutConstraint!
    
    //MARK:-
    //MARK:- variables:
    var strHeaderTitle = ""
    var infoIdict : NSDictionary?
    var askQuestionVM = AskQuestionViewModel()
    var lineCount = 0
    private var previousRect:CGRect = CGRect.zero

    
    //MARK:-
    //MARK:- App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    //MARK:-
    //MARK:-  flow
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSend(_ sender: UIButton) {
        let strDes  = txtViewDes.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard strDes != "" else {
            SnackBar.sharedInstance.show(message: "\(AlertMessages.shareInstance.pleaseWriteSomething)", showMsgAt: .mid); return }
        
        submitQuery_API(strDes: strDes)
    }
    
    func initView(){
        lblHeaderTitle.text = "\(infoIdict?.object(forKey: "type_description") ?? "")"
        self.txtViewDes.placeholderColor = AppDelegate.shared.textFieldPlaceholderColor
        self.lblTopConstraint.constant = 3
        self.txtViewHtConstraint.constant = 40
    }
}


extension AskQuestionVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        checkIfReturnOrLineWrap(textView: textView)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.lineCount = self.lineCount + 1
        checkIfReturnOrLineWrap(textView: textView)
        return true
    }
    
    private func checkIfReturnOrLineWrap(textView:UITextView) {
        let currentRect = textView.caretRect(for:textView.endOfDocument)
        if (currentRect.origin.y != previousRect.origin.y && (previousRect.origin.y != CGFloat.infinity || currentRect.origin.y > 0) || currentRect.origin.y == CGFloat.infinity) && (self.lblTopConstraint.constant == 1 && self.lineCount > 2) {
            //React to the line change!
            self.lblTopConstraint.constant = self.lblTopConstraint.constant + 5.0
            self.txtViewHtConstraint.constant = self.txtViewHtConstraint.constant + 15.0
            
        }
        previousRect = currentRect
    }
}


//MARK:-
//MARK:- API:

extension AskQuestionVC {
    
    func submitQuery_API(strDes : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        let toEmail = "\(infoIdict?.object(forKey: "output") ?? "")"
        let subject = "\(infoIdict?.object(forKey: "type_description") ?? "")"
        
        askQuestionVM.submitQuery_API(fromEmail: settingInfo.email, toEmail: toEmail, subject: subject, text: strDes) { (status, message) in
            Loader.sharedInstance.stopLoader()
            if status == "success"
            {
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                self.txtViewDes.text = ""
            }else{
                
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}
