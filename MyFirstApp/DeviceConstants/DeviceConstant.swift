//
//  DeviceConstants.swift
//  MCQ
//
//  Created by cis on 28/01/22.
//

import UIKit

/// Struct to check device type
struct DeviceType
{
    static let is_iPad : Bool          = UIDevice.current.userInterfaceIdiom == .pad
    static let is_iPhone : Bool        = UIDevice.current.userInterfaceIdiom == .phone
    static let is_iPhone_4 : Bool      = (UIScreen.main.bounds.size.height == 480)
    static let is_iPhone_5 : Bool      = (UIScreen.main.bounds.size.height == 568)
    static let is_iPhone_6 : Bool      = (UIScreen.main.bounds.size.height == 667)
    static let is_iPhone_6p : Bool     = (UIScreen.main.bounds.size.height == 736)
    static let is_iPhone_X : Bool      = (UIScreen.main.bounds.size.height == 812)
    static let is_Retina : Bool        = (UIScreen.main.scale >= 2.0)
}

/// Struct to device Size
struct DeviceSize
{
    static let screen_Width = UIScreen.main.bounds.size.width
    static let screen_Height = UIScreen.main.bounds.size.height
    
    static let widthFactor = 320/UIScreen.main.bounds.size.width
    static let heightFactor = 568/UIScreen.main.bounds.size.height
}

