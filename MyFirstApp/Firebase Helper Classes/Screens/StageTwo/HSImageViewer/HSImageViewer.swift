//
//  HSImageViewer.swift
//

import UIKit

class HSImageViewer: UIViewController, UIScrollViewDelegate {

    //MARK: Variables
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var imgView_Picture: UIImageView!
    var returnErro:((String)->Void)! = nil
    var imgURL:String = ""
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.isHidden = true
        self.function_downloadImage(imageView: self.imgView_Picture, imageURL: "\(self.imgURL)", success: { (image) in
            self.scrollView.isHidden = false
            self.imgView_Picture.image = image
        }) {
            self.scrollView.isHidden = false
            self.returnErro("Unable to load the image, Please try again later.")
        }
    }
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: Buttons
    @IBAction func btn_Close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: Delegates
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgView_Picture
    }
    //MARK: Functions
    func function_downloadImage(imageView:UIImageView,imageURL:String,success:@escaping (_ orignalImage:UIImage)->Void,fail:@escaping ()->Void) {
        if let url = URL.init(string: "\(imageURL)") {
            if UIApplication.shared.canOpenURL(url) {
                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                    if error == nil {
                        if let isData = data {
                            DispatchQueue.main.async(execute: {
                                if let isImage = UIImage.init(data: isData) {
                                    success(isImage)
                                } else {
                                    fail()
                                }
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                fail()
                            })
                        }
                    } else {
                        DispatchQueue.main.async(execute: {
                            fail()
                        })
                    }
                }).resume()
            } else {
                DispatchQueue.main.async(execute: {
                    fail()
                })
            }
        }
    }
}
