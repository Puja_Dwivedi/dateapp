//
//  Class_StageTwo.swift

//

import UIKit
import AVKit
import AVFoundation
import VisualEffectView


class Class_StageTwo: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MyMediaCellDelegate, OtherMediaCellDelegate {
    
    enum ComeFromScreen {
        case fromMatch
        case fromChat
        case none
    }
    
    @IBOutlet var viewTopHeader: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var view_TextView: HSExpandableTextView!
    
    //MARK: Variables
    var chatDetail: HSChatUser! = nil
    var friendImage = ""
    let matchesVM = MatchesViewModel()
    var matchInfo = (match_id : 0, user_id : 0, event_id: 0)
    var backToPostVCDelegate : BackToPostVCDelegate!
    
    var sectionArray = [String]()
    var profile_visibility = 0
    var profile_indicator = 0
    var strprofilePicture = ""
    var profileVM = ProfileViewModel() // for get the profile info
    var comeFromScreenStatus = ComeFromScreen.none
    
    let countryDialCode = Constants.userDefault.object(forKey: Variables.date_format) as! Int
    var strDateFormate = ""
    
    @IBOutlet weak var btnMenu: UIButton!
    var chatArray = [String: [HSMessage]]()
    
    @IBOutlet weak var lblUserName: BlurredLabel!
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib.init(nibName: "Cell_MyMessage", bundle: nil), forCellReuseIdentifier: "Cell_MyMessage")
        self.tableView.register(UINib.init(nibName: "Cell_OtherMessage", bundle: nil), forCellReuseIdentifier: "Cell_OtherMessage")
        self.tableView.register(UINib.init(nibName: "Cell_MyMediaMessage", bundle: nil), forCellReuseIdentifier: "Cell_MyMediaMessage")
        self.tableView.register(UINib.init(nibName: "Cell_OtherMediaMessage", bundle: nil), forCellReuseIdentifier: "Cell_OtherMediaMessage")
        self.tableView.transform = CGAffineTransform.init(rotationAngle: -(CGFloat(Double.pi)))
        self.tableView.backgroundView = UIImageView.init(image: UIImage(named: "chat_bg"))
        lblUserName.text = chatDetail.name
        
        viewHeader.addShadowToView()
        viewTopHeader.addShadowToView()
        
        AppTheme.sharedInstance.currentChatID = self.chatDetail.chatID
        
        self.view_TextView.sendMessage = { text in
            
            let userImg = Constants.userDefault.object(forKey: Variables.picture)
            
            HSRealTimeMessagingLIbrary.function_SendTextMessage("\(text)",match_id : self.matchInfo.event_id, otherUser_id : self.matchInfo.user_id, isSenderName: "\(HSRealTimeMessagingLIbrary.HSCurrentUserName())", isReciverName: "\(self.chatDetail.name)", isSenderID: HSRealTimeMessagingLIbrary.HSCurrentUserID(), isReciverID: self.chatDetail.id, isReciverImage: self.friendImage, isSenderImage: userImg as! String, to_post_id: "\(self.matchInfo.event_id)", isSuccess: {
            }, isError: { (message) in
                self.function_Pop("Unable to send the message, Please try again later.")
            })
        }
        
        self.view_TextView.sendMedia = {
            let alert = UIAlertController.init(title: "", message: "What would you like to share?", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction.init(title: "Image", style: .default, handler: { (action) in
                let picker = UIImagePickerController.init()
                picker.sourceType = .photoLibrary
                picker.allowsEditing = true
                picker.delegate = self
                self.navigationController?.present(picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction.init(title: "Video", style: .default, handler: { (action) in
                let picker = UIImagePickerController.init()
                picker.sourceType = .photoLibrary
                picker.mediaTypes = ["public.movie"]
                picker.allowsEditing = true
                picker.delegate = self
                self.navigationController?.present(picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            }))
            self.navigationController?.present(alert, animated: true, completion: nil)
        }
        self.function_LoadData()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        HSRealTimeMessagingLIbrary.function_updateUnreadCount(_isSenderID: HSRealTimeMessagingLIbrary.HSCurrentUserID(), _isReceiverID: self.chatDetail.id, isunread_count: 0)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if  countryDialCode == 1 { // for US : 12 hours time foemate // for US : 12 hours time foemate
            strDateFormate =  "MM/dd/yyyy"
        }else{
            strDateFormate = "dd/MM/yyyy"
        }
        
        self.title = " " + self.chatDetail.name
        
        imgProfile.sd_setImage(with: URL(string: strprofilePicture), placeholderImage: UIImage(named: "img_profile_placeholder"))
        
        if profile_visibility == 0{
            
            // for label:---------
            for tempView in self.lblUserName.subviews{
                tempView.removeFromSuperview()
            }
            
            lblUserName.text = "Fallon"
            imgProfile.image = UIImage(named: "profile_Placeholder")
            
            lblUserName.blur()
            
            //for profile image :
            // for image:
            
            // for image :
            for tempView in imgProfile.subviews{
                tempView.removeFromSuperview()
            }
            
            let visualEffectView = VisualEffectView(frame: imgProfile.bounds)
            
            // Configure the view with tint color, blur radius, etc
            visualEffectView.colorTint = .clear
            visualEffectView.colorTintAlpha = 0.1
            visualEffectView.blurRadius = 3
            visualEffectView.scale = 1
            
            imgProfile.addSubview(visualEffectView)
            
        }else if profile_visibility == 1{
            
            // for label:
            for tempView in self.lblUserName.subviews{
                tempView.removeFromSuperview()
            }
            
            lblUserName.text = chatDetail.name
            imgProfile.sd_setImage(with: URL(string: strprofilePicture), placeholderImage: UIImage(named: "img_profile_placeholder"))
            lblUserName.unblur()
            
            // for profile image:
            for tempView in imgProfile.subviews {
                tempView.removeFromSuperview()
            }
        }
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        AppTheme.sharedInstance.currentChatID = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Buttons
    
    
    @IBAction func actionmenu(_ sender: UIButton) {
        
        var strMsg = ""
        if profile_indicator == 0{
            strMsg =  "Share my profile"
        }else if profile_indicator == 1{
            strMsg = "Profile shared"
        }
        
        viewMoreMatch.sharedInstance.Show(arr: [strMsg,"Remove","Report"], y: btnMenu.frame.origin.y + 30, strMatch_id: "") { [weak self](result) in
            
            if result == strMsg {
                
                if result == "Profile shared" {
                    // self?.updateStatusOfProfileIndicator(profileIndicator: 0)
                }else if result == "Share my profile" {
//                    self?.callShareprofile()
                    AlertTheme.sharedInstance.Show(popupCategory: .normal,message: "\(AlertMessages.shareInstance.YourProfileWillBe)", attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["Got it, share","Cancel"], isBottomTxt: "") { [weak self](clicked) in
                        if clicked == "Got it, share" {
                            self?.updateStatusOfProfileIndicator(profileIndicator: 1)
                        }
                    }
                }
            }else if result == "Remove" {
                self?.removeMatch_api()
            }else if result == "Report"{
                print("Report")
                
                self?.callReportView(toUserId: "\(self?.matchInfo.user_id ?? 0)")
            }
        }
    }
    
    func callShareprofile() {
        ProfileShareView.sharedInstance.Show { [weak self](strShare) in
            self?.updateStatusOfProfileIndicator(profileIndicator: 1)
        }
    }
    
    func callReportView(toUserId: String){
        ReportReasonView.sharedInstance.Show { [weak self](strReason) in
            // print("reason report : \(strReason)")
            self?.callReport(to_user: toUserId, report_reason: strReason)
        }
    }
    
    @IBAction func actonOpenProfile(_ sender: UIButton) {
        
        if self.profile_visibility == 1 {
            getProfileInfo(openUser_id: "\(matchInfo.user_id)")
        }
    }
    
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArray.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let label = UILabel()
        label.frame = CGRect(x: 0.0, y: 10.0, width: self.view.frame.width, height: 30)
        label.text = sectionArray[section]
        label.textAlignment = .center
        label.font = UIFont(name: "Poppins-Regular", size: 12)
        label.backgroundColor = .clear
        label.textColor = #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1)
        label.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
        return label
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatArray[sectionArray[section]]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let chatDict = chatArray[sectionArray[indexPath.section]]?[indexPath.row] else {
            return UITableViewCell()
        }
        
        if chatDict.senderID ==  HSRealTimeMessagingLIbrary.HSCurrentUserID() {
            if chatDict.type == "text" {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_MyMessage") as? Cell_MyMessage else { return UITableViewCell.init(style: .default, reuseIdentifier: "Cell_MyMessage")}
                cell.lbl_Message.text = "\(chatDict.message)"
                cell.lbl_Time.text = "\(self.function_ReturnDate(chatDict.time))"
                
//                cell.functionAddGradient()
                
                cell.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_MyMediaMessage") as? Cell_MyMediaMessage else { return UITableViewCell.init(style: .default, reuseIdentifier: "Cell_MyMediaMessage")}
                cell.delegate = self
                cell.lbl_Time.text = "\(self.function_ReturnDate(chatDict.time))"
                if chatDict.type == "image" {
                    // cell.btn_OpenMedia.sd_setBackgroundImage(with: URL(string: chatDict.message), for: .normal)
                    cell.btn_OpenMedia.imageView?.kf.indicatorType = .activity
                    cell.btn_OpenMedia.imageView?.kf.setImage(with: URL(string: chatDict.message))
                    
                } else if chatDict.type == "video" {
                    cell.btn_OpenMedia.setTitle("Video", for: .normal)
                } else {cell.btn_OpenMedia.setTitle("", for: .normal)}
                cell.btn_OpenMedia.addTarget(cell, action: #selector(cell.btnOpenMediaClicked(_:)), for: .touchUpInside)
                cell.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
                return cell
            }
        } else {
            if chatDict.type == "text" {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_OtherMessage") as? Cell_OtherMessage else { return UITableViewCell.init(style: .default, reuseIdentifier: "Cell_OtherMessage")}
                cell.lbl_Message.text = "\(chatDict.message)"
                cell.lbl_Time.text = "\(self.function_ReturnDate(chatDict.time))"
                cell.view_BackGround.backgroundColor =  #colorLiteral(red: 0.9098039216, green: 0.9098039216, blue: 0.9098039216, alpha: 1)
                cell.view_BackGround.roundCorners(corners: [.topLeft,.topRight,.bottomRight,.bottomLeft], radius: 12.5)
                cell.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_OtherMediaMessage") as? Cell_OtherMediaMessage else { return UITableViewCell.init(style: .default, reuseIdentifier: "Cell_OtherMediaMessage")}
                cell.btn_OpenMedia.setTitle("", for: .normal)
                cell.lbl_Time.text = "\(self.function_ReturnDate(chatDict.time))"
                cell.delegate = self
                if chatDict.type == "image" {
                    // cell.btn_OpenMedia.sd_setBackgroundImage(with: URL(string: chatDict.message), for: .normal)
                    
                    cell.btn_OpenMedia.imageView?.kf.indicatorType = .activity
                    cell.btn_OpenMedia.imageView?.kf.setImage(with: URL(string:  chatDict.message))
                    
                } else if chatDict.type == "video" {
                    cell.btn_OpenMedia.setTitle("Video", for: .normal)
                } else {cell.btn_OpenMedia.setTitle("", for: .normal)}
                cell.btn_OpenMedia.addTarget(cell, action: #selector(cell.btnOpenMediaClicked(_:)), for: .touchUpInside)
                cell.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width/8
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell is Cell_MyMessage {
            // cell.functionAddGradient()
        }
    }
    
    
    func sendDirectMessage(txtMsg : String, isReciverName : String,isReciverID : Int64, isReciverImage : String, match_id : Int, otherUser_id : Int){
        //  self.view_TextView.sendMessage = { text in
        let userImg = Constants.userDefault.object(forKey: Variables.picture)
        HSRealTimeMessagingLIbrary.function_SendTextMessage("\(txtMsg)",match_id : match_id, otherUser_id : otherUser_id, isSenderName: "\(HSRealTimeMessagingLIbrary.HSCurrentUserName())", isReciverName: isReciverName, isSenderID: HSRealTimeMessagingLIbrary.HSCurrentUserID(), isReciverID: UInt64(isReciverID), isReciverImage: isReciverImage, isSenderImage: userImg as! String, to_post_id: "\(self.matchInfo.event_id)", isSuccess: {
            
        }, isError: { (message) in
            self.function_Pop("Unable to send the message, Please try again later.")
        })
        // }
    }
    
    
    var isserviceCallForNexrMessgae = Bool()
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //        if self.chatArray.count >= 10 {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            if !isserviceCallForNexrMessgae{
                self.isserviceCallForNexrMessgae = true
                if let isMsgID =  self.chatArray[self.sectionArray.last ?? ""]?.last?.msgID {
                    
                    HSRealTimeMessagingLIbrary.function_GetAllMessages(chatID: self.chatDetail.chatID, lastMsgID: isMsgID) { (messages) in
                        
                        //                        self.tableData.append(contentsOf: messages.sorted(by: {$0.time > $1.time }))
                        //                        self.tableView.reloadData()
                        let sortedMessages = messages.sorted(by: {$0.time > $1.time })
                        for message in sortedMessages {
                            let date = Convertor.convertDate(timeStamp: UInt(message.time), toFormat: self.strDateFormate, sendDay: true)
                            if !self.sectionArray.contains(date) {
                                self.sectionArray.append(date)
                                self.chatArray[date] = [message]
                            } else {
                                self.chatArray[date]?.append(message)
                                
                            }
                            self.tableView.reloadData()
                            self.isserviceCallForNexrMessgae = false
                        }
                        
                    }
                }
            }
        }
        //        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    //MARK: Functions
    func function_ReturnDate(_ timeStamp: UInt64) -> String {
        let date = Date.init(timeIntervalSince1970: Double(timeStamp))
        let formater = DateFormatter.init();
        formater.dateFormat = "hh:mm a"
        return formater.string(from: date)
    }
    func function_Pop(_ message: String) {
        let alert = UIAlertController.init(title: "My First App", message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: nil))
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    //MARK :
    //MARK : Cell Delegates
    func btnOpenMedia(cell: Cell_MyMediaMessage) {
        guard let chatDict = chatArray[sectionArray[self.tableView.indexPath(for: cell)!.section]]?[self.tableView.indexPath(for: cell)!.row] else {
            return
        }
        if chatDict.type == "image" {
            let vc = HSImageViewer.init(nibName: "HSImageViewer", bundle: Bundle.main)
            vc.imgURL = "\(chatDict.message)"
            vc.returnErro = { error in
                vc.dismiss(animated: true, completion: {
                    let alert = UIAlertController.init(title: "HAPPIU", message: "\(error)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: nil))
                    self.navigationController?.present(alert, animated: true, completion: nil)
                })
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
        }else if chatDict.type == "video" {
            if let videoURL = URL(string: "\(chatDict.message)") {
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    if let isPLayer = playerViewController.player {
                        isPLayer.play()
                    }
                }
            }
        }
    }
    
    func btnOpenOtherMedia(cell: Cell_OtherMediaMessage) {
        guard let chatDict = chatArray[sectionArray[self.tableView.indexPath(for: cell)!.section]]?[self.tableView.indexPath(for: cell)!.row] else {
            return
        }
        if chatDict.type == "image" {
            let vc = HSImageViewer.init(nibName: "HSImageViewer", bundle: Bundle.main)
            vc.imgURL = "\(chatDict.message)"
            vc.returnErro = { error in
                vc.dismiss(animated: true, completion: {
                    let alert = UIAlertController.init(title: "", message: "\(error)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: nil))
                    self.navigationController?.present(alert, animated: true, completion: nil)
                })
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
        }else if chatDict.type == "video" {
            if let videoURL = URL(string: "\(chatDict.message)") {
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    if let isPLayer = playerViewController.player {
                        isPLayer.play()
                    }
                }
            }
        }
    }
    
    
    func function_LoadData() {
        let message_id = "\(self.chatDetail.chatID):\(self.matchInfo.event_id)"
        print("message_id -> \(message_id)")
        HSRealTimeMessagingLIbrary.function_GetFirstTenMessages(chatID: message_id) { (message) in
            
            let date = Convertor.convertDate(timeStamp: UInt(message.time), toFormat: self.strDateFormate, sendDay: true)
            if !self.sectionArray.contains(date) {
                self.sectionArray.insert(date, at: 0)
                self.chatArray[date] = [message]
            } else {
                self.chatArray[date]?.insert(message, at: 0)
            }
            self.tableView.reloadData()
            if self.sectionArray.count != 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
}


class Convertor {
    class func convertDate(timeStamp: UInt, toFormat: String = "hh:mm a", addDay: Bool = false, sendDay: Bool = false) -> String {
        let dateFormatter = DateFormatter()
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        let hourFormat = "hh:mm a"
        dateFormatter.dateFormat = hourFormat
        
        if sendDay,
           NSCalendar.current.isDateInToday(date) {
            return "Today"
        } else if sendDay,
                  NSCalendar.current.isDateInYesterday(date) {
            return "Yesterday"
        } else if addDay,
                  NSCalendar.current.isDateInToday(date) {
            return "Today, \(dateFormatter.string(from: date))"
        } else if addDay,
                  NSCalendar.current.isDateInYesterday(date) {
            return "Yesterday, \(dateFormatter.string(from: date))"
        } else {
            dateFormatter.dateFormat = toFormat
            return dateFormatter.string(from: date)
        }
    }
    
    class func convertDate(dateInString: String, fromFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", toFormat: String = "hh:mm a") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let date1 = dateFormatter.date(from: dateInString)!
        
        let diffBtwDatesInMin = Calendar.current.dateComponents([.minute], from: date1, to: Date()).minute ?? 0
        if diffBtwDatesInMin < 60 {
            return "\(diffBtwDatesInMin) min ago"
        }
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: date1)
    }
    
    class func convertStringToDate(dateInString: String, fromFormat: String = "yyyy-MM-dd") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        return dateFormatter.date(from: dateInString)!
    }
}

//MARK:-
//MARK:- APi calling.

extension Class_StageTwo {
    
    func moveToProfile(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let objProfileVC = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        objProfileVC.isComeFromMatchIfo.status = true
        objProfileVC.isComeFromMatchIfo.user_id =  matchInfo.user_id
        self.navigationController?.pushViewController(objProfileVC, animated: true)
    }
    
    // http://localhost:3000/api/profile-show-hide
    func updateStatusOfProfileIndicator(profileIndicator  : Int){
        
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        matchesVM.profileIndicatorUpdateAPI(match_id: matchInfo.match_id, profile_indicator : profileIndicator) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                self.profile_indicator = profileIndicator
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
            else
            {
                if message == "No Data Available." || message == "No more data."
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
    
    func removeMatch_api()
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        matchesVM.removeMatchAPI(match_id: matchInfo.match_id, index: 0, like_id: "") { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                if self.comeFromScreenStatus == .fromMatch {
                    
                    self.navigationController?.popViewController(animated: false)
                    self.backToPostVCDelegate.backToPostVc()
                    
                }else if self.comeFromScreenStatus == .fromChat {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else
            {
                if message == "No Data Available." || message == "No more data."
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
                else
                {
                    SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
                }
            }
        }
    }
    
    // get profile info:
    func getProfileInfo(openUser_id : String)
    {
        var profilePicture = ""
        Loader.sharedInstance.showLoader(msg: "Loading...")
        profileVM.getProfileApi(user_id: openUser_id) { (status, message, dictData)  in
            Loader.sharedInstance.stopLoader()
            
            if status == "success"
            {
                let item = dictData.object(forKey: "data") as! NSDictionary
                
                // get info :
                let userName = item.object(forKey: "name") as? String
                
                let bio =  item.object(forKey: "Description") as! String
                let Work =  item.object(forKey: "Work") as! String
                let Education =  item.object(forKey: "Education") as! String
                
                if let image = item.object(forKey: "post_image") as? String {
                    if image == "" {
                        let picture_img = item.object(forKey: "picture") as? String ?? "--"
                        profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(picture_img)"
                    } else {
                        profilePicture = "\(APPURL.baseAPI.imageURL)post_pictures/\(image)"
                    }
                } else {
                    let picture_img = item.object(forKey: "picture") as? String ?? "--"
                    profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(picture_img)"
                }
                
                
//                let profilePicture = "\(APPURL.baseAPI.imageURL)profile_pictures/\(item.object(forKey: "picture") as! String)"
                
                let mainStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                
                let objProfileEditVC = mainStoryBoard.instantiateViewController(withIdentifier: "profileEditVC") as! profileEditVC
                objProfileEditVC.valueFromPreviousScreen.profileName = userName ?? ""
                objProfileEditVC.valueFromPreviousScreen.strProfilePicture = profilePicture
                objProfileEditVC.activeComeFromStatus = .matchScreen
                objProfileEditVC.valueFromPreviousScreen.bio = bio
                objProfileEditVC.valueFromPreviousScreen.work = Work
                objProfileEditVC.valueFromPreviousScreen.education = Education
                if self.profile_visibility == 0 {
                    objProfileEditVC.actionScreen = .readOnlyWithBlur
                }else{
                    objProfileEditVC.actionScreen = .readOnly
                }
                AppDelegate.shared.navController = self.navigationController!
                self.navigationController?.pushViewController(objProfileEditVC, animated: true)
                
            }else{
                
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}

extension  Class_StageTwo {  // Report API:
    
    func callReport( to_user : String, report_reason : String)
    {
        Loader.sharedInstance.showLoader(msg: "Loading...")
        
        let userID = "\(Constants.userDefault.object(forKey: Variables.user_id) ?? "")"
        matchesVM.reportPost_Api(from_user: userID, to_user: to_user, report_reason: report_reason) { (status, message) in
            Loader.sharedInstance.stopLoader()
            
            self.navigationController?.popViewController(animated: true)
            SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
        
        }
    }
}
