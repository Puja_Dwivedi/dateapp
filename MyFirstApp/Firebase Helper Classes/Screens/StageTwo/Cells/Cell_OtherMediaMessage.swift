//
//  Cell_OtherMediaMessage.swift
//

import UIKit
protocol OtherMediaCellDelegate: AnyObject {
    func btnOpenOtherMedia(cell: Cell_OtherMediaMessage)
}
class Cell_OtherMediaMessage: UITableViewCell {
    @IBOutlet var view_Background: UIView!
    @IBOutlet var imgView_Thumb: UIImageView!
    @IBOutlet var lbl_Time: UILabel!
    @IBOutlet var btn_OpenMedia: UIButton!
    weak var delegate: OtherMediaCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.view_Background.layer.cornerRadius = 5
            self.view_Background.layer.borderColor = UIColor.white.cgColor
            self.view_Background.layer.borderWidth = 6.0
            self.view_Background.clipsToBounds = true
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func btnOpenMediaClicked( _ sender : UIButton){
           delegate?.btnOpenOtherMedia(cell: self)
    }
}
