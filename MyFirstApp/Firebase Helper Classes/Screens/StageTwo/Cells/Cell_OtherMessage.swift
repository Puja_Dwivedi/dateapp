//
//  Cell_OtherMessage.swift
//

import UIKit

class Cell_OtherMessage: UITableViewCell {
    @IBOutlet var lbl_Message: UILabel!
    @IBOutlet var lbl_Time: UILabel!
    @IBOutlet var view_BackGround: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DispatchQueue.main.async {
            //self.view_BackGround.layer.cornerRadius = 12.5
            self.view_BackGround.clipsToBounds = true
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
