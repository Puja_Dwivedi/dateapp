//
//  Cell_MyMessage.swift
//

import UIKit

class Cell_MyMessage: UITableViewCell {
    @IBOutlet var lbl_Message: UILabel!
    @IBOutlet var view_BackGround: UIView!
    @IBOutlet var lbl_Time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DispatchQueue.main.async {
            self.view_BackGround.layer.cornerRadius = 12.5
            self.view_BackGround.clipsToBounds = true
            self.view_BackGround.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func functionAddGradient() {
        DispatchQueue.main.async {
            
            if let isLayer = self.view_BackGround.layer.sublayers, isLayer[0] is CAGradientLayer {isLayer[0].removeFromSuperlayer()}
            let gradient = CAGradientLayer()
            gradient.frame = self.view_BackGround.bounds
            gradient.colors = [UIColor.init(red: 177/255, green: 187/255, blue: 255/255, alpha: 1.0).cgColor, UIColor.init(red: 0/255, green: 173/255, blue: 210/255, alpha: 1.0).cgColor]
            
            self.view_BackGround.layer.insertSublayer(gradient, at: 0)
        }
    }
}

