//
//  Cell)_MyMediaMessage.swift
//

import UIKit

//1. delegate method
protocol MyMediaCellDelegate: AnyObject {
    func btnOpenMedia(cell: Cell_MyMediaMessage)
}

class Cell_MyMediaMessage: UITableViewCell {
    @IBOutlet var view_Background: UIView!
    @IBOutlet var imgView_Thumb: UIImageView!
    @IBOutlet var lbl_Time: UILabel!
    @IBOutlet var btn_OpenMedia: UIButton!
    weak var delegate: MyMediaCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.view_Background.layer.cornerRadius = 5
            self.view_Background.layer.borderColor = UIColor.init(named: "chat_bubble_color_sent")?.cgColor
            self.view_Background.layer.borderWidth = 6.0
            self.view_Background.clipsToBounds = true
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func btnOpenMediaClicked( _ sender : UIButton){
        delegate?.btnOpenMedia(cell: self)
    }
}
