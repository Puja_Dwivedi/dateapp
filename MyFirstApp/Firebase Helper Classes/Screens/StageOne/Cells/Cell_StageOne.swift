//
//  Cell_StageOne.swift
//

import UIKit

class Cell_StageOne: UITableViewCell {
    @IBOutlet var btn_ImgView: UIButton!
    @IBOutlet var lbl_Name: UILabel!
    @IBOutlet var btn_Status: UIButton!
    @IBOutlet var lblLastMessage : UILabel!
    @IBOutlet var lblTime : UILabel!
    @IBOutlet var lblUnreadCount : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btn_Status.layer.cornerRadius = 5
        self.btn_Status.clipsToBounds = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
