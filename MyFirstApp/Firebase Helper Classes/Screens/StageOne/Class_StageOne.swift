//
//  Class_StageOne.swift
//

import UIKit
import Kingfisher

class Class_StageOne: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Variables
    @IBOutlet var tableView: UITableView!
    @IBOutlet var SegmentOnlineOfline: UISegmentedControl!
    @IBOutlet var tableHeaderView: UIView!
    private var tableData:[HSChatUser] = []
    @IBOutlet weak var sbSearchBar: UISearchBar!
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showBackButtonWith(Image: UIImage.init(named: "back") ?? UIImage.init(named: "")!)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "HAPPIU"
        self.tableView.register(UINib.init(nibName: "Cell_StageOne", bundle: nil), forCellReuseIdentifier: "Cell_StageOne")
        self.tableView.tableFooterView = UIView()
        HSRealTimeMessagingLIbrary.function_GetUserDetails(HSRealTimeMessagingLIbrary.HSCurrentUserID()) { (isdata) in
            
            let isOnline = RV_CheckDataType.getBoolValue(obj: isdata["status"] as AnyObject)
            if isOnline {self.SegmentOnlineOfline.selectedSegmentIndex = 1} else {self.SegmentOnlineOfline.selectedSegmentIndex = 0}
        }
        if let textfield = sbSearchBar.value(forKey: "searchField") as? UITextField {
            
            textfield.backgroundColor = UIColor.init(red: 245/255.0, green: 249/255.0, blue: 254/255.0, alpha: 1.0)
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = UIColor.black
            }
        }
        self.function_LoadData()
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: Buttons
    @IBAction func SegmentedControllerOnlineOfline(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            HSRealTimeMessagingLIbrary.function_UpdateOnlineStatus(HSRealTimeMessagingLIbrary.HSCurrentUserID(), false)
        case 1:
            HSRealTimeMessagingLIbrary.function_UpdateOnlineStatus(HSRealTimeMessagingLIbrary.HSCurrentUserID(), true)
        default:
            return
        }
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    //MARK: Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_StageOne") as? Cell_StageOne else { return UITableViewCell.init(style: .default, reuseIdentifier: "Cell_StageOne") }
        
        HSRealTimeMessagingLIbrary.function_GetUserDetails(self.tableData[indexPath.row].id) { (isdata) in
            let isname = isdata["name"] as! String
            cell.lbl_Name.text = isname
            
            cell.btn_ImgView.imageView?.kf.indicatorType = .activity
            cell.btn_ImgView.imageView?.kf.setImage(with: URL(string: RV_CheckDataType.getString(anyString: isdata["picture"] as AnyObject)))
            
            cell.btn_ImgView.layer.cornerRadius = cell.btn_ImgView.frame.size.height/2
            cell.lblLastMessage.text = self.tableData[indexPath.row].lastMessage
            let isOnline = RV_CheckDataType.getBoolValue(obj: isdata["status"] as AnyObject)
            if isOnline {
                cell.btn_Status.backgroundColor = UIColor.green
            } else {
                cell.btn_Status.backgroundColor = UIColor.lightGray
            }
            HSRealTimeMessagingLIbrary.function_getUnreadCount(_isReceiverID: UInt64(RV_CheckDataType.getInteger(anyString: isdata["id"] as AnyObject)), _isSenderID: HSRealTimeMessagingLIbrary.HSCurrentUserID()) { (count) in
                
                if count == 0 {
                    cell.lblUnreadCount.text = ""
                    
                }else{
                    cell.lblUnreadCount.text =  "\(RV_CheckDataType.getString(anyString: count as AnyObject))"
                }
                cell.lblUnreadCount.cornerRadius = cell.lblUnreadCount.frame.size.width/2
                cell.lblUnreadCount.textColor = .white
                cell.lblUnreadCount.clipsToBounds = true
            }
        }
        let backView = UIView.init(frame: cell.contentView.frame); backView.backgroundColor = .clear
        cell.selectedBackgroundView = backView
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
        vc.chatDetail = self.tableData[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: Functions
    func function_LoadData() {
        HSRealTimeMessagingLIbrary.function_LoadChatUsers(isID: HSRealTimeMessagingLIbrary.HSCurrentUserID()) { (user) in
            
            if user.id != 0 {
                self.tableData.append(user)
                self.tableView.reloadData()
            }
        }
    }
}
