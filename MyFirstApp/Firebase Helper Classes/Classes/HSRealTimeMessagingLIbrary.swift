//
//  HSFireBase.swift
//
//
import Foundation
import AVKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import CoreLocation

// variables:
let pushNotificationVM = PushNotificationClass()

class HSUser {
    var name: String = ""
    var email: String = ""
    var id: UInt64 = 0
    var picture: String = ""
    
    init(isName: String, isEmail: String, isID: UInt64, isPicture: String) {
        self.name = isName
        self.email = isEmail
        self.id = isID
        self.picture = isPicture
    }
}

class HSChatUser {
    var chatID: String = ""
    var id: UInt64 = 0
    var name: String = ""
    var time : String = ""
    var lastMessage : String = ""
    init(isChatID: String, isID: UInt64, isName: String, isTime : String, isLastMessage : String) {
        self.chatID = isChatID
        self.id = isID
        self.name = isName
        self.time = isTime
        self.lastMessage = isLastMessage
    }
}

class HSMessage: Equatable {
    static func == (lhs: HSMessage, rhs: HSMessage) -> Bool {
        return lhs.message == rhs.message &&
            lhs.type == rhs.type &&
            lhs.time == rhs.time &&
            lhs.senderName == rhs.senderName &&
            lhs.senderID == rhs.senderID &&
            lhs.chatID == rhs.chatID &&
            lhs.msgID == rhs.msgID
    }
    
    var message: String = ""
    var type: String = ""
    var time: UInt64 = 0
    var senderName: String = ""
    var senderID: UInt64 = 0
    var chatID: String = ""
    var msgID: String = ""
    init(isMessage: String, isType: String, isTime: UInt64, isSenderName: String, isSenderID: UInt64, isChatID: String, isMsgID: String) {
        self.message = isMessage
        self.type = isType
        self.time = isTime
        self.senderName = isSenderName
        self.senderID = isSenderID
        self.chatID = isChatID
        self.msgID = isMsgID
    }
}

class HSRealTimeMessagingLIbrary {
    
    //Private Function
    class func function_CreatChatID(_ isSender: UInt64, _ isReceiver: UInt64) -> String {
        if isSender > isReceiver { return "\(isSender):\(isReceiver)" } else { return "\(isReceiver):\(isSender)" }
    }
    
    private class func function_UpdateChatList(_ isMsg: String, isSenderName: String, isReciverName: String, isSenderID: UInt64, isReciverID: UInt64, isReciverImage: String, isSenderImage: String) {
        let time = UInt64(Date().timeIntervalSince1970)
        for i in 0...1 {
            if i == 0 {
                let _:[String:Any] = [
                    "text":"\(isMsg)",
                    "id":time,
                    "sender_name":"\(isSenderName)",
                    "senderId":"\(isSenderID)",
                    "createdAt":"\(time)",
                    "receiverId" : "\(isReciverID)",
                    "friend_name" : "\(isReciverName)",
                    "friend_image" : isReciverImage,
                    "sender_image" : isSenderImage
                ]
                
            } else {
                let _:[String:Any] = [
                    "text":"\(isMsg)",
                    "id":time,
                    "sender_name":"\(isSenderName)",
                    "senderId":"\(isSenderID)",
                    "createdAt":"\(time)",
                    "receiverId" : "\(isReciverID)",
                    "friend_name" : "\(isSenderName)",
                    "friend_image" : isSenderImage,
                    "sender_image" : isSenderImage
                ]
            }
        }
        
        function_getandUpdateUnreadMessageCount(_isSenderID: isSenderID, _isReceiverID: isReciverID)
    }
    class func function_getUnreadCount( _isReceiverID : UInt64, _isSenderID : UInt64, _ Success:@escaping(Int)-> Void) {    Database.database().reference().child("user").child("\(_isSenderID)").child("recent_chat").child("\(self.function_CreatChatID(_isSenderID, _isReceiverID))").child("unread_count").observe(.value) { (datashot) in
        let unread_count = RV_CheckDataType.getInteger(anyString: datashot.value as AnyObject)
        Success(unread_count)
    }
    Success(0)
    
    }
    
    class func function_updateUnreadCount( _isSenderID : UInt64, _isReceiverID : UInt64, isunread_count: NSInteger){
        Database.database().reference().child("user").child("\(_isSenderID)").child("recent_chat").child("\(self.function_CreatChatID(_isSenderID, _isReceiverID))").updateChildValues(["unread_count" : isunread_count])
        
    }
    class func function_getandUpdateUnreadMessageCount( _isSenderID: UInt64, _isReceiverID: UInt64){
        Database.database().reference().child("user").child("\(_isReceiverID)").child("recent_chat").child("\(self.function_CreatChatID(_isSenderID, _isReceiverID))").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                //fcmKey
                let data = snapshot.value as! [String: AnyObject]
                let dictData = RV_CheckDataType.getDictionary(anyDict: data as AnyObject)
                let chatId = dictData.allKeys as NSArray
                var unread_count = 0
                if chatId.contains("unread_count")
                {
                    unread_count = RV_CheckDataType.getInteger(anyString: dictData.value(forKey: "unread_count") as AnyObject)
                    unread_count = unread_count + 1
                }
                Database.database().reference().child("user").child("\(_isReceiverID)").child("recent_chat").child("\(self.function_CreatChatID(_isSenderID, _isReceiverID))").updateChildValues(["unread_count" : unread_count])
            } else {
                
            }
        })
    }
    
    //Update Online Status
    class func function_GetUserDetails(_ isID: UInt64, _ Success:@escaping([String : Any])-> Void) {
        Database.database().reference().child("user").child("\(isID)").observe(.value) { (dataSnap) in
            if let isData = dataSnap.value as? [String: Any], isData.isEmpty == false {
                Success((isData as [String : Any]))
            }
        }
    }
    
    //CreatUser
    class func function_CreatAndUpdateUser(_ HSUser: HSUser) {
        let user:[String:Any] = [
            "email":"\(HSUser.email)",
            "name":"\(HSUser.name)",
            "image":"\(HSUser.picture)",
            "id":"\(HSUser.id)"
        ]
        
        Database.database().reference().child("user").child("\(HSUser.id)").updateChildValues(user) { (error, dataRef) in
            if let _ = error {
                self.function_CreatAndUpdateUser(HSUser)
            }
        }
    }
    
    //LoadChat
    class func function_LoadChatUsers(isID: UInt64, success:@escaping(HSChatUser)-> Void) {
        Database.database().reference().child("user").child("\(isID)").child("recent_chat").observe(.childAdded) { (dataSnap) in
            if let isUser = dataSnap.value as? [String:Any] {
                var friendsID = ""
                if self.HSCurrentUserID() == UInt64(isUser["receiverId"] as! String){
                    friendsID = isUser["senderId"] as! String
                }else{
                    friendsID = isUser["receiverId"] as! String
                }
                success(HSChatUser.init(isChatID: "\(self.function_CreatChatID(UInt64(isUser["senderId"] as! String) ?? 0, UInt64(isUser["receiverId"] as! String) ?? 0))", isID: UInt64(friendsID)! , isName: "\(isUser["friend_name"] as? String ?? "")", isTime: "\(isUser["id"]!)", isLastMessage: "\(isUser["text"] as? String ?? "")"))
            }
        }
    }
    //LoadMessages
    class func function_GetFirstTenMessages( chatID: String,  success:@escaping(HSMessage)-> Void) {
        Database.database().reference().child("chat").child("\(chatID)").queryOrderedByKey().queryLimited(toLast: 20).observe(.childAdded) { (datashot) in
            if let isData = datashot.value as? [String:Any] {
                let message = HSMessage.init(isMessage: "\(isData["text"] as? String ?? "")", isType: "\(isData["type"] as? String ?? "text")", isTime: isData["id"] as? UInt64 ?? 0, isSenderName: "\(isData["sender_name"] as? String ?? "")", isSenderID: UInt64(isData["senderId"] as! String)!, isChatID: "\(isData["chatID"] as? String ?? "")", isMsgID: "\(isData["id"] as? String ?? "")")
                success(message)
            }
        }
    }
    class func function_GetAllMessages( chatID: String,  lastMsgID: String, _ success:@escaping([HSMessage])-> Void) {
        Database.database().reference().child("chat").child("\(chatID)").queryOrderedByKey().queryLimited(toLast: 20).queryEnding(atValue: "\(lastMsgID)").observeSingleEvent(of: .value) { (datashot) in
            if let isData = datashot.value as? [String:Any], isData.count != 0 {
                var tempData:[HSMessage] = []
                for item in isData.keys {
                    if item != "\(lastMsgID)" {
                        if let isMessage = isData["\(item)"] as? [String:Any], isMessage.isEmpty != true {
                            let message = HSMessage.init(isMessage: "\(isData["text"] as? String ?? "")", isType: "\(isData["type"] as? String ?? "text")", isTime: isData["id"] as? UInt64 ?? 0, isSenderName: "\(isData["sender_name"] as? String ?? "")", isSenderID: UInt64(isData["senderId"] as! String)!, isChatID: "\(isData["chatID"] as? String ?? "")", isMsgID: "\(isData["id"] as? String ?? "")")
                            tempData.append(message)
                        }
                    }
                }
                success(tempData)
            }
        }
    }
    
    class func function_SendTextMessage(_ isMsg: String, match_id : Int, otherUser_id : Int , isSenderName: String, isReciverName: String, isSenderID: UInt64, isReciverID: UInt64, isReciverImage: String, isSenderImage: String, to_post_id : String, isSuccess:@escaping()-> Void, isError:@escaping(String)-> Void) {
        let time = UInt64(Date().timeIntervalSince1970)
        let message:[String:Any] = [
            "text":"\(isMsg)",
            "id":time,
            "sender_name":"\(isSenderName)",
            "senderId":"\(isSenderID)",
            "createdAt":"\(time)",
            "receiverId" : "\(isReciverID)",
            "unread_count" : "\(0)",
            "friend_name" : "\(isReciverName)",
            "friend_image" : isReciverImage,
            "sender_image" : isSenderImage,
            "event_id" : to_post_id
        ]
        
        let messageID = "\(self.function_CreatChatID(isSenderID, isReciverID)):\(to_post_id)"
        
        Database.database().reference().child("chat").child(messageID).child("\(time)").setValue(message) { (error, dataRefrence) in
            if let isSomeError = error {
                isError(isSomeError.localizedDescription)
            } else {
                
                sendPushNotification(toID: RV_CheckDataType.getString(anyString: isReciverID as AnyObject), message: isMsg,chatID: "\(self.function_CreatChatID(isSenderID, isReciverID))", match_id : match_id, otherUser_id : otherUser_id)
                
                isSuccess()
            }
        }
    }
    
    
    class func sendPushNotification(toID : String, message : String, match_id :Int, otherUser_id : Int)
    {
        // api calling :
        pushNotificationVM.callPushNotification_Api(otherUser_id : otherUser_id, match_id: match_id, message: message) { (_, _) in
            print("success send message")
        }
    }
    
    
    // Send notifications
    class func sendPushNotification(toID : String, message : String, chatID : String, match_id :Int, otherUser_id : Int)
    {
        // api calling :
        pushNotificationVM.callPushNotification_Api(otherUser_id : otherUser_id, match_id: match_id, message: message) { (_, _) in
            // print("success send message")
        }
    }
    
    class func function_UpdateOnlineStatus(_ isID: UInt64, _ isOnline: Bool) {
        Database.database().reference().child("user").child("\(isID)").updateChildValues(["status" : isOnline])
    }
    
    //User
    class func HSCurrentUserID() -> UInt64 {
        
        if RV_CheckDataType.getInteger(anyString: Constants.userDefault.object(forKey: Variables.user_id) as AnyObject) != 0{
            return UInt64(RV_CheckDataType.getInteger(anyString: Constants.userDefault.object(forKey: Variables.user_id) as AnyObject))
        }else{
            return 0
        }
    }
    class func HSCurrentUserName() -> String {
        return RV_CheckDataType.getString(anyString: Constants.userDefault.object(forKey: Variables.name) as AnyObject)
    }
    class func fcmKeyFor(userId : String, completion: @escaping (String) -> Swift.Void)
    {
        Database.database().reference().child("user").child("\(userId)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                //fcmKey
                let data = snapshot.value as! [String: AnyObject]
                let dictData = RV_CheckDataType.getDictionary(anyDict: data as AnyObject)
                let chatId = dictData.allKeys as NSArray
                if chatId.contains("fcmtoken")
                {
                    let fcmvToken = RV_CheckDataType.getString(anyString: dictData.value(forKey: "fcmtoken") as AnyObject)
                    completion(fcmvToken)
                }
                else { completion("") }
            } else {
                completion("")
            }
        })
    }
}
