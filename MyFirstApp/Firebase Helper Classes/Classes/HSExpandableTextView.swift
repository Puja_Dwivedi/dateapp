//
//  HSExpandableTextView.swift
//

//

import UIKit

class HSExpandableTextView: UIView, UITextViewDelegate {
    //MARK: Variable
    @IBOutlet var contentView: UIView!
    @IBOutlet var btnMedia : UIButton!
    @IBOutlet var btnMediaWidthConstraints : NSLayoutConstraint!
    @IBOutlet var activityIndicator: UIView!
    @IBOutlet var txtView_Msg: UITextView!
    var sendMessage:((String)->Void)! = nil
    var sendMedia:(()->Void)! = nil
    
    let placeholderLabel: UILabel = {
        let label = UILabel()
        label.text = "Enter message"
        label.textColor = #colorLiteral(red: 0.7921568627, green: 0.7921568627, blue: 0.8, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    //MARK: Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.function_init()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.function_init()
    }
    //MARK: Button
    @IBAction func btn_Send(_ sender: UIButton) {
        if let isString = self.txtView_Msg.text, isString.replacingOccurrences(of: " ", with: "") != "" {
            self.txtView_Msg.text = ""
            self.sendMessage("\(isString)")
            self.setupPlaceholder()
        }
    }
    @IBAction func btn_SelectMedia(_ sender: UIButton) {
        self.sendMedia()
    }
    //MARK: Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" { textView.resignFirstResponder() }
        return true
    }
    //MARK: Function
    func function_init() {
        Bundle.main.loadNibNamed("HSExpandableTextView", owner: self, options: nil)
        addSubview(contentView)
        
        let leadingConstraint = contentView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16)
        leadingConstraint.isActive = true
        let trailingConstraint = contentView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16)
        trailingConstraint.isActive = true
        
        self.contentView.borderWidth = 1
        self.contentView.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        self.contentView.cornerRadius = 30.0
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth ]
        DispatchQueue.main.async {
            self.txtView_Msg.clipsToBounds = true
            self.txtView_Msg.textContainer.heightTracksTextView = true
            self.txtView_Msg.isScrollEnabled = false
            self.setupPlaceholder()
        }
    }
    
    private func setupPlaceholder() {
        txtView_Msg.addSubview(placeholderLabel)

        // Set constraints for the placeholder label
        NSLayoutConstraint.activate([
            placeholderLabel.leadingAnchor.constraint(equalTo: txtView_Msg.leadingAnchor, constant: 5),
            placeholderLabel.topAnchor.constraint(equalTo: txtView_Msg.topAnchor, constant: 8),
            placeholderLabel.trailingAnchor.constraint(equalTo: txtView_Msg.trailingAnchor, constant: -5),
            placeholderLabel.heightAnchor.constraint(equalToConstant: 20)
        ])

        // Add an observer to detect when the text in the UITextView changes
        NotificationCenter.default.addObserver(self, selector: #selector(textViewDidChange), name: UITextView.textDidChangeNotification, object: txtView_Msg)

        // Show or hide the placeholder based on the initial text
        placeholderLabel.isHidden = !txtView_Msg.text.isEmpty
    }

  
    @objc private func textViewDidChange() {
        placeholderLabel.isHidden = !txtView_Msg.text.isEmpty
    }


}
