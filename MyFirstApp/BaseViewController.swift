//
//  BaseViewController.swift
//  Amen App
//
//  Created by cis on 13/01/20.
//  Copyright © 2020 cis. All rights reserved.
//

import UIKit
import Firebase

class BaseViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet var scrollView : UIScrollView?
    var tap: UITapGestureRecognizer = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        view.addGestureRecognizer(tap)
    }
    
    func showBackButtonWith(Image : UIImage) {
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: Image, style: .plain, target: self, action: #selector(btnBackClicked(sender:)))
    }
   
    func backToPreviousController(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func keyboardHide(){
        self.view.endEditing(true)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view is UIControl) {
            return false
        }
        if (touch.view?.superview is UITableViewCell)
        {
            return false
        }
        else
        {
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func btnBackClicked(sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension BaseViewController : UITextFieldDelegate
{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.scrollView?.contentSize = CGSize(width: 320, height: 1000)
        registerKeyboardNotifications()
    }
    
    func registerKeyboardNotifications() {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
    }
}
