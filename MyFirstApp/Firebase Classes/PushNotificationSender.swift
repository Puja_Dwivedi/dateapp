//
//  PushNotificationSender.swift

//

import Foundation
import UIKit

class PushNotificationSender {
    func sendPushNotification(to token: String, title: String, body: String, data : [String : AnyObject], chatID : String) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : token,
                                           "notification" : ["title" : title, "body" : body],
                                           "data" : ["body" : body,
                                                     "title": title,
                                                     "user_ID" : RV_CheckDataType.getString(anyString: Constants.userDefault.object(forKey: Variables.user_id) as AnyObject),
                                                     "username" :RV_CheckDataType.getString(anyString: Constants.userDefault.object(forKey: Variables.name) as AnyObject),
                                                     "chatID" : chatID]
        ]
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAAj3jrjls:APA91bHR7VL5pgsTe-wNx9aliw2Cl3SVG4rHMchm4_ojqyUprV2Y0aG0eMAq7OzzQeZTw9BRI4QMiyvWLYjW1Brcmd-bo-_tE8LjYBYtpbIL1jj1ZoO9CcH79XaeDuWP_MDHme6pmq6k", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
}
