//
//  PushNotificationClass.swift
//  MyFirstApp
//
//  Created by cis on 03/05/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class PushNotificationClass: NSObject {
    
    func callPushNotification_Api(otherUser_id : Int, match_id : Int, message : String, completion: @escaping (_ result: String, _ message : String) -> Void)
    {
        var msg  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.sendingNotification
        
        
        param.updateValue(otherUser_id as AnyObject, forKey:"user_id")
        param.updateValue(match_id as AnyObject, forKey: "match_id")
        param.updateValue(message as AnyObject, forKey: "message")
        
        
        //   print("header \(header) \n API: \(strService) \n request : \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 {
                    
                    if data.message == "" {
                        msg = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        msg = data.message
                    }
                    
                    completion("success", msg)
                    
                }else if dict.object(forKey: "code") as! Int == 400 {
                    
                    if data.message == "" {
                        msg = "Something went wrong,Please try again."
                    }else{
                        msg = data.message
                    }
                    completion("error", msg)
                }
            }
            else
            {
                if data.message == "" {
                    msg = "Something went wrong,Please try again."
                }else{
                    msg = data.message
                }
                
                completion("error", msg)
            }
        }
    }
}
