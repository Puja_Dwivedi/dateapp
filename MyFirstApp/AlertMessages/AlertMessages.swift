//
//  AlertMessages.swift
//  MyFirstApp
//
//  Created by cis on 28/07/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class AlertMessages: NSObject {
    static let shareInstance = AlertMessages()
    
    // server error:
    var somethingWentWrong = "Something went wrong. Please try again later"
    var noDataFound = "No data found"
    
    // login:
    let pleaseEnterMobile = "Enter mobile number"
    let PhoneNumberShouldBe = "Phone number should be"
    let digits = "digits"
    let pleaseEnterCountryCode = "Please select country code"
    
    // registration
    let registrationSuccessfully = "Registration successful"
    
    // search result screen
    let Thatsyourpost = "That’s your post!"
    
    //search respond scree:
    let PleaseWriteSomething = "Please write something"
    
    //Create Post
    var pleaseEnterTitle = "Enter a post title"
    var pleaseAddAtLeastOneTagInMyTags = "Please select/enter at least one tag under Me"
    var pleaseAddAtLeastOneTagInMyMatchTags = "Please select/enter at least one tag under You"
    var OnlyPublicPostsAvailableInYourArea = "Only public posts available in your area"
    
    var flightTitleAlert = "Enter a flight #"
    var trainTitleAlert = "Enter a train/line"
    var subwayTitleAlert = "Select/enter a rail line"
    var placeTitleAlert = "Select a place"
    var streetTitleAlert = "Select a street"
    var busTitleAlert = "Select/enter a bus line"
    
    var emptyMessageAlert = "Enter message"
    var messageLessTenAlert = "Minimum 10 characters"
    var messageMoreHundTitleAlert = "Maximum 100 characters"
    
    var pleaseSelectAtleastOneTag = "Please select/enter at least one tag"
    var completedAllTags = "Completed all tags"
    var enter5Chacrater = "Flight # should be at least 3 characters"
    var field = "field"
    var enterField = "Enter a"
    var flightMesage =  "Flight # should be at least 3 characters"
    var notEnough = "Not specific enough. Select a venue that can be clearly identified such as a bar, restaurant, grocery store, transit station, etc."
    var gotIt = "Got it"
    var doNotInclude = "Do not include any street number in your selection"
    var selectAValidStreet = "Select a valid street name"

    
    // settings:    recreate_days
    let areYouSureWantToLogout = "Log out of your account?"
    
    let yesLogOut = "Yes, log out"
    let cancel = "Cancel"
    let areYouSureWantToDelete = "You will only be able to recreate an account after"
    let deactivatingIt = "days of deactivating it"
    
    let yesDelete = "Yes, delete"
    let deleteNow = "Delete now"
    let accountDeactivatedSuccuessfully = "User account deactivated"
    
    //Post Screen:
    var noMatchAvailableForThisPost = "No match to display"
    var matchesConversationsRemoved = "Matches and chats related to this post will also be removed"
    var noPublicPostAvailable = "No public posts to display"
    var youAreAlreadyPostToday = "You can only post once per day. You will be able to post again tomorrow"
    
    
    
    
    //Contact us:
    var pleaseWriteSomething = "Message cannot be empty"
    
    // send message:
    let sendEmailSuccessfully = "Message sent"
    
    // edit screen:
    let informationAlreadyUpdated = "Information is already updated."
    
    
    //Create post for plane Alert:
    let somethingWrongTryAgain = "Something went wrong,Please try again."
    let someIssueAPICalling = "Something went wrong. Please try again later"
    let pleaseEnterDes = "Please enter description"
    let yourPostIsPublic = "Your post is public. It will be searchable by and visible to anyone"
    let yourPostIsPrivate = "Your post is private. It won’t be searchable by nor visible to anyone.\n\nYou will be matched with others only if the tags you’ve used to describe your crush and yourself are found on another post and vice versa.\n\nOnce your post is created, you will be able to manage your tags and select/deselect the ones you do require to match with others and the ones you don't anymore"
    
    // edit post:
    var pleaseSelectAtLeastOneTagInMyTags = "Select at least one tag under You"
    var pleaseSelectAtLeastOneTagInMyMatchTags = "Select at least one tag under Me"
    
    // Report screen:
    var reportPleaseEnter = "Please enter your report reason"
    
    //Match screen
    var YourProfileWillBe = "Your profile will be visible to your matches only when they share theirs"
}


