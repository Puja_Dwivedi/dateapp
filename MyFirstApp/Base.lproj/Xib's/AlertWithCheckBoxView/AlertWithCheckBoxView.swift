//
//  AlertWithCheckBoxView.swift
//  MyFirstApp
//
//  Created by cis on 24/08/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

class AlertWithCheckBoxView: UIView {
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var uiViewCenterY: NSLayoutConstraint!
    
    //MARK:-
    //MARK: Variables
    
    var onCloser : ((String,Bool)-> Void)!
    var isCheckBoxActive = false
    
    //MARK:-
    //MARK: Flow of Project :
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    //Main calling
    class func Show( alertTitle: String,btn1Title : String, btn2Title: String , onCompletion: @escaping (String, Bool)-> Void){
        let customPopUp = Bundle.main.loadNibNamed("AlertWithCheckBoxView", owner: self, options: nil)?[0] as! AlertWithCheckBoxView
        customPopUp.frame = UIScreen.main.bounds
        
        customPopUp.onCloser = onCompletion
        
        customPopUp.btn1.setTitle(btn1Title, for: .normal)
        customPopUp.btn2.setTitle(btn2Title, for: .normal)
        customPopUp.lblTitle.text = alertTitle
        customPopUp.btnCheckBox.setImage(#imageLiteral(resourceName: "uncheckbox"), for: .normal)
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        
        keyWindow?.addSubview(customPopUp)
        customPopUp.showWithAnimation()
    }
    
    
    //MARK:-
    //MARK:- Action buttons:
    
    @IBAction func actionClose(_ sender: UIButton) {
        removeWithAnimation()
    }
    
    @IBAction func actionBtn1(_ sender: UIButton) {
        onCloser(btn1.titleLabel?.text ?? "", isCheckBoxActive)
        removeWithAnimation()
    }
    
    
    @IBAction func actionBtn2(_ sender: UIButton) {
        onCloser(btn2.titleLabel?.text ?? "", isCheckBoxActive)
        removeWithAnimation()
    }
    
    @IBAction func actionCheckBox(_ sender: UIButton) {
        
        if isCheckBoxActive {
            isCheckBoxActive = false
            btnCheckBox.setImage(#imageLiteral(resourceName: "uncheckbox"), for: .normal)
            
        }else{
            isCheckBoxActive = true
            btnCheckBox.setImage(#imageLiteral(resourceName: "checkbox"), for: .normal)
        }
    }
    
    
    //MARK: Methods
    //MARK:-
    
    func showWithAnimation(){
        uiView.center.y =   -UIScreen.main.bounds.height - 200
        uiView.alpha = 0
        
        UIView.animate(withDuration: 0.6) {
            self.uiView.center.y = UIScreen.main.bounds.height/2
            self.uiView.alpha = 1
        }
    }
    
    func removeWithAnimation(){
        self.layoutIfNeeded()
        uiView.center.y = UIScreen.main.bounds.height/2
        uiView.alpha = 1
        UIView.animate(withDuration: 0.6) {
            self.uiView.center.y = -UIScreen.main.bounds.height - 200
            self.uiView.alpha = 0
        } completion: {_ in
            self.removeFromSuperview()
        }
    }
}
