//
//  AlertViewS.swift
//

import UIKit

class AlertViewS: UIView {
    
    // enum :
    enum AlignmentType {
        case Horizontal
        case Vertical
    }
    
   static let shared = AlertViewS()

    //=====================================
    //MARK: Outlats :
    //=====================================
    var arr = NSArray();
    var AlignmentType : AlignmentType!
    
    //=====================================
    //MARK: variables :
    //=====================================
    var onCompletion : ((String)-> Void)!
    var alertUIView : UIView!
    var  stackView : UIStackView!
    var scrollView : UIScrollView!
    let gradientLayer = CAGradientLayer()
    var gradientBtnView = UIView.init()
    var btn = [UIButton]()

    //=====================================
    //MARK: Project Flow  :
    //=====================================
    override func awakeFromNib() {

    }
    
    // single color : set
    func Show(AlertTitle: String,Message : String, ThemeColor : [UIColor], BtnAlignmentType : AlignmentType, ButtonOfArrayInString : NSArray, compblock: @escaping (String)-> Void){
       
        let customAlert = AlertViewS()
        customAlert.onCompletion = compblock;
        customAlert.arr = ButtonOfArrayInString;
    
        customAlert.displayAlert(AlertTitle: AlertTitle , Message: Message, ThemeColor: ThemeColor , BtnAlignmentType: BtnAlignmentType)
    }
    
    func displayAlert(AlertTitle : String, Message : String, ThemeColor : [UIColor] , BtnAlignmentType :AlignmentType ){
        
        self.AlignmentType = BtnAlignmentType
        
        if arr.count == 1 {
            self.AlignmentType = AlertViewS.AlignmentType.Vertical
        }
        
        //================ Root view :
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5);
        
        
        //================= AlertUIView :
        alertUIView = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 20 , height: 20))
        alertUIView.backgroundColor = UIColor.white
        alertUIView.center = self.center
        alertUIView.layer.cornerRadius = 5
        
        scrollView = UIScrollView.init()
        scrollView.frame = CGRect(x: 0, y: 0, width: self.frame.width - 20 , height: 10)
        scrollView.backgroundColor = UIColor.clear
        
        //================= Alert title :
        let  titleUILabel = UILabel.init(frame: CGRect(x: 10, y: 20, width: UIScreen.main.bounds.width - 40, height: 20))
        titleUILabel.text = AlertTitle
        titleUILabel.textAlignment = .center
        titleUILabel.numberOfLines = 0
        titleUILabel.frame.size.height =  getLabelHeight(titleUILabel)
        scrollView.addSubview(titleUILabel)
        
        
        //================= Message title :
        let messageUILabel = UILabel.init(frame: CGRect(x: 10, y: titleUILabel.frame.size.height + 40, width: UIScreen.main.bounds.width - 40, height: 20))
        messageUILabel.text = Message
        messageUILabel.textAlignment = .center
        messageUILabel.numberOfLines = 0
        messageUILabel.frame.size.height = getLabelHeight(messageUILabel)
        scrollView.addSubview(messageUILabel);
        
        
        // Root Scroll view set frame :
        //==== : total completeHeightCount
        let completeHeightCount   = 20  + (self.AlignmentType == AlertViewS.AlignmentType.Horizontal ? ( arr.count % 3 == 0  ? (arr.count/3) * 40 : ((arr.count/3) + 1) * 40): arr.count * 40);
        
        let calclulatedRemainHeight =  (UIScreen.main.bounds.size.height - 50 - (titleUILabel.frame.size.height + messageUILabel.frame.size.height + 60))
        
        let totalHeight: CGFloat = CGFloat(completeHeightCount) > UIScreen.main.bounds.size.height ?  calclulatedRemainHeight : (CGFloat(completeHeightCount - 20))
        
        
        scrollView.frame.size.height = (titleUILabel.frame.size.height  + messageUILabel.frame.size.height + 60) + totalHeight
        scrollView.isScrollEnabled = true
        
        scrollView.contentSize = CGSize(width: (UIScreen.main.bounds.size.width - 20), height: (titleUILabel.frame.size.height + messageUILabel.frame.size.height + 60 ) + CGFloat(completeHeightCount - 20))
        
        // ================= Array of button :
        let  rootStackView = UIStackView.init(frame: CGRect(x: 10, y: titleUILabel.frame.size.height + messageUILabel.frame.size.height + 60, width: UIScreen.main.bounds.size.width - 40 , height: CGFloat(completeHeightCount - 20) ));
        
        rootStackView.spacing = 10
        rootStackView.distribution = .fillEqually
        rootStackView.axis = .vertical
        
        for i in 0..<arr.count {
            if self.AlignmentType == AlertViewS.AlignmentType.Horizontal  { // horizontal
                
                if i%3 == 0 {
                    self.stackView = UIStackView.init()
                    self.stackView.frame = CGRect(x: 10, y: 30, width: UIScreen.main.bounds.size.width - 20, height: 40)
                    stackView.spacing = 10
                    stackView.distribution = .fillEqually
                    stackView.axis = .horizontal
                    rootStackView.addArrangedSubview(stackView)
                }
                
                let btn = UIButton.init()
                btn.layer.cornerRadius = 5
                
                btn.setTitleColor(UIColor.white, for: .normal)
                
                btn.setTitle(arr[i] as? String, for: .normal)
                
                btn.tag = i
                btn.addTarget(self, action: #selector(AlertViewS.actionStackButton(sender:)), for: .touchUpInside)
                
                stackView.addArrangedSubview(btn)
                
                if  i < ThemeColor.count{
                    btn.backgroundColor = ThemeColor[i]
                    
                    if ThemeColor[i] == UIColor.white {
                        btn.setTitleColor(UIColor.black, for: .normal)
                        btn.layer.borderColor = UIColor.black.cgColor
                        btn.layer.borderWidth = 1
                    }else if ThemeColor[i] == UIColor.black {
                        btn.layer.borderColor = UIColor.white.cgColor
                        btn.layer.borderWidth = 1
                        btn.setTitleColor(UIColor.white, for: .normal)
                    }else{
                        btn.setTitleColor(UIColor.white, for: .normal)
                    }
                    
                }else{
                    btn.backgroundColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
                }
                
            }else{ //============ Vertically managed :
                
                let btn = UIButton.init()
                btn.layer.cornerRadius = 5
                btn.setTitleColor(UIColor.white, for: .normal)
                btn.backgroundColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
                
                btn.setTitle(arr[i] as? String, for: .normal)
                btn.tag = i
                rootStackView.addArrangedSubview(btn)
                btn.addTarget(self, action: #selector(AlertViewS.actionStackButton(sender:)), for: .touchUpInside)
                
                if  i < ThemeColor.count{
                    btn.backgroundColor = ThemeColor[i]
                    if ThemeColor[i] == UIColor.white {
                        btn.setTitleColor(UIColor.black, for: .normal)
                        btn.layer.borderColor = UIColor.black.cgColor
                        btn.layer.borderWidth = 1
                        
                    }else if ThemeColor[i] == UIColor.black {
                        btn.setTitleColor(UIColor.white, for: .normal)
                        btn.layer.borderColor = UIColor.white.cgColor
                        btn.layer.borderWidth = 1
                    }else{
                        btn.setTitleColor(UIColor.white, for: .normal)
                    }
                }else{
                    btn.backgroundColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
                }
            }
        }
        
        //=================== Frame of alertView
        alertUIView.frame.size.height = scrollView.frame.size.height + 20
        alertUIView.center = self.center
        scrollView.addSubview(rootStackView)
        
        alertUIView.addSubview(scrollView)
        self.addSubview(alertUIView)
        
      //  UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(self)
        
        //============== Show with animation
        alertUIView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        alertUIView.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.alertUIView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alertUIView.alpha = 1
        }) { (true) in
            //   print("Date picker isplay: Done")
        }
    }
    

    //=================================================================================================
    
    // gradient color set :
    func Show(AlertTitleWithGradient: String,Message : String, ThemeColor : [(UIColor,UIColor)], BtnAlignmentType : AlignmentType, ButtonOfArrayInString : NSArray, viewController : UIViewController, compblock: @escaping (String)-> Void){
        

        let customAlert = AlertViewS()
        customAlert.onCompletion = compblock;
        customAlert.arr = ButtonOfArrayInString;
        
        customAlert.displayAlertWithGradient(AlertTitle: AlertTitleWithGradient , Message: Message, ThemeColor: ThemeColor , BtnAlignmentType: BtnAlignmentType,viewController : viewController )
    }
    
    
    // with gradient color set :
    func displayAlertWithGradient(AlertTitle : String, Message : String, ThemeColor : [(UIColor, UIColor)] , BtnAlignmentType :AlignmentType , viewController : UIViewController){
        
            
            self.AlignmentType = BtnAlignmentType
            
            if arr.count == 1 {
                self.AlignmentType = AlertViewS.AlignmentType.Vertical
            }
            
            //================ Root view :
            self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.backgroundColor = UIColor.black.withAlphaComponent(0.5);
            
            
            //================= AlertUIView :
            alertUIView = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 20 , height: 20))
            alertUIView.backgroundColor = UIColor.white
            alertUIView.center = self.center
            alertUIView.layer.cornerRadius = 20
            
            scrollView = UIScrollView.init()
            scrollView.frame = CGRect(x: 0, y: 0, width: self.frame.width - 20 , height: 10)
            scrollView.backgroundColor = UIColor.clear
            
            //================= Alert title :
            let  titleUILabel = UILabel.init(frame: CGRect(x: 10, y: 20, width: UIScreen.main.bounds.width - 40, height: 20))
            titleUILabel.text = AlertTitle
            titleUILabel.textAlignment = .center
            titleUILabel.numberOfLines = 0
            titleUILabel.frame.size.height =  getLabelHeight(titleUILabel)
            scrollView.addSubview(titleUILabel)
            
            
            //================= Message title :
            let messageUILabel = UILabel.init(frame: CGRect(x: 10, y: titleUILabel.frame.size.height + 40, width: UIScreen.main.bounds.width - 40, height: 20))
            messageUILabel.text = Message
            messageUILabel.textAlignment = .center
            messageUILabel.numberOfLines = 0
            messageUILabel.frame.size.height = getLabelHeight(messageUILabel)
            scrollView.addSubview(messageUILabel);
            
            
            // Root Scroll view set frame :
            //==== : total completeHeightCount
            let completeHeightCount   = 20  + (self.AlignmentType == AlertViewS.AlignmentType.Horizontal ? ( arr.count % 3 == 0  ? (arr.count/3) * 40 : ((arr.count/3) + 1) * 40): arr.count * 40);
            
            let calclulatedRemainHeight =  (UIScreen.main.bounds.size.height - 50 - (titleUILabel.frame.size.height + messageUILabel.frame.size.height + 60))
            
            let totalHeight: CGFloat = CGFloat(completeHeightCount) > UIScreen.main.bounds.size.height ?  calclulatedRemainHeight : (CGFloat(completeHeightCount - 20))
            
            
            scrollView.frame.size.height = (titleUILabel.frame.size.height  + messageUILabel.frame.size.height + 60) + totalHeight
            scrollView.isScrollEnabled = true
            
            scrollView.contentSize = CGSize(width: (UIScreen.main.bounds.size.width - 20), height: (titleUILabel.frame.size.height + messageUILabel.frame.size.height + 60 ) + CGFloat(completeHeightCount - 20))
            
            // ================= Array of button :
            let  rootStackView = UIStackView.init(frame: CGRect(x: 10, y: titleUILabel.frame.size.height + messageUILabel.frame.size.height + 60, width: UIScreen.main.bounds.size.width - 40 , height: CGFloat(completeHeightCount - 20) ));
            
            rootStackView.spacing = 10
            rootStackView.distribution = .fillEqually
            rootStackView.axis = .vertical
        
            for i in 0..<arr.count {
                if self.AlignmentType == AlertViewS.AlignmentType.Horizontal  { // horizontal
                      btn.append(UIButton.init())
                    
                    self.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
                    
                    if i%3 == 0 {
                      //  print("index : \(i)")
                        self.stackView = UIStackView.init()
                        self.stackView.frame = CGRect(x: 10, y: 30, width: UIScreen.main.bounds.size.width - 20, height: 40)
                        stackView.spacing = 10
                        stackView.distribution = .fillEqually
                        stackView.axis = .horizontal
                        rootStackView.addArrangedSubview(stackView)
                        
                    }
                    
                    gradientBtnView  = UIView.init()
                   // gradientBtnView.layer.cornerRadius = 15
                
                   // btn[i] = UIButton.init()
                  
                  
                  //  btn.layer.cornerRadius = 5
                    btn[i].backgroundColor = UIColor.clear
                    
                    btn[i].setTitleColor(UIColor.white, for: .normal)
                    
                    btn[i].setTitle(arr[i] as? String, for: .normal)
                    
                    btn[i].tag = i
                    btn[i].addTarget(self, action: #selector(AlertViewS.actionStackButton(sender:)), for: .touchUpInside)
                    
                    btn[i].frame =  gradientLayer.bounds// CGRect(x: 0, y: 0, width: 80, height: 40)
                  //  btn.center = gradientBtnView.center
                 
                    gradientBtnView.addSubview(btn[i])
                    stackView.addArrangedSubview(gradientBtnView)
                    
                    stackView.layoutIfNeeded()
                  //  print(stackView.arrangedSubviews[i].frame)
                   
                    
                    if  i < ThemeColor.count{

                        let topColor =   ThemeColor[i].0.cgColor  //UIColor(red: 220/255.0, green: 60/255.0, blue: 20/255.0, alpha: 1).cgColor
                        let bottomColor =  ThemeColor[i].1.cgColor//UIColor(red: 255/255.0, green: 255/255.0, blue: 102/255.0, alpha: 1).cgColor
                        let gradientColors = [topColor, bottomColor]
                        let gradientLocations: [NSNumber] = [0.0, 2.0]

                        gradientLayer.colors = gradientColors
                        gradientLayer.locations = gradientLocations
                        //Set startPoint and endPoint property also
//                        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
//                        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
                        gradientLayer.frame = gradientLayer.bounds
                        gradientLayer.cornerRadius = 20
                        gradientBtnView.layer.insertSublayer(gradientLayer, at: 0)
                    }else{
                          gradientBtnView.layer.cornerRadius = 20
                         gradientBtnView.backgroundColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
                    }
                    
                }else{ //============ Vertically managed :
                    
                    self.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
                    
                    let gradientBtnView  = UIView.init(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.size.width - 60, height: 40))
                    gradientBtnView.layer.cornerRadius = 20
                    
                    let btn = UIButton.init()
                 //   btn.layer.cornerRadius = 5
                    btn.setTitleColor(UIColor.white, for: .normal)
                   // btn.backgroundColor = UIColor.darkGray
                    
                    btn.setTitle(arr[i] as? String, for: .normal)
                    btn.tag = i
                    
                    btn.frame = gradientBtnView.frame
                    
                    gradientBtnView.addSubview(btn)
                    rootStackView.addArrangedSubview(gradientBtnView)
                    btn.addTarget(self, action: #selector(AlertViewS.actionStackButton(sender:)), for: .touchUpInside)
                 
                    
                    if  i < ThemeColor.count{
                        
                       // gradientBtnView.layer.insertSublayer(gradientFrom(firstcolor:  ThemeColor[i].0 , secandColor:  ThemeColor[i].1), at: 0)
                    
                        let topColor =   ThemeColor[i].0.cgColor  //UIColor(red: 220/255.0, green: 60/255.0, blue: 20/255.0, alpha: 1).cgColor
                        let bottomColor =  ThemeColor[i].1.cgColor//UIColor(red: 255/255.0, green: 255/255.0, blue: 102/255.0, alpha: 1).cgColor
                        let gradientColors = [topColor, bottomColor]
                        let gradientLocations: [NSNumber] = [0.0, 2.0]
                        
                        gradientLayer.colors = gradientColors
                        gradientLayer.locations = gradientLocations
                        //Set startPoint and endPoint property also
//                        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
//                        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
                        gradientLayer.frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.size.width - 60, height: 40)
                        gradientLayer.cornerRadius = 20
                        gradientBtnView.layer.insertSublayer(gradientLayer, at: 0)

                    }else{
                         gradientBtnView.layer.cornerRadius = 20
                        gradientBtnView.backgroundColor = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
                    }
                }
            }
            
            //=================== Frame of alertView
            alertUIView.frame.size.height = scrollView.frame.size.height + 10
            alertUIView.center = self.center
            scrollView.addSubview(rootStackView)
      
            
            alertUIView.addSubview(scrollView)
            self.addSubview(alertUIView)
            
          //  UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)
        
        viewController.view.addSubview(self)
        viewController.view.bringSubviewToFront(self)
       // UIApplication.shared.keyWindow?.rootViewController?.view.bringSubview(toFront: self)
        //insertSubview(self, belowSubview:  (UIApplication.shared.keyWindow?.rootViewController?.view)!)
            
            
            //============== Show with animation
            alertUIView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            alertUIView.alpha = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.alertUIView.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.alertUIView.alpha = 1
            }) { (true) in
                //   print("Date picker isplay: Done")
            }
        }

    
    // Remove functions :
    @objc func actionStackButton(sender : UIButton){
     // print("\(arr[sender.tag])")
        
        sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        
        UIView.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform(scaleX: 1,y: 1)
        }) { (result) in
            self.onCompletion("\(self.arr[sender.tag])")
            self.alertUIView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alertUIView.alpha = 1
            UIView.animate(withDuration: 0.3, animations: {
                self.alertUIView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                self.alertUIView.alpha = 0
            }) { (true) in
                
                for i in self.subviews{ // remove all subviews
                    i.removeFromSuperview()
                }
            
                self.removeFromSuperview() // remove from superView
            }
        }
    }
    
func getLabelHeight(_ label: UILabel?) -> CGFloat {
    let constraint = CGSize(width: label?.frame.size.width ?? 0.0, height: CGFloat.greatestFiniteMagnitude)
    var size: CGSize
    
    let context = NSStringDrawingContext()
    let boundingBox: CGSize? = label?.text?.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: label?.font as Any], context: context).size
    
    size = CGSize(width: ceil((boundingBox?.width)!), height: ceil((boundingBox?.height)!))
    
    return size.height
}
}

// gradient  color  :
extension AlertViewS {
//    func gradientFrom(firstcolor : UIColor, secandColor  : UIColor )-> CAGradientLayer{
//        let topColor =   firstcolor.cgColor  //UIColor(red: 220/255.0, green: 60/255.0, blue: 20/255.0, alpha: 1).cgColor
//        let bottomColor =  secandColor.cgColor//UIColor(red: 255/255.0, green: 255/255.0, blue: 102/255.0, alpha: 1).cgColor
//        let gradientColors = [topColor, bottomColor]
//        let gradientLocations: [NSNumber] = [0.0, 2.0]
//
//        gradientLayer.colors = gradientColors
//        gradientLayer.locations = gradientLocations
//        //Set startPoint and endPoint property also
//        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
//        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
//        gradientLayer.frame = gradientBtnView.bounds
//        return gradientLayer
//    }
    
    override func layoutSubviews() {
      //  gradientLayer.frame = self.stackView.arrangedSubviews[1].frame
        
        if self.AlignmentType == AlertViewS.AlignmentType.Horizontal  { // horizontal
            //print("gradientBtnView : self \(gradientBtnView.frame)")
            gradientLayer.frame = self.stackView.arrangedSubviews[0].bounds
            
            for i in 0..<self.btn.count {
                btn[i].frame = self.stackView.arrangedSubviews[0].bounds
            }
        }
    }
    
    override func didAddSubview(_ subview: UIView) {
     //print(" self.stackView.frame : \(self.stackView.arrangedSubviews[0].bounds)")
      //  print("gradientBtnView : \(gradientBtnView.frame)")

    }
}
