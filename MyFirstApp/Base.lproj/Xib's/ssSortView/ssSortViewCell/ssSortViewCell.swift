//
//  ssSortViewCell.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ssSortViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgSelectedIcon: UIImageView!
    @IBOutlet weak var btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
