//
//  ssSortView.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ssSortView: UIView {
    static let sharedInstance = ssSortView.initLoader()
        class func initLoader() -> ssSortView {
             return UINib(nibName: "ssSortView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ssSortView
         }
    
    
    @IBOutlet weak var sortby_y: NSLayoutConstraint!
    @IBOutlet weak var sortby_x: NSLayoutConstraint!
    @IBOutlet weak var tblViewSort: UITableView!
    
    @IBOutlet weak var sortViewContainer: UIView!
    @IBOutlet weak var tableviewContainer: UIViewClass!
    
    //MARK:-
    //MARK:- Variables :
    
    var onCloser : ((String)-> Void)!
    var selectedValue = ""
    var arrSort = ["Newest to Oldest", "Oldest to Newest", "Matches"]
    
    
    //MARK:-
    //MARK:- App Flow
     
    override func awakeFromNib() {
//        backgroundColor = UIColor.black.withAlphaComponent(0.8)
        backgroundColor = .clear
        tblViewSort.delegate = self
        tblViewSort.dataSource =  self
        
        let nib = UINib.init(nibName: "ssSortViewCell", bundle: nil)
        self.tblViewSort.register(nib, forCellReuseIdentifier: "ssSortViewCell")
        
        self.tblViewSort.estimatedRowHeight = 40
        self.tblViewSort.rowHeight = UITableView.automaticDimension
    }

    
       //MARK:-
       //MARK:- Main Method
           
    func Show(x: CGFloat, y: CGFloat,selectedValue : String, onCompletion: @escaping (String)-> Void){
           self.frame = UIScreen.main.bounds
           self.onCloser = onCompletion
           self.selectedValue = selectedValue
           
        UIView.performWithoutAnimation {
            sortby_y.constant = y
            sortby_x.constant = x
        }
        
        self.tblViewSort.contentInset =  UIEdgeInsets(top: 10 , left: 0, bottom: 0, right:0)
        
         //  UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)   //-->> add to subview
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(self)

           self.tblViewSort.reloadData()
           self.showWithAnimation()
    }
    
    //MARK:-
        //MARK:- Methods :
        
            func showWithAnimation(){
                //============== Show with animation
                
                sortViewContainer.alpha = 1
                tableviewContainer.alpha = 0

                UIView.animate(withDuration: 0, animations: {
                self.sortViewContainer.alpha = 1
                    self.tableviewContainer.addShadowToView()
                }) { (true) in
                    
                    UIView.animate(withDuration: 0.5) {
                        self.tableviewContainer.alpha = 1
                       // self.layoutIfNeeded()
                    }
                }
    }
    
            func removeWithAnimation(){
               sortViewContainer.alpha = 1
                tableviewContainer.alpha = 1
                
               UIView.animate(withDuration: 0.7, animations: {
                self.sortViewContainer.alpha = 0
                self.tableviewContainer.alpha = 0
                     self.layoutIfNeeded()
                }) { (true) in
                    self.removeFromSuperview()
                }
        }
       
       func removeWithAnimationWithValue(value : String){
             sortViewContainer.alpha = 1
             tableviewContainer.alpha = 1
              
              UIView.animate(withDuration: 0.7, animations: {
                 self.sortViewContainer.alpha = 0
                 self.tableviewContainer.alpha = 0
                    self.layoutIfNeeded()
               }) { (true) in
                   self.removeFromSuperview()
                   self.onCloser(value)
               }
       }


    
    // for close the tag:
    @IBAction func actionSortBy(_ sender: UIButton) {
        removeWithAnimation()
    }
}

extension ssSortView: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSort.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ssSortViewCell", for: indexPath) as! ssSortViewCell
        
        cell.lblTitle.text = arrSort[indexPath.row]
        
        if selectedValue == arrSort[indexPath.row] {
             cell.imgSelectedIcon.isHidden = false
        }else{
            cell.imgSelectedIcon.isHidden = true
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(self.actionSelectButton(sender: )), for:.touchUpInside)
    return cell
    }
    
    @objc func actionSelectButton(sender : UIButton){
        selectedValue = arrSort[sender.tag]
        tblViewSort.reloadData()
        removeWithAnimationWithValue(value: arrSort[sender.tag])
    }
}
