//
//  SplashUIView.swift
//  MyFirstApp
//
//  Created by cis on 29/04/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

class SplashUIView: UIView {
    
    enum SelectedAction {
        case blockUser
        case moveToTheWelcomeScreenWithLogout
        case moveToTheWelcomeScreenWithOutLogout
        case deactivatedAccount
        case none
    }
    
    static let sharedInstance = SplashUIView.initLoader()
    class func initLoader() -> SplashUIView {
        return UINib(nibName: "SplashUIView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SplashUIView
    }
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var acitivityIndicator: UIActivityIndicatorView!
    
    //MARK:-
    //MARK:- Variables :
    
    var onCloser : ((SelectedAction)-> Void)!
    var splashVM = SplashViewModel()
   
    
    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        acitivityIndicator.isHidden = false
    }
    
    //MARK:-
    //MARK:- Main Method
    
    func Show(objSplashVM : SplashViewModel , onCompletion: @escaping (SelectedAction)-> Void){
        
        //  self.arrMatches = arr
        self.frame = UIScreen.main.bounds
        self.onCloser = onCompletion
        self.splashVM = objSplashVM
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        
        keyWindow?.addSubview(self)
        self.showWithAnimation()
        getInfo()
        getUserStatus()
    }
    
    //MARK:-
    //MARK:- Methods :
    
    func showWithAnimation(){
        //============== Show with animation
        self.alpha = 1
    }
    
    func removeWithAnimation(){
        self.alpha = 0
        self.removeFromSuperview()
    }
    
    func getInfo(){
        
        splashVM.getInfoData { (status, message) in }
            //---------------------------------------------------
        }
    
    func removeWithAnimation(withValue: SelectedAction){
        self.alpha = 0
        self.onCloser(withValue)
        removeFromSuperview()
    }

    
    // get profile info:
    func getUserStatus()
    {
        acitivityIndicator.isHidden = false
        acitivityIndicator.startAnimating()
        let device_token = "\(Constants.userDefault.object(forKey: Variables.deviceToken) ?? "")"
        
        let country_code = "\(Constants.userDefault.object(forKey: Variables.country_code) ?? "")"
        let phone_number = "\(Constants.userDefault.object(forKey: Variables.phone_number) ?? "")"
        
        splashVM.getUserStatus(country_code: country_code, phone_number: phone_number) { [weak self](status, message)  in
            self?.acitivityIndicator.isHidden = true
            self?.acitivityIndicator.stopAnimating()
            
            if status == "success200"
            {
                //1.-----------------------------------------
                if self?.splashVM.userStatus.blockStatus == "1"{ // different
                    
                    self?.removeWithAnimation(withValue: .blockUser)
                }else
                
                //2.-----------------------------------------
                if self?.splashVM.userStatus.deviceToken != device_token {
                    
                    self?.removeWithAnimation(withValue: .moveToTheWelcomeScreenWithLogout)
                    
                    //3.-----------------------------------------
                }else
                
                if Constants.userDefault.object(forKey: Variables.phone_number) != nil  {
                    
                    if (self?.splashVM.userStatus.last_login_local_time)! > self?.splashVM.userStatus.login_check_days ?? 0 {
                        
                        self?.removeWithAnimation(withValue: .moveToTheWelcomeScreenWithLogout)
                        
                    }else{
                        //   // dashboard API calling for update local time
                        Singleton.shared.isLoginActive = false
                        self?.removeWithAnimation(withValue: .moveToTheWelcomeScreenWithOutLogout)
                    }
                }else{ // user logout
                    self?.removeWithAnimation(withValue: .moveToTheWelcomeScreenWithLogout)
                }
                
            }else if status  == "success401" {
                
                self?.removeWithAnimation(withValue: .deactivatedAccount)
                
            }else if status == "success400"{
                self?.removeWithAnimation(withValue: .moveToTheWelcomeScreenWithOutLogout)
            }else{
                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
            }
        }
    }
}



