//
//  SplashView1.swift
//  MyFirstApp
//
//  Created by cis on 02/05/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

class SplashView1: UIView {
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "SplashView1", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
