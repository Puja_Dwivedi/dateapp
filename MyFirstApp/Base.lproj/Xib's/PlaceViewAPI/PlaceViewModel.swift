//
//  PlaceViewModel.swift
//  MyFirstApp
//
//  Created by cis on 12/10/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

class PlaceViewModel: NSObject {
  

    // Get filter array option :-------------------------------------------------------------------
    func getFilterOptionDataPlace(keys : String ,completion: @escaping (_ result: String, _ message : String,_ isPlaceDetailEnough : Bool) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.postPlaceFiltered
        
        param.updateValue(keys as AnyObject, forKey: "types")
        print("strService \(strService)\n request param: \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    if let detailEnough = dict.object(forKey: "isPlaceDetailEnough") as? Bool{
                     
                        completion("success", message, detailEnough)
                    }else{
                        completion("success", message,false)
                    }
                }else { // error :
                    if data.message == "" {
                        message = AlertMessages.shareInstance.somethingWentWrong
                    }else{
                        message = data.message
                    }
                    completion("error", message,false)
                }
            }
            else
            {
                if data.message == "" {
                    message = AlertMessages.shareInstance.somethingWentWrong
                }else{
                    message = data.message
                }
                completion("error", message,false)
            }
        }
    }
    
    // Get filter array option :-------------------------------------------------------------------
    func getFilterOptionDataStreet(keys : String ,completion: @escaping (_ result: String, _ message : String,_ isPlaceDetailEnough : Bool) -> Void)
    {
        var message  = ""
        var param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        let strService = APPURL.Urls.postStreetFiltered
        
        param.updateValue(keys as AnyObject, forKey: "types")
        print("strService \(strService), \n request parm: \(param)")

        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                print(data.message)
                
                let dict = data.response
                
                if dict.object(forKey: "code") as! Int == 200 { // user is already register :
                    
                    if data.message == "" {
                        message = "\(dict.object(forKey: "message") ?? "")"
                        
                    }else{
                        message = data.message
                    }
                    if let detailEnough = dict.object(forKey: "isStreetDetailedEnough") as? Bool{
                     
                        completion("success", message, detailEnough)
                    }else{
                        completion("success", message,false)
                    }
                }else { // error :
                    if data.message == "" {
                        message = AlertMessages.shareInstance.somethingWentWrong
                    }else{
                        message = data.message
                    }
                    completion("error", message,false)
                }
            }
            else
            {
                if data.message == "" {
                    message = AlertMessages.shareInstance.somethingWentWrong
                }else{
                    message = data.message
                }
                completion("error", message,false)
            }
        }
    }
}
