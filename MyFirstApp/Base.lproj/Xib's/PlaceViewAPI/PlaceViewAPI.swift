//
//  PlaceViewAPI.swift
//  MyFirstApp
//
//  Created by cis on 04/08/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class PlaceViewAPI: UIView {
    
    enum ActiveAction {
        case city
        case subway
        case place
        case street
        case bus
        case none
        case placeCreate
        case streetCreate
    }
    
    static let sharedInstance = PlaceViewAPI.initLoader()
    class func initLoader() -> PlaceViewAPI {
        return UINib(nibName: "PlaceViewAPI", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PlaceViewAPI
    }
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var viewMainContainer: UIView!
    
    //MARK:-
    //MARK:- Variables :
    
    var onCloser : ((String,String,String,String,String, NSDictionary)-> Void)!
    var arrPlace = [Any]()
    var activeAction = ActiveAction.none
    var filterTypesValue = ""
    var placeVM = PlaceViewModel()
    var filterIndex = (isSelectedRow: false, selectedIndex : 0)
    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        
        backgroundColor = UIColor.black.withAlphaComponent(0.8)
        tblview.delegate = self
        tblview.dataSource =  self
        searchBar.showsCancelButton = false
        searchBar.delegate = self
        
        let nib = UINib.init(nibName: "PlaceViewCell", bundle: nil)
        self.tblview.register(nib, forCellReuseIdentifier: "PlaceViewCell")
        
        self.tblview.estimatedRowHeight = 40
        self.tblview.rowHeight = UITableView.automaticDimension
        filterIndex.isSelectedRow  = false
        filterIndex.selectedIndex = 0
    }
    
    
    //MARK:-
    //MARK:- Main Method
    
    func Show(activeAction : ActiveAction, onCompletion: @escaping (String, String, String,String,String, NSDictionary)-> Void){
        self.arrPlace.removeAll()
        
        //  self.arrMatches = arr
        self.frame = UIScreen.main.bounds
        self.activeAction = activeAction
        self.applyFilterByTypes()
        self.onCloser = onCompletion
        self.searchBar.text = ""
        self.searchBar.placeholder = "Search.."
        self.arrPlace.removeAll()
        self.tblview.reloadData()
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(self)
      
        self.showWithAnimation()
    }
    
    
    func applyFilterByTypes(){
        
        switch activeAction {
        case .bus:
            filterTypesValue = "(regions)" // for city only
            break;
            
        case .place:
            filterTypesValue = "" // for gettting all the value not by the reason
            break;
            
        case .street:
            filterTypesValue = "" // for gettting all the value not by the reason
            break;
            
        case .streetCreate:
            filterTypesValue = "" // for gettting all the value not by the reason
            break;
            
        case .subway:
            filterTypesValue = "(regions)" // for city only
            break;
            
        case .city:
            filterTypesValue = "(regions)" // for city only
            break;
            
        case .placeCreate:
            filterTypesValue = "" // for gettting all the value not by the reason
            if Singleton.shared.selectedFilterPlace.selectedValue.count > 0 {
                filterTypesValue = Singleton.shared.selectedFilterPlace.selectedValue // for gettting all the value not by the reason
            }
            
            break
        default:
            break;
        }
    }
    
    //MARK:-
    //MARK:- Action Methods
    
    @IBAction func actionCancel(_ sender: UIButton) {
        removeWithAnimation()
    }
    
    //MARK:-
    //MARK:- Methods :
    
    func showWithAnimation(){
        //============== Show with animation
        self.viewMainContainer.transform = CGAffineTransform(translationX: 0, y: self.viewMainContainer.frame.size.height)
        UIView.animate(withDuration: 0.6, animations: {
            self.viewMainContainer.transform = CGAffineTransform(translationX: 0, y: 0)
        }) { (_) in
        }
    }
    
    func removeWithAnimation(){
        self.viewMainContainer.transform = CGAffineTransform(translationX: 0, y: 0)
        UIView.animate(withDuration: 0.6, animations: {
            self.viewMainContainer.transform = CGAffineTransform(translationX: 0, y: self.viewMainContainer.frame.size.height)
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    func removeWithAnimationWithValue(city : String,country : String, shortName : String,address : String,placeID : String, index : Int){
        self.viewMainContainer.transform = CGAffineTransform(translationX: 0, y: 0)
        
        UIView.animate(withDuration: 0.6, animations: {
            self.viewMainContainer.transform = CGAffineTransform(translationX: 0, y: self.viewMainContainer.frame.size.height)
        }) { (true) in
            self.removeFromSuperview()
            let selectedItemDict  = self.arrPlace[index] as! NSDictionary
            self.onCloser(city, country,shortName, address, placeID, selectedItemDict)
        }
    }
    }

extension PlaceViewAPI : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        arrPlace.removeAll()
        tblview.reloadData()
        removeWithAnimation()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getPlaceByKeyword(searchKeyword: searchText)
    }
}


extension PlaceViewAPI: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlace.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceViewCell", for: indexPath) as! PlaceViewCell
        
        cell.loader.alpha = 0
        
        if  filterIndex.isSelectedRow && filterIndex.selectedIndex == indexPath.row {
            // show the loader for only place
            UIView.animate(withDuration: 0.5) {
                cell.loader.startAnimating()
                cell.loader.alpha = 1
                cell.loader.isHidden = false
                cell.containerBackground.backgroundColor = #colorLiteral(red: 0.4586814046, green: 0.7333828807, blue: 0.9998760819, alpha: 0.5)
                
               // cell.layoutIfNeeded()
            }

        }else{
            // hide the filter for only place
            UIView.animate(withDuration: 0.5) {
                cell.loader.stopAnimating()
                cell.loader.alpha = 0
                cell.loader.isHidden = true
                cell.containerBackground.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                //cell.layoutIfNeeded()
            }
        }
        let dict = arrPlace[indexPath.row] as! NSDictionary
        let dictFormate = dict.object(forKey: "structured_formatting") as! NSDictionary
        cell.lblTitle.text = "\(dictFormate.object(forKey: "main_text") ?? "--")"
        cell.lblSubTitle.text = "\(dictFormate.object(forKey: "secondary_text") ?? "--")"
        return cell
    }
    
    func loadLoader(isSeectedIndex : Bool,indexPath : IndexPath){
        
            self.filterIndex.isSelectedRow = isSeectedIndex
            self.filterIndex.selectedIndex = indexPath.row
            self.tblview.reloadRows(at: [indexPath], with: .fade)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if activeAction == .placeCreate{
            guard filterIndex.isSelectedRow == false else { return }
            
            DispatchQueue.main.async {
                self.loadLoader(isSeectedIndex: true, indexPath: indexPath) // active loader:
            }
            
            let dict = arrPlace[indexPath.row] as! NSDictionary
            let strPlaceID = "\(dict.object(forKey: "place_id") ?? "")"
           
            PlaceDetailClass.getDetailByPlaceID(placeID: strPlaceID) { [weak self](country, shortName,stateNameLong, formatedAddress,dictItem)   in
                // Matching value
                self?.getFilterOptionPlace(dict: dictItem, indexPath: indexPath)
            }
            
        }else if activeAction == .streetCreate {
            
            guard filterIndex.isSelectedRow == false else { return }
            
            DispatchQueue.main.async {
                self.loadLoader(isSeectedIndex: true, indexPath: indexPath) // active loader:
            }
            
            let dict = arrPlace[indexPath.row] as! NSDictionary
            let strPlaceID = "\(dict.object(forKey: "place_id") ?? "")"
           
            PlaceDetailClass.getDetailByPlaceID(placeID: strPlaceID) { [weak self](country, shortName,stateNameLong, formatedAddress,dictItem)   in
                // Matching value
                self?.getFilterOptionStreet(dict: dictItem, indexPath: indexPath)
            }
        }else{
            callback(indexPath: indexPath)
      }
    }
    
    func callback(indexPath : IndexPath){
        let dict = arrPlace[indexPath.row] as! NSDictionary
        let strPlaceID = "\(dict.object(forKey: "place_id") ?? "")"
        let dictFormate = dict.object(forKey: "structured_formatting") as! NSDictionary
        let strSelectedCity =  "\(dictFormate.object(forKey: "main_text") ?? "")"
        let strSelected = "\(dictFormate.object(forKey: "secondary_text") ?? "")"
        
        let address = "\(dictFormate.object(forKey: "main_text") ?? ""), \(dictFormate.object(forKey: "secondary_text") ?? "")"
        
        let array = strSelected.components(separatedBy: ", ")
        print(array)

        if array.count == 1 {
            removeWithAnimationWithValue(city: strSelectedCity, country: array[0], shortName: "",address: address, placeID: strPlaceID, index: indexPath.row)
        }else if array.count == 2 {
            removeWithAnimationWithValue(city: strSelectedCity, country: array[0], shortName: array[1],address: address, placeID: strPlaceID, index: indexPath.row)
        }else{
            removeWithAnimationWithValue(city: strSelectedCity, country: "", shortName: "",address: address, placeID: strPlaceID, index: indexPath.row)
        }
    }
}

//MARK: API's

extension PlaceViewAPI {
    
    func getPlaceByKeyword(searchKeyword : String){
        
        let param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        print(searchKeyword)
        
        let strService =   "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchKeyword)&types=\(filterTypesValue)&key=\(AppDelegate.shared.googlePlaceAPI)"
        
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print(data.response)
                let responseData = data.response
                if let arrResponse = responseData.object(forKey: "predictions") as? [Any]{
                    self.arrPlace = arrResponse
                }else{
                    self.arrPlace.removeAll()
                }
                self.tblview.reloadData()
            }
            else
            {
                self.tblview.reloadData()
            }
        }
    }
    
    func getFilterOptionPlace(dict : NSDictionary,indexPath : IndexPath){
        placeVM.getFilterOptionDataPlace(keys: retrieveAllKeysPlace(dict: dict)) { [weak self](status, message,isPlaceDetailEnough ) in
            
            if status == "success" {
                if isPlaceDetailEnough {
                    // value matched continue process:
                    self?.callback(indexPath: indexPath)
                }else{
                    // show popup
                    AlertTheme.sharedInstance.Show(popupCategory : .normal,message: AlertMessages.shareInstance.notEnough, attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["\(AlertMessages.shareInstance.gotIt)"], isBottomTxt: "") { (clicked) in
                    }
            }
            }else{
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.youAreAlreadyPostToday, showMsgAt: .bottom)
            }
            self?.loadLoader(isSeectedIndex: false, indexPath: indexPath)
    }
    }
    
    func getFilterOptionStreet(dict : NSDictionary,indexPath : IndexPath){
        let streetValue = retrieveAllKeysStreet(dict: dict)
        placeVM.getFilterOptionDataStreet(keys:streetValue.keys) { [weak self](status, message,isPlaceDetailEnough ) in
            
            if status == "success" {
                if isPlaceDetailEnough {
                    // value matched continue process:
                    if streetValue.isStreetNumberAvailable {
                        // show popup2:
                        AlertTheme.sharedInstance.Show(popupCategory : .normal,message: AlertMessages.shareInstance.doNotInclude, attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
                        }
                    }else{
                        // continue
                        self?.callback(indexPath: indexPath)
                    }
                }else{
                    // show popup 1
                    AlertTheme.sharedInstance.Show(popupCategory : .normal,message: AlertMessages.shareInstance.selectAValidStreet, attributeRangeString: "", isCancelButtonVisible: false, arrBtn: ["Ok"], isBottomTxt: "") { (clicked) in
                    }
            }
            }else{
                SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.youAreAlreadyPostToday, showMsgAt: .bottom)
            }
            self?.loadLoader(isSeectedIndex: false, indexPath: indexPath)
    }
        }
    
    
    func retrieveAllKeysPlace(dict : NSDictionary)-> String{
        var arrTypes = [String]()
        if let arr = dict.object(forKey: "types") as? [Any] {
            for item in arr {
                arrTypes.append("\(item)")
            }
        }
        print("types: \(arrTypes)")
        return arrTypes.joined(separator: ",")
    }
    
    
    func retrieveAllKeysStreet(dict : NSDictionary)-> (keys: String, isStreetNumberAvailable : Bool){
        var arrTypes = [String]()
        if let arr = dict.object(forKey: "types") as? [Any] {
            for item in arr {
                arrTypes.append("\(item)")
            }
        }
        //print("types: \(arrTypes)")
        
        var isStreetNumberAvailable = false
        if let arrComponents = dict.object(forKey: "address_components") as? [Any]{
            
            for dictItem in arrComponents{
                
                if let dictComponent = dictItem as? NSDictionary{
                    
                    if let arrType = dictComponent.object(forKey: "types") as? [Any] {
                        
                        for type in arrType {
                           
                            if "\(type)" == "street_number" {
                                isStreetNumberAvailable = true
                            }
                        }
                    }
                }
            }
        }
        
    return (keys: arrTypes.joined(separator: ","), isStreetNumberAvailable : isStreetNumberAvailable)
    }
}

extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}

