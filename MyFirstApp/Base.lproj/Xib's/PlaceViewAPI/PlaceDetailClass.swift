//
//  PlaceDetailClass.swift
//  MyFirstApp
//
//  Created by cis on 30/12/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class PlaceDetailClass: NSObject {
    
    class func getDetailByPlaceID(placeID : String,onCompletion: @escaping (String,String,String,String,NSDictionary)-> Void){
        
        let param = AppTheme.sharedInstance.GetBasicParam()
        let header = RV_GetPostMethod.getHeaders()
        
        let strService = "https://maps.googleapis.com/maps/api/place/details/json?place_id=\(placeID)&key=\(AppDelegate.shared.googlePlaceAPI)"
        
        print("strService \(strService) \n param \(param)")
        
        RV_GetPostMethod.postService(urlString: strService, param: param, headers: header) { (result, data) in
            if result == "success"
            {
                print("Place details : \(data.response)")
                var selectedCountry = ""
                var currentStateLocationName = ""
                var formatedAddress = ""
                var currentStateLocationNameLong = ""
                
                let dictResponse = data.response
                if let dictResult = dictResponse.object(forKey: "result") as? NSDictionary {
                    
                    if let arrComponents = dictResult.object(forKey: "address_components") as? [Any]{
                        
                        for dictItem in arrComponents{
                            
                            if let dictComponent = dictItem as? NSDictionary{
                                
                                if let arrType = dictComponent.object(forKey: "types") as? [Any] {
                                    
                                    for type in arrType {
                                        
                                        switch("\(type)"){
                                        case "country":
                                            let country = "\(dictComponent.object(forKey: "long_name") ?? "")"
                                            selectedCountry = country
                                            
                                        case "administrative_area_level_1":
                                            currentStateLocationName =  "\(dictComponent.object(forKey: "short_name") ?? "")"
                                            currentStateLocationNameLong = "\(dictComponent.object(forKey: "long_name") ?? "")"
                                            
                                            break
                                            
                                        default:
                                            break
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    // formated address:
                    if  let formatedAdd =  dictResult.object(forKey: "formatted_address") as? String  {
                        formatedAddress = formatedAdd
                    }
                    onCompletion(selectedCountry,currentStateLocationName,currentStateLocationNameLong, formatedAddress, dictResult)
                }
            }
        }
    }
}
