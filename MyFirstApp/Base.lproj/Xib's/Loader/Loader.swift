//
//  Loader.swift
//  Singleton Class
//
//  Created by Aman Pathak on 8/2/17.
//  Copyright © 2017 Neuron Solutions Inc. All rights reserved.
//

import UIKit

class Loader: UIView {
    
    /**
     //MARK:-  IBOutlet
     */
    
    @IBOutlet weak var lblmsg: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    /*
     //MAR:- Variables
     */
    
    static let sharedInstance = Loader.initLoader()
    
    class func initLoader() -> Loader {
        return UINib(nibName: "Loader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! Loader
    }
    
    
    /*
     //MARK:- App flow
     */
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    
    /*
    //MARK:- Main method
     */
    
    func showLoader(msg : String) {
        
     //   UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(self)
        self.frame = UIScreen.main.bounds
        
        lblmsg.text = msg                                                       //-->> set msg
       
        startAnimation()
    }
    

    func startAnimation(){
        
        DispatchQueue.main.async(execute: {
            self.activityIndicator.startAnimating()
        })
    }
    
    //MARK: Stop loader
    func stopLoader() {
        
        DispatchQueue.main.async(execute: {
            self.activityIndicator.stopAnimating()
               UIView.animate(withDuration: 0.1, animations: {
                   
               }) { (_) in
                      self.removeFromSuperview()
               }
        })
}
}

/*
 calling like :
 Loader.sharedInstance.showLoader(msg: "loading main view ")    //-->> Start Loader
 Loader.sharedInstance.stopLoader()          //-->> Stop loader
 */

