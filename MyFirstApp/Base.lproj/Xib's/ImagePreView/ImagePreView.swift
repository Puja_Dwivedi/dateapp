//
//  ImagePreView.swift
//

import UIKit

class ImagePreView: UIView {

    @IBOutlet weak var imgPreview: UIImageView!
    
    override func awakeFromNib() {
         self.backgroundColor = UIColor.black.withAlphaComponent(0.7)
     }
    
    
     //MARK:-
    //MARK:- Main Method :>>>>>>>>>>>>>>->
    
    func show(image : UIImage){
        let objCameraView = Bundle.main.loadNibNamed("ImagePreView", owner: self, options: nil)?[0] as! ImagePreView
        objCameraView.frame = UIScreen.main.bounds

        objCameraView.imgPreview.image = image
    
   //  UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(objCameraView)
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(objCameraView)
        objCameraView.showWithAnimation()
    }
    

        //MARK: Methods :>>>>>>>>>>>>>>->
           func showWithAnimation(){
            
            self.frame = CGRect(x: 0, y: UIScreen.main.bounds.height + 200, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
               UIView.animate(withDuration: 0.3, animations: {
                  self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.layoutIfNeeded()
               }) { (true) in
               }
           }
        
           func removeWithAnimation(){
            
            self.alpha = 1
                UIView.animate(withDuration: 0.2, animations: {
    //                        self.rootViewContainer.frame = CGRect(x: 0, y: UIScreen.main.bounds.height + 200, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                         
                     self.alpha = 0
                     self.layoutIfNeeded()
               }) { (true) in
                   self.removeFromSuperview()
                   //   print("Date picker isplay: Done")
               }
           }
    
    
    @IBAction func actionCancel(_ sender: UIButton) {
        removeWithAnimation()
    }
}
