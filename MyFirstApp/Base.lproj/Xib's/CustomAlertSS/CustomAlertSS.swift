//
//  CustomAlertSS.swift
//  TourGuideApp
//
//  Created by cis on 11/07/19.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

enum ErrorType {
    case NetworkError
    case ServerError
}


class CustomAlertSS: UIView {

    //MARK: IBOutlets :>>>>>>->
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnTabToRefersh: ButtonClass!
    
    @IBOutlet weak var uiView: UIView!
    
    @IBOutlet weak var imgBackGround: UIImageView!
    
    //MARK: Variables :>>>>>>>>>>>>>>->
      var onCloser : ((String)-> Void)!
    
    //MARK: Flow of Project :>>>>>->
    override func awakeFromNib() {
        uiView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
    }
    
    
    //MARK: Main Method :>>>>>>>>>>>>>>->
     func show(message : String, errorType : ErrorType, onCompletion: @escaping (String)-> Void){
        let customPopUp = Bundle.main.loadNibNamed("CustomAlertSS", owner: self, options: nil)?[0] as! CustomAlertSS
        customPopUp.frame = UIScreen.main.bounds
        
        customPopUp.onCloser = onCompletion
       //  UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(customPopUp)   //-->> add to subview
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(customPopUp)
        
        if errorType == ErrorType.NetworkError {
            customPopUp.imgType.image = #imageLiteral(resourceName: "networkErrorIcon")
            customPopUp.lblMessage.text = "Internet Connection not Available!, Please check or Tab to Refresh or Try again later."
            customPopUp.imgBackGround.image = #imageLiteral(resourceName: "background2")
        }else{
             customPopUp.imgType.image = #imageLiteral(resourceName: "serverErrorIcon1")
             customPopUp.lblMessage.text = "Something went wrong,Tab to Refresh or Try again latter.  \n\n" + message
        
             customPopUp.imgBackGround.image = #imageLiteral(resourceName: "background1")
        }
        customPopUp.showWithAnimation()
    }
    
    
    //MARK: Action Methods :>>>>>>>>>>>>>>->
    @IBAction func actionBack(_ sender: UIButton) {
        // remove from super view :
        onCloser("BACK")
        removeWithAnimation()
    }
    
    
    @IBAction func actionTabToRefersh(_ sender: ButtonClass) {
        onCloser("REFRESH")
        removeWithAnimation()
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        removeWithAnimation()
    }
    
    
    //MARK: Methods :>>>>>>>>>>>>>>->
    func showWithAnimation(){
        //============== Show with animation
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alpha = 1
        }) { (true) in
            //   print("Date picker isplay: Done")
        }
    }
    
    func removeWithAnimation(){
        //============== Show with animation
       // self.transform = CGAffineTransform(scaleX: 1, y: 1)
        self.alpha = 1
        UIView.animate(withDuration: 0.3, animations: {
           // self.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
            self.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
            //   print("Date picker isplay: Done")
        }
    }

}
