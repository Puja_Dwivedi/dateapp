//
//  ImagePickerView.swift
//  AVEX
//
//  Created by  on 17/10/17.
//  Copyright © 2017 5Exceptions. All rights reserved.
//

import UIKit
import SDWebImage

class ImagePickerView: UIView,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    // View declerations
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var imageView: ImageViewClass!
    
    @IBOutlet var btnOk: UIButton!
    @IBOutlet var btnCancel: UIButton!
    
    // view decleration
    var picker:UIImagePickerController? = UIImagePickerController()
    //static var shared  = ImagePickerView()
    
    
    var onCloser : ((UIImageView)-> Void)!
    var preview = (isPreview : false , image : UIImage())
    
    @IBAction func btnClicked(_ sender: UIButton){// ok button
        
        if self.preview.isPreview{
            removeWithAnimation()
        }else{
            if sender.tag == 1 { // OK :  set profile
                onCloser(imageView)
                self.removeWithAnimation()
                
            }else{ // cancel
                self.removeWithAnimation()
            }
        }
    }
    
    override func awakeFromNib() {
        //background color 0.5 transparent
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // uiView.layer.cornerRadius = 6
        picker!.delegate=self
        
        //
        //        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.checkAction(sender:)))
        //        self.addGestureRecognizer(gesture)
    }
    
    @IBAction func actionRemove(_ sender: UIButton) {
        removeWithAnimation()
    }
    
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // remove()
        
    }
    
    
    func show(_ viewController : UIViewController, preview : (isPreview : Bool , image : UIImage), onCompletion: @escaping (UIImageView)-> Void){
        let customPopUp = Bundle.main.loadNibNamed("ImagePickerViewXIB", owner: self, options: nil)?[0] as! ImagePickerView
        customPopUp.frame = UIScreen.main.bounds
        
        viewController.view.addSubview(customPopUp)
        
        // UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(customPopUp)   //-->> add to subview
        //UIApplication.shared.keyWindow?.rootViewController?.view.superview?.insertSubview(customPopUp, belowSubview: self )
        
        customPopUp.onCloser = onCompletion
        customPopUp.startWithAnimation()
        
        // customPopUp.displayUIview(preview : preview)
        
        customPopUp.preview = preview
        
        if preview.isPreview { // preview :>>>>>>>>>>>>>>>>>->>
            
            // self.btnOk.setTitle("Ok", for: .normal)
            customPopUp.btnCancel.isHidden = true
            customPopUp.btnOk.isHidden = false
            
            customPopUp.imageView.image = preview.image
            
        }else{ // take image from gallary :>>>>>>>>>>>>>>>>>>->>
            
            customPopUp.btnOk.isHidden = false
            customPopUp.btnCancel.isHidden = false
            
            // self.btnOk.setTitle("Ok", for: .normal)
            //self.btnCancel.setTitle("Cancel",  for: .normal)
            
            customPopUp.alpha = 0
            customPopUp.start() // start action
        }
        
        //-->> Display view with animation
    }
    
    func removeCurrentPhoto(){
        imageView.image = UIImage(named: "img_profile_placeholder")
        onCloser(imageView)
        self.removeWithAnimation()
    }
    
    
    // MARK:  display with animation
    func startWithAnimation(){
        uiView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        uiView.alpha = 0
        
        UIView.animate(withDuration: 0.3, animations: {
            // var viewFrame = self.uiView.frame
            //viewFrame.origin.y = 0
            self.uiView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.uiView.alpha = 1
            
        }) { (true) in
            
        }
    }
    
    
    //MARK:  remove animation
    func removeWithAnimation(){
        //uiView.frame.origin.y = 250
        
        uiView.transform = CGAffineTransform(scaleX: 1, y: 1)
        uiView.alpha = 1
        
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame = self.uiView.frame
            viewFrame.origin.y = UIScreen.main.bounds.height + 250
            self.uiView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.uiView.alpha = 0
            
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    //imagePicker view :
    func start(){
        
        let alert:UIAlertController=UIAlertController(title: "Change Profile Photo", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let removeAction = UIAlertAction(title: "Remove Current Photo", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.removeCurrentPhoto()
        }
        
        let cameraAction = UIAlertAction(title: "Take Photo", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
            
        }
        let gallaryAction = UIAlertAction(title: "Choose from Library", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
            self.removeFromSuperview()
        }
        
        // Add the actions
        picker?.delegate = self
        alert.addAction(removeAction)
        alert.addAction(gallaryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        // Present the controller
        
        alert.popoverPresentationController?.sourceView = self
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        
        alert.popoverPresentationController?.barButtonItem = UIApplication.shared.keyWindow?.rootViewController?.navigationItem.rightBarButtonItem
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker!.sourceType = UIImagePickerController.SourceType.camera
            UIApplication.shared.keyWindow?.rootViewController?.present(picker!, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        UIApplication.shared.keyWindow?.rootViewController?.present(picker!, animated: true, completion: nil)
    }
    
    
    //    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    //
    //        self.alpha = 1
    //        picker .dismiss(animated: true, completion: nil)
    //       // imageView.image = info[]
    //    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.alpha = 1
        picker .dismiss(animated: true, completion: nil)
        imageView.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        onCloser(imageView)
        self.removeWithAnimation()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        //  print("picker cancel.").
        picker.dismiss(animated: true, completion: nil)
    }
}

/*
 Calling like :
 
 ImagePickerView.show(self)
 */

