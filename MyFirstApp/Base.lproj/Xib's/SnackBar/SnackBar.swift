//
//  SnackBar.swift
//  Epitaph App
//
//  Created by cis on 29/06/20.
//  Copyright © 2020 cis. All rights reserved.
//

import UIKit

class SnackBar: UIView {
    
    enum ActivityDisplay {
        case top
        case mid
        case bottom
    }
    
    static let sharedInstance = SnackBar.initLoader()
    @IBOutlet weak var bottom_space : NSLayoutConstraint!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    var viewcontroller : UIViewController?
    var activityStatus = ActivityDisplay.bottom
    
    var custombar : SnackBar?
    
    class func initLoader() -> SnackBar {
        return UINib(nibName: "SnackBar", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SnackBar
    }
    
    
    func show(message : String, showMsgAt: ActivityDisplay){
        
        //let customPopUp = Bundle.main.loadNibNamed("SnackBar", owner: self, options: nil)?[0] as! SnackBar
        self.lblMsg.text = message
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        //-->> add to subview
        self.activityStatus = showMsgAt
        
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.fireTimer), userInfo: nil, repeats: false)
        
        let keyWindow = UIApplication.shared.connectedScenes
                   .filter({$0.activationState == .foregroundActive})
                   .compactMap({$0 as? UIWindowScene})
                   .first?.windows
                   .filter({$0.isKeyWindow}).first
               
               keyWindow?.addSubview(self)
        self.showWithAnimation()
    }
    
    @objc func fireTimer() {
       
        DispatchQueue.main.async(execute: {
            self.removeWithAnimation()
        })
        
    }
    
    
    //MARK: Methods :>>>>>>>>>>>>>>->
    func showWithAnimation(){
        
        
        switch activityStatus {
            
        case .top:
            self.bottom_space.constant = -500 + UIScreen.main.bounds.height
            self.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.bottom_space.constant = UIScreen.main.bounds.height - 200
                self.layoutIfNeeded()
            }
            break
            
        case .mid:
            self.bottom_space.constant = -500 + UIScreen.main.bounds.height/2
            self.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.bottom_space.constant = 100 + UIScreen.main.bounds.height/2
                self.layoutIfNeeded()
            }
            break
            
        case .bottom:
            self.bottom_space.constant = -500
            self.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.bottom_space.constant = 100
                self.layoutIfNeeded()
            }
            break
        }
    }
    
    func removeWithAnimation(){
        switch activityStatus {
        case .top:
            UIView.animate(withDuration: 0.5, animations: {
                self.bottom_space.constant = -500 - UIScreen.main.bounds.height
                self.layoutIfNeeded()
            }) { (_) in
                self.removeFromSuperview()
            }
            break
            
        case .mid:
            UIView.animate(withDuration: 0.5, animations: {
                self.bottom_space.constant = -500 - UIScreen.main.bounds.height/2
                self.layoutIfNeeded()
            }) { (_) in
                self.removeFromSuperview()
            }
            break
            
        case .bottom:
            UIView.animate(withDuration: 0.5, animations: {
                self.bottom_space.constant = -500
                self.layoutIfNeeded()
            }) { (_) in
                self.removeFromSuperview()
            }
            break
        }
    }
}
