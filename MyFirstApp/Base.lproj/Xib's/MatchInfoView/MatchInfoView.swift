//
//  MatchInfoView.swift
//  MyFirstApp
//
//  Created by cis on 02/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class MatchInfoView: UIView {
    static let sharedInstance = MatchInfoView.initLoader()
        class func initLoader() -> MatchInfoView {
             return UINib(nibName: "MatchInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MatchInfoView
         }
    
    
    
    @IBOutlet var viewCancel: UIViewClass!
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var tableviewContainer: UIViewClass!
    
    //MARK:-
    //MARK:- Variables :
    
    var onCloser : ((String)-> Void)!
    var selectedValue = ""
    var arrSort = ["Newest to Oldest", "Oldest to Newest", "Matches"]
    
    var info1 = (title: "" , url : "")
    var info2 = (title: "" , url : "")
    
    
    
    //MARK:-
    //MARK:- App Flow
     
    override func awakeFromNib() {
        backgroundColor = UIColor.black.withAlphaComponent(0.8)
        viewCancel.addShadowToView()
        let nib = UINib.init(nibName: "MatchesCell", bundle: nil)
        tblView.register(nib, forCellReuseIdentifier: "MatchesCell")
        
        tblView.estimatedRowHeight = 100
        tblView.rowHeight = UITableView.automaticDimension
      
    }

    
       //MARK:-
       //MARK:- Main Method
           
    func Show(x: CGFloat, y: CGFloat,selectedValue : String, onCompletion: @escaping (String)-> Void){
           self.frame = UIScreen.main.bounds
           self.onCloser = onCompletion
           self.selectedValue = selectedValue
           
        UIView.performWithoutAnimation {
          
        }
        
        
         //  UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)   //-->> add to subview
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(self)

           self.showWithAnimation()
    }
    
    //MARK:-
        //MARK:- Methods :
        
            func showWithAnimation(){
                //============== Show with animation
            
                tableviewContainer.alpha = 0
                getInfoList()

                UIView.animate(withDuration: 0, animations: {
                }) { (true) in
                    
                    UIView.animate(withDuration: 0.5) {
                        self.viewCancel.addShadowToView()
                        self.tableviewContainer.alpha = 1
                        self.tblView.reloadData()
                       // self.layoutIfNeeded()
                    }
                }
    }
    
            func removeWithAnimation(){
                tableviewContainer.alpha = 1
                
               UIView.animate(withDuration: 0.7, animations: {
                self.tableviewContainer.alpha = 0
                     self.layoutIfNeeded()
                }) { (true) in
                    self.removeFromSuperview()
                }
        }
       
       func removeWithAnimationWithValue(value : String){
             tableviewContainer.alpha = 1
              
              UIView.animate(withDuration: 0.7, animations: {
                 self.tableviewContainer.alpha = 0
                    self.layoutIfNeeded()
               }) { (true) in
                   self.removeFromSuperview()
                   self.onCloser(value)
               }
       }


    
    // for close the tag:
    @IBAction func actionCancel(_ sender: UIButton) {
        removeWithAnimation()
    }
}

extension MatchInfoView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesCell", for: indexPath) as! MatchesCell
        cell.viewMainCantainer.isHidden = true
        cell.viewInfoContainer.isHidden = false
        
        cell.btnInfo1.setAttributedTitle(setUnderlineAttributeString(title: info1.title), for: .normal)
        cell.btnInfo1.tag = 1
        cell.btnInfo1.addTarget(self, action: #selector(self.actionInfoButton(sender:)), for: .touchUpInside)
        
        cell.btnInfo2.setAttributedTitle(setUnderlineAttributeString(title: info2.title), for: .normal)
        cell.btnInfo2.tag = 2
        cell.btnInfo2.addTarget(self, action: #selector(self.actionInfoButton(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func actionInfoButton(sender: UIButton){
        switch sender.tag {
        case 1:
            callURL(selectInfoPage: info1.url)
            break;
            
        case 2:
            callURL(selectInfoPage: info2.url)
            break;
            
            
        default:
            break;
        }
        
    }
    
    func callURL(selectInfoPage : String){
        if let url = URL(string: selectInfoPage), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            self.removeWithAnimation()
        }
    }
    
    func setUnderlineAttributeString(title : String) -> NSAttributedString? {
        let attributes : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0),
            NSAttributedString.Key.foregroundColor : UIColor.gray,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
        ]
        let attributedString = NSAttributedString(string: title, attributes: attributes)
        return attributedString
    }
    
    
    func getInfoList(){
        
        for item in Singleton.shared.InfoPage{
            if let dict = item as? NSDictionary {
                
                if (dict.object(forKey: "type_id") as? Int) != nil  {
                    if let subtype_id = dict.object(forKey: "subtype_id") as? Int{
                        switch subtype_id {
                        case 8:
                            if let output = dict.object(forKey: "output") as? String {
                                if output.count > 0 {
                                    
                                    info1.title = "\(dict.object(forKey: "subtype_description") ?? "--")".replacingOccurrences(of: "\n", with: "")
                                    info1.url = output
                                    
                                    
                                    //  print("lblInfoPage1.text : \(lblInfoPage1.text)")
                                }
                            }
                            
                            break
                            
                        case 9:
                            
                            if let output = dict.object(forKey: "output") as? String {
                                if output.count > 0 {
                                    
                                    info2.title = "\(dict.object(forKey: "subtype_description") ?? "--")".replacingOccurrences(of: "\n", with: "")
                                    info2.url = output
                                    
                                    //   print("lblInfoPage2.text : \(lblInfoPage2.text)")
                                }
                            }
                            break
                            
                            
                        default:
                            break
                        }
                    }
                }
            }
        }
    }
    
}
