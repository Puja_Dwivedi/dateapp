//
//  CreatePostCell.swift
//  MyFirstApp
//
//  Created by cis on 05/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class CreatePostCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
