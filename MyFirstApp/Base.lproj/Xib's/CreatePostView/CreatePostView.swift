//
//  CreatePostView.swift
//  MyFirstApp
//
//  Created by cis on 05/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class CreatePostView: UIView {
    static let sharedInstance = CreatePostView.initLoader()
    class func initLoader() -> CreatePostView {
        return UINib(nibName: "CreatePostView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CreatePostView
    }
    
    @IBOutlet weak var sortby_x: NSLayoutConstraint!
    @IBOutlet weak var sortby_y: NSLayoutConstraint!
    @IBOutlet weak var tblViewCreatePost: UITableView!
    
    @IBOutlet weak var sortViewContainer: UIView!
    @IBOutlet weak var tableviewContainer: UIView!
    
    
    //MARK:-
    //MARK:- Variables :
    
    var onCloser : ((String)-> Void)!
    var arrCreatePost = [(img :#imageLiteral(resourceName: "airplaneIcon") , title : "Airplane"), (img :#imageLiteral(resourceName: "train 1") , title :"Subway"), (img :#imageLiteral(resourceName: "food-stall 1") , title :"Place"),(img :#imageLiteral(resourceName: "map 1") , title :"Street"),(img :#imageLiteral(resourceName: "bus-stop 1") , title :"Bus"),(img :#imageLiteral(resourceName: "train (1) 2") , title :"Train")]
    
    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        
        backgroundColor = UIColor.black.withAlphaComponent(0.8)
        tblViewCreatePost.delegate = self
        tblViewCreatePost.dataSource =  self
        
        let nib = UINib.init(nibName: "CreatePostCell", bundle: nil)
        self.tblViewCreatePost.register(nib, forCellReuseIdentifier: "CreatePostCell")
        
        self.tblViewCreatePost.estimatedRowHeight = 40
        self.tblViewCreatePost.rowHeight = UITableView.automaticDimension
    }
    
    //MARK:-
    //MARK:- Main Method
    
    func Show(x: CGFloat, y : CGFloat, onCompletion: @escaping (String)-> Void){
        self.frame = UIScreen.main.bounds
        self.onCloser = onCompletion
        
        sortby_x.constant = x
        sortby_y.constant = y
        
   //     UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)   //-->> add to subview
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(self)
        
        self.tblViewCreatePost.reloadData()
        self.showWithAnimation()
    }
    
    //MARK:-
    //MARK:- Methods :
    
    func showWithAnimation(){
        //============== Show with animation
        
        sortViewContainer.alpha = 1
        
        UIView.animate(withDuration: 0, animations: {
            self.sortViewContainer.alpha = 1
        }) { (true) in
            
            UIView.animate(withDuration: 0.5) {
                self.tableviewContainer.alpha = 1
               // self.layoutIfNeeded()
            }
        }
    }
    
    
    func removeWithAnimation(){
        sortViewContainer.alpha = 1
        tableviewContainer.alpha = 1
        
        UIView.animate(withDuration: 0.7, animations: {
            self.sortViewContainer.alpha = 0
            self.tableviewContainer.alpha = 0
            self.layoutIfNeeded()
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    func removeWithAnimationWithValue(value : String){
        sortViewContainer.alpha = 1
        tableviewContainer.alpha = 1
        
        UIView.animate(withDuration: 0.7, animations: {
            self.sortViewContainer.alpha = 0
            self.tableviewContainer.alpha = 0
            self.layoutIfNeeded()
        }) { (true) in
            self.removeFromSuperview()
            self.onCloser(value)
        }
    }
    
    // for close the tag:
    @IBAction func actionSortBy(_ sender: UIButton) {
        removeWithAnimation()
    }
}

extension CreatePostView: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCreatePost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreatePostCell", for: indexPath) as! CreatePostCell
        
        cell.lblTitle.text = arrCreatePost[indexPath.row].title
        cell.img.image = arrCreatePost[indexPath.row].img
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 39
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        removeWithAnimationWithValue(value: arrCreatePost[indexPath.row].title)
    }
}
