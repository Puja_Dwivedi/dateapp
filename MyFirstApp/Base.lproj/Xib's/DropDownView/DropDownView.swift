//
//  DropDownView.swift

//

import UIKit
import SDWebImage



class DropDownView: UIView , UIPickerViewDelegate, UIPickerViewDataSource{
    
    
    //MARK: IBOutlet :>>>>>>>>>>>>>>>>>>>>>>>>>>>->>
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var pickarView: UIPickerView!
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var uiView_buttomSpace: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: variables :>>>>>>>>>>>>>>>>>>>>>>>>>>->>
    var onResult : ((String, Int)-> Void)!
   
    var arrList =  [(title: String, id: Int)]()
    
    var currentIndex : Int = 0
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        pickarView.delegate = self
        pickarView.dataSource = self
    }
    
    
    func show(title : String, arr : [Any], onCompletion: @escaping (String,Int)-> Void){
              //-->> add to subview
        let customPopUp = Bundle.main.loadNibNamed("DropDownView", owner: self, options: nil)?[0] as! DropDownView
        customPopUp.onResult = onCompletion
        customPopUp.frame = UIScreen.main.bounds
        customPopUp.currentIndex = 0

        
       
        customPopUp.lblTitle.text = title
        customPopUp.arrList = arr as! [(title: String, id: Int)]
        customPopUp.displayUIview()
    
      //  UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(customPopUp)
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(customPopUp)
    }
    
    @IBAction func actionDone(_ sender: UIButton) {
       removeWithCallBack(callBack: (arrList[currentIndex].title, arrList[currentIndex].id))
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        remove()
    }
    
    
    @IBAction func acitonRemove(_ sender: UIButton) {
        remove()
    }
    
    
    func displayUIview() {

        UIView.animate(withDuration: 0, animations: {
            self.uiView_buttomSpace.constant =  UIScreen.main.bounds.height
            self.uiView.layoutIfNeeded()
            self.layoutIfNeeded()
        }) { (true) in
            UIView.animate(withDuration: 0.5, animations: {
                self.uiView_buttomSpace.constant =  0
                self.layoutIfNeeded()
            })
        }
    }
    
    
    func remove(){
        uiView_buttomSpace.constant =  0
         
        UIView.animate(withDuration: 0.5, animations: {
            
            self.uiView_buttomSpace.constant =  UIScreen.main.bounds.height
            self.layoutIfNeeded()
            
        }) { (true) in
            
            self.removeFromSuperview()
        }
    }
    
    
    func removeWithCallBack(callBack : (String,Int)){
           uiView_buttomSpace.constant =  0
           //  self.uiView.alpha = 1
           UIView.animate(withDuration: 0.5, animations: {
               
               self.uiView_buttomSpace.constant =  UIScreen.main.bounds.height
               //   self.uiView.alpha = 0
               self.layoutIfNeeded()
               
           }) { [weak self](true) in
               
               self?.removeFromSuperview()
               self?.onResult(callBack.0,callBack.1)
               //   print("Date picker isplay: Done")
           }
       }
    
    
    //MARK: picker view delegate :
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
 
               return arrList.count
     
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrList[row].title
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 100.0
    }

    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    
          
            var label:UILabel
            
            if let v = view as? UILabel{
                label = v
            }
            else{
                label = UILabel.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            }
            
            label.textColor = UIColor.darkGray
            label.textAlignment = .center
            label.font = UIFont(name: "Titillium Web Regular", size: 25)
        
           label.text = arrList[row].title
           return label
        
    }

    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         currentIndex = row
    }
}
