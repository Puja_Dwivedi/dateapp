//
//  successSnackBar.swift
//  MyFirstApp
//
//  Created by cis on 12/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class successSnackBar: UIView {
    
    static let sharedInstance = successSnackBar.initLoader()
    @IBOutlet weak var bottom_space : NSLayoutConstraint!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    var viewcontroller : UIViewController?
    
    var custombar : SnackBar?
    
    class func initLoader() -> successSnackBar {
        return UINib(nibName: "successSnackBar", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! successSnackBar
    }
    
    
    func show(message : String ){
        
    //let customPopUp = Bundle.main.loadNibNamed("SnackBar", owner: self, options: nil)?[0] as! SnackBar
        self.lblMsg.text = message
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
         //-->> add to subview
        
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.fireTimer), userInfo: nil, repeats: false)
        
         // UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)   //-->> add to subview
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(self)
        self.showWithAnimation()
    }
    
    @objc func fireTimer() {
        DispatchQueue.main.async(execute: {
            self.removeWithAnimation()
        })
        
    }
    
    
    //MARK: Methods :>>>>>>>>>>>>>>->
           func showWithAnimation(){
            
              self.bottom_space.constant = -500
              self.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.bottom_space.constant = 100
                self.layoutIfNeeded()
            }
    
           }

       func removeWithAnimation(){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.bottom_space.constant = -500
                                  self.layoutIfNeeded()
        }) { (_) in
             self.removeFromSuperview()
        }
}
}
