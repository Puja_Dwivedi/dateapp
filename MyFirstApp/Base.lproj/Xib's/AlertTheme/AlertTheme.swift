//
//  AlertTheme.swift
//  LockApp
//
//  Created by cis on 12/09/19.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

class AlertTheme: UIView {
    static let sharedInstance = AlertTheme.initLoader()
    class func initLoader() -> AlertTheme {
        return UINib(nibName: "AlertTheme", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AlertTheme
    }
    
    enum ActivePopup {
        case normal
        case attribute
    }
    
    @IBOutlet weak var lblMesaage: UILabel!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblBottomtxt: UILabel!
    
    @IBOutlet weak var viewBtn1Container: UIViewClass!
    @IBOutlet weak var viewBtn2Container: UIViewClass!
    
    //MARK: Variables :>>>>>>>>>>>>>>->
    var onCloser : ((String)-> Void)!
    var activePopup = ActivePopup.normal
    var message = ""
    var attributeRangeString = ""

    //MARK: Flow of Project :>>>>>->
    override func awakeFromNib() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    //MARK: Attributed Alert
    
    func ShowAttributedAlert(popupCategory : ActivePopup, attributed_string: NSAttributedString, message : String, attributeRangeString : String ,isCancelButtonVisible: Bool,  arrBtn : [String],isBottomTxt : String, onCompletion: @escaping (String)-> Void){
        self.frame = UIScreen.main.bounds
        
        self.onCloser = onCompletion
        self.activePopup = popupCategory
        self.message = message
        self.attributeRangeString = attributeRangeString
        
        if isBottomTxt == "" {
            lblBottomtxt.isHidden = true
        }else{
            lblBottomtxt.isHidden = false
            lblBottomtxt.text = isBottomTxt
        }
            
        if activePopup == .normal {
            self.lblMesaage.text = message
        }else{
            self.lblMesaage.attributedText = attributed_string
        }
        
        if arrBtn.count > 1 {
            self.viewBtn1Container.isHidden = false
            self.viewBtn2Container.isHidden = false
            self.btn1.setTitle(arrBtn[0], for: .normal)
            self.btn2.setTitle(arrBtn[1], for: .normal)
        }else if arrBtn.count == 1{
            self.viewBtn1Container.isHidden = false
            self.viewBtn2Container.isHidden = true
            self.btn1.setTitle(arrBtn[0], for: .normal)
        }
        
        if isCancelButtonVisible {
            self.btnCancel.isHidden = false
        }else{
            self.btnCancel.isHidden = true
        }
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        
        keyWindow?.addSubview(self)
        
        self.showWithAnimation()
    }
    
    
    //MARK: Main Method :>>>>>>>>>>>>>>->
    func Show(popupCategory : ActivePopup, message : String, attributeRangeString : String ,isCancelButtonVisible: Bool,  arrBtn : [String],isBottomTxt : String, onCompletion: @escaping (String)-> Void){ 
        self.frame = UIScreen.main.bounds
        
        self.onCloser = onCompletion
        self.activePopup = popupCategory
        self.message = message
        self.attributeRangeString = attributeRangeString
        
        if isBottomTxt == "" {
            lblBottomtxt.isHidden = true
        }else{
            lblBottomtxt.isHidden = false
            lblBottomtxt.text = isBottomTxt
        }
            
        if activePopup == .normal {
            self.lblMesaage.text = message
        }else{
            DispatchQueue.main.async { [weak self] in
                self?.addAttributeText()
            }
        }
        
        if arrBtn.count > 1 {
            self.viewBtn1Container.isHidden = false
            self.viewBtn2Container.isHidden = false
            self.btn1.setTitle(arrBtn[0], for: .normal)
            self.btn2.setTitle(arrBtn[1], for: .normal)
        }else if arrBtn.count == 1{
            self.viewBtn1Container.isHidden = false
            self.viewBtn2Container.isHidden = true
            self.btn1.setTitle(arrBtn[0], for: .normal)
        }
        
        if isCancelButtonVisible {
            self.btnCancel.isHidden = false
        }else{
            self.btnCancel.isHidden = true
        }
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        
        keyWindow?.addSubview(self)
        
        self.showWithAnimation()
    }
    
    @IBAction func actionBtn1(_ sender: UIButton) {
        removeWithAnimation()
        self.onCloser((sender.titleLabel?.text)!)
    }
    
    @IBAction func actionBtn2(_ sender: UIButton) {
        
        removeWithAnimation()
        self.onCloser((sender.titleLabel?.text)!)
    }
    
    
    @IBAction func actionRemove(_ sender: UIButton) {
        removeWithAnimation()
    }
    
    
    //MARK: Methods :>>>>>>>>>>>>>>->
    func showWithAnimation(){
        //============== Show with animation
        self.uiView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        //self.uiView.alpha = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.uiView.transform = CGAffineTransform(scaleX: 1, y: 1)
            //  self.uiView.alpha = 1
        }) { (true) in
            //   print("Date picker isplay: Done")
        }
        
        
        //        let topPosition  = self.uiView.frame.origin.y
        //           self.uiView.frame = CGRect(x: self.uiView.frame.origin.x, y: -(self.uiView.frame.origin.y + UIScreen.main.bounds.height), width: self.uiView.frame.width, height: self.uiView.frame.height)
        //
        //        UIView.animate(withDuration:0.8,
        //                       delay: 0,
        //                       usingSpringWithDamping:0.5,
        //                       initialSpringVelocity: 1,
        //                       options: .curveEaseInOut,
        //                       animations: {
        //
        //                 self.uiView.frame = CGRect(x: self.uiView.frame.origin.x, y: topPosition, width: self.uiView.frame.width, height: self.uiView.frame.height)
        //
        //        }, completion: {(value: Bool) in
        //
        //        })
        
    }
    
    func removeWithAnimation(){
        //============== Show with animation
        self.transform = CGAffineTransform(scaleX: 1, y: 1)
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.uiView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            
        }) { (true) in
            self.removeFromSuperview()
            //   print("Date picker isplay: Done")
        }
        
        //        UIView.animate(withDuration:0.8,
        //                       delay: 0,
        //                       usingSpringWithDamping:0.5,
        //                       initialSpringVelocity: 1,
        //                       options: .curveEaseIn,
        //                       animations: {
        //
        //                        self.uiView.frame = CGRect(x: self.uiView.frame.origin.x, y: self.uiView.frame.origin.y + 500, width: self.uiView.frame.width, height: self.uiView.frame.height)
        //
        //                        self.layoutIfNeeded()
        //        }, completion: {(value: Bool) in
        //            self.removeFromSuperview()
        //        })
        
    }
}


/*
 Add attribute text
 */

extension AlertTheme {
    
    func addAttributeText(){
        
        let text = message
        lblMesaage.text = text
        self.lblMesaage.textColor =  UIColor.black
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: attributeRangeString)
//        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Poppins Bold", size: 15)!, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: range1)
        lblMesaage.attributedText = underlineAttriString
//        lblMesaage.isUserInteractionEnabled = true
//        lblMesaage.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
    }
    
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        
  let numberRange = (message as NSString).range(of: attributeRangeString)
            if gesture.didTapAttributedTextInLabel(label: lblMesaage, inRange: numberRange) {
                print("number tapped")
                removeWithAnimation()
                self.onCloser(attributeRangeString)
                
            }
       // let clickOnRange = (message as NSString).range(of: attributeRangeString)
        // comment for now
        //let privacyRange = (text as NSString).range(of: "Privacy Policy")
      
        
       // if gesture.didTapAttributedTextInLabel(label: lblMesaage, inRange: clickOnRange) {
       //     print("Tapped \(clickOnRange)")
       // }
    }
}

extension UITapGestureRecognizer {

   func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
         guard let attributedText = label.attributedText else { return false }

         let mutableStr = NSMutableAttributedString.init(attributedString: attributedText)
         mutableStr.addAttributes([NSAttributedString.Key.font : label.font!], range: NSRange.init(location: 0, length: attributedText.length))
         
         // If the label have text alignment. Delete this code if label have a default (left) aligment. Possible to add the attribute in previous adding.
         let paragraphStyle = NSMutableParagraphStyle()
         paragraphStyle.alignment = .center
         mutableStr.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: attributedText.length))

         // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
         let layoutManager = NSLayoutManager()
         let textContainer = NSTextContainer(size: CGSize.zero)
         let textStorage = NSTextStorage(attributedString: mutableStr)
         
         // Configure layoutManager and textStorage
         layoutManager.addTextContainer(textContainer)
         textStorage.addLayoutManager(layoutManager)
         
         // Configure textContainer
         textContainer.lineFragmentPadding = 0.0
         textContainer.lineBreakMode = label.lineBreakMode
         textContainer.maximumNumberOfLines = label.numberOfLines
         let labelSize = label.bounds.size
         textContainer.size = labelSize
         
         // Find the tapped character location and compare it to the specified range
         let locationOfTouchInLabel = self.location(in: label)
         let textBoundingBox = layoutManager.usedRect(for: textContainer)
         let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                           y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
         let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                      y: locationOfTouchInLabel.y - textContainerOffset.y);
         let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
         return NSLocationInRange(indexOfCharacter, targetRange)
     }
}
