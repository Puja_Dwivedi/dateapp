//
//  ViewHours.swift
//  HappyHours
//
//  Created by cis on 06/08/20.
//  Copyright © 2020 cis. All rights reserved.
//

import UIKit

class ViewHours: UIView {
    
    enum HoursActive {
        case twelve
        case twentyFour
    }
    
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    @IBOutlet weak var uiView: UIView!
    
    //MARK:-
    //MARK:- Variables :
    var onResult : ((String,Int)-> Void)!
    var activeHours = HoursActive.twelve
    var viewHoursVM = ViewHoursViewModel()
    var previousSelectedTime = ""
    
    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        pickerView.dataSource = self
        pickerView.delegate = self
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    
    //MARK: Main Method :>>>>>>>>>>>>>>->
    class func Show(hoursFormate :HoursActive,previousSelectedTime : String , onCompletion:@escaping (String,Int)-> Void){
        let customPopUp = Bundle.main.loadNibNamed("ViewHours", owner: self, options: nil)?[0] as! ViewHours
        customPopUp.frame = UIScreen.main.bounds
        
        customPopUp.onResult = onCompletion
        customPopUp.activeHours = hoursFormate
        customPopUp.previousSelectedTime = previousSelectedTime
        //  UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(customPopUp)   //-->> add to subview
        // customPopUp.pickerView.selectRow(currentNumber - 1, inComponent: 0, animated: false)
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(customPopUp)
        
        customPopUp.jumpToSelectedValue()
        customPopUp.showWithAnimation()
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        removeWithAnimation()
    }
    
    @IBAction func actionDone(_ sender: UIButton) {
        
        var strTime = ""
        var id = 0
        
        var mins = ""
        let minsRowSelected =  pickerView.selectedRow(inComponent: 1)
        if minsRowSelected == 0{ mins =  "00-15" }else if (minsRowSelected)*15 == 45 { mins = "\((minsRowSelected)*15)-00"
            
        }else{
            mins = "\((minsRowSelected)*15)-\((minsRowSelected)*15 + 15)"
        }
        
        if activeHours == .twelve{
            var aMorPMValue = ""
            let amRowSelected = pickerView.selectedRow(inComponent: 2)
            if amRowSelected == 0 { aMorPMValue = "AM" }else{ aMorPMValue = "PM" }
            
            
            strTime = "\(String(format: "%02d", pickerView.selectedRow(inComponent: 0) + 1)):\(mins) \(aMorPMValue)"
            
            if  let index =  viewHoursVM.arrIDs.firstIndex(where: {$0.timeOfTwelve == strTime}){
                id = viewHoursVM.arrIDs[index].id
            }
            
        }else{
            
            strTime = "\(String(format: "%02d", pickerView.selectedRow(inComponent: 0))):\(mins)"
            if  let index =  viewHoursVM.arrIDs.firstIndex(where: {$0.timeOfTwentyFour == strTime}){
                id = viewHoursVM.arrIDs[index].id
            }
        }
        onResult(strTime, id)
        removeWithAnimation()
    }
    
    //MARK:-
    //MARK:- Methods:
    
    func showWithAnimation() {
        bottomCons.constant = -UIScreen.main.bounds.height
        self.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomCons.constant = 0
            self.layoutIfNeeded()
            
        }) { (true) in
        }
    }
    
    
    func removeWithAnimation(){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomCons.constant = -UIScreen.main.bounds.height
            self.layoutIfNeeded()
            
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    func jumpToSelectedValue(){
        
        if activeHours == .twelve{
            
            
            let time = previousSelectedTime.components(separatedBy: ":")
            let hoursTime = (Int(time[0]) ?? 0) - 1
            //pickerView.selectRow(hoursTime, inComponent: 0, animated: true)
            let min = time[1].components(separatedBy: " ")
            
            pickerView.selectRow(hoursTime, inComponent: 0, animated: true)
            
            switch  min[0] {
            case "00-15":
                pickerView.selectRow(0, inComponent: 1, animated: true)
                break
                
            case "15-30":
                pickerView.selectRow(1, inComponent: 1, animated: true)
                break
                
            case "30-45":
                pickerView.selectRow(2, inComponent: 1, animated: true)
                break
                
            case "45-00":
                pickerView.selectRow(3, inComponent: 1, animated: true)
                break
            default:
                break
            }
            
            
            switch  min[1] {
            case "AM":
                pickerView.selectRow(0, inComponent: 2, animated: true)
                break
                
            case "PM":
                pickerView.selectRow(1, inComponent: 2, animated: true)
                break
                
            default:
                break
            }
            
            
            
        }else if activeHours == .twentyFour{
            
            let time = previousSelectedTime.components(separatedBy: ":")
            pickerView.selectRow(Int(time[0]) ?? 0, inComponent: 0, animated: true)
            
            
            switch  time[1] {
            case "00-15":
                pickerView.selectRow(0, inComponent: 1, animated: true)
                break
                
            case "15-30":
                pickerView.selectRow(1, inComponent: 1, animated: true)
                break
                
            case "30-45":
                pickerView.selectRow(2, inComponent: 1, animated: true)
                break
                
            case "45-00":
                pickerView.selectRow(3, inComponent: 1, animated: true)
                break
            default:
                break
            }
        }
    }
}
extension ViewHours : UIPickerViewDataSource,UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if activeHours == .twelve{
            pickerView.subviews.forEach({
                $0.isHidden = $0.frame.height < 1.0
            })
            return 3
        }else if activeHours == .twentyFour {
            pickerView.subviews.forEach({
                $0.isHidden = $0.frame.height < 1.0
            })
            return 2
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if activeHours == .twelve{ // 3
            
            if component == 0 {
                return 12
            }else if component == 1 {
                return 4
            }else{
                return 2
            }
            
        }else if activeHours == .twentyFour {
            if component == 0 {
                return 24
            }else{
                return 4
            }
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 70
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        55
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let customView = Bundle.main.loadNibNamed("viewHoursXib", owner: nil, options: nil)![0] as! viewHoursXib
        
        if component == 0 {
            if activeHours == .twelve{
                customView.lblValue.text = "\(String(format: "%02d", row + 1))"
            }else{
                customView.lblValue.text = "\(String(format: "%02d", row))"
            }
            
            return customView
            
        }else if component == 1 {
            if row == 0{
                customView.lblValue.text =  "00 - 15"
                return customView
            }
            
            if (row)*15 == 45 {
                customView.lblValue.text = "\((row)*15) - 00"
                return customView
            }else{
                customView.lblValue.text = "\((row)*15) - \((row)*15 + 15)"
                return customView
            }
            
        }else if component == 2 {
            
            if row == 0 {
                customView.lblValue.text = "AM"
                return customView
            }else{
                customView.lblValue.text = "PM"
                return customView
            }
        }
        return customView
    }
}

