//
//  ViewHoursViewModel.swift
//  MyFirstApp
//
//  Created by cis on 09/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ViewHoursModelClass {
    
    var timeOfTwelve : String
    var timeOfTwentyFour : String
    var id : Int
    
    
    init(timeOfTwelve : String, timeOfTwentyFour : String ,id : Int) {
        self.timeOfTwelve = timeOfTwelve
        self.timeOfTwentyFour = timeOfTwentyFour
        self.id = id
    }
}

class ViewHoursViewModel: NSObject {
    
    var arrIDs = [ViewHoursModelClass]()
    
    override init() {
        arrIDs = [
                  //AM
                  ViewHoursModelClass(timeOfTwelve: "12:00-15 AM", timeOfTwentyFour: "00:00-15", id: 1),
                  ViewHoursModelClass(timeOfTwelve: "12:15-30 AM", timeOfTwentyFour: "00:15-30", id: 2),
                  ViewHoursModelClass(timeOfTwelve: "12:30-45 AM", timeOfTwentyFour: "00:30-45", id: 3),
                  ViewHoursModelClass(timeOfTwelve: "12:45-00 AM", timeOfTwentyFour: "00:45-00", id: 4),
                  
                  ViewHoursModelClass(timeOfTwelve: "01:00-15 AM", timeOfTwentyFour: "01:00-15", id: 5),
                  ViewHoursModelClass(timeOfTwelve: "01:15-30 AM", timeOfTwentyFour: "01:15-30", id: 6),
                  ViewHoursModelClass(timeOfTwelve: "01:30-45 AM", timeOfTwentyFour: "01:30-45", id: 7),
                  ViewHoursModelClass(timeOfTwelve: "01:45-00 AM", timeOfTwentyFour: "01:45-00", id: 8),
                  
                  ViewHoursModelClass(timeOfTwelve: "02:00-15 AM", timeOfTwentyFour: "02:00-15", id: 9),
                  ViewHoursModelClass(timeOfTwelve: "02:15-30 AM", timeOfTwentyFour: "02:15-30", id: 10),
                  ViewHoursModelClass(timeOfTwelve: "02:30-45 AM", timeOfTwentyFour: "02:30-45", id: 11),
                  ViewHoursModelClass(timeOfTwelve: "02:45-00 AM", timeOfTwentyFour: "02:45-00", id: 12),
                  
                  ViewHoursModelClass(timeOfTwelve: "03:00-15 AM", timeOfTwentyFour: "03:00-15", id: 13),
                  ViewHoursModelClass(timeOfTwelve: "03:15-30 AM", timeOfTwentyFour: "03:15-30", id: 14),
                  ViewHoursModelClass(timeOfTwelve: "03:30-45 AM", timeOfTwentyFour: "03:30-45", id: 15),
                  ViewHoursModelClass(timeOfTwelve: "03:45-00 AM", timeOfTwentyFour: "03:45-00", id: 16),
                  
                  ViewHoursModelClass(timeOfTwelve: "04:00-15 AM", timeOfTwentyFour: "04:00-15", id: 17),
                  ViewHoursModelClass(timeOfTwelve: "04:15-30 AM", timeOfTwentyFour: "04:15-30", id: 18),
                  ViewHoursModelClass(timeOfTwelve: "04:30-45 AM", timeOfTwentyFour: "04:30-45", id: 19),
                  ViewHoursModelClass(timeOfTwelve: "04:45-00 AM", timeOfTwentyFour: "04:45-00", id: 20),
                  
                  ViewHoursModelClass(timeOfTwelve: "05:00-15 AM", timeOfTwentyFour: "05:00-15", id: 21),
                  ViewHoursModelClass(timeOfTwelve: "05:15-30 AM", timeOfTwentyFour: "05:15-30", id: 22),
                  ViewHoursModelClass(timeOfTwelve: "05:30-45 AM", timeOfTwentyFour: "05:30-45", id: 23),
                  ViewHoursModelClass(timeOfTwelve: "05:45-00 AM", timeOfTwentyFour: "05:45-00", id: 24),
                  
                  ViewHoursModelClass(timeOfTwelve: "06:00-15 AM", timeOfTwentyFour: "06:00-15", id: 25),
                  ViewHoursModelClass(timeOfTwelve: "06:15-30 AM", timeOfTwentyFour: "06:15-30", id: 26),
                  ViewHoursModelClass(timeOfTwelve: "06:30-45 AM", timeOfTwentyFour: "06:30-45", id: 27),
                  ViewHoursModelClass(timeOfTwelve: "06:45-00 AM", timeOfTwentyFour: "06:45-00", id: 28),
                  
                  ViewHoursModelClass(timeOfTwelve: "07:00-15 AM", timeOfTwentyFour: "07:00-15", id: 29),
                  ViewHoursModelClass(timeOfTwelve: "07:15-30 AM", timeOfTwentyFour: "07:15-30", id: 30),
                  ViewHoursModelClass(timeOfTwelve: "07:30-45 AM", timeOfTwentyFour: "07:30-45", id: 31),
                  ViewHoursModelClass(timeOfTwelve: "07:45-00 AM", timeOfTwentyFour: "07:45-00", id: 32),
                  
                  ViewHoursModelClass(timeOfTwelve: "08:00-15 AM", timeOfTwentyFour: "08:00-15", id: 33),
                  ViewHoursModelClass(timeOfTwelve: "08:15-30 AM", timeOfTwentyFour: "08:15-30", id: 34),
                  ViewHoursModelClass(timeOfTwelve: "08:30-45 AM", timeOfTwentyFour: "08:30-45", id: 35),
                  ViewHoursModelClass(timeOfTwelve: "08:45-00 AM", timeOfTwentyFour: "08:45-00", id: 36),
                  
                  ViewHoursModelClass(timeOfTwelve: "09:00-15 AM", timeOfTwentyFour: "09:00-15", id: 37),
                  ViewHoursModelClass(timeOfTwelve: "09:15-30 AM", timeOfTwentyFour: "09:15-30", id: 38),
                  ViewHoursModelClass(timeOfTwelve: "09:30-45 AM", timeOfTwentyFour: "09:30-45", id: 39),
                  ViewHoursModelClass(timeOfTwelve: "09:45-00 AM", timeOfTwentyFour: "09:45-00", id: 40),
                  
                  ViewHoursModelClass(timeOfTwelve: "10:00-15 AM", timeOfTwentyFour: "10:00-15", id: 41),
                  ViewHoursModelClass(timeOfTwelve: "10:15-30 AM", timeOfTwentyFour: "10:15-30", id: 42),
                  ViewHoursModelClass(timeOfTwelve: "10:30-45 AM", timeOfTwentyFour: "10:30-45", id: 43),
                  ViewHoursModelClass(timeOfTwelve: "10:45-00 AM", timeOfTwentyFour: "10:45-00", id: 44),
                  
                  ViewHoursModelClass(timeOfTwelve: "11:00-15 AM", timeOfTwentyFour: "11:00-15", id: 45),
                  ViewHoursModelClass(timeOfTwelve: "11:15-30 AM", timeOfTwentyFour: "11:15-30", id: 46),
                  ViewHoursModelClass(timeOfTwelve: "11:30-45 AM", timeOfTwentyFour: "11:30-45", id: 47),
                  ViewHoursModelClass(timeOfTwelve: "11:45-00 AM", timeOfTwentyFour: "11:45-00", id: 48),
                  
                  //PM
                  ViewHoursModelClass(timeOfTwelve: "12:00-15 PM", timeOfTwentyFour: "12:00-15", id: 49),
                  ViewHoursModelClass(timeOfTwelve: "12:15-30 PM", timeOfTwentyFour: "12:15-30", id: 50),
                  ViewHoursModelClass(timeOfTwelve: "12:30-45 PM", timeOfTwentyFour: "12:30-45", id: 51),
                  ViewHoursModelClass(timeOfTwelve: "12:45-00 PM", timeOfTwentyFour: "12:45-00", id: 52),
                  
                  ViewHoursModelClass(timeOfTwelve: "01:00-15 PM", timeOfTwentyFour: "13:00-15", id: 53),
                  ViewHoursModelClass(timeOfTwelve: "01:15-30 PM", timeOfTwentyFour: "13:15-30", id: 54),
                  ViewHoursModelClass(timeOfTwelve: "01:30-45 PM", timeOfTwentyFour: "13:30-45", id: 55),
                  ViewHoursModelClass(timeOfTwelve: "01:45-00 PM", timeOfTwentyFour: "13:45-00", id: 56),
                  
                  ViewHoursModelClass(timeOfTwelve: "02:00-15 PM", timeOfTwentyFour: "14:00-15", id: 57),
                  ViewHoursModelClass(timeOfTwelve: "02:15-30 PM", timeOfTwentyFour: "14:15-30", id: 58),
                  ViewHoursModelClass(timeOfTwelve: "02:30-45 PM", timeOfTwentyFour: "14:30-45", id: 59),
                  ViewHoursModelClass(timeOfTwelve: "02:45-00 PM", timeOfTwentyFour: "14:45-00", id: 60),
                  
                  ViewHoursModelClass(timeOfTwelve: "03:00-15 PM", timeOfTwentyFour: "15:00-15", id: 61),
                  ViewHoursModelClass(timeOfTwelve: "03:15-30 PM", timeOfTwentyFour: "15:15-30", id: 62),
                  ViewHoursModelClass(timeOfTwelve: "03:30-45 PM", timeOfTwentyFour: "15:30-45", id: 63),
                  ViewHoursModelClass(timeOfTwelve: "03:45-00 PM", timeOfTwentyFour: "15:45-00", id: 64),
                  
                  
                  ViewHoursModelClass(timeOfTwelve: "04:00-15 PM", timeOfTwentyFour: "16:00-15", id: 65),
                  ViewHoursModelClass(timeOfTwelve: "04:15-30 PM", timeOfTwentyFour: "16:15-30", id: 66),
                  ViewHoursModelClass(timeOfTwelve: "04:30-45 PM", timeOfTwentyFour: "16:30-45", id: 67),
                  ViewHoursModelClass(timeOfTwelve: "04:45-00 PM", timeOfTwentyFour: "16:45-00", id: 68),
                  
                  ViewHoursModelClass(timeOfTwelve: "05:00-15 PM", timeOfTwentyFour: "17:00-15", id: 69),
                  ViewHoursModelClass(timeOfTwelve: "05:15-30 PM", timeOfTwentyFour: "17:15-30", id: 70),
                  ViewHoursModelClass(timeOfTwelve: "05:30-45 PM", timeOfTwentyFour: "17:30-45", id: 71),
                  ViewHoursModelClass(timeOfTwelve: "05:45-00 PM", timeOfTwentyFour: "17:45-00", id: 72),
                  
                  ViewHoursModelClass(timeOfTwelve: "06:00-15 PM", timeOfTwentyFour: "18:00-15", id: 73),
                  ViewHoursModelClass(timeOfTwelve: "06:15-30 PM", timeOfTwentyFour: "18:15-30", id: 74),
                  ViewHoursModelClass(timeOfTwelve: "06:30-45 PM", timeOfTwentyFour: "18:30-45", id: 75),
                  ViewHoursModelClass(timeOfTwelve: "06:45-00 PM", timeOfTwentyFour: "18:45-00", id: 76),
                  
                  ViewHoursModelClass(timeOfTwelve: "07:00-15 PM", timeOfTwentyFour: "19:00-15", id: 77),
                  ViewHoursModelClass(timeOfTwelve: "07:15-30 PM", timeOfTwentyFour: "19:15-30", id: 78),
                  ViewHoursModelClass(timeOfTwelve: "07:30-45 PM", timeOfTwentyFour: "19:30-45", id: 79),
                  ViewHoursModelClass(timeOfTwelve: "07:45-00 PM", timeOfTwentyFour: "19:45-00", id: 80),
                  
                  ViewHoursModelClass(timeOfTwelve: "08:00-15 PM", timeOfTwentyFour: "20:00-15", id: 81),
                  ViewHoursModelClass(timeOfTwelve: "08:15-30 PM", timeOfTwentyFour: "20:15-30", id: 82),
                  ViewHoursModelClass(timeOfTwelve: "08:30-45 PM", timeOfTwentyFour: "20:30-45", id: 83),
                  ViewHoursModelClass(timeOfTwelve: "08:45-00 PM", timeOfTwentyFour: "20:45-00", id: 84),
                  
                  ViewHoursModelClass(timeOfTwelve: "09:00-15 PM", timeOfTwentyFour: "21:00-15", id: 85),
                  ViewHoursModelClass(timeOfTwelve: "09:15-30 PM", timeOfTwentyFour: "21:15-30", id: 86),
                  ViewHoursModelClass(timeOfTwelve: "09:30-45 PM", timeOfTwentyFour: "21:30-45", id: 87),
                  ViewHoursModelClass(timeOfTwelve: "09:45-00 PM", timeOfTwentyFour: "21:45-00", id: 88),
                  
                  ViewHoursModelClass(timeOfTwelve: "10:00-15 PM", timeOfTwentyFour: "22:00-15", id: 89),
                  ViewHoursModelClass(timeOfTwelve: "10:15-30 PM", timeOfTwentyFour: "22:15-30", id: 90),
                  ViewHoursModelClass(timeOfTwelve: "10:30-45 PM", timeOfTwentyFour: "22:30-45", id: 91),
                  ViewHoursModelClass(timeOfTwelve: "10:45-00 PM", timeOfTwentyFour: "22:45-00", id: 92),
                  
                  ViewHoursModelClass(timeOfTwelve: "11:00-15 PM", timeOfTwentyFour: "23:00-15", id: 93),
                  ViewHoursModelClass(timeOfTwelve: "11:15-30 PM", timeOfTwentyFour: "23:15-30", id: 94),
                  ViewHoursModelClass(timeOfTwelve: "11:30-45 PM", timeOfTwentyFour: "23:30-45", id: 95),
                  ViewHoursModelClass(timeOfTwelve: "11:45-00 PM", timeOfTwentyFour: "23:45-00", id: 96)
              ]
    }
}
