//
//  filterViewCell.swift
//  MyFirstApp
//
//  Created by cis on 04/10/22.
//  Copyright © 2022 cis. All rights reserved.
//

import UIKit

class filterViewCell: UITableViewCell {

    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnClick: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setInfo(title: String,isRadioOn : Bool){
        lblTitle.text = title
        if isRadioOn{
            btnRadio.setImage(#imageLiteral(resourceName: "radioBtnOn"), for: .normal)
        }else{
            btnRadio.setImage(#imageLiteral(resourceName: "unselectedIcon"), for: .normal)
        }
    }
}
