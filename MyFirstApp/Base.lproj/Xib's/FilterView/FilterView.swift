//
//  FilterView.swift
//  MyFirstApp
//
//  Created by cis on 04/10/22.
//  Copyright © 2022 cis. All rights reserved.
//

import Foundation
import UIKit

class FilterView: UIView {
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    @IBOutlet weak var uiView: UIView!
    @IBOutlet weak var uiViewHeight: NSLayoutConstraint!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    //MARK:-
    //MARK:- Variables :
    var onResult : ((String, String)-> Void)!
    var selected = (value: "", key: "")
    var arrFilterOption = [Any]()
    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        tblView.dataSource = self
        tblView.delegate = self
        uiViewHeight.constant = UIScreen.main.bounds.height/3 + 125
        backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        let nib = UINib.init(nibName: "filterViewCell", bundle: nil)
        tblView.register(nib, forCellReuseIdentifier: "filterViewCell")
    }
    
    
    //MARK: Main Method :>>>>>>>>>>>>>>->
    class func Show(previousSelectedValue : String, arrFilterOption : [Any], onCompletion:@escaping (String,String)-> Void){
        let customPopUp = Bundle.main.loadNibNamed("FilterView", owner: self, options: nil)?[0] as! FilterView
        customPopUp.frame = UIScreen.main.bounds
        
        customPopUp.onResult = onCompletion
        customPopUp.selected.key = previousSelectedValue
        customPopUp.arrFilterOption = arrFilterOption
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(customPopUp)
        
        customPopUp.showWithAnimation()
        customPopUp.getFilterData()
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        removeWithAnimation()
    }
    
    
    //MARK:-
    //MARK:- Methods:
    
    func showWithAnimation() {
        bottomCons.constant =  -uiViewHeight.constant
        self.uiView.cornerRadius = 0
      
        // Loader manage:-----------------
        self.loader.startAnimating()
        self.loader.isHidden = false
        self.lblHeaderTitle.text = "Please Wait..."
        //---------------------------------
        
        self.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomCons.constant =  -self.uiViewHeight.constant + 60 // here 60 is the header size:
            self.layoutIfNeeded()
            
        }) { (true) in
        }
    }
    
    
    func removeWithAnimation(){
        
        UIView.animate(withDuration: 0.7, animations: {
            self.bottomCons.constant = -UIScreen.main.bounds.height
            self.layoutIfNeeded()
            
        }) { (true) in
            self.removeFromSuperview()
        }
    }
}


//MARK:-
//MARK:- Tableview deeletes:

extension FilterView : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilterOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterViewCell", for: indexPath) as! filterViewCell
        
        let dict = arrFilterOption[indexPath.row] as! NSDictionary
        
        cell.setInfo(title: "\(dict.object(forKey: "description") ?? "")", isRadioOn: selected.key == "\(dict.object(forKey: "type") ?? "")" ? true : false)
        cell.btnClick.tag = indexPath.row
        cell.btnClick.addTarget(self, action: #selector(self.selectedOption(sender:)), for: .touchUpInside)
    
        return cell
    }
    
    @objc func selectedOption(sender: UIButton) {
        let dict = arrFilterOption[sender.tag] as! NSDictionary
        
        selected.key = "\(dict.object(forKey: "type") ?? "")"
        selected.value = "\(dict.object(forKey: "description") ?? "")"
        tblView.reloadData()
        perform(#selector(self.remove), with: self, afterDelay: 0.3)
      
    }
    
    @objc func remove(){
        onResult(selected.value,selected.key)
        removeWithAnimation()
    }
}


/*
 MARK:- API
 */
extension FilterView {
    
    func getFilterData()
    {
        // Loader manage:-----------------------------------
        self.perform(#selector(self.loadView), with: self, afterDelay: 0.9)
    }
    
    @objc func loadView() {
        self.loader.stopAnimating()
        self.loader.isHidden = true
        self.lblHeaderTitle.text = "Narrow your search"
        self.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.7, animations: {
            self.uiView.cornerRadius = 25
            self.bottomCons.constant = 0 // here 60 is the header size:
            self.layoutIfNeeded()
            
        }) { (true) in
        }
    }
}
