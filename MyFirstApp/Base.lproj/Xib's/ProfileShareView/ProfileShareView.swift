//
//  ReportReasonView.swift
//  MyFirstApp
//
//  Created by cis on 21/09/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class ProfileShareView: UIView {
    
    static let sharedInstance = ProfileShareView.initLoader()
    class func initLoader() -> ProfileShareView {
        return UINib(nibName: "ProfileShareView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ProfileShareView
    }
    
    
    //MARK:-
    //MARK:- IBOutlets
    @IBOutlet var viewCancel: UIViewClass!
    
    @IBOutlet weak var rootView: UIView!
    @IBOutlet var viewSubmit: UIViewClass!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    //MARK:-
    //MARK:- Variables :
    
    var onCloser : ((String)-> Void)!
    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        
        backgroundColor = UIColor.black.withAlphaComponent(0.8)
        viewSubmit.addShadowToView()
        viewCancel.addShadowToView()
    }
    
    //MARK:-
    //MARK:- Main Method
    
    func Show(onCompletion: @escaping (String)-> Void){
        
        self.frame = UIScreen.main.bounds
        self.onCloser = onCompletion
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        
        keyWindow?.addSubview(self)
        self.showWithAnimation()
    }
    
    
    
    //MARK:- Action Methods:
    
    @IBAction func btnCancel(_ sender: UIButton) {
        removeWithAnimation()
    }
    
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        removeWithAnimation(withValue: "share")
//    self.endEditing(true)
//    let strReason  = txtView.text!.trimmingCharacters(in: .whitespacesAndNewlines)
//
//        guard strReason.count > 0 else {
//        SnackBar.sharedInstance.show(message: AlertMessages.shareInstance.YourProfileWillBe, showMsgAt: .bottom); return }
//
//        removeWithAnimation(withValue: strReason)
    }
    
    
    //MARK:-
    //MARK:- Methods :
    
    func showWithAnimation(){
        //============== Show with animation
        self.rootView.transform = CGAffineTransform(translationX: 0, y: self.rootView.frame.size.height)
        self.alpha = 0
        
        UIView.animate(withDuration: 0.4, animations: {
             self.alpha = 1
            self.rootView.transform = CGAffineTransform(translationX: 0, y: 0)
        }) { (_) in
        }
    }
    
    func removeWithAnimation(){
        self.rootView.transform = CGAffineTransform(translationX: 0, y: 0)
         self.alpha = 1
        
        UIView.animate(withDuration: 0.4, animations: {
             self.alpha = 0
            self.rootView.transform = CGAffineTransform(translationX: 0, y: self.rootView.frame.size.height)
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    func removeWithAnimation(withValue: String){
        self.rootView.transform = CGAffineTransform(translationX: 0, y: 0)
         self.alpha = 1
        
        UIView.animate(withDuration: 0.6, animations: {
             self.alpha = 0
            self.rootView.transform = CGAffineTransform(translationX: 0, y: self.rootView.frame.size.height)
        }) { (true) in
            self.removeFromSuperview()
            self.onCloser(withValue)
        }
    }
}


