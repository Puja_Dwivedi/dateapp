//
//  viewMoreMatchCell.swift
//  MyFirstApp
//
//  Created by cis on 26/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class viewMoreMatchCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
