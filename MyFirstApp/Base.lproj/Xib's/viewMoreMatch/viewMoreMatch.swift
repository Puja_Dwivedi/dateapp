//
//  viewMoreMatch.swift
//  MyFirstApp
//
//  Created by cis on 26/02/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class viewMoreMatch: UIView {
    static let sharedInstance = viewMoreMatch.initLoader()
    class func initLoader() -> viewMoreMatch {
        return UINib(nibName: "viewMoreMatch", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! viewMoreMatch
    }
    
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var y_cons: NSLayoutConstraint!
    @IBOutlet weak var moreViewContainer: UIView!
    
    //MARK:-
    //MARK:- Variables :
    
    var onCloser : ((String)-> Void)!
    var arrMatches = [String]()
    var strMatch_id = ""
    
    //MARK:-
    //MARK:- App Flow
    
    override func awakeFromNib() {
        moreViewContainer.addShadowToView()
        //backgroundColor = UIColor.black.withAlphaComponent(0.8)
        tblview.delegate = self
        tblview.dataSource =  self
        
        let nib = UINib.init(nibName: "viewMoreMatchCell", bundle: nil)
        self.tblview.register(nib, forCellReuseIdentifier: "viewMoreMatchCell")
        
    }
    
    //MARK:-
    //MARK:- Main Method
    
    func Show(arr : [String], y : CGFloat, strMatch_id  : String ,  onCompletion: @escaping (String)-> Void){
     self.arrMatches.removeAll()
       
        for i in arr {
            if i != "" {
                self.arrMatches.append(i)
            }
        }
        
      //  self.arrMatches = arr
        self.frame = UIScreen.main.bounds
        self.onCloser = onCompletion
        
        self.strMatch_id = strMatch_id
        y_cons.constant = y
        
       // UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)   //-->> add to subview
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(self)
        
        self.tblview.reloadData()
        self.showWithAnimation()
    }
    
    //MARK:-
    //MARK:- Methods :
    
    func showWithAnimation(){
        //============== Show with animation
        
        moreViewContainer.alpha = 1
        
        UIView.animate(withDuration: 0, animations: {
            self.moreViewContainer.alpha = 1
        }) { (_) in
        }
    }
    
    func removeWithAnimation(){
        moreViewContainer.alpha = 1
        
        UIView.animate(withDuration: 0.7, animations: {
            self.moreViewContainer.alpha = 0
            self.layoutIfNeeded()
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    func removeWithAnimationWithValue(value : String){
        moreViewContainer.alpha = 1
        
        
        UIView.animate(withDuration: 0.7, animations: {
            self.moreViewContainer.alpha = 0
            
            self.layoutIfNeeded()
        }) { (true) in
            self.removeFromSuperview()
            self.onCloser(value)
        }
    }
    
    // for close the tag:
    @IBAction func actionClose(_ sender: UIButton) {
        removeWithAnimation()
    }
}

extension viewMoreMatch: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMatches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "viewMoreMatchCell", for: indexPath) as! viewMoreMatchCell
        
        cell.lblTitle.text = arrMatches[indexPath.row]
        
        if arrMatches[indexPath.row] == "Remove match" {
            if strMatch_id != ""{
                cell.viewContainer.backgroundColor = UIColor.white
                cell.lblTitle.textColor = UIColor.black
            }else{
                cell.viewContainer.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
                cell.lblTitle.textColor = UIColor.darkGray.withAlphaComponent(0.5)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 39
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrMatches[indexPath.row] == "Remove match" {
            if strMatch_id != ""{
                removeWithAnimationWithValue(value: arrMatches[indexPath.row])
            }
            return;
        }
        removeWithAnimationWithValue(value: arrMatches[indexPath.row])
    }
}

