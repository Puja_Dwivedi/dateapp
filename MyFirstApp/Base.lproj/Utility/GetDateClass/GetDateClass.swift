//
//  GetDateClass.swift
//  MyFirstApp
//
//  Created by cis on 23/06/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class GetDateClass: NSObject {

    func getDate()-> [(date : String,day : String)]{
         var arrDate = [(date : String,day : String)]()
      
        let todayDate = Date()
        let yesterDate :Date = todayDate - 1// Jan 21, 2019 example..
        
        let strToday = todayDate.string(format: "dd/MM/yyyy")
        let strYesterday = yesterDate.string(format: "dd/MM/yyyy")
        
        for index in 1...5 {
            
            let calclulateDate :Date = yesterDate - index// Jan 21, 2019
           
            let strCalclulateDate = calclulateDate.string(format: "dd/MM/yyyy")
            if strCalclulateDate != strToday && strCalclulateDate != strYesterday{
               // print("added date and day : \(strCalclulateDate), \(calclulateDate.dayofTheWeek)")
                arrDate.append((date: strCalclulateDate, day: calclulateDate.dayofTheWeek))
            }
        }
        return arrDate
    }
    
    
    func getDateTodayAndYesterDay()-> (today: Date, yesterDay: Date){
        let todayDate = Date()
        let yesterDate :Date = todayDate - 1// Jan 21, 2019
        return (today: todayDate, yesterDay: yesterDate)
    }
}
