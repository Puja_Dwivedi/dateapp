//
//  IBInspectableFunctions.swift

//

import Foundation
import UIKit

extension UITextField {
    
    @IBInspectable override var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
    }
    
    @IBInspectable override var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    
    @IBInspectable override var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var localizedPlaceholder: String {
        get {
            return ""
        }
        set {
            self.placeholder = ManageLocalization.getLocalizedString(key: newValue)
        }
    }
    
    @IBInspectable var placeholderColor: UIColor? {
        get {
            return self.attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .lightText
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [.foregroundColor: newValue!])
        }
    }
    
    
    @IBInspectable var localizedText: String {
        get {
            return ""
        }
        set {
            self.text = ManageLocalization.getLocalizedString(key: newValue)
        }
    }
}

extension UITextView {
    
    @IBInspectable override var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
    }
    
    @IBInspectable override var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    
    @IBInspectable override var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var localizedText: String {
        get {
            return ""
        }
        set {
            self.text = ManageLocalization.getLocalizedString(key: newValue)
        }
    }
}

extension UILabel {
    
    @IBInspectable var localizedText: String {
        get {
            return ""
        }
        set {
            self.text = ManageLocalization.getLocalizedString(key: newValue)
        }
    }
}

extension UINavigationItem {
    
    @IBInspectable var localizedTitle: String {
        get {
            return ""
        }
        set {
            self.title = ManageLocalization.getLocalizedString(key: newValue)
        }
    }
}

extension UIButton {
    
    @IBInspectable override var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
    }
    
    @IBInspectable override var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    
    @IBInspectable override var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var localizedTitle: String {
        get {
            return ""
        }
        set {
            self.setTitle(ManageLocalization.getLocalizedString(key: newValue), for: .normal)
        }
    }
    
}

extension UISearchBar {
    
    @IBInspectable var localizedPrompt: String {
        get {
            return ""
        }
        set {
            self.prompt = ManageLocalization.getLocalizedString(key: newValue)
        }
    }
    
    @IBInspectable var localizedPlaceholder: String {
        get {
            return ""
        }
        set {
            self.placeholder = ManageLocalization.getLocalizedString(key: newValue)
        }
    }
}

extension UISegmentedControl {
    
    @IBInspectable var localized: Bool {
        get {
            return true
        }
        set {
            for index in 0..<numberOfSegments {
                //                let title = NSLocalizedString(titleForSegment(at: index)!, comment: "")
                let title = ManageLocalization.getLocalizedString(key: titleForSegment(at: index)!)
                setTitle(title, forSegmentAt: index)
            }
        }
    }
}

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
}
