//
//  Location.swift

//

import Foundation
import CoreLocation

protocol LocationDelegate: class {
    func didReceivedCurrentLocation (latitude:CLLocationDegrees, longitude:CLLocationDegrees)
    func didReceivedtLocationError (error:String)
}

class Location:NSObject, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager ()
    weak var delegate:LocationDelegate?
    var setAddress: ((_ address:String)->Void)?
    var isConvertAddress = false
    var onCloser : ((String)-> Void)!
    
    override init() {
        super.init()
    }
    
    func setupLocationManager () {
        self.locationManager = CLLocationManager()
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
        } else {
            locationManager.startUpdatingLocation()
        }
    }
    
    func getCountryCode(lat : Double, long : Double, onCompletion: @escaping (String)-> Void){
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = long
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                guard placemarks != nil else{return}
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    if pm.isoCountryCode != nil {
                        
                        onCompletion("\(pm.isoCountryCode ?? "")")
                    }else{
                        onCompletion("")
                    }
                    
                   
                }
        })
    }
    
    
    func getCitName(lat : Double, long : Double, onCompletion: @escaping (String,String,String,String)-> Void){
        
          var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
              
              let ceo: CLGeocoder = CLGeocoder()
              center.latitude = lat
              center.longitude = long
              
              let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
              
              ceo.reverseGeocodeLocation(loc, completionHandler:
                  {(placemarks, error) in
                      if (error != nil)
                      {
                          print("reverse geodcode fail: \(error!.localizedDescription)")
                      }
                //    print("placemarks : \(placemarks)")
                  
                      
                      guard placemarks != nil else{return}
                      let pm = placemarks! as [CLPlacemark]
                   
                      
                      if pm.count > 0 {
                          let pm = placemarks![0]
                      
                          if pm.isoCountryCode != nil {
                            
                            onCompletion("\(pm.locality ?? "")","\(pm.country ?? "")","\(pm.administrativeArea ?? "")", "\(pm.isoCountryCode ?? "")")
                           
                            
                          }else{
                             onCompletion("","","","")
                          }
                          
                          // return self.setAddress!(addressString)
                      }
              })
          }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + " "
                    }
                    
                    
                    
                    // return self.setAddress!(addressString)
                }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            self.locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        
        self.locationManager.delegate = nil
        let location = locations.last
        
        if isConvertAddress {
            getAddressFromLatLon(pdblLatitude: "\(location?.coordinate.latitude ?? 0.0)", withLongitude: "\(location?.coordinate.longitude ?? 0.0)")
        }
        delegate?.didReceivedCurrentLocation(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate?.didReceivedtLocationError(error: error.localizedDescription)
    }
}


//extension CLPlacemark {
//    /// street name, eg. Infinite Loop
//    var streetName: String? { thoroughfare }
//    /// // eg. 1
//    var streetNumber: String? { subThoroughfare }
//    /// city, eg. Cupertino
//    var city: String? { locality }
//    /// neighborhood, common name, eg. Mission District
//    var neighborhood: String? { subLocality }
//    /// state, eg. CA
//    var state: String? { administrativeArea }
//    /// county, eg. Santa Clara
//    var county: String? { subAdministrativeArea }
//    /// zip code, eg. 95014
//    var zipCode: String? { postalCode }
//    /// postal address formatted
//    @available(iOS 11.0, *)
//    var postalAddressFormatted: String? {
//        guard let postalAddress = postalAddress else { return nil }
//        return CNPostalAddressFormatter().string(from: postalAddress)
//    }
//}
