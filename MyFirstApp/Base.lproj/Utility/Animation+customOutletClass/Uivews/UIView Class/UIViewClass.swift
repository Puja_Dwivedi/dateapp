//
//  UIViewClass.swift
//  BizzFinder
//
//  Created by Sumit on 24/07/17.
//  Copyright © 2017 5Exceptions. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class UIViewClass: UIView {
    
    var left : CGFloat!
    var right : CGFloat!
    var top : CGFloat!
    var buttom : CGFloat!
    let gradientLayer = CAGradientLayer()
    var shadowView : UIView = UIView()
    
    @IBInspectable var shadow: Bool = false
    @IBInspectable var shadowOpacity: Float = 0.0
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 1, height: 1)
    @IBInspectable var shadowRadius: CGFloat = 0.0
    @IBInspectable var shadowColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    //MARK: Border color
    @IBInspectable var borderColorCode: UIColor = UIColor.clear  {
        didSet {
            self.layer.borderColor = borderColorCode.cgColor
        }
    }
    
    // padding left :
    @IBInspectable var firstBackgroundColor: UIColor = UIColor.clear {
        didSet {
            createBlueGreenGradient()
        }
    }
    
    // padding left :
    @IBInspectable var secoandBackgroundColor: UIColor = UIColor.clear {
        didSet {
            createBlueGreenGradient()
        }
    }
    
    @IBInspectable var gradientLayerStartPoint: CGPoint = CGPoint(x: 0.0, y: 0.0)
    @IBInspectable var gradientLayerEndPoint: CGPoint = CGPoint(x: 0.0, y: 0.0)
  
    
    func createBlueGreenGradient(){
        self.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
        
        if firstBackgroundColor == UIColor.clear && secoandBackgroundColor == UIColor.clear {
            self.backgroundColor = UIColor.clear
            
        }else if firstBackgroundColor == UIColor.clear || secoandBackgroundColor == UIColor.clear{
            
            if firstBackgroundColor == UIColor.clear {
                self.backgroundColor = secoandBackgroundColor
                 // self.layer.cornerRadius = cornerRadius
                   //  self.layer.masksToBounds = true
                
            }else if secoandBackgroundColor == UIColor.clear {
                self.backgroundColor = firstBackgroundColor
                 // self.layer.cornerRadius = cornerRadius
                    // self.layer.masksToBounds = true
            }
//            if shadow{
//                addShadow()
//            }
        }else{
            self.backgroundColor = UIColor.clear
        //  self.layer.sublayers?.remove(at: 0)
    
//            for i in self.subviews{ // remove
//                i.removeFromSuperview()
//            }
            self.layer.insertSublayer(gradientFrom(firstcolor: firstBackgroundColor , secandColor: secoandBackgroundColor), at: 0)
        }
    }
    
    func gradientFrom(firstcolor : UIColor, secandColor  : UIColor )-> CAGradientLayer{
        let topColor =   firstcolor.cgColor  //UIColor(red: 220/255.0, green: 60/255.0, blue: 20/255.0, alpha: 1).cgColor
        let bottomColor =  secandColor.cgColor//UIColor(red: 255/255.0, green: 255/255.0, blue: 102/255.0, alpha: 1).cgColor
        let gradientColors = [topColor, bottomColor]
       // let gradientLocations: [NSNumber] = [1, 0]
        
        gradientLayer.colors = gradientColors
       // gradientLayer.locations = gradientLocations
        
           gradientLayer.cornerRadius = cornerRadius
        //Set startPoint and endPoint property also
        gradientLayer.startPoint = gradientLayerStartPoint
        gradientLayer.endPoint = gradientLayerEndPoint
       
//        if shadow{
//             addShadow()
//        }

        gradientLayer.frame =  CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        return gradientLayer
    }
    
    
    /*--------------------------------------------------------------------------------------------------*/
    
    // MARK: - View
    override func awakeFromNib() {
        self.setupView()
    }
    
    
    override func layoutSubviews() {
        gradientLayer.frame = self.bounds
        shadowView.frame = self.frame
        if shadow{
            if !self.isHidden {
                 addShadow()
            }
            
            
          
        }
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = cornerRadius
      
        createBlueGreenGradient()
        
        self.setNeedsLayout()
        self.setNeedsDisplay()
        self.clipsToBounds = true
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    func addShadow() {
        // for gradient shadow :
        
        if shadowView != UIView() {
             shadowView.removeFromSuperview()
        }
            shadowView = UIView(frame: frame)
        
            shadowView.layer.shadowColor = shadowColor.cgColor
            shadowView.layer.shadowOffset = shadowOffset
            shadowView.layer.masksToBounds = false
        
            shadowView.layer.shadowOpacity = shadowOpacity
            shadowView.layer.shadowRadius = shadowRadius
            shadowView.layer.shadowPath =  UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowView.layer.rasterizationScale = UIScreen.main.scale
            shadowView.layer.shouldRasterize = true
     
      //  print("frame : \(frame), shadow : \(shadowView.frame)")
            superview?.insertSubview(shadowView, belowSubview: self)
        
    }
}

// rounded only upper corner : -
extension UIView {
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        
        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.frame = self.bounds
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }
}

/*
 calling like be :
 class View: UIView {
 override func layoutSubviews() {
 super.layoutSubviews()
 
 self.myView.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 8.0)
 }
 }
 */


