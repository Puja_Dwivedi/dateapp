//
//  ImageViewClass.swift

//


import Foundation
import UIKit

@IBDesignable class ImageViewClass: UIImageView {

    //MARK: - Background color
    @IBInspectable var backgroundColorCode: UIColor = UIColor.clear {
        didSet {
            self.backgroundColor =  backgroundColorCode
        }
    }
    
    
    //MARK: Border color
    @IBInspectable var borderColorCode: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColorCode.cgColor
        }
    }
    
//    
//    //MARK: Border width
//    @IBInspectable var borderWidth: CGFloat = 0.0 {
//        didSet {
//            self.layer.borderWidth = borderWidth
//        }
//    }
    
    
    //Mark: - Corner radious
    @IBInspectable var circleView: Bool = false {
        didSet {
            
            if circleView != false{
                
                self.layer.cornerRadius = self.frame.size.width / 2
                self.clipsToBounds = true
                
                self.layer.shadowColor   = UIColor.red.cgColor
                self.layer.shadowOffset  = CGSize(width: 1, height: 2)
                self.layer.shadowOpacity = 0.3
                self.layer.shadowRadius  = 1
            }
        }
    }
    
    
    /*--------------------------------------------------------------------------------------------------*/
    
    // MARK: - View
    override func awakeFromNib() {
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    func setupView() {
        self.backgroundColor = backgroundColorCode
        
        if circleView != false{
            
            self.layer.cornerRadius = self.frame.size.width / 2
            self.clipsToBounds = true
        }
        
        //self.layer.masksToBounds = false
        
        self.setNeedsLayout()
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
    }
}

