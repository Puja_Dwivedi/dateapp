//
//  LabelClass.swift


import Foundation
import UIKit

@IBDesignable class LabelClass: UILabel {
    
    var left : CGFloat = 0
    var right : CGFloat = 0
    var top : CGFloat = 0
    var buttom : CGFloat = 0
    
    
    //MARK: - Background color
    @IBInspectable var backgroundColorCode: UIColor = UIColor.clear {
        didSet {
            self.backgroundColor =  backgroundColorCode
        }
    }
    
    
    //MARK: Border color
    @IBInspectable var borderColorCode: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColorCode.cgColor
        }
    }
 
    
    /*--------------------------------------------------------------------------------------------------*/
    //MARK: - pading : group ==: left , right , top , buttom*
    
    //
    // padding left :
    @IBInspectable var paddingLeft: CGFloat = 0.0 {
        didSet {
            self.left = paddingLeft
        }
    }
    
    // padding left :
    @IBInspectable var paddingRight: CGFloat = 0.0 {
        didSet {
            self.right = paddingRight
            
        }
    }
    
    
    // padding left :
    @IBInspectable var paddingTop: CGFloat = 0.0 {
        didSet {
            self.top = paddingTop
            
        }
    }
    
    
    // padding left :
    @IBInspectable var paddingButtom: CGFloat = 0.0 {
        didSet {
            self.buttom = paddingButtom
        }
    }
    
    
    /*--------------------------------------------------------------------------------------------------*/
    
    // MARK: - View
    override func awakeFromNib() {
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    func setupView() {
        self.backgroundColor = backgroundColorCode
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        
        // self.font = UIFont(name: GlobalCustomFont.Robo_Font, size: tintSize)
        
        self.setNeedsLayout()
        self.setNeedsDisplay()
        
    }

    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 5.0
    @IBInspectable var rightInset: CGFloat = 5.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}

