//
//  UtiltyFunctions.swift

//

import Foundation
import UIKit
import CoreLocation



struct Constants {
    static let alertTitle = ManageLocalization.getLocalizedString(key: "app_name")
    static let noInternet  = ManageLocalization.getLocalizedString(key: "no_intenet_connection")
    static let userDefault = UserDefaults.standard
    static let color_orange = UIColor.init(red: 245.0/255.0, green: 158.0/255.0, blue: 19.0/255.0, alpha: 1.0)
    static let color_black = UIColor.black
    static let color_green = UIColor.init(red: 53.0/255.0, green: 170.0/255.0, blue: 44.0/255.0, alpha: 1.0)
    static let Storyboard_Main = UIStoryboard.init(name: "Main", bundle: Bundle.main)
    static let device = "I"
}

struct Variables {
    static let email = "email"
    static let phone_number = "phone_number"
    static let name = "name"
    static let picture = "picture"
    static let user_id = "user_id"
    static let country_code = "country_code"
    static let deviceToken = "deviceToken"
    static let date_format = "date_format"
 
}

class ManageLocalization {
    
    static var deviceLang = ""
    class func getdeviceLangBundle()-> Bundle{
        // To get Language of device ...
        var path : String = String()
        
        if deviceLang.isEmpty {
            if let preferredLanguage = NSLocale.preferredLanguages[0] as String? {
                let language = (preferredLanguage as NSString).substring(to: 2)
                deviceLang = language
            }
        }
        
        let bundleLang = ["en"]
        if bundleLang.contains(deviceLang) {
            path = Bundle.main.path(forResource: deviceLang, ofType: "lproj")!
        } else {
            path = Bundle.main.path(forResource: "en", ofType: "lproj")!
        }
        
        return Bundle(path: path)!
    }
    
    class func getLocalizedString(key:String)-> String{
       return NSLocalizedString(key, tableName: nil, bundle: getdeviceLangBundle(), value: "", comment: "")
    }
}

extension UIViewController {

    static let test = Bool()
    
   
    func makeNavigationTitleClickable (title:String) -> UIButton {
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        button.backgroundColor = .clear
        button.setTitle(title, for: .normal)
        navigationItem.titleView = button
        
        return button
    }
    
    func setNavigationBar(navigationBarColor:UIColor, navigationTitleColor:UIColor) {
        navigationController?.navigationBar.barTintColor = navigationBarColor
        //navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:navigationTitleColor]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func showAlert(alertMessage : String) {
        let alertController : UIAlertController = UIAlertController(title: Constants.alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let OKAction : UIAlertAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showToast (title:String, msg:String, timeInterval:TimeInterval) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) {[weak self] _ in
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    func ValidateEmail(email :String)->Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func isNetworkConnected() -> Bool {
        
        let reachability: Reachability =  Reachability.init()!
        switch reachability.currentReachabilityStatus{
        case .reachableViaWiFi:
            print("Connected With wifi")
            return true
        case .reachableViaWWAN:
            print("Connected With Cellular network(3G/4G)")
            return true
        case .notReachable:
            print("Not Connected")
            return false
        }
    }
    
    func makeCall(phoneNum: String) {
        if let url = URL(string: "tel://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
}

extension String{ // return localize string
    func localize()-> String{
        return (ManageLocalization.getLocalizedString(key: self))
    }
}
