//
//  UIViewController+Extensions.swift
//  MyFirstApp
//
//  Created by cis on 11/07/23.
//  Copyright © 2023 cis. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func removeUserDefaults() {
        UserDefaults.standard.removeObject(forKey: "PostTimeId")
        UserDefaults.standard.removeObject(forKey: "eventType")
        UserDefaults.standard.removeObject(forKey: "tell_us_your_mind")
        UserDefaults.standard.removeObject(forKey: "MyPreferences")
        UserDefaults.standard.removeObject(forKey: "MyAttributes")
        UserDefaults.standard.removeObject(forKey: "ProfileImage")
        UserDefaults.standard.removeObject(forKey: "isBackTapped")
        UserDefaults.standard.removeObject(forKey: "TimeInterval")
        UserDefaults.standard.removeObject(forKey: "attribute")
        UserDefaults.standard.removeObject(forKey: "placeId")
        UserDefaults.standard.removeObject(forKey: "placeAPIDetailData1")
        UserDefaults.standard.removeObject(forKey: "placeAPIDetailData2")
        UserDefaults.standard.removeObject(forKey: "isCountryFound")
        UserDefaults.standard.removeObject(forKey: "TimeSelected")
        UserDefaults.standard.removeObject(forKey: "PublicIndicator")
        UserDefaults.standard.removeObject(forKey: "DisplayPicture")
        UserDefaults.standard.removeObject(forKey: "TimePostActiveStatus")
        UserDefaults.standard.removeObject(forKey: "AutomatchActiveStatus")
        UserDefaults.standard.removeObject(forKey: "attribute_text")
        UserDefaults.standard.removeObject(forKey: "attribute_city")
    }
}

extension UIViewController {
    func removeViewController() {
        if let navigationController = navigationController {
            // Check if there is at least one view controller in the navigation stack.
            if navigationController.viewControllers.count > 1 {
                // Pop the topmost view controller from the navigation stack.
                navigationController.popViewController(animated: true)
            }
        }
    }
    
    func removeSpecificViewControllerFromStack() {
        if let navigationController = navigationController {
            // Specify the view controller you want to remove.
            if let viewControllerToRemove = navigationController.viewControllers.first(where: { $0 is SelectPostTypeVC }) {
                // Pop to the specified view controller, effectively removing all view controllers after it.
                navigationController.popToViewController(viewControllerToRemove, animated: true)
            }
        }
    }
    
    func keepOnlyFirstViewController() {
        if let navigationController = navigationController {
            // Get the root view controller (the first one) from the navigation stack.
            if let rootViewController = navigationController.viewControllers.first {
                // Set the navigation stack with only the root view controller.
                navigationController.setViewControllers([rootViewController], animated: false)
                
                // Alternatively, if you don't want any animation, use the following line:
                // navigationController.setViewControllers([rootViewController], animated: false)
            }
        }
    }
    
    func keepOnlyFirstTwoViewControllers() {
           if let navigationController = navigationController {
               // Get the first two view controllers from the navigation stack.
               let firstTwoViewControllers = Array(navigationController.viewControllers.prefix(2))

               // Set the navigation stack with only the first two view controllers.
               navigationController.setViewControllers(firstTwoViewControllers, animated: true)

               // Alternatively, if you don't want any animation, use the following line:
               // navigationController.setViewControllers(firstTwoViewControllers, animated: false)
           }
       }
}

extension UIViewController {
    func checkIsTagRepeated(str: String, strArr: [String]) -> Bool {
        for i in 0..<strArr.count {
            if strArr[i].lowercased() == str.trimmingCharacters(in: .whitespacesAndNewlines).lowercased() {
                return true
            }
        }
        return false
    }
}

extension UIView {
    func checkIsTagRepeated(str: String, strArr: [String]) -> Bool {
        for i in 0..<strArr.count {
            if strArr[i].lowercased() == str.trimmingCharacters(in: .whitespacesAndNewlines).lowercased() {
                return true
            }
        }
        return false
    }
}
