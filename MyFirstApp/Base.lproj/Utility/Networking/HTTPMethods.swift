//
//  HTTPMethods.swift

//

import Foundation

enum HTTPMethods: String {
    
    case post = "POST"
    case get = "GET"
    case delete = "DELETE"
    case put = "PUT"
}
