//
//  Endpoint.swift

//

import Foundation

/// Protocol for easy construction of URls, ideally an enum will be the one conforming to this protocol.
protocol Endpoint {
    
    var base:  String { get }
    var path: String { get }
}

extension Endpoint {
    
//    var urlComponents: URLComponents? {
//        guard var components = URLComponents(string: base) else { return nil }
//        components.path = path
//        return components
//    }
    
    var request: URLRequest? {
//        guard let url = urlComponents?.url ?? URL(string: "\(self.base)\(self.path)") else { return nil }
        let request = URLRequest.init(url: URL.init(string: "\(self.base)\(self.path)")!)
        print("URL : \(request)")
        return request
    }

    func postRequest<T: Encodable>(parameters: T, headers: [HTTPHeader]) -> URLRequest? {
        
        guard var request = self.request else { return nil }
        request.httpMethod = HTTPMethods.post.rawValue
        do {
            request.httpBody = try JSONEncoder().encode(parameters)
        } catch let error {
            print(APIError.postParametersEncodingFalure(description: "\(error)").customDescription)
            return nil
        }
        headers.forEach { request.addValue($0.header.value, forHTTPHeaderField: $0.header.field) }
        return request
    }
    
//    func postRequest<T: Encodable>(dict: T) -> URLRequest?{
//        //print(dict)
//        guard var request = self.request else { return nil }
//        request.httpMethod = HTTPMethods.post.rawValue
//        request.cachePolicy = URLRequest.CachePolicy.useProtocolCachePolicy
//        request.httpShouldHandleCookies = false
//        request.timeoutInterval = 90.0
//        do {
//
//           let jsonData = try JSONEncoder().encode(dict) // pass dictionary to nsdata object and set it as request body
//            request.httpBody = jsonData
//            print("request : \(String(data: jsonData, encoding: .utf8)!)")
//
//        } catch let error {
//            print(APIError.postParametersEncodingFalure(description: "\(error)").customDescription)
//            return nil
//        }
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//
//        //request.addValue("application/json",forHTTPHeaderField: "Accept")
//        return request
//    }
//
    func postRequest () -> URLRequest?{
        //print(dict)
        guard var request = self.request else { return nil }
        request.httpMethod = HTTPMethods.post.rawValue
        request.cachePolicy = URLRequest.CachePolicy.useProtocolCachePolicy
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 90.0
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }

    
    func postRequestWithAccessToken(parameterDict : Dictionary<String, String>) -> URLRequest?{
           //print(dict)
           guard var request = self.request else { return nil }
           request.httpMethod = HTTPMethods.post.rawValue
           request.cachePolicy = URLRequest.CachePolicy.useProtocolCachePolicy
           request.httpShouldHandleCookies = false
           request.timeoutInterval = 90.0
        
        var paramString = ""
                        for (key, value) in parameterDict {
                            let escapedKey =  key.addingPercentEncoding( withAllowedCharacters: .urlHostAllowed)
                            var escapedValue = value.addingPercentEncoding( withAllowedCharacters: .urlHostAllowed)
                            
                            if escapedKey == "mobile"{
                                escapedValue = escapedValue?.replacingOccurrences(of: "+", with: "%2B")
                            }
                            paramString += "\(escapedKey ?? "")=\(escapedValue ?? "")&"
                        }
        request.httpBody = paramString.data(using: String.Encoding.utf8)

        print("request response: \(parameterDict)")
//        request.addValue((Constants.userDefault.object(forKey: Variables.accessToken)! as! String), forHTTPHeaderField: "API-ACCESS-TOKEN")
        
         
           return request
       }
    
    func postRequestWithAccessTokenWithBearer(parameterDict : Dictionary<String, String>) -> URLRequest?{
              //print(dict)
              guard var request = self.request else { return nil }
              request.httpMethod = HTTPMethods.post.rawValue
              request.cachePolicy = URLRequest.CachePolicy.useProtocolCachePolicy
              request.httpShouldHandleCookies = false
              request.timeoutInterval = 90.0
        
           
           var paramString = ""
                           for (key, value) in parameterDict {
                               let escapedKey =  key.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                            var escapedValue = value.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                             
                            if escapedKey == "mobile"{
                            escapedValue = escapedValue?.replacingOccurrences(of: "+", with: "%2B")
                            }
                            
                            paramString += "\(escapedKey ?? "")=\(escapedValue ?? "")&"
                           }
           request.httpBody = paramString.data(using: String.Encoding.utf8)
            print("request : \(parameterDict)")
           // print("server request form: \(paramString)")
     //      request.addValue((Constants.userDefault.object(forKey: Variables.accessToken)! as! String), forHTTPHeaderField: "API-ACCESS-TOKEN")
     //   request.addValue("Bearer \(Constants.userDefault.object(forKey: Variables.token) ?? "")", forHTTPHeaderField: "Authorization")
              return request
    }
    
    
    
    func getRequestWithBearer() -> URLRequest?{
        //print(dict)
        guard var request = self.request else { return nil }
        request.httpMethod = HTTPMethods.get.rawValue
        request.cachePolicy = URLRequest.CachePolicy.useProtocolCachePolicy
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 90.0
    //    request.addValue((Constants.userDefault.object(forKey: Variables.accessToken)! as! String), forHTTPHeaderField: "API-ACCESS-TOKEN")
     //          request.addValue("Bearer \(Constants.userDefault.object(forKey: Variables.token) ?? "")", forHTTPHeaderField: "Authorization")
        
        return request
    }
    
    func postRequestWithMultipart(parameterDict : Dictionary<String, String>, images:[Data]) -> URLRequest?{
       let boundary = "Boundary-\(UUID().uuidString)"
        guard var request = self.request else { return nil }
        request.httpMethod = HTTPMethods.post.rawValue
        request.cachePolicy = .useProtocolCachePolicy
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 90.0
        

        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        let body = createMultipleBody(parameters: parameterDict, boundary: boundary, images:images, mimeType: "image/jpg",filename: "")
        print(body)
        request.httpBody =   body
        
        let postLength = "\(body.count)"
        
         //   request.addValue((Constants.userDefault.object(forKey: Variables.accessToken)! as! String), forHTTPHeaderField: "API-ACCESS-TOKEN")
         //   request.setValue("Bearer \(Constants.userDefault.object(forKey: Variables.token)!)", forHTTPHeaderField: "Authorization")
             
        
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        return request
    }
    
    
    func createMultipleBody(parameters: [String: String],
                            boundary: String,
                            images:[Data],
                            mimeType: String,
                            filename: String) -> Data {
        
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        print("parameters : \(parameters)")
        for (key, value) in parameters {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        var i = 1
        for img in images {
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"user_img\";filename=\(Date()).jpg\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimeType)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(img)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            i = i+1
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        //body.appendString("--\(boundary)--\r\n")
        return body as Data
    }
}

extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        print("request json : \(dictionary)")
        return dictionary
    }
}



class DictionaryEncoder {
    
    private let encoder = JSONEncoder()
    
    var dateEncodingStrategy: JSONEncoder.DateEncodingStrategy {
        set { encoder.dateEncodingStrategy = newValue }
        get { return encoder.dateEncodingStrategy }
    }
    
    var dataEncodingStrategy: JSONEncoder.DataEncodingStrategy {
        set { encoder.dataEncodingStrategy = newValue }
        get { return encoder.dataEncodingStrategy }
    }
    
    var nonConformingFloatEncodingStrategy: JSONEncoder.NonConformingFloatEncodingStrategy {
        set { encoder.nonConformingFloatEncodingStrategy = newValue }
        get { return encoder.nonConformingFloatEncodingStrategy }
    }
    
    var keyEncodingStrategy: JSONEncoder.KeyEncodingStrategy {
        set { encoder.keyEncodingStrategy = newValue }
        get { return encoder.keyEncodingStrategy }
    }
    
    func encode<T>(_ value: T) throws -> [String: Any] where T : Encodable {
        let data = try encoder.encode(value)
        return try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
   
    }
}

class DictionaryDecoder {
    
    private let decoder = JSONDecoder()
    
    var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy {
        set { decoder.dateDecodingStrategy = newValue }
        get { return decoder.dateDecodingStrategy }
    }
    
    var dataDecodingStrategy: JSONDecoder.DataDecodingStrategy {
        set { decoder.dataDecodingStrategy = newValue }
        get { return decoder.dataDecodingStrategy }
    }
    
    var nonConformingFloatDecodingStrategy: JSONDecoder.NonConformingFloatDecodingStrategy {
        set { decoder.nonConformingFloatDecodingStrategy = newValue }
        get { return decoder.nonConformingFloatDecodingStrategy }
    }
    
    var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy {
        set { decoder.keyDecodingStrategy = newValue }
        get { return decoder.keyDecodingStrategy }
    }
    
    func decode<T>(_ type: T.Type, from dictionary: [String: Any]) throws -> T where T : Decodable {
        let data = try JSONSerialization.data(withJSONObject: dictionary, options: [])
        return try decoder.decode(type, from: data)
    }
}


extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
