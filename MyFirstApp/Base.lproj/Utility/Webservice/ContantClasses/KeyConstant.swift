//
//  KeyConstant.swift
//  ClinicalTimeTracker
//
//  Created by cis on 01/06/18.
//  Copyright © 2018 CIS. All rights reserved.
//

import Foundation

struct KEY {
    
    static let DeviceType = "iOS"
    
    struct UserDefaults {
        static let k_App_Running_FirstTime = "userRunningAppFirstTime"
        static let isFirstTimeLaunch = "isFirstTimeLaunch"
        static let Token = "token"
    }
    
    struct Headers {
        static let Authorization = "Authorization"
        static let ContentType = "Content-Type"
    }
    struct Google{
        static let placesKey = "some key here"
        static let serverKey = "some key here"
    }
    
    struct ErrorMessage{
        static let listNotFound = "ERROR_LIST_NOT_FOUND"
        static let validationError = "ERROR_VALIDATION"
    }
    
    struct LanguageValue {
        static let English = "en"
    }
    
    struct ServiceKeys {
        static let Status = "status"
        static let Message = "msg" //"message"
        static let ResponseData = "responseData"
        static let MsgCode = "code"
        static let Token = "token"
        static let Data = "body"
        static let dataResponse = "data"
        
        static let Error = "error"
        static let ErrorMessage = "error_message"
        static let PageNo = "page"
        static let PageLimit = "pagination_limit"
        static let NumberOfPages = "number_of_pages"
    }
    
    struct DeviceDetails
    {
        static let DeviceType = "device_type"
        static let DeviceID = "device_id"
        static let DeviceKey = "device_token"
    }
    

    
   
    struct UserDetails {
        static let FirstName = "firstname"
        static let LastName = "lastname"
        static let EmailId = "email"
        static let UserImage = "profile_image"
        static let UserId = "user_id"
        static let ProfileId = "id"
        static let UserData = "body"
        static let User = "user"
        static let FollowStatus = "follow_status"
        static let isFollowing = "isfollowing"
        static let FollowerCount = "follower_count"
        static let BlockStatus = "block_status"
        static let IsBlocked = "is_blocked"
    }
    
    struct Notifications {
        static let DateTime = "date_time"
        static let NotificationMsg = "notification_msg"
        static let NotificationId = "id"
    }
    
    struct ContactUs {
        static let EmailId = "email"
        static let Content = "content"
    }

}
