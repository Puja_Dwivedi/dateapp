//
//  ErrorMessageConstant.swift
//  BudSeeker
//
//  Created by cis on 22/11/17.
//  Copyright © 2017 CIS. All rights reserved.
//

import Foundation

struct ERROR {
    
    struct Service
    {
        static let NoInternet = "No Internet Connection";
        static let ConnectionLost = "The network connection was lost.";
        static let DataError = "Error in getting data";
    }
    struct DataError {
        static let NoDataAvailable = "No Data Available."
        static let ShownAllData = "No more data to load."
    }
}
