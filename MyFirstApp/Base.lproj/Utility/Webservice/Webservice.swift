//
//  Webservice.swift
//  Driver
//
//  Created by cis on 01/05/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

class Webservice: GenericAPIClient {
    var session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
}

extension KeyedDecodingContainer {
    public func decode<T: Decodable>(_ key: Key, as type: T.Type = T.self) throws -> T {
        return try self.decode(T.self, forKey: key)
    }
    
    public func decodeIfPresent<T: Decodable>(_ key: KeyedDecodingContainer.Key) throws -> T? {
        return try decodeIfPresent(T.self, forKey: key)
    }
}


//MARK:
struct APPURL {
    
    private struct Domains {
        static let Dev = ""
        static let Local = ""
        static let Live = "http://3.137.46.73:4000/"
        static let QA = "testAddress.qa.com"
    }
    
    private  struct Routes {
        static let Api = "api/"
    }
    
    private  static let Domain = Domains.Live
    private  static let Route = Routes.Api
    private  static let BaseURL = Domain + Route //comment route value for dev server
    private static let ImageBaseURL = "http://3.137.46.73:4000/images/"

    
    struct StaticContent {
        static var PrivacyPolicy: String {
            return BaseURL  + "PrivacyPolicy.html"
        }
        static var AboutUs: String {
            return Domain  + "/static_content/about-us.html"
        }
        static var TermsOfUse: String {
            return BaseURL  + "Terms_of_services.html"
        }
    }
    
    
    struct baseAPI {
        
        static var url : String {
            return BaseURL
        }
        
        static var imageURL : String {
            return ImageBaseURL
        }
    }
    
    struct Urls {
        
        static var userLogin : String {
            return BaseURL + "user-login"
        }
        
        static var userRegistration : String {
            return BaseURL + "user-registration"
        }
        
        static var getUserDetails : String {
            return BaseURL + "user-details"
        }
        
        static var reportUser : String {
            return BaseURL + "report-user"
        }
        
        static var getTagListByCategory : String {
            return BaseURL + "get-tag-list-by-category"
        }
        
        static var getTagCategoryList : String {
            return BaseURL + "get-tag-category-list-by-attribute"
        }
        
        static var getCategorylist : String {
            return BaseURL + "get-tag-category-list"
        }
        
        static var getProfileTagCategoryList : String {
            return BaseURL + "get-profile-tag-category-list"
        }
        
        static var getAttributeList : String {
             return BaseURL + "get-attribute-list"
         }
        
        static var getPostDetails : String {
            return BaseURL + "get-post-details"
        }
        
        static var getPostList : String {
            return BaseURL + "get-post-list"
        }
        
        static var getPlaceAllowed : String {
            return BaseURL + "get_place_allowed"
        }
        
        static var getLinkPostMatch : String {
            return BaseURL + "get-link-post-match"
        }

        static var postPlaceFiltered : String {
            return BaseURL + "post_place_filtered"
        }
        
        static var postStreetFiltered : String {
            return BaseURL + "post_street_filtered"
        }
        
        static var checkUserAbleCreatePost : String {
            return BaseURL + "check-user-able-create-post"
        }
        
        
        static var checkCountryExist : String {
            return BaseURL + "check-country-exist"
        }

        static var deletePost : String {
            return BaseURL + "delete-post"
        }
        
        
        static var getChatList : String {
            return BaseURL + "get-chat-list"
        }
        
        static var getMatchListByPostId : String {
            return BaseURL + "get-match-list-by-post-id"
        }
        
        static var getMatchDetails : String {
            return BaseURL + "get-match-details"
        }
        
        static var updateChatAgreeStatus : String {
            return BaseURL + "update_chat_agree_status"
        }
        
        static var removeMatch : String {
            return BaseURL + "delete-chat"
        }
        
        static var updateProfileIndicatorAPI : String {
            return BaseURL + "profile-show-hide"
        }
        
        static var setProfileUpdate : String {
            return BaseURL + "user-update"
        }
        
        static var setNotificationUpdate : String {
            return BaseURL + "user-update-settings"
        }
        
        static var logout : String {
            return BaseURL + "logout"
        }
        
        
        static var checkUserStatus : String {
            return BaseURL + "check-user-status"
        }
        
        static var updateLastActivityLocalTime : String {
            return BaseURL + "update-last-activity-local-time"
        }
        
        static var updateCountry : String {
            return BaseURL + "update-country"
        }
        
        static var updateEmail : String {
            return BaseURL + "user-update-settings"
        }
        
        static var getAboutURL : String {
            return BaseURL + "get-about-URL"
        }
        
        static var sendContactUSURL : String {
            return BaseURL + "send-contact-us"
        }
        
        static var sendTagSuggestion : String {
            return BaseURL + "send-tag-suggestion"
        }
        
        
        static var deleteUserAccount : String {
            return BaseURL + "user-delete"
        }
        
        static var userUpdateProfilePic : String {
            return BaseURL + "user-update-profile-picture"
        }
        
        static var createPOst : String {
            return BaseURL + "create-post"
        }
        
        static var preCreatePOst : String {
            return BaseURL + "check-create-post"
        }
      
        
        static var updatePostImage : String {
            return BaseURL + "update-post-image"
        }
        
        
        static var insertPlaceData : String {
            return BaseURL + "insert-place-data"
        }
        
        static var sendingNote : String {
            return BaseURL + "sending-note"
        }
        
        static var editPost : String {
            return BaseURL + "edit-post"
        }
        
        static var searchData : String {
            return BaseURL + "search-data-new"
        }
        
        static var checkFlightDataExist : String {
            return BaseURL + "check-flight-data-exist"
        }
        
        
        
        static var sendingNotification : String {
            return BaseURL + "sending-notification"
        }
    }
}
