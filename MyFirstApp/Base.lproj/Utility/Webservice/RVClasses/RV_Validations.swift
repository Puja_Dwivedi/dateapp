//
//  RV_Validations.swift
//

import Foundation
import UIKit

class RV_Validations: NSObject {
    
    // MARK: - Shared Instance
    static let sharedInstance: RV_Validations = {
        let instance = RV_Validations()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    
    class func isEmptyField(testStr: String) -> Bool
    {
        let trimmed = testStr.trimmingCharacters(in: .whitespaces)
        if trimmed.count == 0
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    class func isValidEmail(testStr:String) -> Bool
    {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func passwordStrength(passString : String) -> Bool
    {
        let emailRegEx = "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{6}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: passString)
        //
    }
    
    //Working Check Password Strength
    class func checkTextSufficientComplexity( text : String) -> Bool{
        
        let text = text
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: text)
        print("\(capitalresult)")
        
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: text)
        print("\(numberresult)")
  
        var isShort : Bool = false
        if text.count < 6
        {
            isShort = true
        }
        
        if(capitalresult && numberresult && !isShort)// && specialresult)
        {
            return true
        }
        else
        {
            return false
        }
        
    }
    
    //validate PhoneNumber
    class func isValidPhoneNumber(phoneNo : String) -> Bool
    {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: phoneNo, options: [], range: NSMakeRange(0, phoneNo.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == phoneNo.count && phoneNo.count == 10
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    class func isValidPhone(phone: String) -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: phone)
    }
    
    class func isAlphaNumeric(strValue : String) -> Bool
    {
        if strValue.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil {
            return false
        }
        else
        {
            return true
        }
    }
}
