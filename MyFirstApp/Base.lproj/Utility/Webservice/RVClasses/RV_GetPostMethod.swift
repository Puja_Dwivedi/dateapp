//
//  RV_GetPostMethod.swift
//

import UIKit
import SystemConfiguration
import Alamofire

class RV_GetPostMethod: NSObject {
    // MARK: - Shared Instance
    static let sharedInstance: RV_GetPostMethod = {
        let instance = RV_GetPostMethod()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    struct ResponseData {
        var response : NSDictionary
        var message : String
        
        init(response: NSDictionary, message: String)
        {
            self.response = response
            self.message = message
        }
    }
    
    class func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    class func getJsonHeader() -> HTTPHeaders
    {
        let headers: HTTPHeaders = ["Accept": "application/json" ]
        
        return headers
    }
    
    class func getHeaders() -> HTTPHeaders
    {
        /*
         Accept : form data
         API-ACCESS-TOKEN : 58BF0F94F0DD53961EC15923CE804FBC
         */
        
        //TODO:- Uncomment below code -
        let headers: HTTPHeaders = ["Accept": "application/json" ]
        
        
        return headers
    }
    
    class func postService(urlString: String, param : [String: AnyObject]?, headers: HTTPHeaders?,  completion: @escaping (_ result: String, _ data : ResponseData) -> Void)
    {
        let url1 : String = urlString
        let url = url1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        if(isInternetAvailable())
        {
            let manager = Alamofire.Session.default// Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            var encodingType : ParameterEncoding!
            
            if AppTheme.sharedInstance.encodeType == .URL
            {
                encodingType = URLEncoding.default
            }
            else
            {
                encodingType = JSONEncoding.default
            }
            manager.request(url!, method: .post, parameters: param, encoding: encodingType, headers: headers)
                .responseJSON
                {
                    response in
                    
                    switch response.result
                    {
                    case .success(let value):
                        
                        let dictResponse = RV_CheckDataType.getDictionary(anyDict: value as AnyObject)
                        // print(dictResponse)
                        if dictResponse.count > 0
                        {
                            completion("success", ResponseData(response: dictResponse, message: ""))
                        }
                        else
                        {
                            completion("error", ResponseData(response: NSDictionary(), message: ""))
                        }
                        
                    case .failure(let error):
                        print(error)
                        
                        if(error.localizedDescription != "")
                        {
                            if(!isInternetAvailable())
                            {
                                completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                            }
                            else
                            {
                                let strError =  error.localizedDescription
                                if(strError.contains("-1009"))
                                {
                                    completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                                }
                                else if(strError.contains("-1005"))
                                {
                                    completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.ConnectionLost))
                                }
                                else
                                {
                                    if let data = response.data {
                                        let json = String(data: data, encoding: String.Encoding.utf8)
                                        print("Failure Response: \(json)")
                                    }
                                    //completion("Error", ("Error in getting data. Please try again!" as AnyObject))
                                    completion("Error", ResponseData(response: NSDictionary(), message: (error.localizedDescription)))
                                }
                            }
                        }
                        else
                        {
                            
                            completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError))
                        }
                    }
                }
        }
        else
        {
            completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
        }
    }
    
    class func getService(urlString: String, param : [String: AnyObject]?, headers: HTTPHeaders?,  completion: @escaping (_ result: String, _ data : ResponseData) -> Void)
    {
        let url1 : String = urlString
        let url = url1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let manager = Alamofire.Session.default// Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        if(isInternetAvailable())
        {
            var encodingType : ParameterEncoding!
            
            if AppTheme.sharedInstance.encodeType == .URL
            {
                encodingType = URLEncoding.default
            }
            else
            {
                encodingType = JSONEncoding.default
            }
            manager.request(url!, method: .get, parameters: param, encoding: encodingType, headers: headers)
                .responseJSON
                {
                    response in
                    
                    switch response.result
                    {
                    case .success(let value):
                        
                        
                        let dictResponse = RV_CheckDataType.getDictionary(anyDict: value as AnyObject)
                        // print(dictResponse)
                        if dictResponse.count > 0
                        {
                            completion("success", ResponseData(response: dictResponse, message: ""))
                        }
                        else
                        {
                            completion("error", ResponseData(response: NSDictionary(), message: ""))
                        }
                        
                        
                    /*
                     handleResponse(data: value as AnyObject, completion: { (result, data) in
                     completion(result,data)
                     })
                     */
                    
                    case .failure(let error):
                        print(error)
                        
                        if(error.localizedDescription != "")
                        {
                            if(!isInternetAvailable())
                            {
                                
                                completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                            }
                            else
                            {
                                let strError =  error.localizedDescription
                                if(strError.contains("-1009"))
                                {
                                    completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                                }
                                else if(strError.contains("-1005"))
                                {
                                    completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.ConnectionLost))
                                }
                                else
                                {
                                    //completion("Error", ("Error in getting data. Please try again!" as AnyObject))
                                    completion("Error", ResponseData(response: NSDictionary(), message: error.errorDescription ?? strError))
                                }
                            }
                        }
                        else
                        {
                            completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError))
                        }
                    }
                }
        }
        else
        {
            completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
        }
    }
    
    //MARK: -- Handle the response here and customize this method as per need
    class func handleResponse(data : AnyObject,  completion: @escaping (_ result: String, _ data : ResponseData) -> Void)
    {
        
        
        let dictResponse = RV_CheckDataType.getDictionary(anyDict: data)
        // print(dictResponse)
        if dictResponse.count > 0
        {
            //---New Structure--
            var dictData = RV_CheckDataType.getDictionary(anyDict: dictResponse.value(forKey: KEY.ServiceKeys.ResponseData) as AnyObject)
            if dictData.count == 0
            {
                dictData = dictResponse
            }
            if RV_CheckDataType.getInteger(anyString: dictResponse.value(forKey: KEY.ServiceKeys.Status) as AnyObject) == 1
            {
                let message = RV_CheckDataType.getString(anyString: dictResponse.value(forKey: KEY.ServiceKeys.Message) as AnyObject)
                completion("success", ResponseData(response: dictData, message: message))
            }
            else
            {
                let errorMessage = RV_CheckDataType.getString(anyString: dictResponse.value(forKey: KEY.ServiceKeys.Message) as AnyObject)
                completion("error", ResponseData(response: dictData, message: errorMessage))
            }
        }
    }
    
    //MARK: -- Upload image to sever --
    class func uploadImage(strUrl: String, imageData : Data, parameters : [String : AnyObject], imageName : String ,completion: @escaping (_ result: String, _ data : ResponseData)-> Void)
    {
        let manager = Alamofire.Session.default// Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        
        manager.upload(
            multipartFormData:
                { (multipartFormData) in
                    if imageData.count > 0
                    {
                        var Timestamp: String {
                            return "\(NSDate().timeIntervalSince1970 * 1000)"
                        }
                        let imageFileName = "\(imageName).png"
                        
                        multipartFormData.append(imageData, withName: imageName, fileName: imageFileName, mimeType: "image/jpeg")
                    }
                    
                    for (key, value) in parameters
                    {
                        let string : String = RV_CheckDataType.getString(anyString: value)
                        multipartFormData.append(string.data(using: .utf8)!, withName: key);
                    }
                    //
                }, to: strUrl, method: .post).response { (response) in
                    // print(response)
                    
                    if let err = response.error{
                        print(err)
                        //onError?(err)
                        
                        if(err.localizedDescription != "")
                        {
                            if(!isInternetAvailable())
                            {
                                
                                completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                            }
                            else
                            {
                                let strError =  err.localizedDescription
                                if(strError.contains("-1009"))
                                {
                                    completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                                }
                                else if(strError.contains("-1005"))
                                {
                                    completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.ConnectionLost))
                                }
                                else
                                {
                                    //completion("Error", ("Error in getting data. Please try again!" as AnyObject))
                                    completion("Error", ResponseData(response: NSDictionary(), message: strError))
                                }
                            }
                        }
                        else
                        {
                            completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError))
                        }
                        
                        return
                    }
                    print("Succesfully uploaded")
                    
                    let value = response.data
                    
                    if (value != nil)
                    {
                        let dictResponse = RV_CheckDataType.getDictionary(anyDict: value as AnyObject)
                        completion("success", ResponseData(response: dictResponse, message: "success"))
                    }
                }
        
        
        
    }
    
    //MARK: -- Form Data Post service with header --
    class func formRequestWith(strUrl: String, imageData : Data?, imageName : String? ,parameters: [String : Any], headers: HTTPHeaders?, completion:@escaping (_ result: String, _ data : ResponseData) -> Void) {
        
        let url = strUrl
        
        let manager = Alamofire.Session.default// Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.upload(multipartFormData: { (multipartFormData) in
            if imageData != nil && (imageData?.count)! > 0
            {
                var Timestamp: String {
                    return "\(NSDate().timeIntervalSince1970 * 1000)"
                }
                let imageFileName = "\(imageName!).png"
                
                multipartFormData.append(imageData!, withName: imageName!, fileName: imageFileName, mimeType: "image/jpeg")
            }
            
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers).response { (response) in
            //   print(response
            
            if let err = response.error{
                print(err)
                //onError?(err)
                
                if(err.localizedDescription != "")
                {
                    if(!isInternetAvailable())
                    {
                        
                        completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                    }
                    else
                    {
                        let strError =  err.localizedDescription
                        if(strError.contains("-1009"))
                        {
                            completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                        }
                        else if(strError.contains("-1005"))
                        {
                            completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.ConnectionLost))
                        }
                        else
                        {
                            //completion("Error", ("Error in getting data. Please try again!" as AnyObject))
                            completion("Error", ResponseData(response: NSDictionary(), message: strError))
                        }
                    }
                }
                else
                {
                    completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError))
                }
                
                return
            }
            print("Succesfully uploaded")
            
            let value = response.data
            do {
                if let parseJSON = try JSONSerialization.jsonObject(with: value!, options: []) as? NSDictionary {
                    //  print(parseJSON)
                    let dictResponse = RV_CheckDataType.getDictionary(anyDict: parseJSON as AnyObject)
                    
                    handleResponse(data: dictResponse as AnyObject, completion: { (result, data) in
                        completion(result,data)
                    })
                    
                    //completion("success", ResponseData(response: dictResponse, message: "success"))
                }
            } catch let error as NSError {
                print(error.localizedDescription)
                completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError))
            }
            
        }
        
    }
    
    //MARK: -- Form Data Upload Multiple Images on sever --
    class func uploadPostImages(strUrl: String, imageArray : NSArray, parameters : [String : AnyObject], headers: HTTPHeaders?, completion: @escaping (_ result: String, _ data : ResponseData, _ progress : Double)-> Void)
    {
        let url = strUrl
        let manager = Alamofire.Session.default// Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.upload(
            multipartFormData:
                { multipartFormData in
                    var index = 0
                    for imageData in imageArray
                    {
                        let dictData = RV_CheckDataType.getDictionary(anyDict: imageData as AnyObject)
                        let imageName = RV_CheckDataType.getString(anyString: dictData.value(forKey: "ImageName") as AnyObject)
                        let image = dictData.value(forKey: "Image") as? Data
                        if (image?.count)! > 0
                        {
                            var Timestamp: String {
                                return "\(Int(NSDate().timeIntervalSince1970))"
                            }
                            let imageFileName = "\(Timestamp).png"
                            
                            multipartFormData.append(image!, withName: "post_file[]", fileName: imageFileName, mimeType: "image/png")
                        }
                        index += 1
                    }
                    
                    
                    for (key, value) in parameters
                    {
                        let string : String = RV_CheckDataType.getString(anyString: value)
                        multipartFormData.append(string.data(using: .utf8)!, withName: key);
                    }
                }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers)
            .uploadProgress { progress in
                completion("progress", ResponseData(response: NSDictionary(), message: ""), progress.fractionCompleted)
            }
            
            .response { response in
                //  debugPrint(response)
                
                if let err = response.error{
                    print(err)
                    //onError?(err)
                    
                    if(err.localizedDescription != "")
                    {
                        if(!isInternetAvailable())
                        {
                            
                            completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet), 0)
                        }
                        else
                        {
                            let strError =  err.localizedDescription
                            if(strError.contains("-1009"))
                            {
                                completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet), 0)
                            }
                            else if(strError.contains("-1005"))
                            {
                                completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.ConnectionLost), 0)
                            }
                            else
                            {
                                //completion("Error", ("Error in getting data. Please try again!" as AnyObject))
                                completion("Error", ResponseData(response: NSDictionary(), message: strError), 0)
                            }
                        }
                    }
                    else
                    {
                        completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError), 0)
                    }
                    
                    return
                } else {
                    
                    
                    let value = response.data
                    do {
                        if let parseJSON = try JSONSerialization.jsonObject(with: value!, options: []) as? NSDictionary {
                            // print(parseJSON)
                            let dictResponse = RV_CheckDataType.getDictionary(anyDict: parseJSON as AnyObject)
                            
                            handleResponse(data: dictResponse as AnyObject, completion: { (result, data) in
                                completion(result,data, 0)
                            })
                            
                            //completion("success", ResponseData(response: dictResponse, message: "success"))
                        }
                    } catch let error as NSError {
                        print(error.localizedDescription)
                        completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError), 0)
                    }
                    
                }
                
            }
        
    }
    
    
    //MARK: -- Form Data Upload Multiple Images on sever --
    class func uploadPostCreateImages(strUrl: String, imageArray : NSArray, parameters : [String : AnyObject], headers: HTTPHeaders?, completion: @escaping (_ result: String, _ data : ResponseData)-> Void)
    {
        let url = strUrl
        
        let manager = Alamofire.Session.default// Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.upload(
            multipartFormData:
                { multipartFormData in
                    var index = 0
                    for imageData in imageArray
                    {
                        let dictData = RV_CheckDataType.getDictionary(anyDict: imageData as AnyObject)
                        let imageName = RV_CheckDataType.getString(anyString: dictData.value(forKey: "ImageName") as AnyObject)
                        let image = dictData.value(forKey: "Image") as? Data
                        if (image?.count)! > 0
                        {
                            var Timestamp: String {
                                return "\(Int(NSDate().timeIntervalSince1970))"
                            }
                            let imageFileName = "\(Timestamp).png"
                            
                            multipartFormData.append(image!, withName: "playlist_image", fileName: imageFileName, mimeType: "image/png")
                        }
                        index += 1
                    }
                    
                    
                    for (key, value) in parameters
                    {
                        let string : String = RV_CheckDataType.getString(anyString: value)
                        multipartFormData.append(string.data(using: .utf8)!, withName: key);
                    }
                }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers)
            .uploadProgress { progress in
                
            }
            
            .response { response in
                // debugPrint(response)
                
                if let err = response.error{
                    print(err)
                    //onError?(err)
                    
                    if(err.localizedDescription != "")
                    {
                        if(!isInternetAvailable())
                        {
                            
                            completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                        }
                        else
                        {
                            let strError =  err.localizedDescription
                            if(strError.contains("-1009"))
                            {
                                completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet))
                            }
                            else if(strError.contains("-1005"))
                            {
                                completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.ConnectionLost))
                            }
                            else
                            {
                                //completion("Error", ("Error in getting data. Please try again!" as AnyObject))
                                completion("Error", ResponseData(response: NSDictionary(), message: strError))
                            }
                        }
                    }
                    else
                    {
                        completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError))
                    }
                    
                    return
                } else {
                    let value = response.data
                    do {
                        if let parseJSON = try JSONSerialization.jsonObject(with: value!, options: []) as? NSDictionary {
                            // print(parseJSON)
                            let dictResponse = RV_CheckDataType.getDictionary(anyDict: parseJSON as AnyObject)
                            
                            handleResponse(data: dictResponse as AnyObject, completion: { (result, data) in
                                completion(result,data)
                            })
                            
                            //completion("success", ResponseData(response: dictResponse, message: "success"))
                        }
                    } catch let error as NSError {
                        print(error.localizedDescription)
                        completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError))
                    }
                }
            }
    }
    
    
    
    
    
    
    //MARK: -- Form Data Upload Multiple Images on sever --
    class func uploadPostVideos(strUrl: String, imageArray : NSArray, parameters : [String : AnyObject], headers: HTTPHeaders?, completion: @escaping (_ result: String, _ data : ResponseData, _ progress : Double)-> Void)
    {
        let url = strUrl
        
        let manager = Alamofire.Session.default// Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.upload(
            multipartFormData:
                { multipartFormData in
                    var index = 0
                    for imageData in imageArray
                    {
                        let dictData = RV_CheckDataType.getDictionary(anyDict: imageData as AnyObject)
                        let imageName = RV_CheckDataType.getString(anyString: dictData.value(forKey: "ImageName") as AnyObject)
                        let image = dictData.value(forKey: "Video") as? Data
                        if (image?.count)! > 0
                        {
                            var Timestamp: String {
                                return "\(Int(NSDate().timeIntervalSince1970))"
                            }
                            let imageFileName = "\(Timestamp).mp4"
                            
                            multipartFormData.append(image!, withName: "post_file[]", fileName: imageFileName, mimeType: "video/mp4")
                            
                            print("\n\n---\n imageFileName:- \(imageFileName) \n---\n\n")
                        }
                        index += 1
                    }
                    
                    
                    for (key, value) in parameters
                    {
                        let string : String = RV_CheckDataType.getString(anyString: value)
                        multipartFormData.append(string.data(using: .utf8)!, withName: key);
                        //print("\n\n---\n value:- \(value) , key:- \(key) \n---\n\n")
                    }
                }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers)
            .uploadProgress { progress in
                completion("progress", ResponseData(response: NSDictionary(), message: ""), progress.fractionCompleted)
            }
            
            .response { response in
                //debugPrint(response)
                
                if let err = response.error{
                    print(err)
                    //onError?(err)
                    
                    if(err.localizedDescription != "")
                    {
                        if(!isInternetAvailable())
                        {
                            
                            completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet), 0)
                        }
                        else
                        {
                            let strError =  err.localizedDescription
                            if(strError.contains("-1009"))
                            {
                                completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.NoInternet), 0)
                            }
                            else if(strError.contains("-1005"))
                            {
                                completion("Error",  ResponseData(response: NSDictionary(), message: ERROR.Service.ConnectionLost), 0)
                            }
                            else
                            {
                                //completion("Error", ("Error in getting data. Please try again!" as AnyObject))
                                completion("Error", ResponseData(response: NSDictionary(), message: strError), 0)
                            }
                        }
                    }
                    else
                    {
                        completion("Error", ResponseData(response: NSDictionary(), message: ERROR.Service.DataError), 0)
                    }
                    
                    return
                } else {
                    handleResponse(data: response.data as AnyObject, completion: { (result, data) in
                        completion(result,data, 0)
                    })
                }
            }
    }
    
    
    
    class func getService_API(API : String ,completion: @escaping (_ result: String, _ data: NSDictionary) -> Void)
        {
             print("Request : \(API)")
    
            // Set up the URL request
            let todoEndpoint: String = API
            guard let url = URL(string: todoEndpoint) else {
                print("Error: cannot create URL")
                return
            }
            let urlRequest = URLRequest(url: url)
    
    
            // set up the session
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
    
            // make the request
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                // check for any errors
                guard error == nil else {
                    print("error calling GET on /todos/1")
                    print(error!)
                    return
                }
                // make sure we got data
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                // parse the result as JSON, since that's what the API provides
                do {
                    guard let todo = try JSONSerialization.jsonObject(with: responseData, options: [])
                        as? [String: Any] else {
                            print("error trying to convert data to JSON")
                            return
                    }
                //    print("todo : \(todo)")
                print("Get information : \(todo)")
                   // let arr = todo["data"] as! [Any]
                   // self.arrSuggestedAttributeList =  self.getAttributeList(arrResponseData: arr)
                    completion("success", todo as NSDictionary)
                    // now we have the todo
                    // let's just print it to prove we can access it
                    // print("The todo is: " + todo.description)
    
                    // the todo object is a dictionary
                    // so we just access the title using the "title" key
                    // so check for a title and print it if we have one
                    //  guard let todoTitle = todo["title"] as? String else {
                    //    print("Could not get todo title from JSON")
                    //    return
                    // }
                    // print("The title is: " + todoTitle)
                } catch  {
                    completion("error", NSDictionary())
                    print("error trying to convert data to JSON")
                    return
                }
            }
            task.resume()
        }
}
