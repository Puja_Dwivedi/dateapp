//
//  RV_CheckDataType.swift
//

import Foundation
import UIKit

class RV_CheckDataType: NSObject
{
    // MARK: - Shared Instance
    static let sharedInstance: RV_CheckDataType = {
        let instance = RV_CheckDataType()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    class func getString(anyString : AnyObject) -> String
    {
        var returnString : String = ""
        if let str : String = anyString as? String
        {
            returnString = str
        }
        else if let intt : NSInteger = anyString as? NSInteger
        {
            returnString = String(intt)
        }
        else if let double : Double = anyString as? Double
        {
            returnString = String(format:"%.1f", double)
        }
        
        return returnString
    }
    
    class func getInteger(anyString : AnyObject) -> NSInteger
    {
        var returnInteger : NSInteger = 0
        if let intt : NSInteger = anyString as? NSInteger
        {
            returnInteger = intt
        }
        else if let str : String = anyString as? String
        {
            if(str != "")
            {
                returnInteger = NSInteger(str)!
            }
        }
        else if let intt : Float = anyString as? Float
        {
            returnInteger = Int(intt)
        }
        
        return returnInteger
    }
    
    class func getDouble(anyString : AnyObject) -> Double
    {
        var returnDouble : Double = 0.0
        if let intt : Double = anyString as? Double
        {
            returnDouble = intt
        }
        else if let strValue : String = anyString as? String
        {
            if(strValue != "")
            {
                if let value : Double = Double(strValue)
                {
                    returnDouble = Double(value)
                }
            }
            else
            {
                returnDouble = 0.0
            }
        }
        
        ////print("double string:- \(anyString) & return value :- \(returnDouble)")
        return returnDouble
    }
    
    class func getDictionary(anyDict : AnyObject) -> NSDictionary
    {
        var returnDict : NSDictionary = NSDictionary()
        if let dict : NSDictionary = anyDict as? NSDictionary
        {
            if(dict.count > 0)
            {
                returnDict = dict
            }
        }
        return returnDict
    }
    
    class func getArray(anyArray : AnyObject) -> NSArray
    {
        var returnArray : NSArray = NSArray()
        if let ary : NSArray = anyArray as? NSArray
        {
            if(ary.count > 0)
            {
                returnArray = ary
            }
        }
        return returnArray
    }
    
    class func getBoolValue(obj : AnyObject) -> Bool
    {
        var itemStatus : Bool = false
        if let bool : Bool = obj as? Bool
        {
            itemStatus = bool
        }
        else if let intt : NSInteger = obj as? NSInteger
        {
            if(intt == 0)
            {
                itemStatus = false
            }
            else
            {
                itemStatus = true
            }
        }
        else if let str : String = obj as? String
        {
            if(str == "0")
            {
                itemStatus = false
            }
            else if(str == "1")
            {
                itemStatus = true
            }
            else if(str == "true")
            {
                itemStatus = true
            }
            else if(str == "false")
            {
                itemStatus = false
            }
            else if(str == "Yes")
            {
                itemStatus = true
            }
            else if(str == "No")
            {
                itemStatus = false
            }
            else if(str == "yes")
            {
                itemStatus = true
            }
            else if(str == "no")
            {
                itemStatus = false
            }
            else if(str == "YES")
            {
                itemStatus = true
            }
            else if(str == "NO")
            {
                itemStatus = false
            }
            else
            {
                itemStatus = false
            }
        }
        
        return itemStatus
    }
    
}
