//
//  AppTheme.swift
//
//

//

import UIKit
import Photos
let storyBoard = UIStoryboard(name: "Main", bundle: nil)
let dashboardStoryBoard = UIStoryboard(name: "Dashboard", bundle: nil)
var isCreatingPostInBg = false

enum RVAlertType : Int
{
    case loading = 1
    case error
    case warning
    case cancel
    case done
}
var RVType: RVAlertType = RVAlertType.init(rawValue: 1)!

class AppTheme: NSObject {

    //--APNS Token value--
    var strDeviceToken = ""
    
    var activityIndicator = UIActivityIndicatorView()
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    let AppNameTitle = "MyFirst App"
    
    //-- Application Access Token --
    var AccessTokenValue = ""
    var TokenId = ""
    //Authorization
    
    var currentChatID = ""
    
    enum encodingType : Int
    {
        case JSON = 1
        case URL
    }
    var encodeType: encodingType = encodingType.init(rawValue: 1)!
    
    // MARK: - Shared Instance
    static let sharedInstance: AppTheme = {
        let instance = AppTheme()
        // setup code
        return instance
    }()
    
    func requestForPhotoAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void)
    {
        PHPhotoLibrary.requestAuthorization() { status in
            switch status {
            case .authorized:
            // as above
                completionHandler(true)
            case .denied, .restricted:
                // as above
                completionHandler(false)
                break
            case .notDetermined:
                completionHandler(false)
                
            default:
                break
            }
        }
    }
    //---
    
    func GetBasicParam() -> [String : AnyObject]
    {
        var param = [String : AnyObject]()
        return param
    }
    
    func GetFormattedDate() -> String
    {
        //Date Formatter
        let dateFormate = DateFormatter()
        dateFormate.timeZone = TimeZone.current
        dateFormate.dateFormat = "yyyy-MM-dd HH:mm:ss" //"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
        let date = Date()
        let stringOfDate = dateFormate.string(from: date)
        return stringOfDate
    }
    
    func dropShadow(view : UIView, scale: Bool = true) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 3
        
        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

    
    class func getInteger(anyString : AnyObject) -> NSInteger
    {
        var returnInteger : NSInteger = 0
        if let intt : NSInteger = anyString as? NSInteger
        {
            returnInteger = intt
        }
        else if let str : String = anyString as? String
        {
            if(str != "")
            {
                returnInteger = NSInteger(str)!
            }
        }
        else if let intt : Float = anyString as? Float
        {
            returnInteger = Int(intt)
        }
        
        return returnInteger
    }
    
    func compareDateTime(date1: Date, date2: Date) -> String
    {
        var result = ""
        if date1 == date2
        {
            result = "Same"
        }
        else if date1 > date2
        {
            result = "Date1"
        }
        else if date1 < date2
        {
            result = "Date2"
        }
        return result
    }
    
    var alert = UIAlertController()
    func showAlert(strTitle : String, strMessge : String, controller : UIViewController)
    {
        var alertTitle = strTitle
        if alertTitle.count == 0 {
            alertTitle = AppTheme.sharedInstance.AppNameTitle
        }
        alert = UIAlertController(title: alertTitle, message: strMessge, preferredStyle: .alert)
        
        let OkAction = UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
        })
        alert.addAction(OkAction)
        
        controller.present(alert, animated: true, completion: {
            //
        })
    }
    
    func dismissAlert()
    {
        alert.dismiss(animated: true, completion: nil)
    }
    
    func shake(layer: CALayer)
    {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    
    func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //"h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }

}

extension UILabel {
    func textWidth() -> CGFloat {
        return UILabel.textWidth(label: self)
    }
    
    class func textWidth(label: UILabel) -> CGFloat {
        return textWidth(label: label, text: label.text!)
    }
    
    class func textWidth(label: UILabel, text: String) -> CGFloat {
        return textWidth(font: label.font, text: text)
    }
    
    class func textWidth(font: UIFont, text: String) -> CGFloat {
        let myText = text as NSString
        
        let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(labelSize.width)
    }
}


extension DispatchQueue {

    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
}
