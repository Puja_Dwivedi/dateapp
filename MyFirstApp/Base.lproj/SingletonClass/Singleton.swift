//
//  Singleton.swift



import UIKit
import MapKit

class DropDownAtributesList{
    
    var attribute_city : String?
    var attribute_text : String?
    var attribute_type : Int?
   // var attribute_country : String?
    
    init(attribute_city : String, attribute_text : String, attribute_type : Int) {
        self.attribute_city = attribute_city
        self.attribute_text = attribute_text
        self.attribute_type =  attribute_type
      //  self.attribute_country = attribute_country
    }
}

class Singleton : NSObject {

    static let shared : Singleton  = Singleton()
    override init()
    {
        super.init()
      
    }

    //==============================================================
    
    // add custom info accroding to Info
    var isLoginActive = false
    var userInfo =  NSDictionary()
    var arrAtributeList = [DropDownAtributesList]()
    
    var isHomeScreenActive : Bool = true

    var topSpaceSafeArea : CGFloat = CGFloat()
    var isPushNotificationActive : Bool = false
    var isVideoPushNotificationActive =  (status : false , videoURL : "" , id : "")
    
    
    //------------: Selected Place filter option :-----------
    var selectedFilterPlace = (selectedValue : "", arrFilterOption: [Any]())
    var InfoPage = [Any]()
    
    /*
     gradient color :
     calling like this :
     let loginButtonGradient = createBlueGreenGradient(from: btnSubmit.bounds, firstcolor : color, secandColor: color)
     self.btnSubmit.layer.insertSublayer(loginButtonGradient, at: 0)
     */
    func createBlueGreenGradient(from bounds: CGRect, firstcolor : CGColor, secandColor: CGColor) -> CAGradientLayer{
        let topColor =   firstcolor  //UIColor(red: 220/255.0, green: 60/255.0, blue: 20/255.0, alpha: 1).cgColor
        let bottomColor =  secandColor//UIColor(red: 255/255.0, green: 255/255.0, blue: 102/255.0, alpha: 1).cgColor
        let gradientColors = [topColor, bottomColor]

        let gradientLocations: [NSNumber] = [0.0, 2.0]
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        //Set startPoint and endPoint property also
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        gradientLayer.frame = bounds
        return gradientLayer
     
    }
    
    func txtFieldUnderLine(txtField : UITextField, underLineColor : UIColor, UnderLineHeight : Int ){
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: (Int(txtField.frame.size.height) - UnderLineHeight), width: Int(txtField.frame.size.width) , height: UnderLineHeight)
        bottomLine.backgroundColor = underLineColor.cgColor
        txtField.borderStyle = .none
        txtField.layer.addSublayer(bottomLine)
    }
    
    
    
    func labelUnderLine(text : String)-> NSAttributedString {
        let textRange = NSMakeRange(0, text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        // Add other attributes if needed
      return attributedText
    }
    

    
    func getCurrentUnixTime() -> String {
        let unixTime = String.init(format: "%f", NSDate().timeIntervalSince1970)
        let splitArray = unixTime.components(separatedBy: ".")
        print("Current unix timestamp \(splitArray[0])")
        return splitArray[0]
    }
   
    
    func roundCornerButton(btn:UIButton,corner_rad:CGFloat)
    {
        btn.layer.cornerRadius = corner_rad
        btn.layer.shadowColor = btn.backgroundColor?.cgColor
        btn.layer.shadowRadius = 1
        btn.layer.shadowOffset = CGSize.init(width: 2, height: 5)
        btn.layer.shadowOpacity = 0.2
    }
    
    func addShadowInView(view:UIView , borderColor:UIColor , borderWidth : CGFloat , shadowColor:UIColor ,shadowRadius : CGFloat)  {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowRadius = shadowRadius
        view.layer.shadowOffset = CGSize.init(width: 3, height: 5)
        view.layer.shadowOpacity = 0.1
    }
    
    func convertTimeStampToDate(timeStamp:String , withTime:Bool , dateFormat:String , timeFormat:String) -> String {
        
        var timeSta = Double(timeStamp)
        if timeSta == nil {
            timeSta = Double(Singleton.shared.getCurrentUnixTime())
        }
        let interval = TimeInterval.init(timeSta!)
        let date = Date.init(timeIntervalSince1970: interval)
        let formatter = DateFormatter.init()
        formatter.locale = Locale.current
        if !withTime {
            formatter.dateFormat = dateFormat
            
        }else{
            formatter.dateFormat = "\(dateFormat) \(timeFormat)"
            
        }
        return formatter.string(from: date);
        
    }
    
    
    func formatPhoneNumber(_ simpleNum: String, deleteLastChar: Bool) -> String {
        var simpleNumber = simpleNum
        if (simpleNumber.count ) == 0 {
            return ""
        }
        // use regex to remove non-digits(including spaces) so we are left with just the numbers
        let regex = try? NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive)
        simpleNumber = (regex?.stringByReplacingMatches(in: simpleNumber, options: [], range: NSRange(location: 0, length: (simpleNumber.count )), withTemplate: ""))!
        // check if the number is to long
        
        if (simpleNumber.count ) > 10 {
            // remove last extra chars.
            //simpleNumber = ((simpleNumber as? NSString)?.substring(from: simpleNumber.length - 1))!
            let length = Int(simpleNumber.count )
            if length > 10 {
                simpleNumber = ((simpleNumber as? NSString)?.substring(from: length - 10))!
            }
            
        }
        if deleteLastChar {
            // should we delete the last digit?
            simpleNumber = ((simpleNumber as? NSString)?.substring(to: (simpleNumber.count ) - 1))!
            
        }
        if simpleNumber.count < 7 {
            simpleNumber = (simpleNumber as NSString).replacingOccurrences(of: "(\\d{3})(\\d+)", with: "RM1-RM2", options: .regularExpression, range: NSRange(location: 0, length: simpleNumber.count))
        }
        else {
            // else do this one..
            simpleNumber = (simpleNumber as NSString).replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "RM1-RM2-RM3", options: .regularExpression, range: NSRange(location: 0, length: simpleNumber.count))
        }
        return simpleNumber
    }

    
     func convertTimeFormatTo12Hour(time:String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let date = formatter.date(from: time)
        formatter.dateFormat = "hh:mm a"
        return formatter.string(from: date!)
        
    }
    
     func convertDateFormat(date:String,format:String,oldFormat:String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = oldFormat
        formatter.locale = .current
        let date1 = formatter.date(from: date)
        formatter.dateFormat = format
        return formatter.string(from: date1!)
        
    }
    
     func formatDate(date: Date ,time : Bool , dateFormat:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        
        if time {
            dateFormatter.dateFormat = "HH:mm:ss"
            
        }else{
            dateFormatter.dateFormat = dateFormat
            
        }
        let formattedDate = dateFormatter.string(from: date)
        return formattedDate;
        
    }
    
     func isValidEmail(_ enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
      func printNetworkError(onCompletion : @escaping () -> Void){

    }

    
    func printAlert(alertIcon: UIImage ,title: String, message: String) {
   
      //  let s = AlertTheme()
       // s.Show(imgAlertIcon: alertIcon, alertTitle: title , message: message, arrBtn: ["OK"]) { (_) in
       // }
    }
    
    
     func printAlertWithCallBack(alertIcon: UIImage,title: String, message: String, arrBtn: [String],onCompletion : @escaping (String) -> Void) {
        
       // let s = AlertTheme()
       // s.Show(imgAlertIcon: alertIcon,alertTitle: title , message: message, arrBtn: arrBtn) { (result) in
            // onCompletion(result)
       // }
    }

    

 func printServerError( message : String, onCompletion : @escaping () -> Void){

    }
    
   
    
    func modify(txtFld:UITextField,withimage:String,borderColor:UIColor?,borderWidth:CGFloat?,cornerRadius:CGFloat?) {
        if borderWidth != nil{
            txtFld.layer.borderWidth = borderWidth!
        }else{
            txtFld.layer.borderWidth = 1
        }
        if cornerRadius != nil {
            txtFld.layer.cornerRadius = cornerRadius!
        }else{
            txtFld.layer.cornerRadius = 1
        }
        if borderColor != nil {
            txtFld.layer.borderColor = borderColor?.cgColor
        }else{
            txtFld.layer.borderColor = #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1).cgColor
        }
        
        if withimage.count > 0 {
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: txtFld.frame.size.height))
            view.backgroundColor = .clear
            
            let icon = UIImageView.init(frame: CGRect.init(x: 10, y: 12, width: 15, height: 15))
            icon.image = UIImage.init(named: withimage)
            icon.contentMode = .scaleAspectFit
            view.addSubview(icon)
            txtFld.leftView = view
            txtFld.leftViewMode = .always
        }else{
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: txtFld.frame.size.height))
            view.backgroundColor = .clear
            txtFld.leftView = view
            txtFld.leftViewMode = .always
        }
    }
    
    func modifyTxtView(txtViewFld:UITextView,withimage:String,borderColor:UIColor?,borderWidth:CGFloat?,cornerRadius:CGFloat?) {
        if borderWidth != nil{
            txtViewFld.layer.borderWidth = borderWidth!
        }else{
            txtViewFld.layer.borderWidth = 1
        }
        if cornerRadius != nil {
            txtViewFld.layer.cornerRadius = cornerRadius!
        }else{
            txtViewFld.layer.cornerRadius = 1
        }
        if borderColor != nil {
            txtViewFld.layer.borderColor = borderColor?.cgColor
        }else{
            txtViewFld.layer.borderColor = #colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1).cgColor
        }
        
        if withimage.count > 0 {
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: txtViewFld.frame.size.height))
            view.backgroundColor = .clear
            
            let icon = UIImageView.init(frame: CGRect.init(x: 10, y: 12, width: 15, height: 15))
            icon.image = UIImage.init(named: withimage)
            icon.contentMode = .scaleAspectFit
            view.addSubview(icon)
            txtViewFld.addSubview(view)
          
        }else{
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: txtViewFld.frame.size.height))
            view.backgroundColor = .clear
           
             txtViewFld.addSubview(view)
        }
    }

    func getDistanceInMiles(kilometer:String) -> String {
        let distance = Double(kilometer)
        let distanceMiles = distance! * 0.621371
        if distanceMiles > 999 {
            return String.init(format: ">999 mi", distanceMiles)
        }else{
            return String.init(format: "%.1f mi", distanceMiles)
        }
    }
}


extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
