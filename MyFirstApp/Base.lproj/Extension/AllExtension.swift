//
//  AllExtension.swift
//  Mbaka
//
//  Created by cis on 28/11/19.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

extension UIScrollView {
   func scrollToBottom(animated: Bool) {
     if self.contentSize.height < self.bounds.size.height { return }
     let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height + 40)
     self.setContentOffset(bottomOffset, animated: animated)
  }
    
        func scrollToTop() {
            let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
            setContentOffset(desiredOffset, animated: true)
    }
}


extension UIView {

  // OUTPUT 1
  func dropShadow(scale: Bool = true) {
    layer.masksToBounds = true
    layer.shadowColor = UIColor.lightGray.cgColor
    layer.shadowOpacity = 0.5
    layer.shadowOffset = CGSize(width: -1, height: 1)
    layer.shadowRadius = 1

    layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }

  // OUTPUT 2
  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius

    layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
}

extension String {
  func replace(string:String, replacement:String) -> String {
      return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
  }

  func removeWhitespace() -> String {
      return self.replace(string: " ", replacement: "")
  }
}


// gradient color color of the uiview :
public extension UIView {
        
    func gradientColor(firstColor: UIColor, secoandColor : UIColor,  corners:UIRectCorner,  cornerRadius: CGFloat ){

        
        let gradientx = CAGradientLayer()
        gradientx.colors = [firstColor,secoandColor]
        gradientx.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientx.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientx.frame = self.bounds
    
        let mask = CAShapeLayer()
        let maskRect = self.bounds
        mask.path = UIBezierPath(roundedRect: maskRect,
        byRoundingCorners: corners,
        cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        
        //mask.fillColor = UIColor.clear.cgColor
       // mask.strokeColor = UIColor.clear.cgColor
        
        gradientx.mask = mask
        
       // let exists = (gradientx != nil)
      //  if !exists {
            layer.addSublayer(gradientx)
       // }
    }
    
    

    private static let kLayerNameGradientBorder = "GradientBorderLayer"

    func gradientBorder(width: CGFloat,
                        colors: [UIColor],
                        corners:UIRectCorner,
                        startPoint: CGPoint = CGPoint(x: 0.5, y: 0.0),
                        endPoint: CGPoint = CGPoint(x: 0.5, y: 1.0),
                        andRoundCornersWithRadius cornerRadius: CGFloat = 0) {

        let existingBorder = gradientBorderLayer()
        let border = existingBorder ?? CAGradientLayer()
        border.frame = CGRect(x: bounds.origin.x, y: bounds.origin.y,
                              width: bounds.size.width + width, height: bounds.size.height + width)
        border.colors = colors.map { return $0.cgColor }
        border.startPoint = startPoint
        border.endPoint = endPoint

        let mask = CAShapeLayer()
        let maskRect = CGRect(x: bounds.origin.x + width/2, y: bounds.origin.y + width/2,
                              width: bounds.size.width - width, height: bounds.size.height - width)
        mask.path = UIBezierPath(roundedRect: maskRect,
        byRoundingCorners: corners,
        cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        
        mask.fillColor = UIColor.clear.cgColor
        mask.strokeColor = UIColor.white.cgColor
        mask.lineWidth = width

        border.mask = mask

        let exists = (existingBorder != nil)
        if !exists {
            layer.addSublayer(border)
        }
    }
    private func gradientBorderLayer() -> CAGradientLayer? {
        let borderLayers = layer.sublayers?.filter { return $0.name == UIView.kLayerNameGradientBorder }
        if borderLayers?.count ?? 0 > 1 {
            fatalError()
        }
        return borderLayers?.first as? CAGradientLayer
    }
    
    func gradientTestBorder(width: CGFloat,
                           colors: [UIColor],
                           corners:UIRectCorner,
                           startPoint: CGPoint = CGPoint(x: 0.5, y: 0.0),
                           endPoint: CGPoint = CGPoint(x: 0.5, y: 1.0),
                           andRoundCornersWithRadius cornerRadius: CGFloat = 0) {

           let existingBorder = gradientBorderLayer()
           let border = existingBorder ?? CAGradientLayer()
        
           border.frame = CGRect(x: bounds.origin.x, y: bounds.origin.y,
                                 width: bounds.size.width , height: bounds.size.height + width)
           border.colors = colors.map { return $0.cgColor }
           border.startPoint = startPoint
           border.endPoint = endPoint

           let mask = CAShapeLayer()
           let maskRect = CGRect(x: bounds.origin.x + width/2, y: bounds.origin.y + width/2,
                                 width: bounds.size.width - width, height: bounds.size.height - width)
           mask.path = UIBezierPath(roundedRect: maskRect,
           byRoundingCorners: corners,
           cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
           
           mask.fillColor = UIColor.clear.cgColor
           mask.strokeColor = UIColor.white.cgColor
           mask.lineWidth = width

           border.mask = mask

           let exists = (existingBorder != nil)
           if !exists {
               layer.addSublayer(border)
           }
       }
}
public extension CGPoint {

    enum CoordinateSide {
        case topLeft, top, topRight, right, bottomRight, bottom, bottomLeft, left
    }

    static func unitCoordinate(_ side: CoordinateSide) -> CGPoint {
        switch side {
        case .topLeft:      return CGPoint(x: 0.0, y: 0.0)
        case .top:          return CGPoint(x: 0.5, y: 0.0)
        case .topRight:     return CGPoint(x: 1.0, y: 0.0)
        case .right:        return CGPoint(x: 0.0, y: 0.5)
        case .bottomRight:  return CGPoint(x: 1.0, y: 1.0)
        case .bottom:       return CGPoint(x: 0.5, y: 1.0)
        case .bottomLeft:   return CGPoint(x: 0.0, y: 1.0)
        case .left:         return CGPoint(x: 1.0, y: 0.5)
        }
    }
}

extension Date {
    func string(format: String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension Date {
    
    static func -(lhs: Date, rhs: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: -rhs, to: lhs)!
    }
    
    static func +(lhs: Date, rhs: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: +rhs, to: lhs)!
    }
}

extension UISlider {
    var thumbCenterX: CGFloat {
        let trackRect = self.trackRect(forBounds: frame)
        let thumbRect = self.thumbRect(forBounds: bounds, trackRect: trackRect, value: value)
        return thumbRect.minX
    }
}


// Gradient label color
/*
 label.gradientColors = [UIColor.blue.cgColor, UIColor.red.cgColor]
 */
class GradientLabel: UILabel {
    var gradientColors: [CGColor] = []

    override func drawText(in rect: CGRect) {
        if let gradientColor = drawGradientColor(in: rect, colors: gradientColors) {
            self.textColor = gradientColor
        }
        super.drawText(in: rect)
    }

    private func drawGradientColor(in rect: CGRect, colors: [CGColor]) -> UIColor? {
        let currentContext = UIGraphicsGetCurrentContext()
        currentContext?.saveGState()
        defer { currentContext?.restoreGState() }

        let size = rect.size
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                        colors: colors as CFArray,
                                        locations: nil) else { return nil }

        let context = UIGraphicsGetCurrentContext()
        context?.drawLinearGradient(gradient,
                                    start: CGPoint.zero,
                                    end: CGPoint(x: size.width, y: 0),
                                    options: [])
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let image = gradientImage else { return nil }
        return UIColor(patternImage: image)
    }
}

//"dd/MM/yyyy HH:mm:ss ssZZZ"
extension Date {

   static func getCurrentDate(date : Date) -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "dd/MM/yyyy"

        return dateFormatter.string(from: date)

    }
    
    static func getCurrentDateWithTime(date : Date) -> String {

         let dateFormatter = DateFormatter()

         dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss ssZZZ"

         return dateFormatter.string(from: date)

     }
}

//Using
//
//print(Date.getCurrentDate())


extension String {

  //MARK:- Convert UTC To Local Date by passing date formats value
  func UTCToLocal(incomingFormat: String, outGoingFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = incomingFormat
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

    let dt = dateFormatter.date(from: self)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = outGoingFormat

    return dateFormatter.string(from: dt ?? Date())
  }

  //MARK:- Convert Local To UTC Date by passing date formats value
  func localToUTC(incomingFormat: String, outGoingFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = incomingFormat
    dateFormatter.calendar = NSCalendar.current
    dateFormatter.timeZone = TimeZone.current

    let dt = dateFormatter.date(from: self)
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.dateFormat = outGoingFormat

    return dateFormatter.string(from: dt ?? Date())
  }
}


//----------------------------------------------------------------------------
/*
 syncImage.image = UIImage(named:"sync-spinning")
    syncImage.startRotating()
 
 syncImage.stopRotating()
 syncImage.image = UIImage(named:"sync-not-spinning")
 
 */
extension UIView {

//Start Rotating view
func startRotating(duration: Double = 1) {
    let kAnimationKey = "rotation"
    
    if self.layer.animation(forKey: kAnimationKey) == nil {
        let animate = CABasicAnimation(keyPath: "transform.rotation")
        animate.duration = duration
        animate.repeatCount = Float.infinity

        animate.fromValue = 0.0
        animate.toValue = Float(Double.pi * 2.0)
        self.layer.add(animate, forKey: kAnimationKey)
    }
}

//Stop rotating view
func stopRotating() {
    let kAnimationKey = "rotation"
    
    if self.layer.animation(forKey: kAnimationKey) != nil {
        self.layer.removeAnimation(forKey: kAnimationKey)
    }
}
}


//for got device token:
/*
 Usage:

 func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
     let deviceTokenString = deviceToken.hexString
     print(deviceTokenString)
 }
 */
extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}


extension Date {
    
    var dayofTheWeek: String {
        let dayNumber = Calendar.current.component(.weekday, from: self)
        // day number starts from 1 but array count from 0
        return daysOfTheWeek[dayNumber - 1]
    }
    
    private var daysOfTheWeek: [String] {
        return  ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    }
}
extension Date {
    func startOfWeek(using calendar: Calendar = .gregorian) -> Date {
        calendar.dateComponents([.calendar, .yearForWeekOfYear, .weekOfYear], from: self).date!
    }
    
    func endOfWeek(using calendar: Calendar = .gregorian) -> Date {
        
        let startDate =  calendar.dateComponents([.calendar, .yearForWeekOfYear, .weekOfYear], from: self).date!
        return startDate + 7
    }
}

extension Calendar {
    static let gregorian = Calendar(identifier: .gregorian)
}

extension Array {
    mutating func rearrange(from: Int, to: Int) {
        insert(remove(at: from), at: to)
    }
}


extension CALayer {
    func addGradienBorder(colors:[UIColor],width:CGFloat = 1) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame =  CGRect(origin: .zero, size: self.bounds.size)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.colors = colors.map({$0.cgColor})
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.lineWidth = width
        shapeLayer.path = UIBezierPath(rect: self.bounds).cgPath
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = UIColor.black.cgColor
        gradientLayer.mask = shapeLayer
        
        self.addSublayer(gradientLayer)
    }
}


extension UIImageView {
    func addShadow() {
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 1.0, height: 4.0)
        self.layer.masksToBounds = false
    }
   
}

extension UIView {
    func addShadowView(shadowColor: CGColor, cornerRadius: CGFloat) {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowColor = shadowColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2) //(width: 0.5, height: 4.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 4.0
    }
    
    func addShadowToView() {
        self.layer.shadowOffset = CGSize(width: 10,
                                          height: 10)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowRadius = 4.0
        self.layer.masksToBounds = false
    }
   
}

extension UIImageView {
    func addShadowToImage() {
        self.layer.shadowOffset = CGSize(width: 10,
                                          height: 10)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowRadius = 4.0
        self.layer.masksToBounds = false
    }
}


extension UIButton {
    func addShadowToButton() {
        self.layer.shadowOffset = CGSize(width: 10,
                                          height: 10)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowRadius = 4.0
        self.layer.masksToBounds = false
    }
}

extension UIView {
    func hasNotch() -> Bool {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            if let safeAreaInsets = window?.safeAreaInsets {
                return safeAreaInsets.top > 20 || safeAreaInsets.bottom > 0
            }
        }
        return false
    }
}
