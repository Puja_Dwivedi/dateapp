//
//  ColorUpdate.swift
//  MyFirstApp
//
//  Created by cis on 30/08/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit
                              
extension UIColor {
     
    struct MyTheme {
        struct FirstColor {
            static var gray: UIColor  { return #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)}
            static var white: UIColor  { return #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)}
            static var blue: UIColor  { return #colorLiteral(red: 0.4588235294, green: 0.7333333333, blue: 1, alpha: 1)}
            static var green: UIColor { return #colorLiteral(red: 0, green: 0.6784313725, blue: 0.8235294118, alpha: 1)}
            static var black :  UIColor { return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)}
            static var purple :  UIColor { return #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)}
        }
        
        struct SecondColor {
            static var gray: UIColor  { return #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)}
            static var white: UIColor  { return #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)}
            static var blue: UIColor  { return #colorLiteral(red: 0.4588235294, green: 0.7333333333, blue: 1, alpha: 1)}
            static var green: UIColor { return #colorLiteral(red: 0, green: 0.6784313725, blue: 0.8235294118, alpha: 1)}
            static var black :  UIColor { return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)}
            static var purple :  UIColor { return #colorLiteral(red: 0.7921568627, green: 0.1137254902, blue: 0.2588235294, alpha: 1)}
        }
    }
}
